package hr.fer.zemris.java.tecaj.hw2;

import static java.lang.Math.*;

import java.text.DecimalFormat;

/**
 * Class provides support for working with complex numbers.
 * 
 * @author Filip Dujmušić
 *
 */
public class ComplexNumber {

	/**
	 * Real part of complex number.
	 */
	private double real;

	/**
	 * Imaginary part of complex number.
	 */
	private double imaginary;

	/**
	 * Constructor creates complex number by using given complex and imaginary
	 * part.
	 * 
	 * @param real
	 *            real part of complex number
	 * @param imaginary
	 *            imaginary part of complex number
	 */
	public ComplexNumber(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}

	/**
	 * Creates complex number by using given real part, sets zero for imaginary.
	 * 
	 * @param real
	 *            real part of complex number
	 * @return returns complex number
	 */
	public static ComplexNumber fromReal(double real) {
		return new ComplexNumber(real, 0);
	}

	/**
	 * Creates complex number by using given imaginary part, sets zero for real
	 * part.
	 * 
	 * @param imaginary
	 *            imaginary part of complex number
	 * @return retunrs compex number
	 */
	public static ComplexNumber fromImaginary(double imaginary) {
		return new ComplexNumber(0, imaginary);
	}

	/**
	 * Creates complex number from given magnitude and angle. Converts
	 * magnitud-angle form to real and imaginary part.
	 * 
	 * @param magnitude
	 *            magnitude of complex number
	 * @param angle
	 *            angle of complex number (in radians)
	 * @return returns new complex number instance
	 */
	public static ComplexNumber fromMagnitudeAndAngle(double magnitude, double angle) {
		return new ComplexNumber(magnitude * Math.cos(angle), magnitude * Math.sin(angle));
	}

	/**
	 * Parses complex numbers from string. Accepts complex number form in string
	 * format and parses it into new instance of complex number.
	 * 
	 * @param s
	 *            complex number in string format
	 * @return returns new instance of compelx number
	 */
	public static ComplexNumber parse(String s) {
		String[] tokens = s.replace("-", " -").replace("+", " +").split(" ");
		Double imaginary = 0.0;
		Double real = 0.0;

		for (String component : tokens) {
			component = component.trim();
			if (!component.isEmpty()) {
				if (component.charAt(component.length() - 1) == 'i') {
					if (component.equals("i")) {
						imaginary += 1.0;
					} else {
						try {
							imaginary += Double.parseDouble(component.substring(0, component.length() - 1));
						} catch (NumberFormatException e) {
							throw new IllegalArgumentException("Wrong complex number format!");
						}
					}
				} else {
					try {
						real += Double.parseDouble(component);
					} catch (NumberFormatException e) {
						throw new IllegalArgumentException("Wrong complex number format!");
					}
				}
			}
		}

		return new ComplexNumber(real, imaginary);
	}

	/**
	 * Real part getter.
	 * 
	 * @return returns real part of complex number
	 */
	public double getReal() {
		return real;
	}

	/**
	 * Imaginary part getter.
	 * 
	 * @return returns imaginary part of complex number
	 */
	public double getImaginary() {
		return imaginary;
	}

	/**
	 * Magnitude getter.
	 * 
	 * @return returns magnitude of complex number
	 */
	public double getMagnitude() {
		return Math.hypot(real, imaginary);
	}

	/**
	 * Angle getter.
	 * 
	 * @return returns angle of complex number in radians (angle between -pi and
	 *         pi)
	 */
	public double getAngle() {
		return Math.atan2(imaginary, real);
	}

	/**
	 * Add method calculates sum of this and given complex number.
	 * 
	 * @param c
	 *            complex number
	 * @return returns result as complex number
	 */
	public ComplexNumber add(ComplexNumber c) {
		return new ComplexNumber(real + c.getReal(), imaginary + c.getImaginary());
	}

	/**
	 * Sub method subtracts given complex from this complex number.
	 * 
	 * @param c
	 *            complex number
	 * @return returns result as complex number
	 */
	public ComplexNumber sub(ComplexNumber c) {
		return new ComplexNumber(real - c.getReal(), imaginary - c.getImaginary());
	}

	/**
	 * Method multiplies this and given complex number.
	 * 
	 * @param c
	 *            complex number
	 * @return returns result as complex number
	 */
	public ComplexNumber mul(ComplexNumber c) {

		return new ComplexNumber(real * c.getReal() - imaginary * c.getImaginary(),
				real * c.getImaginary() + imaginary * c.getReal());
	}

	/**
	 * Divides this and given complex number. Throws
	 * {@link IllegalArgumentException} if division by zero.
	 * 
	 * @param c
	 *            complex number
	 * @return returns result of division as complex number
	 */
	public ComplexNumber div(ComplexNumber c) {
		double divisor = c.real * c.real + c.imaginary * c.imaginary;
		if (Math.abs(divisor) < 1E-9) {
			throw new IllegalArgumentException("Division by zero!");
		}
		double real = (this.real * c.real + this.imaginary * c.imaginary) / divisor;
		double imaginary = (this.imaginary * c.real - this.real * c.imaginary) / divisor;
		return new ComplexNumber(real, imaginary);
	}

	/**
	 * Calculates nth power of complex number. Throws exception for negative
	 * powers.
	 * 
	 * @param n
	 *            power to raise this complex number to
	 * @return returns result of power as complex number
	 */
	public ComplexNumber power(int n) {
		if (n < 0) {
			throw new IllegalArgumentException("Power must be nonnegative integer");
		}

		double r = hypot(real, imaginary);
		double phi = atan2(imaginary, real);
		r = Math.pow(r, n);
		return new ComplexNumber(r * cos(n * phi), r * sin(n * phi));
	}

	/**
	 * Calculates all requested roots of given complex number. Throws exception
	 * if n is less than zero.
	 * 
	 * @param n
	 *            nth root to calculate
	 * @return returns array of n complex numbers representing roots
	 */
	public ComplexNumber[] root(int n) {
		if (n <= 1) {
			throw new IllegalArgumentException();
		}

		ComplexNumber[] roots = new ComplexNumber[n];
		double r = hypot(real, imaginary);
		double phi = atan2(imaginary, real);

		r = pow(r, 1.0 / n);

		for (int k = 0; k < n; ++k) {
			roots[k] = new ComplexNumber(r * cos((phi + 2 * k * PI) / n), r * sin((phi + 2 * k * PI) / n));
		}

		return roots;
	}

	@Override
	public String toString() {
		String string = "";
		DecimalFormat df = new DecimalFormat("+0.###;-#");
		if (Math.abs(real) > 1E-6) {
			string += df.format(real);
		}
		if (Math.abs(imaginary) > 1E-6) {
			string += df.format(imaginary);
			string += "i";
		}
		return string;
	}
}