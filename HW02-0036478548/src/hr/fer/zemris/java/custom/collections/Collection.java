package hr.fer.zemris.java.custom.collections;

/**
 * Generic Collection class. Defines usual methods which derived collection
 * classes should override. Some methods are already implemented since they are
 * the same for every collection (such as addAll() and isEmpty()). This class
 * should not be instantiated but rather used as a template for derived
 * collections.
 * 
 * @author Filip Dujmušić
 *
 */
public class Collection {

	/**
	 * Default constructor. Ensures that this collection can only 
	 * be instantiated from derived class.
	 */
	protected Collection() {

	}

	/**
	 * Checks if collection is empty.
	 * 
	 * @return returns true if empty collection, zero otherwise
	 */
	public boolean isEmpty() {
		return size() == 0;
	}

	/**
	 * Returns size of collection
	 * 
	 * @return integer representing number of elements currently stored in
	 *         collection
	 */
	public int size() {
		return 0;
	}

	/**
	 * Adds new element to collection.
	 * 
	 * @param value
	 *            Object to be added to collection
	 */
	public void add(Object value) {

	}

	/**
	 * Checks if element is in collection. On average takes O(n/2) time.
	 * 
	 * @param value
	 *            element for which collection will be searched
	 * @return true if element exists in this collection, false otherwise
	 */
	public boolean contains(Object value) {
		return false;
	}

	/**
	 * Removes object from collection.
	 * 
	 * @param value
	 *            reference to object to be removed from collection
	 * @return returns true if object was found and removed, false otherwise
	 */
	public boolean remove(Object value) {
		return false;
	}

	/**
	 * Array representation of collection.
	 * 
	 * @return returns array of {@link Object} references representing current
	 *         collection
	 */
	public Object[] toArray() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Collection iterator. Goes through all elements of collection and calls
	 * process() method from given {@link Processor} on every object in this
	 * collection.
	 * 
	 * @param processor
	 */
	public void forEach(Processor processor) {

	}

	/**
	 * Adds all elements from given collection to itself. Elements are copied by
	 * reference (shallow copy).
	 * 
	 * @param other
	 *            Collection from which elements will be copied to itself.
	 */
	public void addAll(Collection other) {

		class Processor extends hr.fer.zemris.java.custom.collections.Processor {
			@Override
			public void process(Object value) {
				add(value);
			}
		}

		other.forEach(new Processor());
	}

	/**
	 * Clears collection. Removes all elements (if they exist) from collection.
	 */
	public void clear() {

	}

}
