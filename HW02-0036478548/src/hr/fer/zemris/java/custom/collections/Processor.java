package hr.fer.zemris.java.custom.collections;

/**
 * 
 * Represents an operation that accepts a single input argument and returns no
 * result.
 * 
 * @author Filip Dujmušić
 *
 */
public class Processor {

	/**
	 * Method accepts single Object argument and defines procedure to do on this
	 * argument. Returns no value, simply processes data. Here it does nothing
	 * but concrete {@link Process} derivates will override this method to do
	 * something useful.
	 * 
	 * @param value
	 *            any object to be processed
	 */
	public void process(Object value) {

	}
}
