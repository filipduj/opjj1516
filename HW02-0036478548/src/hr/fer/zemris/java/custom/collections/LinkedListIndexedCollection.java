package hr.fer.zemris.java.custom.collections;

/**
 * Implementation of linked list-backed collection of objects. It is an ordered
 * collection. The user has precise control over where in the list each element
 * is inserted. The user can access elements by their integer index (position in
 * the list), and search for elements in the list. Indexing is zero based.
 * Duplicate elements are allowed but null elements are not.
 * 
 * @author Filip Dujmušić
 *
 */
public class LinkedListIndexedCollection extends Collection {

	/**
	 * 
	 * Base building block for list implementation. This class represents simple
	 * structure to be used for elements in a list.
	 * 
	 * @author Filip Dujmušić
	 *
	 */
	private static class ListNode {

		/**
		 * Holds reference to next element in the list. null if end of list.
		 */
		ListNode next;

		/**
		 * Holds reference to previous element in the list. null if end of list.
		 */
		ListNode prev;

		/**
		 * The "actual data" in list element.
		 */
		Object value;
	}

	/**
	 * Number of elements stored in list.
	 */
	private int size;

	/**
	 * First element of list.
	 */
	private ListNode first;

	/**
	 * Last element of list.
	 */
	private ListNode last;

	/**
	 * Default list constructor. Accepts no parameters and creates empty list.
	 * First and Last null and size 0 by default, no need to set them.
	 */
	public LinkedListIndexedCollection() {

	}

	/**
	 * Constructs new list from some other collection. Copies all elements from
	 * collection passed as argument into itself. It performs "shallow" copy of
	 * references, not the actual objects.
	 * 
	 * @param other
	 *            Collection from which elements will be copied
	 */
	public LinkedListIndexedCollection(Collection other) {
		addAll(other);
	}

	@Override
	public void add(Object value) {
		insert(value, size);
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean contains(Object value) {
		return indexOf(value) >= 0;
	}

	/**
	 * {@inheritDoc} Iterates through list until all elements are visited and
	 * adds them to resulting array.
	 */
	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		ListNode iterator = first;
		int index = 0;
		while (iterator != null) {
			array[index] = iterator.value;
			index++;
			iterator = iterator.next;
		}
		return array;
	}

	/**
	 * Gets element from specified index. Throws exception if index is invalid.
	 * 
	 * @param index
	 *            index of wanted element (integer)
	 * @return returns element from list at position on index
	 */
	public Object get(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
		return getNode(index).value;
	}

	@Override
	public void clear() {
		first = last = null;
		size = 0;
	}

	/**
	 * Inserts (does not overwrite) the given value at the given position in
	 * list. Elements starting from given position are shifted one. The legal
	 * positions are 0 to size . If position is invalid or element is null,
	 * appropriate exception is thrown.
	 * 
	 * @param value
	 *            value to be inserted to list
	 * @param position
	 *            position at which element will be inserted
	 */
	public void insert(Object value, int position) {

		if (position < 0 || position > size) {
			throw new IndexOutOfBoundsException();
		}

		if (value == null) {
			throw new IllegalArgumentException();
		}

		ListNode node = new ListNode();
		node.value = value;

		if (size == 0) { // special case: empty list
			first = last = node;
		} else if (position == 0) {
			node.next = first;
			first.prev = node;
			first = node;
		} else if (position == size) {
			last.next = node;
			node.prev = last;
			last = node;
		} else {
			ListNode iterator = getNode(position);
			iterator.prev.next = node;
			node.prev = iterator.prev;
			node.next = iterator;
			iterator.prev = node;
		}

		size++;
	}

	/**
	 * Finds the first occurence of given value. Equality is determind using
	 * equals method.
	 * 
	 * @param value
	 *            value to search for in list
	 * @return returns index of first occurence or -1 if the value is not found
	 */
	public int indexOf(Object value) {
		ListNode iterator = first;
		int index = 0;
		while (iterator != null) {
			if (iterator.value.equals(value)) {
				return index;
			}
			index++;
			iterator = iterator.next;
		}
		return -1;
	}

	/**
	 * Removes element at specified index from collection.
	 * 
	 * Element that was previously at location index+1 after this operation is
	 * on location index , etc. Legal indexes are 0 to size-1 . In case of
	 * invalid index an exception is thrown.
	 * 
	 * @param index
	 */
	public void remove(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
		if (size == 1) {
			first = last = null;
		} else if (index == 0) {
			first = first.next;
			first.prev = null;
		} else if (index == size - 1) {
			last = last.prev;
			last.next = null;
		} else {
			ListNode node = getNode(index);
			node.prev.next = node.next;
			node.next.prev = node.prev;
		}
		size--;
	}

	@Override
	public boolean remove(Object value) {
		int index = indexOf(value);
		if (index >= 0) {
			remove(index);
			return true;
		}
		return false;
	}

	/**
	 * Gets node reference on given index. Iterates through list from beginning
	 * (or end if more optimal) until at index position. Worst case complexity
	 * is O(n/2+1).
	 * 
	 * @param index
	 *            index of node
	 * @return returns {@link ListNode} at given index
	 */
	private ListNode getNode(int index) {
		ListNode iterator;
		if (index <= size / 2) {
			iterator = first;
			for (int i = 0; i < index; ++i) {
				iterator = iterator.next;
			}
		} else {
			iterator = last;
			for (int i = (size - 1); i > index; --i) {
				iterator = iterator.prev;
			}
		}
		return iterator;
	}

	@Override
	public void forEach(Processor processor) {
		ListNode iterator = first;
		while (iterator != null) {
			processor.process(iterator.value);
			iterator = iterator.next;
		}
	}

}
