package hr.fer.zemris.java.custom.collections;

/**
 * New type of exception which inherits {@link RuntimeException}
 * and is used in collection in this packet.
 * 
 * @author Filip Dujmušić
 *
 */
public class EmptyStackException extends RuntimeException {

	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = 5019575938076748881L;



}
