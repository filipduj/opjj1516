package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;
import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Implementacija instrukcije `mul`.
 * <p>
 * Instrukcija prima tri argumenta.
 * <p>
 * Prvi argument je odredišni registar u koji će se spremiti umnožak podataka iz
 * drugog i trećeg operanda (koji su također registri).
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrMul implements Instruction {

	/**
	 * Indeks odredišnog registra
	 */
	private int indexRegistra1;

	/**
	 * Indeks registra prvog operanda
	 */
	private int indexRegistra2;

	/**
	 * Indeks registra drugog operanda
	 */
	private int indexRegistra3;

	/**
	 * Konstruktor alocira primjerak nove instrukcije `mul` sa danim
	 * argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrMul(List<InstructionArgument> arguments) {

		if (arguments.size() != 3) {
			throw new IllegalArgumentException("Expected 3 arguments!");
		}

		for (int i = 0; i < 3; i++) {
			if (!arguments.get(i).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(i).getValue())) {
				throw new IllegalArgumentException("Type mismatch for argument " + i + "!");
			}
		}

		this.indexRegistra1 = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
		this.indexRegistra2 = RegisterUtil.getRegisterIndex((Integer) arguments.get(1).getValue());
		this.indexRegistra3 = RegisterUtil.getRegisterIndex((Integer) arguments.get(2).getValue());

	}

	public boolean execute(Computer computer) {
		Object value1 = computer.getRegisters().getRegisterValue(indexRegistra2);
		Object value2 = computer.getRegisters().getRegisterValue(indexRegistra3);
		computer.getRegisters().setRegisterValue(indexRegistra1, Integer.valueOf((Integer) value1 * (Integer) value2));
		return false;
	}
}