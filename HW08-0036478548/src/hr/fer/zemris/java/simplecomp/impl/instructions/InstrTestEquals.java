package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Implementacija `testEquals` instrukcije.
 * <p>
 * Instrukcija očekuje dva argumenta: registri čiji se sadržaj uspoređuje
 * metodom equals().
 * <p>
 * Ako je sadržaj jednak interna zastavica se postavlja na {@code true}, a u
 * suprotnom na {@code false}.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrTestEquals implements Instruction {

	/**
	 * Indeks prvog registra
	 */
	private int firstRegisterIndex;
	
	/**
	 * Indeks drugog registra
	 */
	private int secondRegisterIndex;

	/**
	 * Konstruktor alocira primjerak nove instrukcije `testEquals` sa danim
	 * argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrTestEquals(List<InstructionArgument> arguments) {

		if (arguments.size() != 2) {
			throw new IllegalArgumentException("Expected 2 arguments!");
		}

		for (InstructionArgument arg : arguments) {
			if (!arg.isRegister() || RegisterUtil.isIndirect((Integer) arg.getValue())) {
				throw new IllegalArgumentException("Type mismatch");
			}
		}

		firstRegisterIndex = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
		secondRegisterIndex = RegisterUtil.getRegisterIndex((Integer) arguments.get(1).getValue());

	}

	@Override
	public boolean execute(Computer computer) {
		Object value1 = computer.getRegisters().getRegisterValue(firstRegisterIndex);
		Object value2 = computer.getRegisters().getRegisterValue(secondRegisterIndex);
		if (value1 == null || value2 == null)
			computer.getRegisters().setFlag(false);
		computer.getRegisters().setFlag(value1.equals(value2));
		return false;
	}

}
