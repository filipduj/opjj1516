package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Implementacija instrukcije `load`.
 * <p>
 * Instrukcija load očekuje dva argumenta - prvi argument je registar u koji će
 * se učitati podatak, a drugi je memorijska lokacija sa koje će se čitati
 * podatak.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrLoad implements Instruction {

	/**
	 * Indeks registra u kojeg će se učitati podatak
	 */
	private int registerIndex;

	/**
	 * Adresa izvorišta
	 */
	private int memoryAddress;

	/**
	 * Konstruktor alocira primjerak nove instrukcije `load` sa danim
	 * argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrLoad(List<InstructionArgument> arguments) {

		if (arguments.size() != 2) {
			throw new IllegalArgumentException("Expected 2 arguments!");
		}

		if (!arguments.get(0).isRegister() || !arguments.get(1).isNumber()
				|| RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())) {

			throw new IllegalArgumentException("Type mismatch");

		}

		memoryAddress = (Integer) arguments.get(1).getValue();
		registerIndex = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
	}

	public boolean execute(Computer computer) {
		computer.getRegisters().setRegisterValue(registerIndex, computer.getMemory().getLocation(memoryAddress)

		);
		return false;
	}
}
