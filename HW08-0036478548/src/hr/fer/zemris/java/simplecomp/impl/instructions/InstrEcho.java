package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Implementacija `echo` instrukcije.
 * <p>
 * Ova instrukcija prima jedan argument: registar ili indirektno adresiranu
 * memorijsku lokaciju, čiji se sadržaj ispisuje na standardni izlaz.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrEcho implements Instruction {
	
	/**
	 * Zastavica određuje je li dani argument instrukcije zapravo indirektna adresa.
	 * Ako je indirektna, zastavica je na vrijednosti true.
	 */
	private boolean indirect;
	
	/**
	 * Indeks registra predanog kao argument instrukcije
	 */
	private int registerIndex;
	
	/**
	 * Relativni odmak (ako je indirektno adresiranje, inače se ne koristi)
	 */
	private int offset;

	/**
	 * Konstruktor alocira primjerak nove instrukcije `echo` sa danim
	 * argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrEcho(List<InstructionArgument> arguments) {

		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 argument!");
		}

		InstructionArgument arg = arguments.get(0);

		if (!arg.isRegister()) {
			throw new IllegalArgumentException("Type mismatch");
		}

		if (RegisterUtil.isIndirect((Integer) arg.getValue())) {
			offset = RegisterUtil.getRegisterOffset((Integer) arg.getValue());
			indirect = true;
		}

		registerIndex = RegisterUtil.getRegisterIndex((Integer) arg.getValue());
	}

	public boolean execute(Computer computer) {

		Object registerValue = computer.getRegisters().getRegisterValue(registerIndex);

		if (indirect) {
			if (!(registerValue instanceof Integer)) {
				throw new RuntimeException("Expected Integer in register!");
			}
			System.out.print(computer.getMemory().getLocation((Integer) registerValue + offset));

		} else {
			System.out.print(registerValue);
		}

		return false;
	}

}
