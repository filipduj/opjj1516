package hr.fer.zemris.java.simplecomp.impl;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Memory;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Konkretna implementacija {@link Computer} sučelja.
 * <p>
 * Predstavlja virtualni računalni sustav sa registrima i memorijom.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class ComputerImpl implements Computer {

	/**
	 * Apstrakcija memorije
	 */
	private Memory memory;

	/**
	 * Apstrakcija registre
	 */
	private Registers registers;

	/**
	 * Konstruktor alocira primjerak novog računalnog sustava sa danim brojem
	 * registara i veličinom memorije.
	 * 
	 * @param memorySize
	 *            veličina memorije
	 * @param numberOfRegisters
	 *            broj registara u registarskom polju
	 */
	public ComputerImpl(int memorySize, int numberOfRegisters) {
		memory = new MemoryImpl(memorySize);
		registers = new RegistersImpl(numberOfRegisters);
	}

	@Override
	public Registers getRegisters() {
		return registers;
	}

	@Override
	public Memory getMemory() {
		return memory;
	}

}
