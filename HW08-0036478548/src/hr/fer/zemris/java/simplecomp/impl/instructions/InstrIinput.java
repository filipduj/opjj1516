package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Implementacija instrukcije `iinput`.
 * <p>
 * Instrukcija očekuje jedan argument - direktnu adresu memorijske lokacije.
 * <p>
 * Instrukcija čita podatak sa standardnog ulaza.
 * <p>
 * Ako je podatak parsabilni cijeli broj, on se sprema na danu memorijsku
 * lokaciju, a interna zastavica se postavlja na true (što indicira uspješno
 * izvođenje naredbe).
 * <p>
 * Ako je podatak "smeće" zastavica se postavlja na false.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrIinput implements Instruction {

	/**
	 * Adresa na koju će se upisati pročitani podatak
	 */
	private int address;

	/**
	 * Konstruktor alocira primjerak nove instrukcije `iinput` sa danim
	 * argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrIinput(List<InstructionArgument> arguments) {

		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 argument!");
		}

		if (!arguments.get(0).isNumber()) {
			throw new IllegalArgumentException("Type mismatch");
		}

		address = (Integer) arguments.get(0).getValue();

	}

	@Override
	public boolean execute(Computer computer) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			computer.getMemory().setLocation(address, Integer.parseInt(reader.readLine()));
			computer.getRegisters().setFlag(true);
		} catch (NumberFormatException e) {
			computer.getRegisters().setFlag(false);
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}
		return false;
	}

}
