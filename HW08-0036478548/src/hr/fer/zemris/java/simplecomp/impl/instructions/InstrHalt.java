package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Implementacija `halt` instrukcije.
 * <p>
 * Ova instrukcija ne očekuje argumente i jednostavno terminira izvođenje
 * programa na izvršnoj jedinici.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrHalt implements Instruction {

	/**
	 * Konstruktor alocira primjerak nove instrukcije `halt` sa danim
	 * argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrHalt(List<InstructionArgument> arguments) {

		if (arguments.size() != 0) {
			throw new IllegalArgumentException("Expected 0 arguments!");
		}

	}

	public boolean execute(Computer computer) {
		return true;
	}

}
