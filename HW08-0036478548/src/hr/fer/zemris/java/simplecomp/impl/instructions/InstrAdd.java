package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Implementacija instrukcije `add`.
 * <p>
 * Instrukcija add prima tri argumenta.
 * <p>
 * Prvi argument je odredišni registar ili indirektno adresirana memorijska
 * lokacija, on predstavlja odredište na koje se sprema rezultat.
 * <p>
 * Sljedeća dva argumenta predstavljaju operande koji mogu biti brojevne
 * konstante, registri, ili inidrektno adresirane memorijske lokacije sa
 * podacima.
 * <p>
 * Zbroj operanada se sprema na odredišnu lokaciju.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrAdd implements Instruction {

	/**
	 * Indeks odredišnog registra
	 */
	private int firstOperandIndex;

	/**
	 * Indeks registra prvog operanda
	 */
	private int secondOperandIndex;

	/**
	 * Indeks registra drugog operanda
	 */
	private int resultOperandIndex;

	/**
	 * Konstruktor alocira primjerak nove instrukcije add sa danim argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrAdd(List<InstructionArgument> arguments) {

		if (arguments.size() != 3) {
			throw new IllegalArgumentException("Expected 3 arguments!");
		}

		for (int i = 0; i < 3; i++) {
			if (!arguments.get(i).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(i).getValue())) {
				throw new IllegalArgumentException("Type mismatch for argument " + i + "!");
			}
		}

		resultOperandIndex = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
		firstOperandIndex = RegisterUtil.getRegisterIndex((Integer) arguments.get(1).getValue());
		secondOperandIndex = RegisterUtil.getRegisterIndex((Integer) arguments.get(2).getValue());

	}

	public boolean execute(Computer computer) {

		Object value1 = computer.getRegisters().getRegisterValue(firstOperandIndex);
		Object value2 = computer.getRegisters().getRegisterValue(secondOperandIndex);

		if (!(value1 instanceof Integer) || !(value2 instanceof Integer)) {
			throw new RuntimeException("Expected integers in registers!");
		}

		computer.getRegisters().setRegisterValue(resultOperandIndex,
				Integer.valueOf((Integer) value1 + (Integer) value2));

		return false;
	}

}
