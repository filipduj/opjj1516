package hr.fer.zemris.java.simplecomp.impl;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.ExecutionUnit;
import hr.fer.zemris.java.simplecomp.models.Instruction;

/**
 * Konkretna implementacija izvršne jedinice.
 * <p>
 * Izvršna jedinica je apstrakcija sklopa koji
 * će izvoditi program.
 * <p>
 * Taj sklop ima svoj `interni takt` koji diktira njegov
 * rad.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class ExecutionUnitImpl implements ExecutionUnit {

	@Override
	public boolean go(Computer computer) {
		
		computer.getRegisters().setProgramCounter(0);
		
		while(true){
			
			int address = computer.getRegisters().getProgramCounter();
			Instruction nextInstruction = (Instruction)computer.getMemory().getLocation(address);
			computer.getRegisters().incrementProgramCounter();
			
			if(nextInstruction.execute(computer)){
				return true;
			}
		}
		
	}
	

}
