package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Implementacija instrukcije `call`.
 * <p>
 * Ova instrukcija prima jedan argument - numeričku konstantu
 * koja predstavlja direktnu adresu prve instrukcije potprograma
 * koji se poziva.
 * <p>
 * Prije skoka na prvu instrukciju, trenutna vrijednost programskog
 * brojila se stavlja na vrh sistemskog stoga.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrCall implements Instruction {
	
	/**
	 * Adresa prve instrukcije potprograma kojeg treba pozvati
	 */
	private int callAddress;

	/**
	 * Konstruktor alocira primjerak nove instrukcije `call` sa danim argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrCall(List<InstructionArgument> arguments) {

		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 argument!");
		}

		if (!arguments.get(0).isNumber()) {
			throw new IllegalArgumentException("Type mismatch");
		}

		callAddress = (Integer) arguments.get(0).getValue();

	}

	@Override
	public boolean execute(Computer computer) {
		int address = (Integer) computer.getRegisters().getRegisterValue(Registers.STACK_REGISTER_INDEX);
		if (address >= 0) {
			computer.getMemory().setLocation(address, computer.getRegisters().getProgramCounter());
		}
		computer.getRegisters().setRegisterValue(Registers.STACK_REGISTER_INDEX, --address);
		computer.getRegisters().setProgramCounter(callAddress);
		return false;
	}

}
