package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Implementacija instrukcije `pop`.
 * <p>
 * Ova instrukcija prima jedan argument - registar u koji će se spremiti podatak
 * sa vrha sistemskog stoga.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrPop implements Instruction {

	/**
	 * Indeks odredišnog registra
	 */
	private int registerIndex;

	/**
	 * Konstruktor alocira primjerak nove instrukcije `pop` sa danim
	 * argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrPop(List<InstructionArgument> arguments) {

		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 argument");
		}

		if (!arguments.get(0).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())) {
			throw new IllegalArgumentException("Type mismatch");
		}

		registerIndex = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());

	}

	@Override
	public boolean execute(Computer computer) {
		int address = (Integer) computer.getRegisters().getRegisterValue(Registers.STACK_REGISTER_INDEX);
		address++;
		computer.getRegisters().setRegisterValue(registerIndex, computer.getMemory().getLocation(address));
		computer.getRegisters().setRegisterValue(Registers.STACK_REGISTER_INDEX, address);
		return false;
	}
}
