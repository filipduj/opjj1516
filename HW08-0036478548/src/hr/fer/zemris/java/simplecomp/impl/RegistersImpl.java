package hr.fer.zemris.java.simplecomp.impl;

import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Konkretna implementacija {@link Registers} sučelja.
 * <p>
 * Predstavlja apstrakciju registarskog polja koje je dio nekog računalnog
 * sustava.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class RegistersImpl implements Registers {

	/**
	 * Polje registara opće namjene
	 */
	private Object[] registers;

	/**
	 * Programsko brojilo
	 */
	private int programCounter;

	/**
	 * Interna zastavica
	 */
	private boolean flag;

	/**
	 * Konstruktor alocira primjerak novog registarskog polja. Inicijalizira
	 * broj registara opće namjene sa danim argumentom.
	 * 
	 * @param numberOfRegisters
	 *            broj registara opće namjene
	 */
	public RegistersImpl(int numberOfRegisters) {
		if (numberOfRegisters <= 0) {
			throw new IllegalArgumentException("Expected positive number of registers!");
		}
		registers = new Object[numberOfRegisters];
	}

	@Override
	public Object getRegisterValue(int index) {
		return registers[indexCheck(index)];
	}

	@Override
	public void setRegisterValue(int index, Object value) {
		registers[indexCheck(index)] = value;
	}

	@Override
	public int getProgramCounter() {
		return programCounter;
	}

	@Override
	public void setProgramCounter(int value) {
		programCounter = value;
	}

	@Override
	public void incrementProgramCounter() {
		programCounter++;
	}

	@Override
	public boolean getFlag() {
		return flag;
	}

	@Override
	public void setFlag(boolean value) {
		flag = value;
	}

	/**
	 * Provjerava je li dani indeks koji adresira neki registar opće namjene
	 * unutar granica veličine registarskog polja.
	 * 
	 * @param index
	 *            Indeks registra u registarskom polju
	 * @return Vraća predani indeks ako je on validan
	 * @throws IndexOutOfBoundsException
	 *             ako je indeks registra izvan granica
	 */
	private int indexCheck(int index) {
		if (index < 0 || index >= registers.length) {
			throw new IndexOutOfBoundsException();
		}
		return index;
	}

}
