package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Implementacija instrukcije `move`.
 * <p>
 * Ova instrukcija očekuje dva argumenta.
 * <p>
 * Prvi argument je odredište na koje se sprema podatak. Može biti indirektno
 * adresirana memorijska lokacija ili pak registar.
 * <p>
 * Drugi argument je izvorište koje može biti numerička konstanta, registar ili
 * indirektno adresirana memorijska lokacija.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrMove implements Instruction {

	/**
	 * Opisnik izvorišnog registra
	 */
	private int sourceRegisterDescriptor;

	/**
	 * Opisnik odredišnog registra
	 */
	private int destRegisterDescriptor;

	/**
	 * Zastavica koja ako je postavljena, signalizira da je izvorišni podatak
	 * numerička konstanta
	 */
	private boolean sourceNumber;

	/**
	 * Konstruktor alocira primjerak nove instrukcije `move` sa danim
	 * argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrMove(List<InstructionArgument> arguments) {

		if (arguments.size() != 2) {
			throw new IllegalArgumentException("Expected 2 arguments!");
		}

		if (!arguments.get(0).isRegister() || !(arguments.get(1).isRegister() || arguments.get(1).isNumber())) {
			throw new IllegalArgumentException("Type mismatch");
		}

		sourceRegisterDescriptor = (Integer) arguments.get(1).getValue();
		destRegisterDescriptor = (Integer) arguments.get(0).getValue();
		if (arguments.get(1).isNumber()) {
			sourceNumber = true;
		}

	}

	@Override
	public boolean execute(Computer computer) {

		int value;

		if (sourceNumber) {
			value = sourceRegisterDescriptor;
		} else {
			value = RegisterUtil.getIntegerFromRegister(sourceRegisterDescriptor, computer);
		}

		if (RegisterUtil.isIndirect(destRegisterDescriptor)) {
			Object registerValue = computer.getRegisters()
					.getRegisterValue(RegisterUtil.getRegisterIndex(destRegisterDescriptor));
			if (!(registerValue instanceof Integer)) {
				throw new RuntimeException("Expected integer in destination register");
			}
			int address = (Integer) registerValue + RegisterUtil.getRegisterOffset(destRegisterDescriptor);
			computer.getMemory().setLocation(address, value);
		} else {
			computer.getRegisters().setRegisterValue(RegisterUtil.getRegisterIndex(destRegisterDescriptor), value);
		}

		return false;
	}

}
