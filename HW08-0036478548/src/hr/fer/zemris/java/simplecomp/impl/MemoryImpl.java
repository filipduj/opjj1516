package hr.fer.zemris.java.simplecomp.impl;

import hr.fer.zemris.java.simplecomp.models.Memory;

/**
 * Konkretna implementacija {@link Memory} sučelja.
 * <p>
 * Predstavlja apstrakciju radne memorije unutar nekog računalnog sustava.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class MemoryImpl implements Memory {

	/**
	 * Memorija je u ovom slučaju polje Objekata
	 */
	private Object[] memory;

	/**
	 * Konstruktor alocira primjerak novog Memory objekta i inicijalizira
	 * veličinu memorije sa danim parametrom.
	 * 
	 * @param memorySize
	 *            veličina memorije
	 * @throws IllegalArgumentException
	 *             ako veličina nije pozitivan broj
	 */
	public MemoryImpl(int memorySize) {
		if (memorySize <= 0) {
			throw new IllegalArgumentException("Expected positive number of memory locations!");
		}
		memory = new Object[memorySize];
	}

	@Override
	public void setLocation(int location, Object value) {
		memory[locationCheck(location)] = value;
	}

	@Override
	public Object getLocation(int location) {
		return memory[locationCheck(location)];
	}

	/**
	 * Provjerava je li dana lokacija unutar granica veličine memorije.
	 * 
	 * @param location
	 *            memorijska lokacija
	 * @return vraća danu lokaciju ako je ona validna
	 * @throws IndexOutOfBoundsException
	 *             ako lokacija nije unutar granica veličine memorije
	 */
	private int locationCheck(int location) {
		if (location < 0 || location >= memory.length) {
			throw new IndexOutOfBoundsException();
		}
		return location;
	}

}
