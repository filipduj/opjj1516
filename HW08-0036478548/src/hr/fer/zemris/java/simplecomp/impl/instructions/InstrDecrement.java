package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.RegisterUtil;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Implementacija `decrement` instrukcije.
 * <p>
 * Ova instrukcija prima jedan argument - registar čiji će se sadržaj umanjiti
 * za 1.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrDecrement implements Instruction {

	/**
	 * Indeks registra čija se vrijednost treba umanjiti
	 */
	private int registerIndex;

	/**
	 * Konstruktor alocira primjerak nove instrukcije `decrement` sa danim
	 * argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrDecrement(List<InstructionArgument> arguments) {

		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 argument!");
		}

		if (!arguments.get(0).isRegister() || RegisterUtil.isIndirect((Integer) arguments.get(0).getValue())) {
			throw new IllegalArgumentException("Type mismatch");
		}

		registerIndex = RegisterUtil.getRegisterIndex((Integer) arguments.get(0).getValue());
	}

	public boolean execute(Computer computer) {

		Object registerValue = computer.getRegisters().getRegisterValue(registerIndex);
		if (!(registerValue instanceof Integer)) {
			throw new RuntimeException("Expected integer in register!");
		}
		computer.getRegisters().setRegisterValue(registerIndex, (Integer) registerValue - 1);

		return false;
	}

}
