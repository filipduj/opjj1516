package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;
import hr.fer.zemris.java.simplecomp.models.Registers;

/**
 * Implementacija instrukcije `ret`.
 * <p>
 * Instrukcija ne očekuje argumente a obavlja povratak iz potprograma na način
 * da sa stoga skine povratnu adresu na koju skače, te je napuni u programsko
 * brojilo.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrRet implements Instruction {

	/**
	 * Konstruktor alocira primjerak nove instrukcije `ret` sa danim
	 * argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrRet(List<InstructionArgument> arguments) {
		if (arguments.size() != 0) {
			throw new IllegalArgumentException("Expected 0 arguments!");
		}
	}

	@Override
	public boolean execute(Computer computer) {
		int address = (Integer) computer.getRegisters().getRegisterValue(Registers.STACK_REGISTER_INDEX);
		address++;
		computer.getRegisters().setProgramCounter((Integer) computer.getMemory().getLocation(address));
		computer.getRegisters().setRegisterValue(Registers.STACK_REGISTER_INDEX, address);
		return false;
	}
}
