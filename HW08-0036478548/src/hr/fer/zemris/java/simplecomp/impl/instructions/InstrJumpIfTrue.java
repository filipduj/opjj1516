package hr.fer.zemris.java.simplecomp.impl.instructions;

import java.util.List;

import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.Instruction;
import hr.fer.zemris.java.simplecomp.models.InstructionArgument;

/**
 * Implementacija instrukcije `jumpIfTrue`.
 * <p>
 * Ova instrukcija predstavlja uvjetni skok te očekuje jedan parametar -
 * direktnu adresu instrukcije na koju će se skočiti.
 * <p>
 * Instrukcija provjerava internu zastavicu, te obavlja skok ako je vrijednost
 * zastavice {@code true}.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class InstrJumpIfTrue implements Instruction {

	/**
	 * Lokacija skoka
	 */
	private int location;

	/**
	 * Konstruktor alocira primjerak nove instrukcije `jumpIfTrue` sa danim
	 * argumentima.
	 * 
	 * @param arguments
	 *            Argumenti instrukcije
	 * @throws IllegalArgumentException
	 *             ako je broj ili tip argumenata krivi
	 */
	public InstrJumpIfTrue(List<InstructionArgument> arguments) {

		if (arguments.size() != 1) {
			throw new IllegalArgumentException("Expected 1 argument!");
		}

		if (!arguments.get(0).isNumber()) {
			throw new IllegalArgumentException("Type mismatch");
		}

		location = (Integer) arguments.get(0).getValue();

	}

	@Override
	public boolean execute(Computer computer) {
		if (computer.getRegisters().getFlag()) {
			computer.getRegisters().setProgramCounter(location);
		}
		return false;
	}

}
