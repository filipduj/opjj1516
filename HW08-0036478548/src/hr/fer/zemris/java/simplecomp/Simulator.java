package hr.fer.zemris.java.simplecomp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.simplecomp.impl.ComputerImpl;
import hr.fer.zemris.java.simplecomp.impl.ExecutionUnitImpl;
import hr.fer.zemris.java.simplecomp.models.Computer;
import hr.fer.zemris.java.simplecomp.models.ExecutionUnit;
import hr.fer.zemris.java.simplecomp.models.InstructionCreator;
import hr.fer.zemris.java.simplecomp.parser.InstructionCreatorImpl;
import hr.fer.zemris.java.simplecomp.parser.ProgramParser;

/**
 * Simulator predstavlja osnovni razred koji simulira rad virtualnog
 * mikroprocesora.
 * <p>
 * Program očekuje putanju do izvornog koda napisanog sintaksom koju podržava
 * parser, kao argument naredbenog retka.
 * <p>
 * Ako nije dana putanja, korisnik unosi interaktivno putanju do izvorne
 * datoteke.
 * <p>
 * Nakon toga se dani program izršava, a ukoliko se dogodi greška prilikom
 * izvođenja ili parsiranja izvorne datoteke, ona se dojavljiva na standardni
 * izlaz.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class Simulator {

	/**
	 * Početak programa
	 * 
	 * @param args
	 *            Putanja do izvornog koda
	 */
	public static void main(String[] args) {
		try {
			// Stvori računalo s 256 memorijskih lokacija i 16 registara
			Computer comp = new ComputerImpl(256, 16);

			// Stvori objekt koji zna stvarati primjerke instrukcija
			InstructionCreator creator = new InstructionCreatorImpl("hr.fer.zemris.java.simplecomp.impl.instructions");

			// Napuni memoriju računala programom iz datoteke; instrukcije
			// stvaraj
			// uporabom predanog objekta za stvaranje instrukcija
			ProgramParser.parse(getPath(args), comp, creator);

			// Stvori izvršnu jedinicu
			ExecutionUnit exec = new ExecutionUnitImpl();

			// Izvedi program
			exec.go(comp);
		} catch (Exception e) {
			System.out.println("Something awful happened.");
			e.printStackTrace();
		}
	}

	/**
	 * Metoda vraća putanju ako se ona nalazi u danom polju Stringova (koji
	 * predstavljaju argumente naredbenog retka).
	 * <p>
	 * Ako takav argument ne postoji korisnik ga unosi preko standardnog ulaza.
	 * 
	 * @param args
	 *            polje argumenata naredbenog retka
	 * @return vraća putanju do datoteke sa izvornim kodom
	 * @throws IOException
	 *             Ako se dogodi ulazno/izlazna pogreška
	 * @throws IllegalArgumentException
	 *             Ako je broj argumenata naredbenog retka veći od 1
	 */
	private static String getPath(String[] args) throws IOException {
		if (args.length == 0) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String path;
			while (true) {
				System.out.println("Enter path to source: ");
				if (Files.exists(Paths.get(path = reader.readLine()))) {
					return path;
				} else {
					System.out.println("File does not exist.");
				}
			}

		} else if (args.length == 1) {
			return args[0];
		} else {
			throw new IllegalArgumentException("Expected 0 or 1 command line arguments!");
		}
	}

}
