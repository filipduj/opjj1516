package hr.fer.zemris.java.simplecomp;

import hr.fer.zemris.java.simplecomp.models.Computer;

/**
 * Utility razred koji pruza osnovne metode za rad sa opisnicima registara.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 3, 2016)
 */
public class RegisterUtil {

	/**
	 * Iz danog opisnika izvlači vrijednost index-a. Vrijednost index-a
	 * predstavlja zadnjih 8 bita u opisniku.
	 * 
	 * @param registerDescriptor
	 *            Opisnik registra (Integer)
	 * @return zadnjih 8 bita iz {@code registerDescriptor}
	 */
	public static int getRegisterIndex(int registerDescriptor) {
		return registerDescriptor & 0xFF;
	}

	/**
	 * Za dani opisnik određuje je li riječ o indirektnom adresiranju.
	 * 
	 * @param registerDescriptor
	 *            Opisnik registra (Integer)
	 * @return {@code true} ako opisnik predstavlja indirektno adresiranje
	 */
	public static boolean isIndirect(int registerDescriptor) {
		return (registerDescriptor & 0x1000000) != 0;
	}

	/**
	 * Iz danog opisnika izvlači vrijednost relativnog odmaka.
	 * 
	 * @param registerDescriptor
	 *            Opisnik registra (Integer)
	 * @return odmak (Integer)
	 */
	public static int getRegisterOffset(int registerDescriptor) {
		return (short) ((registerDescriptor >> 8) & 0xFFFF);
	}

	/**
	 * Iz danog opisnika registra određuje podatak bilo da je on indirektno
	 * adresiran u memoriji ili direktno čuvan u registru.
	 * 
	 * @param registerDescriptor
	 *            Opisnik registra
	 * @param computer
	 *            {@link Computer} sučelje
	 * @return vraća pronađeni podatak
	 */
	public static int getIntegerFromRegister(int registerDescriptor, Computer computer) {

		Object registerValue = computer.getRegisters().getRegisterValue(getRegisterIndex(registerDescriptor));

		if (!(registerValue instanceof Integer)) {
			throw new RuntimeException("Expected integer in register!");
		}

		Integer value = (Integer) registerValue;

		if (isIndirect(registerDescriptor)) {
			int address = value + getRegisterOffset(registerDescriptor);
			return (Integer) computer.getMemory().getLocation(address);
		} else {
			return value;
		}

	}
}
