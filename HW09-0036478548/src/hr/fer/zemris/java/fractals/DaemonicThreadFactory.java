package hr.fer.zemris.java.fractals;

import java.util.concurrent.ThreadFactory;

/**
 * Thread factory wrapper.
 * <p>
 * Creates daemonic thread from Runnable job.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 11, 2016)
 */
public class DaemonicThreadFactory implements ThreadFactory {
	@Override
	public Thread newThread(Runnable r) {
		Thread thread = new Thread(r);
		thread.setDaemon(true);
		return thread;
	}

}
