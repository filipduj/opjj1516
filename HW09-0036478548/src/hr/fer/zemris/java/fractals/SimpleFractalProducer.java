package hr.fer.zemris.java.fractals;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import hr.fer.zemris.java.fractals.complex.Complex;
import hr.fer.zemris.java.fractals.complex.ComplexPolynomial;
import hr.fer.zemris.java.fractals.complex.ComplexRootedPolynomial;
import hr.fer.zemris.java.fractals.viewer.IFractalProducer;
import hr.fer.zemris.java.fractals.viewer.IFractalResultObserver;

/**
 * Simple implementation of {@link IFractalProducer}.
 * <p>
 * Class provides multithreaded implementation of fractal producer.
 * 
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 11, 2016)
 */
public class SimpleFractalProducer implements IFractalProducer {

	/**
	 * Maximum number of algorithm iterations per one pixel
	 */
	private final static int ITERATIONS_PER_PIXEL = 1000;

	/**
	 * Convergence border. When algorithm goes below this constant iterations
	 * for one pixel are over
	 */
	private final static double CONVERGENCE_TRESHOLD = 1E-3;

	/**
	 * Acceptable root distance
	 */
	private final static double ROOT_DISTANCE_TRESHOLD = 2E-3;

	/**
	 * Number of smaller jobs on which this fractal will be divided to
	 */
	private final static int NUMBER_OF_JOBS = 8*Runtime.getRuntime().availableProcessors();

	/**
	 * Number of working threads
	 */
	private final static int NUMBER_OF_WORKERS = Runtime.getRuntime().availableProcessors();

	/**
	 * Roots of complex polynomial
	 */
	private Complex[] roots;

	/**
	 * Executor service used for parallelizing jobs
	 */
	private ExecutorService eService;

	/**
	 * Constructor initializes new SimpleFractalProducer with given complex
	 * roots.
	 * 
	 * @param roots
	 *            Roots of complex polynomial
	 * @throws IllegalArgumentException
	 *             If {@code roots} is {@code null} reference
	 */
	public SimpleFractalProducer(Complex[] roots) {
		if (roots == null) {
			throw new IllegalArgumentException("Expected complex roots array but NULL given!");
		}
		this.roots = roots;
		this.eService = Executors.newFixedThreadPool(NUMBER_OF_WORKERS, new DaemonicThreadFactory());
	}

	@Override
	public void produce(double reMin, double reMax, double imMin, double imMax, int width, int height, long requestNo,
			IFractalResultObserver observer) {

		short[] data = new short[width * height];

		/**
		 * Local class defines one job.
		 * <p>
		 * Job is defined by it's own range of y values, from some starting y
		 * value (inclusive) to ending y value (exclusive).
		 * <p>
		 * One job models one horizontal "stripe" of fractal picture and it is
		 * represented as Runnable object, so one job will actually produce one
		 * horizontal part of resulting picture.
		 * 
		 * @author Filip Dujmušić
		 * @version 1.0 (May 11, 2016)
		 */
		class Job implements Runnable {

			/**
			 * Starting y value (inclusive)
			 */
			private int yFrom;

			/**
			 * Ending y value (exclusive)
			 */
			private int yTo;

			/**
			 * Derived complex polynomial used for generating fractal image
			 */
			private ComplexPolynomial derived;

			/**
			 * Complex polynomial used for generating fractal image
			 */
			private ComplexRootedPolynomial polynomial;

			/**
			 * Constructor initalizes new Job with range defined by given y
			 * boundaries.
			 * 
			 * @param yFrom
			 *            Lower y boundary (inclusive)
			 * @param yTo
			 *            Upper y boundary (exclusive)
			 */
			public Job(int yFrom, int yTo) {
				this.yFrom = yFrom;
				this.yTo = yTo;
				polynomial = new ComplexRootedPolynomial(roots);
				derived = polynomial.toComplexPolynom().derive();
			}

			@Override
			public void run() {
				for (int y = yFrom; y < yTo; y++) {
					for (int x = 0; x < width; x++) {

						double re = x / (width - 1.0) * (reMax - reMin) + reMin;
						double im = (height - 1.0 - y) / (height - 1) * (imMax - imMin) + imMin;
						Complex zn = new Complex(re, im);

						double module = 0.0;
						int offset = width * y;
						int iter = 0;

						do {
							Complex numerator = polynomial.apply(zn);
							Complex denominator = derived.apply(zn);
							Complex fraction = numerator.divide(denominator);
							Complex zn1 = zn.sub(fraction);
							module = zn1.sub(zn).module();
							zn = zn1;
							iter++;
						} while (module > CONVERGENCE_TRESHOLD && iter < ITERATIONS_PER_PIXEL);

						data[offset + x] = (short) (polynomial.indexOfClosestRootFor(zn, ROOT_DISTANCE_TRESHOLD) + 1);

					}
				}
			}

		}

		Future<?>[] futures = new Future[NUMBER_OF_JOBS];

		// submit jobs
		int yRange = height / NUMBER_OF_JOBS;
		int yStart = 0;
		for (int i = 0; i < NUMBER_OF_JOBS; ++i) {
			int yFrom = yStart;
			int yTo = (i == NUMBER_OF_JOBS - 1) ? height : yStart + yRange;
			futures[i] = eService.submit(new Job(yFrom, yTo));
			yStart = yTo;
		}

		// wait until every job is done
		for (Future<?> future : futures) {
			while (true) {
				try {
					future.get();
					break;
				} catch (InterruptedException e) {
					continue;
				} catch (ExecutionException e) {
					e.printStackTrace();
					System.exit(-1);
				}

			}
		}

		// display result using computed data array
		observer.acceptResult(data, (short) (roots.length + 1), requestNo);

	}

}
