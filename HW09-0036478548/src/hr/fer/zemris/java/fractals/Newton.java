package hr.fer.zemris.java.fractals;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.fractals.complex.Complex;
import hr.fer.zemris.java.fractals.viewer.FractalViewer;

/**
 * Program produces fractals derived from
 * <a href = "https://www.shodor.org/unchem/math/newton/">Newton-Raphson
 * iteration</a>.
 * <p>
 * When started it aksks user for complex roots. Roots are entered in form:
 * a+ib, where a,b are real and imaginary part of complex root.
 * <p>
 * When desired number of roots is entered, command `done` is expected, after
 * which program calculates and displays picture of fractal.
 * 
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 11, 2016)
 */
public class Newton {

	/**
	 * Minimum required number of complex roots used for deriving fractals.
	 */
	private final static int MINIMUM_ROOTS = 2;

	/**
	 * Program entry point.
	 */
	public static void main(String[] args) {

		Complex[] roots = null;

		displayGreetMessage();

		try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8))) {
			roots = getRoots(reader);
		} catch (IOException e) {
			System.out.println("I/O error occured.");
			System.exit(-1);
		}

		if (roots.length < MINIMUM_ROOTS) {
			System.out.println("Error: expected atleast " + MINIMUM_ROOTS + " roots but " + roots.length + " given.");
			System.out.println("Exitinig...");
		} else {
			System.out.println("Image of fractal will appear shortly. Thank you.");
			FractalViewer.show(new SimpleFractalProducer(roots));
		}

	}

	/**
	 * Method reades arbitrary number of complex roots from standard input.
	 * <p>
	 * Reads complex numbers in form "a+ib" from standard input until "done" is
	 * entered, and returns them as array.
	 * 
	 * @param reader
	 *            Buffered reader with opened stream ready for reading
	 * @return returns array complex numbers
	 * @throws IOException
	 *             If I/O error occurs while reading from input stream through
	 *             {@code reader}
	 */
	private static Complex[] getRoots(BufferedReader reader) throws IOException {
		List<Complex> roots = new LinkedList<>();
		int numberOfRootsEntered = 0;
		while (true) {
			try {
				System.out.print("Root " + (numberOfRootsEntered + 1) + "> ");
				String input = reader.readLine().trim();
				if (input == null) {
					System.out.println("Error: input stream is closed!");
					System.exit(-1);
				}
				if (input.isEmpty()) {
					System.out.println("Error: no input given!");
					continue;
				}
				if (input.equals("done")) {
					break;
				}
				roots.add(getNextRoot(input));
				++numberOfRootsEntered;
			} catch (NumberFormatException e) {
				displayErrorMessage();
				continue;
			}
		}
		Complex[] rootsArray = new Complex[roots.size()];
		return roots.toArray(rootsArray);
	}

	/**
	 * Method parses complex number from given String input.
	 * 
	 * @param input
	 *            String representation of complex number
	 * @return returns Complex number
	 * @throws NumberFormatException
	 *             If format of complex number is wrong
	 */
	private static Complex getNextRoot(String input) throws NumberFormatException {
		input = input.replaceAll(" ", "");
		int iIndex = input.indexOf('i');

		String real = "0";
		String imaginary = "0";

		if (iIndex == -1) {
			return new Complex(Double.parseDouble(input), 0);
		}

		if (iIndex == 0) {
			imaginary = input.substring(1);
			if (imaginary.isEmpty())
				imaginary = "1";
		} else {
			String tmp = input.substring(0, iIndex - 1);
			if (!tmp.isEmpty())
				real = tmp;
			imaginary = input.substring(iIndex - 1, input.length()).replace("i",
					iIndex == input.length() - 1 ? "1" : "");
		}

		return new Complex(Double.parseDouble(real), Double.parseDouble(imaginary));
	}

	/**
	 * Displays greeting message to standard output.
	 */
	private static void displayGreetMessage() {
		System.out.println("Welcome to Newton-Raphson iteration-based fractal viewer.");
		System.out.println("Please enter at least two roots, one root per line. Enter 'done' when done.");
	}

	/**
	 * Displays error message. Used when user enters complex number in wrong
	 * format.
	 */
	private static void displayErrorMessage() {
		System.out.println("Invalid format of complex number. Please correct your input.");
		System.out.println("Expected complex number in form of a+bi or `done` for end of input.");
	}

}
