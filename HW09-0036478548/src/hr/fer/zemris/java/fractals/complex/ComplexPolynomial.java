package hr.fer.zemris.java.fractals.complex;

import java.util.Arrays;

/**
 * Immutable model of coefficient-based complex polynomial.
 * <p>
 * Class represents complex polynomial defined by complex coefficients, where
 * each coefficient is bounded to one power.
 * <p>
 * For example if given polynom: a*z^2 + b*z + c, this polynomial would be
 * described with a,b,c respectively where each coefficient can be arbitrary
 * complex number.
 * <p>
 * Class also provides basic methods for manipulating with such polynomials.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 11, 2016)
 */
public class ComplexPolynomial {

	/**
	 * Complex factors of polynomial. Factor at index i is factor of z^i.
	 */
	private Complex[] factors;

	/**
	 * Highest power of this polynom.
	 */
	private short polynomOrder;

	/**
	 * Constructor initializes new complex polynomial with given array of
	 * {@code factors}. Factor at index i is factor of z^i.
	 * 
	 * @param factors
	 *            Array of complex factors
	 * @throws IllegalArgumentException
	 *             <ul>
	 *             <li>If {@code factors} is {@code null} reference</li>
	 *             <li>If {@code factors} array is empty</li>
	 *             </ul>
	 */
	public ComplexPolynomial(Complex... factors) {

		if (factors == null) {
			throw new IllegalArgumentException("Expected complex factors but NULL given.");
		}

		if (factors.length == 0) {
			throw new IllegalArgumentException("Expected atleast 1 factor but 0 given!");
		}

		this.polynomOrder = getOrder(factors);
		this.factors = getFactors(factors);
	}

	/**
	 * @return returns order of this polynomial
	 */
	public short order() {
		return polynomOrder;
	}

	/**
	 * Multiplies this polynom with given {@code p} polynom.
	 * 
	 * @param p
	 *            Complex polynom
	 * @return returns result of multiplication
	 * @throws IllegalArgumentException
	 *             If {@code p} is {@code null} reference
	 */
	public ComplexPolynomial multiply(ComplexPolynomial p) {
		if (p == null) {
			throw new IllegalArgumentException("Expected polynom but given NULL reference!");
		}
		Complex[] newFactors = new Complex[factors.length + p.factors.length];
		for (int i = 0; i < newFactors.length; ++i) {
			newFactors[i] = Complex.ZERO;
		}
		for (int j = 0; j < factors.length; ++j) {
			for (int k = 0; k < p.factors.length; ++k) {
				newFactors[j + k] = newFactors[j + k].add(factors[j].multiply(p.factors[k]));
			}
		}
		return new ComplexPolynomial(newFactors);
	}

	/**
	 * Derives this polynom once.
	 * 
	 * @return result of derivation
	 */
	public ComplexPolynomial derive() {
		if (factors.length == 1) {
			return new ComplexPolynomial(Complex.ZERO);
		}
		Complex[] newFactors = new Complex[factors.length - 1];
		for (int i = 0; i < newFactors.length; ++i) {
			newFactors[i] = factors[i + 1].multiply(new Complex(i + 1, 0));
		}
		return new ComplexPolynomial(newFactors);
	}

	/**
	 * Calculates value of polynom for given complex {@code z} value.
	 * 
	 * @param z
	 *            Complex number
	 * @return resulting complex value of polynom at {@code z}
	 * @throws IllegalArgumentException
	 *             If {@code z} is {@code null} reference
	 */
	public Complex apply(Complex z) {
		if (z == null) {
			throw new IllegalArgumentException("Expected complex number but given NULL reference!");
		}
		Complex result = Complex.ZERO;
		for (int i = 0; i < factors.length; ++i) {
			result = result.add(factors[i].multiply(z.power(i)));
		}
		return result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = factors.length - 1; i >= 0; --i) {
			if (!factors[i].equals(Complex.ZERO)) {

				if (factors[i].getImaginary() != 0 && factors[i].getReal() != 0) {
					if (i == factors.length - 1) {
						sb.append(factors[i]);
					} else {
						sb.append("+").append(factors[i]);
					}
				} else {
					sb.append(factors[i]);
				}

				if (i == 1) {
					sb.append("z");
				} else if (i > 1) {
					sb.append("z^");
				}

			}
		}
		return sb.toString();
	}

	/**
	 * Returns largest index in given {@code factors} array whose value is not
	 * zero. Effectively returns order of polynom described with array of
	 * coefficients.
	 * 
	 * @param factors
	 *            array of complex coefficients
	 * @return order of polynom described with {@code factors} array
	 */
	private short getOrder(Complex[] factors) {
		int i;
		for (i = factors.length - 1; i >= 0; i--) {
			if (!factors[i].equals(Complex.ZERO)) {
				return (short) i;
			}
		}
		return 0;
	}

	/**
	 * Removes trailing zero complex coefficients from given {@code factors}
	 * array (if there are such coefficients).
	 * <p>
	 * If largest index coefficient is not zero, {@code factors} itself is
	 * returned because nothing has to be removed.
	 * 
	 * @param factors
	 *            array of complex coefficients
	 * @return returns new array containing {@code factors} without trailing
	 *         zero elements
	 */
	private Complex[] getFactors(Complex[] factors) {
		if (factors.length - 1 == polynomOrder) {
			return factors;
		} else {
			return Arrays.copyOf(factors, polynomOrder + 1);
		}
	}

}
