package hr.fer.zemris.java.fractals.complex;

/**
 * Immutable model of root-based complex polynomial.
 * <p>
 * Class represents complex polynomial defined by it's roots.<br>
 * For example polynom (z-1)*(z-2)*(z-3)=0 would be defined by its three roots:
 * 1,2,3 and every root can be arbitrary complex number.
 * <p>
 * Class provides basic methods for working with such polynoms as well as method
 * for converting from this representation to coefficient based polynomial.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 11, 2016)
 */
public class ComplexRootedPolynomial {

	/**
	 * Complex roots of this polynomial
	 */
	private Complex[] roots;

	/**
	 * Constructor initalizes new polynomial with given {@code roots} array.
	 * 
	 * @param roots
	 *            Complex roots array
	 * @throws IllegalArgumentException
	 *             <ul>
	 *             <li>If {@code roots} is {@code null} reference</li>
	 *             <li>If {@code roots} is empty array</li>
	 *             </ul>
	 */
	public ComplexRootedPolynomial(Complex... roots) {
		if (roots == null) {
			throw new IllegalArgumentException("Expected roots but NULL given!");
		}
		if (roots.length == 0) {
			throw new IllegalArgumentException("Expected at least 1 root but 0 given.");
		}
		this.roots = roots;
	}

	/**
	 * Calculates value of polynom for given complex {@code z} value.
	 * 
	 * @param z
	 *            Complex number
	 * @return resulting complex value of polynom at {@code z}
	 * @throws IllegalArgumentException
	 *             If {@code z} is {@code null} reference
	 */
	public Complex apply(Complex z) {
		if (z == null) {
			throw new IllegalArgumentException("Expected complex number but given NULL reference!");
		}
		Complex ret = Complex.ONE;
		for (Complex root : roots) {
			ret = ret.multiply(z.sub(root));
		}
		return ret;
	}

	/**
	 * Converts this root-based complex polynomial to coefficient-based complex
	 * polynomial.
	 * 
	 * @return Returns {@link ComplexPolynomial} representation of this
	 *         polynomial
	 */
	public ComplexPolynomial toComplexPolynom() {
		ComplexPolynomial ret = new ComplexPolynomial(roots[0].negate(), Complex.ONE);
		for (int i = 1; i < roots.length; ++i) {
			ret = ret.multiply(new ComplexPolynomial(roots[i].negate(), Complex.ONE));
		}
		return ret;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < roots.length; ++i) {
			sb.append("(z");
			String root = roots[i].negate().toString();
			if (root.charAt(0) == '(') {
				root = root.substring(1, root.length() - 1);
			}
			sb.append(root).append(")");
		}
		return sb.toString();
	}

	/**
	 * Calculates index of root closest to given complex {@code z} whose
	 * euclidean distance is within given {@code threshold}.<br>
	 * If no such root exists, returns -1.
	 * 
	 * @param z
	 *            Complex number
	 * @param threshold
	 *            Acceptable root to {@code z} distance
	 * @return returns index of closest root within {@code threshold} interval
	 *         or -1 if no such root
	 */
	public int indexOfClosestRootFor(Complex z, double threshold) {
		int index = -1;
		double distance = Double.MAX_VALUE;
		for (int i = 0; i < roots.length; ++i) {
			double d = z.sub(roots[i]).module();
			if (d < distance && d < threshold) {
				index = i;
				distance = d;
			}
		}
		return index;
	}

}
