package hr.fer.zemris.java.fractals.complex;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

/**
 * Immutable model of complex number.
 * <p>
 * Class represents complex number defined by it's real and imaginary component.
 * <p>
 * It provides basic methods for operations on complex numbers.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 11, 2016)
 */
public class Complex {

	/**
	 * Decimal formatter used for String representation of complex number
	 */
	private static final DecimalFormat df = new DecimalFormat("+0.###;-0.###");

	/**
	 * 0+0i constant
	 */
	public static final Complex ZERO = new Complex(0, 0);

	/**
	 * 1+0i constant
	 */
	public static final Complex ONE = new Complex(1, 0);

	/**
	 * -1+0i constant
	 */
	public static final Complex ONE_NEG = new Complex(-1, 0);

	/**
	 * 0+i constant
	 */
	public static final Complex IM = new Complex(0, 1);

	/**
	 * 0-i constant
	 */
	public static final Complex IM_NEG = new Complex(0, -1);

	/**
	 * Real part of complex number
	 */
	private double re;

	/**
	 * Imaginary part of complex number
	 */
	private double im;

	/**
	 * Default constructor initializes new complex number with real and
	 * imaginary components set to zero.
	 */
	public Complex() {
		this(0.0, 0.0);
	}

	/**
	 * Constructor initializes new complex number with given real and imaginary
	 * componens.
	 * 
	 * @param re
	 *            Real part of complex number
	 * @param im
	 *            Imaginary part of complex number
	 */
	public Complex(double re, double im) {
		this.re = re;
		this.im = im;
	}

	/**
	 * @return returns real part of complex number
	 */
	public double getReal() {
		return re;
	}

	/**
	 * @return returns imaginary part of complex number
	 */
	public double getImaginary() {
		return im;
	}

	/**
	 * @return returns module of complex number (euclidean distance)
	 */
	public double module() {
		return Math.sqrt(re * re + im * im);
	}

	/**
	 * @return returns angle of complex number (in radians)
	 */
	public double angle() {
		return Math.atan2(im, re);
	}

	/**
	 * Multiplies this complex number with given {@code c}.
	 * 
	 * @param c
	 *            Other complex number
	 * @return Result of multiplicaton of this complex number with given
	 *         {@code c}
	 */
	public Complex multiply(Complex c) {
		return new Complex(re * c.re - im * c.im, re * c.im + im * c.re);
	}

	/**
	 * Divides this complex number with given {@code c}.
	 * 
	 * @param c
	 *            Other complex number
	 * @return Result of dividing this complex number by {@code c}
	 * 
	 */
	public Complex divide(Complex c) {
		double div = c.im * c.im + c.re * c.re;
		return multiply(new Complex(c.re / div, -c.im / div));
	}

	/**
	 * Adds this complex number to given {@code c}.
	 * 
	 * @param c
	 *            Other complex number
	 * @return Result of addition between this complex number and given
	 *         {@code c}
	 */
	public Complex add(Complex c) {
		return new Complex(re + c.re, im + c.im);
	}

	/**
	 * Subtracts {@code c} from this complex number.
	 * 
	 * @param c
	 *            Other complex number
	 * @return Result of subtracting {@code c} from this complex number
	 */
	public Complex sub(Complex c) {
		return new Complex(re - c.re, im - c.im);
	}

	/**
	 * @return negates this complex number
	 */
	public Complex negate() {
		return new Complex(-re, -im);
	}

	/**
	 * Calculates this complex number raised to the power of {@code n}. n has to
	 * be nonnegative integer.
	 * 
	 * @param n
	 *            Power to which this complex number will be raised
	 * @return returns result of power
	 * @throws IllegalArgumentException
	 *             If {@code n} is negative
	 */
	public Complex power(int n) {

		if (n < 0) {
			throw new IllegalArgumentException("Expected nonnegative argument but " + n + " given.");
		}

		if (n == 0) {
			return Complex.ONE;
		}

		Complex result = Complex.ONE;
		for (int i = 0; i < n; ++i) {
			result = result.multiply(this);
		}

		return result;
	}

	/**
	 * Calculates all {@code n} roots from this complex number.
	 * 
	 * @param n
	 *            nth root of this complex number
	 * @return List of {@code n} complex roots
	 * @throws IllegalArgumentException
	 *             If {@code n} is not positive
	 */
	public List<Complex> root(int n) {

		if (n <= 0) {
			throw new IllegalArgumentException("Expected positive argument but " + n + " given.");
		}

		List<Complex> roots = new LinkedList<>();

		if (n == 1) {
			roots.add(new Complex(re, im));
		} else {
			double angle = angle();
			double module = Math.pow(module(), 1.0 / n);
			for (int k = 0; k < n; ++k) {
				roots.add(new Complex(module * Math.cos((angle + 2 * k * Math.PI) / n),
						              module * Math.sin((angle + 2 * k * Math.PI) / n))
				);
			}
		}

		return roots;
	}

	@Override
	public String toString() {
		if (re == 0) {
			return df.format(im) + "i";
		} else if (im == 0) {
			return df.format(re);
		} else {
			return "(" + df.format(re) + df.format(im) + "i" + ")";
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(im);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(re);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Complex)) {
			return false;
		}
		Complex other = (Complex) obj;
		return Double.doubleToLongBits(im) == Double.doubleToLongBits(other.im) && 
			   Double.doubleToLongBits(re) == Double.doubleToLongBits(other.re);

	}

}
