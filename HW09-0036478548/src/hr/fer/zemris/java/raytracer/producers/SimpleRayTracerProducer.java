package hr.fer.zemris.java.raytracer.producers;

import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;

/**
 * Implementation of single-threaded ray tracer producer.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 11, 2016)
 */
public final class SimpleRayTracerProducer extends RayTracerProducer {
	@Override
	void calculateColors() {
		short[] rgb = new short[3];
		int offset = 0;
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				Point3D screenPoint = screenCorner.add(xAxis.scalarMultiply(x * horizontal / (width - 1)))
						.sub(yAxis.scalarMultiply(y * vertical / (height - 1)));
				Ray ray = Ray.fromPoints(eye, screenPoint);
				tracer(scene, ray, rgb);

				red[offset]   = rgb[0] > 255 ? 255 : rgb[0];
				green[offset] = rgb[1] > 255 ? 255 : rgb[1];
				blue[offset]  = rgb[2] > 255 ? 255 : rgb[2];
				offset++;
			}
		}
	}

}
