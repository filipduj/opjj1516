package hr.fer.zemris.java.raytracer.producers;

import static java.lang.Math.pow;

import hr.fer.zemris.java.raytracer.model.GraphicalObject;
import hr.fer.zemris.java.raytracer.model.IRayTracerProducer;
import hr.fer.zemris.java.raytracer.model.IRayTracerResultObserver;
import hr.fer.zemris.java.raytracer.model.LightSource;
import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;
import hr.fer.zemris.java.raytracer.model.RayIntersection;
import hr.fer.zemris.java.raytracer.model.Scene;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * Abstract ray trace producer.
 * <p>
 * It defines basic steps of ray trace rendering algorithm which are the same
 * for every implementation.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 11, 2016)
 */
public abstract class RayTracerProducer implements IRayTracerProducer {

	/**
	 * Precision for comparing double values
	 */
	private static final double PRECISION = 1E-3;

	/**
	 * Resulting array of red components
	 */
	short[] red;

	/**
	 * Resulting array of green components
	 */
	short[] green;

	/**
	 * Resulting array of blue components
	 */
	short[] blue;

	/**
	 * Normalized z-axis view vector
	 */
	Point3D zAxis;

	/**
	 * Normalized y-axis view vector
	 */
	Point3D yAxis;

	/**
	 * Normalized x-axis view vector
	 */
	Point3D xAxis;

	/**
	 * Upper left corner of view screen in space
	 */
	Point3D screenCorner;

	/**
	 * Central point of view screen in space
	 */
	Point3D eye;

	/**
	 * Width of view in pixels
	 */
	int width;

	/**
	 * Height of view in pixels
	 */
	int height;

	/**
	 * Non-scaled width
	 */
	double horizontal;

	/**
	 * Non-scaled height
	 */
	double vertical;

	/**
	 * Scene
	 */
	Scene scene;

	@Override
	public void produce(Point3D eye, Point3D view, Point3D viewUp, double horizontal, double vertical, int width,
			int height, long requestNo, IRayTracerResultObserver observer) {

		System.out.println("Započinjem izračune...");

		red = new short[width * height];
		green = new short[width * height];
		blue = new short[width * height];

		zAxis = view.sub(eye).normalize();
		yAxis = viewUp.sub(zAxis.scalarMultiply(zAxis.scalarProduct(viewUp))).normalize();
		xAxis = zAxis.vectorProduct(yAxis).normalize();
		screenCorner = view.add(xAxis.scalarMultiply(-horizontal / 2)).add(yAxis.scalarMultiply(vertical / 2));
		scene = RayTracerViewer.createPredefinedScene();

		this.eye = eye;
		this.width = width;
		this.height = height;
		this.horizontal = horizontal;
		this.vertical = vertical;

		// implementation dependent!
		calculateColors();

		System.out.println("Izračuni gotovi...");
		observer.acceptResult(red, green, blue, requestNo);
		System.out.println("Dojava gotova...");
	}

	/**
	 * Core of ray-trace algorithm.<br>
	 * Determines final color values (RGB) for each pixel in screen view,
	 * eventually forming rendered 3D scene on 2D display.
	 */
	abstract void calculateColors();

	/**
	 * Method takes one ray and determines color of pixel in screen view which
	 * this ray pierces.<br>
	 * Method used to determine color is well known
	 * <a href="https://en.wikipedia.org/wiki/Phong_shading">Phong shading
	 * technique</a>
	 * 
	 * @param scene
	 *            Scene
	 * @param ray
	 *            One ray from eye position towards screenview
	 * @param rgb
	 *            array of three elements: red, green and blue component
	 *            intensities
	 */
	void tracer(Scene scene, Ray ray, short[] rgb) {

		rgb[0] = rgb[1] = rgb[2] = 0;

		RayIntersection intersection = getClosestIntersection(scene, ray);

		if (intersection != null) {

			rgb[0] = rgb[1] = rgb[2] = 15;

			for (LightSource ls : scene.getLights()) {

				Ray srcToIsection = Ray.fromPoints(ls.getPoint(), intersection.getPoint());
				RayIntersection ps = getClosestIntersection(scene, srcToIsection);

				double distToObstacle = ps.getPoint().sub(ls.getPoint()).norm();
				double distToSource = intersection.getPoint().sub(ls.getPoint()).norm();

				if (ps == null || distToSource - distToObstacle > PRECISION) {
					continue;
				} else {
					double alpha = ps.getNormal().scalarProduct(srcToIsection.direction);
					double beta = getReflectionVector(srcToIsection.direction, ps.getNormal()).scalarProduct(ray.direction);
					double betaToN = (beta == 0) ? 0 : pow(beta, ps.getKrn());

					rgb[0] +=  ls.getR()*(alpha * ps.getKdr() + betaToN * ps.getKrr());
					rgb[1] +=  ls.getG()*(alpha * ps.getKdg() + betaToN * ps.getKrg());
					rgb[2] +=  ls.getB()*(alpha * ps.getKdb() + betaToN * ps.getKrb());
				}
			}
		}

	}

	/**
	 * Method finds closest intersection between point on object and every other
	 * object on scene that is on line between point on object and light source.
	 * 
	 * @param scene
	 *            Scene
	 * @param ray
	 *            Ray from light source to point on object
	 * @return returns closest intersection or {@code null}
	 */
	RayIntersection getClosestIntersection(Scene scene, Ray ray) {
		RayIntersection point = null;
		double distance = Double.POSITIVE_INFINITY;
		for (GraphicalObject gObject : scene.getObjects()) {
			RayIntersection rIntersection = gObject.findClosestRayIntersection(ray);
			if (rIntersection != null) {
				double d = rIntersection.getDistance();
				if (d < distance) {
					distance = d;
					point = rIntersection;
				}
			}
		}
		return point;
	}

	/**
	 * Method calculates reflection of vector {@code v} around vector {@code n}.
	 * 
	 * @param v
	 *            Vector to reflect
	 * @param n
	 *            Central vector around which reflection is calculated
	 * @return returns reflected vector
	 */
	Point3D getReflectionVector(Point3D v, Point3D n) {
		return n.scalarMultiply(2 * v.scalarProduct(n)).sub(v).normalize();
	}

}
