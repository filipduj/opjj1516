package hr.fer.zemris.java.raytracer.producers;

import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.model.Ray;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

/**
 * Implementation of multi-threaded ray tracer producer.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 11, 2016)
 */
public final class ParallelizedRayTracerProducer extends RayTracerProducer {

	/**
	 * Starting job will be divided on minimally this many jobs
	 */
	private static final int NUMBER_OF_JOBS = 8 * Runtime.getRuntime().availableProcessors();

	/**
	 * Minimal number of lines of pixels that should be directly computed
	 */
	private static final int MINIMAL_JOB_RANGE = 10;

	/**
	 * Executor service used for recursively dividing and parallelizing jobs
	 */
	private ForkJoinPool pool = new ForkJoinPool();

	@Override
	void calculateColors() {
		pool.invoke(new Job(0, height));
	}

	/**
	 * Recursive job class implementation.
	 * <p>
	 * Represents one job which might be divided to smaller jobs of this
	 * instance.
	 * 
	 * @author Filip Dujmušić
	 * @version 1.0 (May 11, 2016)
	 */
	private class Job extends RecursiveAction {

		/**
		 * Serialization ID
		 */
		private static final long serialVersionUID = 2042144794148296660L;

		/**
		 * Starting y value (inclusive)
		 */
		private int yStart;

		/**
		 * Ending y value (exclusive)
		 */
		private int yEnd;

		/**
		 * Maximum number of lines that should be computed directly
		 */
		private int maximumDirectRange;

		/**
		 * Constructor initializes new job with given y coordinate boundaries.
		 * 
		 * @param yStart
		 *            starting y value
		 * @param yEnd
		 *            ending y value
		 */
		public Job(int yStart, int yEnd) {
			this.yStart = yStart;
			this.yEnd = yEnd;
			this.maximumDirectRange = Math.max(height / NUMBER_OF_JOBS, MINIMAL_JOB_RANGE);
		}

		@Override
		protected void compute() {
			int range = yEnd - yStart;
			if (range <= maximumDirectRange) {
				computeDirect();
			} else {
				Job firstHalf = new Job(yStart, yStart + range / 2);
				Job secondHalf = new Job(yStart + range / 2, yEnd);
				invokeAll(firstHalf, secondHalf);
			}
		}

		/**
		 * Direct job handling. Method takes range of y values and computes
		 * colors for each pixel in range.
		 */
		private void computeDirect() {
			int offset = yStart * width;
			for (int y = yStart; y < yEnd; y++) {
				for (int x = 0; x < width; x++) {
					Point3D screenPoint = screenCorner.add(xAxis.scalarMultiply(x * horizontal / (width - 1)))
							.sub(yAxis.scalarMultiply(y * vertical / (height - 1)));
					Ray ray = Ray.fromPoints(eye, screenPoint);

					short[] rgb = new short[3];
					tracer(scene, ray, rgb);

					red[offset] = rgb[0] > 255 ? 255 : rgb[0];
					green[offset] = rgb[1] > 255 ? 255 : rgb[1];
					blue[offset] = rgb[2] > 255 ? 255 : rgb[2];
					offset++;
				}
			}
		}
	}

}
