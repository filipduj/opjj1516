package hr.fer.zemris.java.raytracer.model;

import java.util.OptionalDouble;

/**
 * Class represents model of Sphere in 3D space.
 * <p>
 * Sphere is defined by it's center, radius, diffusion and reflection
 * coefficients.
 * 
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 11, 2016)
 */
public class Sphere extends GraphicalObject {

	/**
	 * Center of sphere
	 */
	private Point3D center;

	/**
	 * Sphere radius
	 */
	private double radius;

	/**
	 * Red diffusion coefficient
	 */
	private double kdr;

	/**
	 * Green diffusion coefficient
	 */
	private double kdg;

	/**
	 * Blue diffusion coefficient
	 */
	private double kdb;

	/**
	 * Red reflection coefficient
	 */
	private double krr;

	/**
	 * Green reflection coefficient
	 */
	private double krg;

	/**
	 * Blue reflection coefficient
	 */
	private double krb;

	/**
	 * Surface roughness coefficient
	 */
	private double krn;

	/**
	 * Constructor initializes new Sphere object with given parameters.
	 * 
	 * @param center
	 *            Center of sphere
	 * @param radius
	 *            Sphere radius
	 * @param kdr
	 *            Red diffusion coefficient
	 * @param kdg
	 *            Green diffusion coefficient
	 * @param kdb
	 *            Blue diffusion coefficient
	 * @param krr
	 *            Red reflection coefficient
	 * @param krg
	 *            Green reflection coefficient
	 * @param krb
	 *            Blue reflection coefficient
	 * @param krn
	 *            Surface roughness coefficient
	 */
	public Sphere(Point3D center, double radius, double kdr, double kdg, double kdb, double krr, double krg, double krb,
			double krn) {
		this.center = center;
		this.radius = radius;
		this.kdr = kdr;
		this.kdg = kdg;
		this.kdb = kdb;
		this.krr = krr;
		this.krg = krg;
		this.krb = krb;
		this.krn = krn;
	}

	@Override
	public RayIntersection findClosestRayIntersection(Ray ray) {

		Point3D p = ray.start.sub(center);

		// quadratic equation coefficients
		double a = ray.direction.scalarProduct(ray.direction);
		double b = 2 * ray.direction.scalarProduct(p);
		double c = p.scalarProduct(p) - radius * radius;
		double d = b * b - 4 * a * c;

		/*
		 * calculate roots, take smallest root greater than zero. If no such
		 * root exist return null;
		 */
		OptionalDouble optLambda = getLambda(a, b, c, d);

		if (optLambda.isPresent()) {
			double lambda = optLambda.getAsDouble();
			Point3D intersection = ray.start.add(ray.direction.scalarMultiply(lambda));
			double distance = intersection.sub(ray.start).norm();
			boolean outer = (ray.start.sub(center).norm()) > radius;

			return new RayIntersection(intersection, distance, outer) {
				@Override
				public Point3D getNormal() {
					return center.sub(intersection).normalize();
				}

				@Override
				public double getKrr() {
					return krr;
				}

				@Override
				public double getKrn() {
					return krn;
				}

				@Override
				public double getKrg() {
					return krg;
				}

				@Override
				public double getKrb() {
					return krb;
				}

				@Override
				public double getKdr() {
					return kdr;
				}

				@Override
				public double getKdg() {
					return kdg;
				}

				@Override
				public double getKdb() {
					return kdb;
				}
			};
		}

		return null;
	}

	/**
	 * Takes parameters of quadratic equation and returns smallest positive
	 * solution. <br>
	 * If such solution does not exist (or solutions are complex) null is
	 * returned.
	 * 
	 * @param a
	 *            Coefficient of x^2
	 * @param b
	 *            Coefficient of x^1
	 * @param c
	 *            Coefficient of x^0
	 * @param d
	 *            Discriminant of quadratic equation
	 * @return smallest positive zero or {@code null}
	 */
	private OptionalDouble getLambda(double a, double b, double c, double d) {

		if (d < 0) {
			return OptionalDouble.empty();
		}

		double dRoot = Math.sqrt(d);
		double lambda1 = (-b - dRoot) / (2 * a);
		double lambda2 = (-b + dRoot) / (2 * a);

		if (lambda1 < 0 && lambda2 < 0) {
			return OptionalDouble.empty();
		} else if (lambda1 < 0) {
			return OptionalDouble.of(lambda2);
		} else if (lambda2 < 0) {
			return OptionalDouble.of(lambda1);
		} else {
			return OptionalDouble.of(Math.min(lambda1, lambda2));
		}

	}

}
