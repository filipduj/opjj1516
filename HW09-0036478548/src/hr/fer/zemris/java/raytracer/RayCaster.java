package hr.fer.zemris.java.raytracer;

import hr.fer.zemris.java.raytracer.model.Point3D;
import hr.fer.zemris.java.raytracer.producers.SimpleRayTracerProducer;
import hr.fer.zemris.java.raytracer.viewer.RayTracerViewer;

/**
 * Simple program which demonstrates "ray-tracer" rendering of 3D scene.
 * <p>
 * It uses single threaded production of rendered picture.
 *
 * @author Filip Dujmušić
 * @version 1.0 (May 11, 2016)
 */
public class RayCaster {

	/**
	 * Program entry point
	 * 
	 * @param args
	 *            accepts no command line argument
	 */
	public static void main(String[] args) {
		RayTracerViewer.show(new SimpleRayTracerProducer(), 
							 new Point3D(10, 10,  0), // eye
							 new Point3D(0,  0,  0), // view
							 new Point3D(0,  0, 10), // view-up vector
							 20.0, 					 // horizontal
							 20.0); 				 // vertical
	}

}
