package hr.fer.zemris.java.custom.collections;

/**
 * Resizable array backed collection of Objects. Dynamically doubles it's size
 * when needed, but never reduces it. Duplicate elements are allowed but
 * inserting null elements is not allowed. It offers usual methods such as
 * insert, add (which is just special case of inserting element to end), remove
 * etc.
 * 
 * @author Filip Dujmušić
 *
 */
public class ArrayIndexedCollection extends Collection {

	/**
	 * Number of elements currently stored in collection. Initially set to zero.
	 */
	private int size;

	/**
	 * Maximal number of elements that can be stored in currently allocated
	 * collection.
	 */
	private int capacity;

	/**
	 * Array of objects, actual data is stored here.
	 */
	private Object[] elements;

	/**
	 * Default constructor, if no arguments passed upon creation it sets default
	 * capacity to 16.
	 */
	public ArrayIndexedCollection() {
		this(16);
	}

	/**
	 * Constructor allows to set initial capacity of collection.
	 * 
	 * @param initialCapacity
	 *            initial capacity of collection, must be positive integer
	 */
	public ArrayIndexedCollection(int initialCapacity) {
		this(null, initialCapacity);
	}

	/**
	 * Constructs this collection by copying elements from another.
	 * 
	 * @param other
	 *            Collection from which elements will be copied.
	 */
	public ArrayIndexedCollection(Collection other) {
		this(other, other.size());
	}

	/**
	 * Constructs collection by copying elements from another one and allows
	 * initial capacity to be set. If capacity is less than other collection
	 * size, it will be dinamically changed to fit all the elements.
	 * 
	 * @param other
	 *            other collection from which elements will be copied
	 * @param initialCapacity
	 *            initial capacity of newly created collection
	 */
	public ArrayIndexedCollection(Collection other, int initialCapacity) {
		elements = new Object[initialCapacity];
		capacity = initialCapacity;

		if (other != null) {
			addAll(other);
		}
	}

	public int size() {
		return size;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * New element is inserted at the first empty location (which is first
	 * position after last element in array). It is special case of insert
	 * method where no elements need to be shifted therefore complexity of
	 * operation is O(1)
	 */
	public void add(Object value) {
		insert(value, size);
	}

	/**
	 * Gets element from this collection at given position. If position is
	 * invalid or throws exception. Element is retrieved in constant time.
	 * 
	 * @param index
	 *            position of wanted element, must be integer between [0, size)
	 * @return returns Object at given position
	 */
	public Object get(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
		return elements[index];
	}

	/**
	 * Removes element at given position. If position is invalid exception is
	 * thrown. Average complexity is O(n/2) since some elements have to be
	 * shifted.
	 * 
	 * @param index
	 *            position of element to be removed, integer from [0, size)
	 */
	public void remove(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
		for (int i = index; i < size - 1; ++i) {
			elements[i] = elements[i + 1];
		}
		size--;
		elements[size] = null;
	}

	/**
	 * Inserts element to given position. Shifts all element starting at
	 * position for 1 to the right and inserts element to position. Throws
	 * exception if element is null or position invalid. If there is no room for
	 * new element capacity is doubled. Complexity of operation is O(n/2) on
	 * average since elements must be shifted in order to create free field.
	 * 
	 * 
	 * @param value
	 *            Element to be added to collection
	 * @param position
	 *            Position where element will be added (integer from set [0,
	 *            size])
	 */
	public void insert(Object value, int position) {
		if (value == null) {
			throw new IllegalArgumentException("Cannot insert null in array!");
		}

		if (position < 0 || position > size) {
			throw new IndexOutOfBoundsException("Index out of bounds!");
		}

		if (size >= capacity) {
			doubleSize();
		}

		size++;

		for (int i = size; i > position + 1; --i) {
			elements[i] = elements[i - 1];
		}
		elements[position] = value;
	}

	/**
	 * Returns position of first occurrence of given element. Worst case
	 * complexity is O(n).
	 * 
	 * @param value
	 *            element for which collection will be searched
	 * @return index of first occurrence of element or -1 if element does not
	 *         exist in collection
	 */
	public int indexOf(Object value) {
		for (int i = 0; i < size; ++i) {
			if (value.equals(elements[i])) {
				return i;
			}
		}
		return -1;
	}

	public boolean contains(Object value) {
		return indexOf(value) >= 0;
	}

	public void clear() {
		for (int i = 0; i < size; ++i) {
			elements[i] = null;
		}
		size = 0;
	}

	/**
	 * Doubles the capacity of collection. Creates new array with double the
	 * size and copies all elements from old array.
	 */
	private void doubleSize() {
		Object[] newElements = new Object[2 * capacity];
		for (int i = 0; i < size; ++i) {
			newElements[i] = elements[i];
		}
		capacity *= 2;
		elements = newElements;
	}

	@Override
	public void forEach(Processor processor) {
		for (int i = 0; i < size; ++i) {
			processor.process(elements[i]);
		}
	}

	@Override
	public boolean remove(Object value) {
		int index = indexOf(value);
		if (index >= 0) {
			remove(index);
			return true;
		}
		return false;
	}

	@Override
	public Object[] toArray() {
		Object[] array = new Object[size];
		for (int i = 0; i < size; ++i) {
			array[i] = elements[i];
		}
		return array;
	}

}
