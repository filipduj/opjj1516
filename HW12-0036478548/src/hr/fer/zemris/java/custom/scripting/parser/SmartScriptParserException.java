package hr.fer.zemris.java.custom.scripting.parser;

/**
 * Basic exception class used in {@link Lexer}.
 * 
 * @author Filip Dujmušić
 *
 */
public class SmartScriptParserException extends RuntimeException {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Empty constructor
	 */
	public SmartScriptParserException() {
		super();
	}

	/**
	 * Constructor takes input message and displays it to user
	 * 
	 * @param message
	 *            message describing exception which occured
	 */
	public SmartScriptParserException(String message) {
		super(message);
	}

}
