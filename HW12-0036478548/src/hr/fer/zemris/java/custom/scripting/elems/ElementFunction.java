package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Element representation of function.
 * 
 * @author Filip Dujmušić
 *
 */
public class ElementFunction extends Element {

	/**
	 * Function name
	 */
	private String name;

	/**
	 * Function element constructor
	 * 
	 * @param name
	 *            function string
	 */
	public ElementFunction(String name) {
		this.name = name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText() {
		return name;
	}

	/**
	 * {@inheritDoc} 
	 * Appends '@' to the beginning of function name.
	 */
	@Override
	public String toString() {
		return "@" + asText();
	}

}
