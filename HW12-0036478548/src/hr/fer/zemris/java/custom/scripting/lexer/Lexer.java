package hr.fer.zemris.java.custom.scripting.lexer;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

/**
 * Class provides methods for grouping characters to tokens and retrieving them.
 * It's type of lazy lexer which means you have to ask for next token before
 * lexer provides it. 
 * 
 * Rules by which characters are grouped to tokens:
 *  
 * 1) Document consists of TEXT or TAG areas. 
 * 2) TAG is everything enclosed in between {$ and $} 
 * 3) TAG NAME is first token after opening {$ construct.
 *    Name can be "=" or any other valid variable name.
 * 4) TEXT is everything that is outside tag. 
 * 5) When in TEXT area lexer takes text literally as it is written, until end of file 
 *    is reached or tag was opened. Token is all that text.
 *    Following escaping is supported: \{ -> { and \\ -> \ 
 * 6) When in TAG area symbols are grouped by following rules:
 * 		6.1) Variable starts with letter and then follows zero or more letters/digits/underscores
 * 		6.2) Integer starts with number and then follows zero or more numbers
 * 		6.3) Double starts with number following zero or more numbers following dot
 * 			 and then zero or more numbers, for example -23. is parsed as -23.0 
 * 		6.4) Functions start with @ symbol and then syntax same as 5.1
 * 		6.5) String is enclosed between " and ". 
 * 			 Inside can be anything but with following escapings: \\ -> \
 * 																  \" -> "
 * 																  \n, \r, \t with usual meaning
 * 		6.6) Operand is one of following symbols: +,-,*,/,^
 *  
 *  This lexer will provide methods for tokenizing input and producing next token following
 *  rules above. If rules are not met exceptions are thrown.  
 * 
 * @author Filip Dujmušić
 *
 */
public class Lexer {
	
	/**
	 * Script text data. 
	 */
	private char[] data;

	/**
	 * Last calculated token.
	 */
	private Token token;

	/**
	 * Current position at input data.
	 */
	private int currentPosition;

	/**
	 * Current state of lexer.
	 * Can be either TEXT or TAG.
	 */
	private LexerState state;

	/**
	 * Lexer constructor. Takes input string and sets
	 * lexer to initial state.
	 * 
	 * @param input input string, script body
	 * @throws SmartScriptParserException if passed input string is null reference
	 */
	public Lexer(String input) {
		if (input == null) {
			throw new SmartScriptParserException("Input is null!");
		}
		data = input.toCharArray();
		currentPosition = 0;
		state = LexerState.TEXT;
	}

	/**
	 * Calculates next token starting from current position.
	 * Follows SmartScript token rules and form next token.
	 * 
	 * @return returns {@link Token} representing next calculated token
	 * @throws SmartScriptParserException if token rules are not met
	 */
	public Token nextToken() {
		if (currentPosition == data.length) {
			token = new Token(null, TokenType.EOF);
			currentPosition++;
			return token;
		} else if (currentPosition > data.length) {
			throw new SmartScriptParserException("EOF already reached!");
		} else {
			StringBuilder sb = new StringBuilder();
			if (state == LexerState.TEXT) {
				while (currentPosition < data.length) {
					if (data[currentPosition] == '\\') {
						++currentPosition;
						if (currentPosition < data.length) {
							if (data[currentPosition] == '\\') {
								sb.append('\\');
							} else if (data[currentPosition] == '{') {
								sb.append('{');
							} else {
								throw new SmartScriptParserException("Invalid escaping in document body!");
							}
							++currentPosition;
						} else {
							throw new SmartScriptParserException("Invalid escaping in document body!");
						}
					} else if (data[currentPosition] == '{') {
						++currentPosition;
						if (currentPosition < data.length && data[currentPosition] == '$') {
							if (sb.length() > 0) {
								currentPosition--;
								token = new Token(sb.toString(), TokenType.TEXT);
							} else {
								++currentPosition;
								while (currentPosition < data.length && Character.isWhitespace(data[currentPosition])) {
									++currentPosition;
								}
								if (currentPosition < data.length && data[currentPosition] == '=') {
									sb.append('=');
									currentPosition++;
								} else if (currentPosition < data.length && Character.isLetter(data[currentPosition])) {
									sb.append(data[currentPosition++]);
									while (currentPosition < data.length
											&& (Character.isLetterOrDigit(data[currentPosition])
													|| data[currentPosition] == '_')) {
										sb.append(data[currentPosition++]);
									}
								} else {
									throw new SmartScriptParserException("Wrong tag name or empty tag!");
								}
								token = new Token(sb.toString().toLowerCase(), TokenType.BEGIN);
							}
							return token;
						} else {
							sb.append(data[currentPosition++]);
						}
					} else {
						sb.append(data[currentPosition++]);
					}
				}
				token = new Token(sb.toString(), TokenType.TEXT);
			} else if (state == LexerState.TAG) {
				while (currentPosition < data.length) {
					while (Character.isWhitespace(data[currentPosition])) { // ignore
																			// whitespace
						++currentPosition;
					}
					Character c = data[currentPosition];
					if (Character.isLetter(c)) {
						sb.append(c);
						++currentPosition;
						while (currentPosition < data.length
								&& (Character.isLetterOrDigit(data[currentPosition]) || data[currentPosition] == '_')) {
							sb.append(data[currentPosition++]);
						}
						token = new Token(sb.toString(), TokenType.VARIABLE);
						break;
					} else if ((c == '-' && (currentPosition + 1 < data.length)
							&& Character.isDigit(data[currentPosition + 1])) || Character.isDigit(c)) {
						sb.append(c);
						currentPosition++;
						while (currentPosition < data.length && Character.isDigit(data[currentPosition])) {
							sb.append(data[currentPosition++]);
						}
						if (data[currentPosition] != '.') {
							try {
								token = new Token(Integer.parseInt(sb.toString()), TokenType.INTEGER);
							} catch (NumberFormatException e) {
								throw new SmartScriptParserException("Integer constant not parsable!");
							}
						} else {
							sb.append('.');
							currentPosition++;
							while (currentPosition < data.length && Character.isDigit(data[currentPosition])) {
								sb.append(data[currentPosition++]);
							}
							try {
								token = new Token(Double.parseDouble(sb.toString()), TokenType.DOUBLE);
							} catch (NumberFormatException e) {
								throw new SmartScriptParserException("Real number constant not parsable!");
							}
						}
						break;
					} else if (c == '@') {
						currentPosition++;
						if (currentPosition < data.length && Character.isLetter(data[currentPosition])) {
							sb.append(data[currentPosition++]);
							while (currentPosition < data.length && (Character.isLetterOrDigit(data[currentPosition])
									|| data[currentPosition] == '_')) {
								sb.append(data[currentPosition++]);
							}
							token = new Token(sb.toString(), TokenType.FUNCTION);
						} else {
							throw new SmartScriptParserException("Wrong function name!");
						}
						break;
					} else if (c == '\"') {
						sb.append('\"');
						currentPosition++;
						while (currentPosition < data.length) {
							if (data[currentPosition] == '\\') {
								Character esc;
								++currentPosition;
								if (currentPosition < data.length) {
									esc = data[currentPosition++];
								} else {
									throw new SmartScriptParserException("Wrong string syntax!");
								}
								if (esc == '\\') {
									sb.append('\\');
								} else if (esc == '\"') {
									sb.append('\"');
								} else if (esc == 'n') {
									sb.append('\n');
								} else if (esc == 'r') {
									sb.append('\r');
								} else if (esc == 't') {
									sb.append('\t');
								} else {
									throw new SmartScriptParserException("Wrong escaping syntax in string!");
								}
							} else if (data[currentPosition] == '\"') {
								sb.append('\"');
								currentPosition++;
								break;
							} else {
								sb.append(data[currentPosition++]);
							}
						}
						if (sb.length() >= 2 && sb.toString().charAt(sb.length() - 1) == '\"') {
							token = new Token(sb.toString().substring(1, sb.length() - 1), TokenType.STRING);
						} else {
							throw new SmartScriptParserException("Wrong string syntax!");
						}
						break;
					} else if (c == '+' || c == '-' || c == '*' || c == '/' || c == '^') {
						token = new Token(c.toString(), TokenType.OPERATOR);
						currentPosition++;
						return token;
					} else if (c == '$' && currentPosition + 1 < data.length && data[currentPosition + 1] == '}') {
						token = new Token(null, TokenType.END);
						currentPosition += 2;
						break;
					} else {
						throw new SmartScriptParserException("Wrong tag syntax!");
					}
				}
			}
			return token;
		}
	}

	/**
	 * Token getter.
	 * Gets last calculated token.
	 * Can be null if nextToken() was never called.
	 * 
	 * @return returns last calculated token or null
	 * 
	 */
	public Token getToken() {
		return token;
	}

	/**
	 * State setter.
	 * Allows state of Lexer to be changed from outside.
	 * 
	 * @param state state of lexer to be changed to. Valid states are TEXT or TAG
	 * @throws SmartScriptParserException if passed state parameter is null
	 */
	public void setState(LexerState state) {
		if(state == null){
			throw new SmartScriptParserException("Lexer state cannot be null!");
		}
		this.state = state;
	}
}
