package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enum provides possible {@link Token} types.
 * 
 * @author Filip Dujmušić
 *
 */
public enum TokenType {
	
	/**
	 * Textual data (content outside of {$ ... $}
	 */
	TEXT, 
	
	/**
	 * Beginning of tag: "{$"
	 */
	BEGIN, 
	
	/**
	 * Ending of tag: "$}"
	 */
	END, 
	
	/**
	 * Variable token
	 */
	VARIABLE, 
	
	/**
	 * Function token
	 */
	FUNCTION, 
	
	/**
	 * Integer token
	 */
	INTEGER, 
	
	/**
	 * Double token
	 */
	DOUBLE, 
	
	/**
	 * String token
	 */
	STRING, 
	
	/**
	 * Operator token
	 */
	OPERATOR, 
	
	/**
	 * End of file
	 */
	EOF;
	
}
