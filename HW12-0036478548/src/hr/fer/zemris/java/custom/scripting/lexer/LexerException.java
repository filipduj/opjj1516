package hr.fer.zemris.java.custom.scripting.lexer;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

/**
 * Basic exception class used in {@link Lexer}.
 * 
 * @author Filip Dujmušić
 *
 */
public class LexerException extends SmartScriptParserException {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 7900591097543619015L;

	/**
	 * Empty constructor
	 */
	public LexerException() {

	}

	/**
	 * Constructor takes input message and displays it to user
	 * 
	 * @param message message describing exception which occured
	 */
	public LexerException(String message) {
		super(message);
	}

}
