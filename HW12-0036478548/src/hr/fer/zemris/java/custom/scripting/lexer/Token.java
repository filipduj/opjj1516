package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Token wrapper class. Wraps any kind of token in the way that it keeps it's
 * value and type.
 * 
 * @author Filip Dujmušić
 *
 */
public class Token {

	/**
	 * Token value
	 */
	private Object value;

	/**
	 * Token type
	 */
	private TokenType type;

	/**
	 * Constructor takes value and type and initializes new Token.
	 * 
	 * @param value
	 *            any value
	 * @param type
	 *            legal types are in {@link TokenType} enum
	 * @throws IllegalArgumentException
	 *             if type is null reference
	 */
	public Token(Object value, TokenType type) {
		if (type == null) {
			throw new IllegalArgumentException();
		}
		this.value = value;
		this.type = type;
	}

	/**
	 * Value getter
	 * 
	 * @return returns token value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Type getter
	 * 
	 * @return returns token type
	 */
	public TokenType getType() {
		return type;
	}
}
