package hr.fer.zemris.java.custom.scripting.exec;

import java.io.IOException;
import java.util.Deque;
import java.util.LinkedList;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantDouble;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFunction;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * {@code SmartScriptEngine} class is capable of executing script code parsed
 * with {@link SmartScriptParser}.
 * <p>
 * Every request received on server will have to make one instance of this
 * engine and use it for script execution.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 31, 2016)
 */
public class SmartScriptEngine {

	/**
	 * Root node of tree-like structure representing parsed script
	 */
	private DocumentNode documentNode;

	/**
	 * Context assigned to one request server received
	 */
	private RequestContext requestContext;

	/**
	 * Multistack to be used for variable value storage
	 */
	private ObjectMultistack multistack = new ObjectMultistack();

	/**
	 * {@link INodeVisitor} implementation responsible for traversing through
	 * parsed script nodes and executing every one of them.
	 */
	private INodeVisitor visitor = new INodeVisitor() {

		@Override
		public void visitTextNode(TextNode node) {
			try {
				requestContext.write(node.asText());
			} catch (IOException e) {
				throw new RuntimeException(e.getMessage());
			}
		}

		@Override
		public void visitForLoopNode(ForLoopNode node) {

			String variable = node.getVariable().asText();
			Integer increment = ((ElementConstantInteger) node.getStepExpression()).getValue();
			Integer end = ((ElementConstantInteger) node.getEndExpression()).getValue();

			multistack.push(variable,
					new ValueWrapper(((ElementConstantInteger) node.getStartExpression()).getValue()));

			while ((Integer) multistack.peek(variable).getValue() <= end) {
				visitChildren(node);
				multistack.push(variable, new ValueWrapper((Integer) multistack.peek(variable).getValue() + increment));
			}

		}

		@Override
		public void visitEchoNode(EchoNode node) {

			ArrayIndexedCollection elements = node.getTokens();
			Deque<Object> stack = new LinkedList<>();

			for (int i = 0, n = elements.size(); i < n; ++i) {
				Element element = (Element) elements.get(i);

				if (element instanceof ElementConstantInteger) {
					stack.push(new ValueWrapper(((ElementConstantInteger) element).getValue()));
				} else if (element instanceof ElementConstantDouble) {
					stack.push(new ValueWrapper(((ElementConstantDouble) element).getValue()));
				} else if (element instanceof ElementOperator) {
					Operations.calculateOperation(element, stack);
				} else if (element instanceof ElementVariable) {
					stack.push(new ValueWrapper(((ValueWrapper) multistack.peek(element.asText())).getValue()));
				} else if (element instanceof ElementString) {
					stack.push(new ValueWrapper(element.asText()));
				} else if (element instanceof ElementFunction) {
					Functions.calculateFunction(element.asText(), stack, requestContext);
				} else {
					throw new RuntimeException("Error in echo node. Unknown token!");
				}
			}

			while (!stack.isEmpty()) {
				try {
					requestContext.write(((ValueWrapper) stack.pollLast()).toString());
				} catch (IOException e) {
					e.printStackTrace();
					throw new RuntimeException(e.getMessage());
				}
			}

		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			visitChildren(node);
		}

		private void visitChildren(Node node) {
			for (int i = 0, n = node.numberOfChildren(); i < n; ++i) {
				node.getChild(i).accept(this);
			}
		}

	};

	/**
	 * Constructor initializes one {@link SmartScriptEngine} with given
	 * {@code documentNode} and {@code requestContext}.
	 * 
	 * @param documentNode
	 *            Root node of parsed script
	 * @param requestContext
	 *            Context of received request
	 */
	public SmartScriptEngine(DocumentNode documentNode, RequestContext requestContext) {
		this.documentNode = documentNode;
		this.requestContext = requestContext;
	}

	/**
	 * Calling this method results in beginning of execution of script assigned
	 * to this instance.
	 */
	public void execute() {
		documentNode.accept(visitor);
	}

}
