package hr.fer.zemris.java.custom.scripting.demo;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.INodeVisitor;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.util.Utilities;

/**
 * Program demonstrates use of Visitorn pattern in combination with Composite
 * pattern.
 * <p>
 * One command line argument is expected: path to script file whose content will
 * be parsed into composite object(s). Tree structure of parsed script will then
 * be printed to standard output.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 31, 2016)
 */
public class TreeWriter {

	/**
	 * Visitor implementation for traversing comopsite document node and
	 * printing it's content to standard output.
	 * 
	 * @author Filip Dujmušić
	 * @version 1.0 (May 31, 2016)
	 */
	private static class WriterVisitor implements INodeVisitor {

		@Override
		public void visitTextNode(TextNode node) {
			System.out.println(node);
		}

		@Override
		public void visitForLoopNode(ForLoopNode node) {
			System.out.println(node);
			visitChildren(node);
			System.out.println("{$END$}");
		}

		@Override
		public void visitEchoNode(EchoNode node) {
			System.out.println(node);
		}

		@Override
		public void visitDocumentNode(DocumentNode node) {
			visitChildren(node);
		}

		/**
		 * Method visits all children nodes for given {@code node} and visits
		 * every one of them.
		 * 
		 * @param node
		 *            Node whose children will be visited
		 */
		private void visitChildren(Node node) {
			for (int i = 0, n = node.numberOfChildren(); i < n; ++i) {
				node.getChild(i).accept(this);
			}
		}
	}

	/**
	 * Program entry point.
	 * 
	 * @param args
	 *            Path to .smscr script file
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			Utilities.errorExit("Expected one command line argument: script file path");
		}

		String script = null;

		try {
			script = new String(Files.readAllBytes(Utilities.getPath(args[0])), StandardCharsets.UTF_8);
		} catch (IOException e) {
			Utilities.errorExit(e.getMessage());
		}

		SmartScriptParser parser = new SmartScriptParser(script);
		WriterVisitor visitor = new WriterVisitor();

		parser.getDocumentNode().accept(visitor);

	}

}
