package hr.fer.zemris.java.custom.scripting.elems;

import hr.fer.zemris.java.custom.scripting.lexer.Token;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

/**
 * Factory class that provides only one static method. It handles all
 * {@link Element} derivatives and knows how to construct new objects out of
 * given data.
 * 
 * @author Filip Dujmušić
 *
 */
public class ElementFactory {

	/**
	 * Creates specific Element from passed token.
	 * 
	 * @param token
	 *            token from which element is created
	 * @return returns {@link Element}
	 */
	public static Element newElement(Token token) {
		switch (token.getType()) {
		case VARIABLE:
			return new ElementVariable((String) token.getValue());
		case FUNCTION:
			return new ElementFunction((String) token.getValue());
		case INTEGER:
			return new ElementConstantInteger((Integer) token.getValue());
		case DOUBLE:
			return new ElementConstantDouble((Double) token.getValue());
		case OPERATOR:
			return new ElementOperator((String) token.getValue());
		case STRING:
			return new ElementString((String) token.getValue());

		default:
			throw new SmartScriptParserException("Can not create element out of this token!");
		}

	}
}
