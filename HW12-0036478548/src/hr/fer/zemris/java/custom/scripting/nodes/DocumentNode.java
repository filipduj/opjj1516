package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Node representing entire document. No special purpose, except that it
 * semantically defines root of parsed document.
 * 
 * @author Filip Dujmušić
 *
 */
public class DocumentNode extends Node {

	/**
	 * Empty constructor. Passes null to Node constructor since there is nothing
	 * to parse.
	 */
	public DocumentNode() {

	}
	
	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitDocumentNode(this);
	}

	/**
	 * Constructs string representation of DocumentNode. Reconstructs original
	 * script text out of DocumentNode tree.
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0, n = numberOfChildren(); i < n; i++) {
			recursiveToString(getChild(i), sb);
		}

		return sb.toString();
	}

	/**
	 * Recursively traverses through Node tree and creates cumulative string
	 * representation of tree members. Traversal is in order, and resulting
	 * string is almost equal to the original script from which this tree was
	 * created. Possible loss of whitespace/newline elements.
	 * 
	 * @param node
	 *            root node from which traversal begins
	 * @param sb
	 *            string builder which contains resulting text representation
	 */
	private void recursiveToString(Node node, StringBuilder sb) {

		sb.append(node);

		for (int i = 0, n = node.numberOfChildren(); i < n; ++i) {
			recursiveToString(node.getChild(i), sb);
		}

		if (node instanceof ForLoopNode) {
			sb.append("{$ END $}");
		}
	}
}
