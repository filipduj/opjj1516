package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Element representation of variable.
 * 
 * @author Filip Dujmušić
 *
 */
public class ElementVariable extends Element {

	/**
	 * Variable name as string
	 */
	private String name;

	/**
	 * Variable element constructor
	 * 
	 * @param name
	 *            name of variable
	 */
	public ElementVariable(String name) {
		this.name = name;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText() {
		return name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return asText();
	}

}
