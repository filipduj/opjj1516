package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

/**
 * Base class for all graph nodes. Keeps track of child nodes and provides
 * methods for manipulating with child nodes.
 * 
 * @author Filip Dujmušić
 *
 */
public abstract class Node {

	/**
	 * Collection which keeps potential child nodes of current node.
	 */
	private ArrayIndexedCollection childNodes;

	/**
	 * Calls visit method for this node type.
	 * 
	 * @param visitor
	 *            {@link INodeVisitor} visitor
	 */
	public abstract void accept(INodeVisitor visitor);

	/**
	 * Sets passed node as child of current node.
	 * 
	 * @param child
	 *            {@link Node} input
	 */
	public void addChildNode(Node child) {
		if (childNodes == null) {
			childNodes = new ArrayIndexedCollection();
		}
		childNodes.add(child);
	}

	/**
	 * Returns number of children for current node.
	 * 
	 * @return number of children for current node or zero if no children
	 */
	public int numberOfChildren() {
		return childNodes == null ? 0 : childNodes.size();
	}

	/**
	 * Gets child at given index.
	 * 
	 * @param index
	 *            index of child
	 * @return child node at given index
	 * @throws SmartScriptParserException
	 *             if child does not exist
	 */
	public Node getChild(int index) {
		if (childNodes == null) {
			throw new SmartScriptParserException("Tried to retrieve child but node has no children!");
		}
		try {
			return (Node) childNodes.get(index);
		} catch (IndexOutOfBoundsException e) {
			throw new SmartScriptParserException("Tried to retrieve child on index out of bounds!");
		}
	}
}
