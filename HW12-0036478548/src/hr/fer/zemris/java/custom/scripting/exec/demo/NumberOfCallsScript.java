package hr.fer.zemris.java.custom.scripting.exec.demo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;
import hr.fer.zemris.java.webserver.util.Utilities;

/**
 * Demo program parses and executes "brojPoziva.smscr" script.
 * <p>
 * No parameters needed, it is ready to run.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 31, 2016)
 */
public class NumberOfCallsScript {

	/**
	 * Program entry point.
	 */
	public static void main(String[] args) {

		String documentBody = "";
		try {
			documentBody = new String(Files.readAllBytes(Paths.get("./scripts/brojPoziva.smscr")));
		} catch (IOException e) {
			Utilities.errorExit(e.getMessage());
		}
		
		Map<String, String> parameters = new HashMap<String, String>();
		Map<String, String> persistentParameters = new HashMap<String, String>();
		List<RCCookie> cookies = new ArrayList<RequestContext.RCCookie>();
		persistentParameters.put("brojPoziva", "3");
		RequestContext rc = new RequestContext(System.out, parameters, persistentParameters, cookies);
		
		new SmartScriptEngine(new SmartScriptParser(documentBody).getDocumentNode(), rc).execute();
		
		System.out.println("Vrijednost u mapi: " + rc.getPersistentParameter("brojPoziva"));
	}
}