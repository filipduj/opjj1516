package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;

/**
 * Node representing a command which generates some textual output dynamically.
 * It inherits from Node class.
 * 
 * @author Filip Dujmušić
 *
 */
public class EchoNode extends Node {

	/**
	 * Array holding token parts of parsed string. Every echo node can have many
	 * tokens.
	 */
	private ArrayIndexedCollection tokens;

	/**
	 * Calls super constructor passing string input used to construct new node.
	 * 
	 * @param token
	 *            input token
	 */
	/*
	 * public EchoNode(String token) { super(token); }
	 */
	public EchoNode(ArrayIndexedCollection tokens) {
		this.tokens = tokens;
	}
	
	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitEchoNode(this);
	}

	/**
	 * Gets array of tokens assigned to this node.
	 * 
	 * @return returns token array or <code>null</code> if no tokens assigned
	 */
	public ArrayIndexedCollection getTokens() {
		return tokens;
	}

	/**
	 * String representation of echo construct. Constructs string containing
	 * echo construct in following format: {$ = [token1] [token2] ... $}
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("{$ = ");
		for (int i = 0, n = tokens.size(); i < n; ++i) {
			sb.append(tokens.get(i)).append(" ");
		}
		sb.append("$}");

		return sb.toString();
	}

}
