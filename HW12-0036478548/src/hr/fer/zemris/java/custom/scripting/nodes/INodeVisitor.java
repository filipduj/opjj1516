package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;

/**
 * Node visitor interface.
 * <p>
 * Provides visit methods for all node types that {@link SmartScriptParser}
 * could possibly generate while parsing script.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 31, 2016)
 */
public interface INodeVisitor {

	/**
	 * Called upon visiting text node. It simply sends text to output stream.
	 * 
	 * @param node
	 *            Text node
	 */
	public void visitTextNode(TextNode node);

	/**
	 * Called upon visiting for loop node. It iterates as many times as defined
	 * by for loop start,end,step expressions and visits child nodes in each
	 * iteration.
	 * 
	 * @param node
	 *            For loop node
	 */
	public void visitForLoopNode(ForLoopNode node);

	/**
	 * Called upon visiting echo node. It evaluates echo node taking into that
	 * variables should be switched with their most current values. Then sends
	 * evaluated output to output stream.
	 * 
	 * @param node
	 *            Echo node
	 */
	public void visitEchoNode(EchoNode node);

	/**
	 * Called upon root node of parsed script. This visit starts process of
	 * execution by visiting all documentnode's children.
	 * 
	 * @param node
	 *            Root script node
	 */
	public void visitDocumentNode(DocumentNode node);

}
