package hr.fer.zemris.java.custom.scripting.exec;

import java.util.EmptyStackException;
import java.util.HashMap;
import java.util.Map;

/**
 * Multiple stack class. This class maps String key to Stack of
 * {@link MultiStackEntry} objects. It provides usual stack methods: push, pop
 * and peek.
 * <p>
 * Each stack is adressed by it's key.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class ObjectMultistack {

	/**
	 * Map that holds String,Stack pairs
	 */
	private Map<String, MultiStackEntry> stacks = new HashMap<>();

	/**
	 * This class represents one node entry in stack. It acts as a wrapper and
	 * contains actual value of item on stack as well as reference to next node
	 * "under" the current one.
	 * 
	 * @author Filip Dujmušić
	 * @version 1.0 (Apr 19, 2016)
	 */
	public static class MultiStackEntry {

		/**
		 * Next element in stack hierarchy
		 */
		private MultiStackEntry next;

		/**
		 * Value of current node
		 */
		private ValueWrapper value;

		/**
		 * Constructor allocates and initializes new {@code MultiStackEntry}
		 * object with given arguments.
		 * 
		 * @param value
		 *            Node value
		 * @param next
		 *            Reference to next node
		 */
		public MultiStackEntry(ValueWrapper value, MultiStackEntry next) {
			this.value = value;
			this.next = next;
		}

	}

	/**
	 * Pushes given {@code valueWrapper} on top of Stack associated to given key
	 * {@code name}.
	 * 
	 * @param name
	 *            Key value by which stack is addressed
	 * @param valueWrapper
	 *            Value to push to stack addresed with {@code name}
	 * @throws IllegalArgumentException
	 *             If given key or value are {@code null} references
	 */
	public void push(String name, ValueWrapper valueWrapper) {

		if (valueWrapper == null) {
			throw new IllegalArgumentException("Expected ValueWrapper but null found.");
		}

		if (name == null) {
			throw new IllegalArgumentException("Expected String but null found.");
		}

		stacks.put(name, new MultiStackEntry(valueWrapper, stacks.get(name)));
	}

	/**
	 * Removes one element from top of Stack associated to given key
	 * {@code name}. Returns removed element.
	 * 
	 * @param name
	 *            Key value by which stack is addressed
	 * @return Returns element on top of stack associated to given key
	 *         {@code name}
	 * @throws EmptyStackException
	 *             If this method is called but stack was already empty
	 * @throws IllegalArgumentException
	 *             If given {@code name} is {@code null} reference
	 */
	public ValueWrapper pop(String name) {

		if (name == null) {
			throw new IllegalArgumentException("Expected String but null found.");
		}

		MultiStackEntry stackTop = stacks.get(name);

		ValueWrapper ret = null;

		if (stackTop == null) {
			throw new EmptyStackException();
		} else {
			ret = stackTop.value;
			stacks.put(name, stackTop.next);
		}

		return ret;
	}

	/**
	 * Returns (but does not remove) element on top of stack associated to given
	 * key {@code name}.
	 * 
	 * @param name
	 *            Key value by which stack is addressed
	 * @return Returns element on top of stack associated to given key
	 *         {@code name}
	 * @throws EmptyStackException
	 *             If this method is called but stack was already empty
	 * @throws IllegalArgumentException
	 *             If given {@code name} is {@code null} reference
	 */
	public ValueWrapper peek(String name) {

		if (name == null) {
			throw new IllegalArgumentException("Expected String but null found.");
		}

		MultiStackEntry stackTop = stacks.get(name);

		if (stackTop == null) {
			throw new EmptyStackException();
		}

		return stackTop.value;
	}

	/**
	 * @param name
	 *            Key value by which stack is addressed
	 * @return {@code true} if stack associated to given key {@code name} is
	 *         empty
	 */
	public boolean isEmpty(String name) {

		if (name == null) {
			throw new IllegalArgumentException("Expected String but null found.");
		}

		return stacks.get(name) == null;
	}

}
