package hr.fer.zemris.java.custom.scripting.parser;

import hr.fer.zemris.java.custom.collections.ArrayIndexedCollection;
import hr.fer.zemris.java.custom.collections.EmptyStackException;
import hr.fer.zemris.java.custom.collections.ObjectStack;
import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementConstantInteger;
import hr.fer.zemris.java.custom.scripting.elems.ElementFactory;
import hr.fer.zemris.java.custom.scripting.elems.ElementString;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;
import hr.fer.zemris.java.custom.scripting.lexer.Lexer;
import hr.fer.zemris.java.custom.scripting.lexer.LexerState;
import hr.fer.zemris.java.custom.scripting.lexer.Token;
import hr.fer.zemris.java.custom.scripting.lexer.TokenType;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EchoNode;
import hr.fer.zemris.java.custom.scripting.nodes.ForLoopNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.TextNode;

/**
 * 
 * Parser for structured document format. Provides methods for parsing specially
 * formated documents and building document trees out of it. Parser uses
 * tokens from {@link Lexer} as base building blocks of more complicated expressions.
 * 
 * Parser will succesfully parse the document if syntax rules are met:
 * 
 * 	1) if token type is TEXT no special requirements
 *  2) if token type is BEGIN (denotes beginning of tag - "{$" )
 *  	2.1) TAG name must be either "=", "FOR", "END"
 *  		2.1.1) FOR tag
 *  						syntax: {$ FOR [param1] [param2] [param3] [param4] $}
 *  						param1: must be of type VARIABLE
 *  						param2: must be VARIABLE or STRING(parsable to int or double) or INTEGER or DOUBLE
 *  						param3: same as param2
 *  						param4: optional parameter, same as param2
 * 				   
 * 				   FOR keyword is not case sensitive. If expression has invalid number of parameters
 * 				   or wrong parameter types exception is thrown. Every FOR tag must have it's enclosing END tag.
 * 				   If it doesn't exception is thrown.
 * 			2.1.2) END tag
 * 							syntax: {$ END $}
 * 							usage: encloses non-empty tags (such as for)
 * 			2.1.3) "=" tag (echo tag)
 * 							syntax: {$ = [param1] [param2] ... [paramnN] $}
 * 							param1..N: can be VARIABLE or FUNCTION or STRING or DECIMAL or INTEGER or OPERATOR
 *  3) whitespace is ignored
 *  
 * @author Filip Dujmušić
 *
 */
public class SmartScriptParser {

	/**
	 * Root node for tree holding elements of parsed document.
	 */
	private DocumentNode documentNode;

	/**
	 * Constructs {@link SmartScriptParser} by getting document body as String.
	 * 
	 * @param body
	 *            document body in String form
	 */
	public SmartScriptParser(String body) {
		parseInput(body);
	}

	/**
	 * Parses input string and constructs document tree.
	 * While parsing if rules of syntax are not met exception
	 * can be thrown.
	 * 
	 * @param input
	 *            string representing document body
	 * @throws SmartScriptParserException if syntax is wrong
	 */
	private void parseInput(String input) {

		Lexer lex = new Lexer(input);
		ObjectStack stack = new ObjectStack();
		documentNode = new DocumentNode();

		stack.push(documentNode);

		while (lex.nextToken().getType() != TokenType.EOF) {

			switch (lex.getToken().getType()) {

			// new tag was opened
			case BEGIN:

				// parsing tag so set lexer to TAG STATE
				lex.setState(LexerState.TAG);

				String tagType = ((String) lex.getToken().getValue());

				if (tagType.equals("for")) { // forTag
					ForLoopNode node = getForLoopNode(lex);
					try {
						((Node) stack.peek()).addChildNode(node);
					} catch (EmptyStackException e) {
						throw new SmartScriptParserException("One or more non empty tags closed but never opened!");
					}
					stack.push(node);

				} else if (tagType.equals("end")) { // endTag
					if (lex.nextToken().getType() == TokenType.END) {
						try {
							stack.pop();
						} catch (EmptyStackException e) {
							throw new SmartScriptParserException("One or more non-empty tags opened but never closed!");
						}
					} else {
						throw new SmartScriptParserException("Wrong end tag syntax!");
					}

				} else if (tagType.equals("=")) { // echo tag
					try {
						((Node) stack.peek()).addChildNode(getEchoNode(lex));
					} catch (EmptyStackException e) {
						throw new SmartScriptParserException("One or more non empty tags closed but never opened!");
					}
				} else {
					throw new SmartScriptParserException("Unknown tag name!");
				}

				// parsing tag is over so return lexer to TEXT state
				lex.setState(LexerState.TEXT);

				break;

			// parsing TEXT part
			case TEXT:
				try {
					((Node) stack.peek()).addChildNode(new TextNode((String) lex.getToken().getValue()));
				} catch (EmptyStackException e) {
					throw new SmartScriptParserException("One or more non-empty tags closed but never opened!");
				}
				break;

			case EOF:
				break;

			default:
				throw new SmartScriptParserException("Error!");

			}

		}
		try {
			if (stack.peek() != documentNode) {
				throw new SmartScriptParserException("One or more non empty tags opened but never closed!");
			}
		} catch (EmptyStackException e) {
			throw new SmartScriptParserException("One or more non empty tags closed but never opened!");
		}
	}

	/**
	 * documentNode getter
	 * 
	 * @return returns documentNode or <code>null</code> value if document not
	 *         parsed
	 */
	public DocumentNode getDocumentNode() {
		return documentNode;
	}

	/**
	 * Creates new echo node from stream of tokens in passed Lexer.
	 * 
	 * @param lex Lexer
	 * @return returns {@link EchoNode}
	 * @throws SmartScriptParserException echo syntax is wrong
	 */
	private EchoNode getEchoNode(Lexer lex) {
		ArrayIndexedCollection tokens = new ArrayIndexedCollection();
		while (lex.nextToken().getType() != TokenType.END) {
			tokens.add(ElementFactory.newElement(lex.getToken()));
			if (lex.getToken().getType() == TokenType.EOF) {
				throw new SmartScriptParserException("Wrong echo tag syntax!");
			}
		}
		return new EchoNode(tokens);
	}

	/**
	 * Creates new for loop node from stream of tokens in passed Lexer
	 * 
	 * @param lex Lexer
	 * @return returns {@link ForLoopNode}
	 * @throws SmartScriptParserException for loop syntax is wrong
	 */
	private ForLoopNode getForLoopNode(Lexer lex) {
		if (lex.nextToken().getType() != TokenType.VARIABLE) {
			throw new SmartScriptParserException("First parameter of for loop must be variable!");
		}

		ElementVariable variable = new ElementVariable((String) lex.getToken().getValue());
		Element startExpression = getForLoopElement(lex.nextToken());
		Element endExpression = getForLoopElement(lex.nextToken());
		Element stepExpression = null;

		if (lex.nextToken().getType() != TokenType.END) {
			stepExpression = getForLoopElement(lex.getToken());
			if (lex.nextToken().getType() != TokenType.END) {
				throw new SmartScriptParserException("Wrong for loop syntax!");
			}
		}

		return new ForLoopNode(variable, startExpression, endExpression, stepExpression);
	}

	/**
	 * Checks if passed token is valid for loop element.
	 * Creates new element if valid or throws exception.
	 * 
	 * @param token token input
	 * @return returns new {@link Element} containing token data
	 * @throws SmartScriptParserException if token type is not valid
	 */
	private Element getForLoopElement(Token token) {
		if (token.getType() == TokenType.VARIABLE) {
			return new ElementVariable((String) token.getValue());
		} else if (token.getType() == TokenType.INTEGER) {
			return new ElementConstantInteger((Integer) token.getValue());
		} else if (token.getType() == TokenType.STRING) {

			try {
				Integer.parseInt((String) token.getValue());
			} catch (NumberFormatException e) {
				try {
					Double.parseDouble((String) token.getValue());
				} catch (NumberFormatException ex) {
					throw new SmartScriptParserException("Wrong string in for loop!");
				}
			}

			return new ElementString((String) token.getValue());
		} else {
			throw new SmartScriptParserException("Wrong for loop syntax!");
		}
	}

}
