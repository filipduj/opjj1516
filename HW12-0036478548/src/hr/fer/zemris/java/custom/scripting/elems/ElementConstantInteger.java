package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Element representation of integer.
 * 
 * @author Filip Dujmušić
 *
 */
public class ElementConstantInteger extends Element {

	/**
	 * Integer value of element
	 */
	private int value;

	/**
	 * Element constructor that accepts int and initializes element.
	 * 
	 * @param value
	 *            integer value to set
	 */
	public ElementConstantInteger(int value) {
		this.value = value;
	}

	/**
	 * Value getter
	 * 
	 * @return returns integer assigned to this token
	 */
	public int getValue() {
		return value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText() {
		return Integer.toString(value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return asText();
	}
}
