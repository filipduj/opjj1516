package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementVariable;

/**
 * Node representing a single for-loop construct. It inherits from Node class.
 * 
 * @author filip
 *
 */
public class ForLoopNode extends Node {

	/**
	 * Variable counter in for loop.
	 */
	private ElementVariable variable;

	/**
	 * Initial value of counter.
	 */
	private Element startExpression;

	/**
	 * Final value to count to.
	 */
	private Element endExpression;

	/**
	 * Step increment. It is not necessary so null is allowed.
	 */
	private Element stepExpression; // can be null

	/**
	 * Constructs new for loop node.
	 * 
	 * @param forTag
	 *            For construct to parse.
	 */

	public ForLoopNode(ElementVariable variable, Element startExpression, Element endExpression,
			Element stepExpression) {
		this.variable = variable;
		this.startExpression = startExpression;
		this.endExpression = endExpression;
		this.stepExpression = stepExpression;
	}

	@Override
	public void accept(INodeVisitor visitor) {
		visitor.visitForLoopNode(this);
	}
	
	/**
	 * Variable getter
	 * 
	 * @return returns variable token
	 */
	public ElementVariable getVariable() {
		return variable;
	}

	/**
	 * Start expression getter
	 * 
	 * @return returns start token
	 */
	public Element getStartExpression() {
		return startExpression;
	}

	/**
	 * End expression getter
	 * 
	 * @return returns end token
	 */
	public Element getEndExpression() {
		return endExpression;
	}

	/**
	 * Step expression getter
	 * 
	 * @return returns step token or <code>null</code> if no step given
	 */
	public Element getStepExpression() {
		return stepExpression;
	}

	/**
	 * Returns String representing for loop node in following format: {$ FOR
	 * [variable] [start] [end] [step] $}
	 * 
	 * step is optional and if it's not provided it will be left out
	 */
	@Override
	public String toString() {
		return "{$ FOR " + " " + variable + " " + startExpression + " " + endExpression + " "
				+ (stepExpression == null ? "" : stepExpression) + " $}";
	}

}
