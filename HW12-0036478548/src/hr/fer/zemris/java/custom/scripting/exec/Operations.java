package hr.fer.zemris.java.custom.scripting.exec;

import java.util.Deque;
import java.util.function.BiConsumer;

import hr.fer.zemris.java.custom.scripting.elems.Element;
import hr.fer.zemris.java.custom.scripting.elems.ElementOperator;

/**
 * Class {@code Operations} provides implementations of basic four mathematical
 * binary operationssupported by {@link SmartScriptEngine}.
 * <p>
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 31, 2016)
 */
public class Operations {

	/**
	 * Method parses given {@code operation} and calls appropriate execution of
	 * this operation on two topmost elements on {@code stack}.
	 * 
	 * @param operation
	 *            Operation to perform
	 * @param stack
	 *            Temporary values stack
	 */
	public static void calculateOperation(Element operation, Deque<Object> stack) {
		ElementOperator operator = (ElementOperator) operation;
		String op = operator.asText();
		if (op.equals("+")) {
			performOperation((v1, v2) -> v1.increment(v2.getValue()), stack);
		} else if (op.equals("-")) {
			performOperation((v1, v2) -> v1.decrement(v2.getValue()), stack);
		} else if (op.equals("*")) {
			performOperation((v1, v2) -> v1.multiply(v2.getValue()), stack);
		} else if (op.equals("/")) {
			performOperation((v1, v2) -> v1.divide(v2.getValue()), stack);
		} else {
			throw new RuntimeException("EchoNode error: Unknown mathematical operator!");
		}
	}

	/**
	 * Takes two topmost values from {@code stack} and performs
	 * {@code operation}. Result is pushed back to {@code stack}.
	 * 
	 * @param operation
	 *            Operation to perform
	 * @param stack
	 *            Temporary values stack
	 */
	public static void performOperation(BiConsumer<ValueWrapper, ValueWrapper> operation, Deque<Object> stack) {
		ValueWrapper second = ((ValueWrapper) stack.pop());
		ValueWrapper first = ((ValueWrapper) stack.peek());
		operation.accept(first, second);
	}

}
