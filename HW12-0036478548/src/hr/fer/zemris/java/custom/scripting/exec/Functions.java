package hr.fer.zemris.java.custom.scripting.exec;

import java.text.DecimalFormat;
import java.util.Deque;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Class {@code Functions} provides implementations of functions supported by
 * {@link SmartScriptEngine}.
 * <p>
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 31, 2016)
 */
public class Functions {

	/**
	 * Method parses given {@code functionName} and calls appropriate function
	 * implementation. Some functions are performed with {@code stack} and some
	 * require {@code requestContext}.
	 * 
	 * @param functionName
	 *            Name of function to execute
	 * @param stack
	 *            Temporary stack used for temporary operation results
	 * @param requestContext
	 *            Context of received request
	 */
	public static void calculateFunction(String functionName, Deque<Object> stack, RequestContext requestContext) {
		if (functionName.equals("sin")) {
			((ValueWrapper) stack.peek()).sine();
		} else if (functionName.equals("decfmt")) {
			decimalFormat(stack);
		} else if (functionName.equals("dup")) {
			stack.push((((ValueWrapper) stack.peek())).clone());
		} else if (functionName.equals("swap")) {
			swapValues(stack);
		} else if (functionName.equals("setMimeType")) {
			requestContext.setMimeType((String) ((ValueWrapper) stack.pop()).getValue());
		} else if (functionName.equals("paramGet")) {
			parameterGet(stack, s -> requestContext.getParameter(s));
		} else if (functionName.equals("pparamGet")) {
			parameterGet(stack, s -> requestContext.getPersistentParameter(s));
		} else if (functionName.equals("pparamSet")) {
			parameterSet(stack, (k, v) -> requestContext.setPersistentParameter(k, v));
		} else if (functionName.equals("pparamDel")) {
			parameterDel(stack, s -> requestContext.removePersistentParameter(s));
		} else if (functionName.equals("tparamGet")) {
			parameterGet(stack, s -> requestContext.getTemporaryParameter(s));
		} else if (functionName.equals("tparamSet")) {
			parameterSet(stack, (k, v) -> requestContext.setTemporaryParameter(k, v));
		} else if (functionName.equals("tparamDel")) {
			parameterDel(stack, s -> requestContext.removeTemporaryParameter(s));
		}
	}

	/**
	 * Replaces top two values on {@code stack} with their sum.
	 * 
	 * @param stack
	 *            Temporary stack
	 */
	private static void decimalFormat(Deque<Object> stack) {
		String format = (String) ((ValueWrapper) stack.pop()).getValue();
		DecimalFormat df = new DecimalFormat(format);
		ValueWrapper value = ((ValueWrapper) stack.pop());
		// value.multiply("1.0");
		Double realValue = ((Double) value.getValue());
		stack.push(new ValueWrapper(df.format(realValue)));
	}

	/**
	 * Swaps two values on top of {@code stack}.
	 * 
	 * @param stack
	 *            Temporary stack
	 */
	private static void swapValues(Deque<Object> stack) {
		Object obj1 = stack.pop();
		Object obj2 = stack.pop();
		stack.push(obj1);
		stack.push(obj2);
	}

	/**
	 * Pops two topmost elements from {@code stack} and interprets them as value
	 * and name elements. If there is already another value mapped to name in
	 * given {@code mapValueGetter} this value is pushed to stack. Otherwise
	 * original value (previously popped from stack) is then pushed to top.
	 * 
	 * 
	 * @param stack
	 *            Temporary stack
	 * @param mapValueGetter
	 *            Map value getter
	 */
	private static void parameterGet(Deque<Object> stack, Function<String, String> mapValueGetter) {
		String value = ((ValueWrapper) stack.pop()).toString();
		String name = ((ValueWrapper) stack.pop()).toString();
		String valueInMap = mapValueGetter.apply(name);
		if (valueInMap == null) {
			stack.push(new ValueWrapper(value));
		} else {
			stack.push(new ValueWrapper(valueInMap));
		}
	}

	/**
	 * Pops two topmost elements from {@code stack} and interprets them as value
	 * and name elements. Sets name,value pair in map defined with
	 * {@code mapValueSetter}.
	 * 
	 * @param stack
	 *            Temporary stack
	 * @param mapValueSetter
	 *            Map value setter
	 */
	private static void parameterSet(Deque<Object> stack, BiConsumer<String, String> mapValueSetter) {
		String name = ((ValueWrapper) stack.pop()).toString();
		String value = ((ValueWrapper) stack.pop()).toString();
		mapValueSetter.accept(name, value);
	}

	/**
	 * Deletes from map {@code mapValueDeleter} entry with key being equal to
	 * popped value from {@code stack}.
	 * 
	 * @param stack
	 *            Temporary stack
	 * @param mapValueDeleter
	 *            Map value deleter
	 */
	private static void parameterDel(Deque<Object> stack, Consumer<String> mapValueDeleter) {
		mapValueDeleter.accept(((ValueWrapper) stack.pop()).toString());
	}

}
