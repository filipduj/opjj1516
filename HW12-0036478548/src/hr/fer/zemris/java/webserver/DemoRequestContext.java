package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;

import hr.fer.zemris.java.webserver.RequestContext.RCCookie;
import hr.fer.zemris.java.webserver.util.Utilities;

/**
 * Simple test program that demonstrates usage and expectation of
 * {@link RequestContext} implementation.
 * <p>
 * Program tests streaming some text to three different files on disk:
 * examples/primjer1.txt examples/primjer2.txt examples/primjer3.txt
 * <p>
 * text is streamed to primjer1 using ISO-8859-2 code page while primjer2 and
 * primjer3 used UTF-8 code page.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 31, 2016)
 */
public class DemoRequestContext {

	/**
	 * Program entry point.
	 */
	public static void main(String[] args) {
		try {
			demo1("./examples/primjer1.txt", "ISO-8859-2");
			demo1("./examples/primjer2.txt", "UTF-8");
			demo2("./examples/primjer3.txt", "UTF-8");
		} catch (IOException e) {
			Utilities.errorExit(e.getMessage());
		}
	}

	/**
	 * Streams some text to location defined by {@code filePath} using code page
	 * {@code encoding}.
	 * 
	 * @param filePath
	 *            Path to destination file
	 * @param encoding
	 *            Code page
	 * @throws IOException
	 *             If I/O error occures while writing to disk
	 */
	private static void demo1(String filePath, String encoding) throws IOException {
		OutputStream os = Files.newOutputStream(Paths.get(filePath));
		RequestContext rc = new RequestContext(os, new HashMap<String, String>(), new HashMap<String, String>(),
				new ArrayList<RequestContext.RCCookie>());
		rc.setEncoding(encoding);
		rc.setMimeType("text/plain");
		rc.setStatusCode(205);
		rc.setStatusText("Idemo dalje");
		// Only at this point will header be created and written...
		rc.write("Čevapčići i Šiščevapčići.");
		os.close();
	}

	/**
	 * Streams some text along with cookies to location defined by
	 * {@code filePath} using code page {@code encoding}.
	 * 
	 * @param filePath
	 *            Path to destination file
	 * @param encoding
	 *            Code page
	 * @throws IOException
	 *             If I/O error occures while writing to disk
	 */
	private static void demo2(String filePath, String encoding) throws IOException {
		OutputStream os = Files.newOutputStream(Paths.get(filePath));
		RequestContext rc = new RequestContext(os, new HashMap<String, String>(), new HashMap<String, String>(),
				new ArrayList<RequestContext.RCCookie>());
		rc.setEncoding(encoding);
		rc.setMimeType("text/plain");
		rc.setStatusCode(205);
		rc.setStatusText("Idemo dalje");

		rc.addRCCookie(new RCCookie("korisnik", "perica", 3600, "127.0.0.1", "/"));
		rc.addRCCookie(new RCCookie("zgrada", "B4", null, null, "/"));
		// Only at this point will header be created and written...
		rc.write("Čevapčići i Šiščevapčići.");
		os.close();
	}

}
