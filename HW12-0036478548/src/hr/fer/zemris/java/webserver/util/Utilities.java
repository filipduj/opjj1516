package hr.fer.zemris.java.webserver.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Utilities class provides utility methods used throughout complete project
 * structure.
 * <p>
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 1, 2016)
 */
public class Utilities {

	/**
	 * Tries to parse {@code input} to {@link Path}. Returns instance of Path
	 * only if file exists, is regular and readable. In every other case program
	 * exits with error status.
	 * 
	 * @param input
	 *            String representing some file's path
	 * @return Returns Path or exits with error status
	 */
	public static Path getPath(String input) {

		Path path = Paths.get(input);

		if (!Files.exists(path)) {
			errorExit("File " + input + " does not exist or invalid path.");
		}

		if (!Files.isRegularFile(path)) {
			errorExit("File " + input + " is not regular file.");
		}

		if (!Files.isReadable(path)) {
			errorExit("Access to " + input + " denied!");
		}

		return path;
	}

	/**
	 * Error exit method is used for forced program termination when program
	 * cannot recover from error which caused this method call.
	 * 
	 * @param message
	 *            Message to be displayed before exiting
	 */
	public static void errorExit(String message) {
		System.out.println(message);
		System.exit(-1);
	}

}
