package hr.fer.zemris.java.webserver;

/**
 * Interface toward any object that can process server requests described with
 * {@link RequestContext}.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 1, 2016)
 */
public interface IWebWorker {

	/**
	 * Takes given {@code context} as parameter and it is expected to create a
	 * content for client.
	 * 
	 * @param context
	 *            Server request context
	 */
	public void processRequest(RequestContext context);

}
