package hr.fer.zemris.java.webserver;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import hr.fer.zemris.java.custom.scripting.exec.SmartScriptEngine;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.webserver.RequestContext.RCCookie;
import hr.fer.zemris.java.webserver.util.Utilities;

/**
 * Simple Java Server implementation.
 * <p>
 * Server can provide basic HTTP server functionality with all of it's
 * configuration located in file whose path can be provided through single
 * command line argument.
 * <p>
 * Server opens up for listening immediately after running this class, with
 * parameters set according to given configuration file. If no configuration
 * file was given default server.config is used.
 * <p>
 * All data accessible to outside world is settled in webroot folder.
 * <p>
 * Other than serving usual html, plaintext or jpg files, server has ability to
 * serve and interpret SmartScript scripts which are usually located in
 * webroot/scripts folder.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 1, 2016)
 */
public class SmartHttpServer {

	/**
	 * Package structure path in which external workers will be looked for when
	 * client tries to access /ext/[worker_name]
	 */
	private static final String EXT_SCRIPTS_PACKAGE = "hr.fer.zemris.java.webserver.workers";

	/**
	 * Path to default server configuration file
	 */
	private static final String DEFAULT_CONFIG_PATH = "./config/server.properties";

	/**
	 * Server IP address
	 */
	private String address;

	/**
	 * Port server is bind to
	 */
	private int port;

	/**
	 * Number of worker threads available for serving requests
	 */
	private int workerThreads;

	/**
	 * Maximum session lifespan in seconds
	 */
	private int sessionTimeout;

	/**
	 * Set of supported mime types
	 */
	private Map<String, String> mimeTypes = new HashMap<String, String>();

	/**
	 * Main server thread
	 */
	private ServerThread serverThread;

	/**
	 * Executor service used for parallelizing workers serving requests
	 */
	private ExecutorService threadPool;

	/**
	 * Path to topmost folder accessible to outsideworld
	 */
	private Path documentRoot;

	/**
	 * Map for dinamically loaded web workers
	 */
	private Map<String, IWebWorker> workersMap;

	/**
	 * Map of currently stored sessions
	 */
	private Map<String, SessionMapEntry> sessions = new HashMap<String, SmartHttpServer.SessionMapEntry>();

	/**
	 * Random generator used for constructing random session IDs
	 */
	private Random sessionRandom = new Random();

	/**
	 * Constructor initializes this server with parameters accessible from given
	 * path to server configuration file.
	 * 
	 * @param configFileName
	 *            Path to server configuration file
	 * @throws IOException
	 *             If I/O error occures while reading server configuration
	 */
	public SmartHttpServer(String configFileName) throws IOException {

		Properties serverProperties = new Properties();
		Path serverConfiguration = Utilities.getPath(configFileName);
		serverProperties.load(Files.newInputStream(serverConfiguration));

		address = serverProperties.getProperty("server.address");
		port = Integer.parseInt((serverProperties.getProperty("server.port")));
		workerThreads = Integer.parseInt((serverProperties.getProperty("server.workerThreads")));
		documentRoot = Paths.get(serverProperties.getProperty("server.documentRoot"));
		sessionTimeout = Integer.parseInt((serverProperties.getProperty("session.timeout")));
		serverThread = new ServerThread();

		Properties mimeProperties = new Properties();
		mimeProperties.load(Files.newInputStream(Paths.get(serverProperties.getProperty("server.mimeConfig"))));
		mimeProperties.forEach((k, v) -> mimeTypes.put((String) k, (String) v));

		Properties workerProperties = new Properties();
		workerProperties.load(Files.newInputStream(Paths.get(serverProperties.getProperty("server.workers"))));
		loadWorkers(workerProperties);
	}

	/**
	 * Reads workers configuration file and dinamically instantiates every
	 * worker class path found in that configuration.
	 * 
	 * @param workerProperties
	 *            Properties file conaining workers (path,full qualified class
	 *            name) data
	 * 
	 */
	private void loadWorkers(Properties workerProperties) {
		workersMap = new HashMap<>();
		workerProperties.forEach((path, fqcn) -> {
			try {
				Class<?> classReference = this.getClass().getClassLoader().loadClass((String) fqcn);
				IWebWorker worker = (IWebWorker) classReference.newInstance();
				workersMap.put((String) path, worker);
			} catch (Exception e) {
				throw new RuntimeException(e.getMessage());
			}
		});

	}

	/**
	 * Intializes worker threads pool and starts main server thread. It also
	 * starts daemon thraeds: session cleaner and admin interface.
	 */
	protected synchronized void start() {

		threadPool = Executors.newFixedThreadPool(workerThreads);
		serverThread.start();
		System.out.println("Server started...");

		// start expired sessions cleaner
		new SessionCleanerThread().start();
	}

	/**
	 * Shuts down worker thread pool and stops main server thread
	 */
	protected synchronized void stop() {
		threadPool.shutdownNow();
	}

	/**
	 * Main server thread implementation.
	 * 
	 * @author Filip Dujmušić
	 * @version 1.0 (Jun 1, 2016)
	 */
	protected class ServerThread extends Thread {
		@Override
		public void run() {
			try (ServerSocket serverSocket = new ServerSocket()) {
				serverSocket.bind(new InetSocketAddress(InetAddress.getByName(address), port));

				while (true) {
						Socket toClient = serverSocket.accept();
						ClientWorker cw = new ClientWorker(toClient);
						threadPool.submit(cw);
				}
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
	}


	/**
	 * Thread has one job, to periodically wake up and delete all expired
	 * sessions.
	 * 
	 * @author Filip Dujmušić
	 * @version 1.0 (Jun 2, 2016)
	 */
	private class SessionCleanerThread extends Thread {

		/**
		 * Constructor initializes new instance of this thread and sets it to
		 * daemon.
		 */
		public SessionCleanerThread() {
			setDaemon(true);
		}

		@Override
		public void run() {
			while (true) {
				try {
					sleep(1000 * 60 * 5); // sleep 5 minutes and then remove
											// garbage sessions
				} catch (InterruptedException ignorable) {
				}
				removeInvalidSessions();
			}
		}

		/**
		 * Iterates through all currently stored sessions and deletes those
		 * which are expired.
		 */
		private void removeInvalidSessions() {
			Iterator<Map.Entry<String, SessionMapEntry>> it = sessions.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, SessionMapEntry> entry = it.next();
				if (entry.getValue().validUntil < new Date().getTime()) {
					it.remove();
				}
			}
		}
	}

	/**
	 * Client worker thread implementation.
	 * <p>
	 * Purpose of each instance of this class is to serve requests assigned from
	 * main server thread.
	 * 
	 * @author Filip Dujmušić
	 * @version 1.0 (Jun 1, 2016)
	 */
	private class ClientWorker implements Runnable {

		/**
		 * Client socket
		 */
		private Socket csocket;

		/**
		 * Input stream for receiveng data from client
		 */
		private PushbackInputStream istream;

		/**
		 * Output stream for sending data to client
		 */
		private OutputStream ostream;

		/**
		 * HTTP protocol version
		 */
		@SuppressWarnings("unused")
		private String version;

		/**
		 * Method type in request header
		 */
		@SuppressWarnings("unused")
		private String method;

		/**
		 * Map of parameters received from client request
		 */
		private Map<String, String> params = new HashMap<String, String>();

		/**
		 * Map of session bound parameters
		 */
		private Map<String, String> permPrams = null;

		/**
		 * List of output cookies to store on client side
		 */
		private List<RCCookie> outputCookies = new ArrayList<RequestContext.RCCookie>();

		/**
		 * Random generated session ID
		 */
		private String SID;

		/**
		 * Constructor bounds this working thread to given Socket.
		 * 
		 * @param csocket
		 *            Connection socket
		 */
		public ClientWorker(Socket csocket) {
			super();
			this.csocket = csocket;
		}

		@Override
		public void run() {
			try {
				istream = new PushbackInputStream(csocket.getInputStream());
				ostream = csocket.getOutputStream();

				List<String> request = extractHeaders(new String(readRequest()));
				if (request.size() < 1) {
					sendError(ostream, 400, "Invalid header");
					return;
				}

				String[] firstLine = request.isEmpty() ? null : request.get(0).split(" ");
				if (firstLine == null || firstLine.length != 3) {
					sendError(ostream, 400, "Bad request");
					return;
				}

				String method = firstLine[0].toUpperCase();
				if (!method.equals("GET")) {
					sendError(ostream, 405, "Method Not Allowed");
					return;
				}

				String version = firstLine[2].toUpperCase();
				if (!version.equals("HTTP/1.1")) {
					sendError(ostream, 505, "HTTP Version Not Supported");
					return;
				}
				String[] firstLineParameters = firstLine[1].split("\\?");

				Path path = documentRoot.resolve(firstLineParameters[0].substring(1));
				String paramString = (firstLineParameters.length == 2) ? firstLineParameters[1] : "";

				if (!path.startsWith(documentRoot)) {
					sendError(ostream, 403, "Forbidden");
					return;
				}

				// cookie checking
				checkSession(request);

				parseParameters(paramString);
				RequestContext rc = new RequestContext(ostream, params, permPrams, outputCookies);

				// try to parse path as /ext/ script
				if (tryExternalScript(firstLineParameters[0].substring(1), rc)) {
					return;
				}

				// try to parse path as configured worker
				IWebWorker iww = workersMap.get(firstLineParameters[0]);
				if (iww != null) {
					iww.processRequest(rc);
					return;
				}

				// if reqested directory, try to locate index.html befor
				// throwing exception
				if (path.toFile().isDirectory()) {
					path = path.resolve("index.html");
				}

				String fileName = path.toFile().getName();

				if (fileName.isEmpty() || !Files.exists(path) || !Files.isReadable(path)) {
					sendError(ostream, 404, "Bad request");
					return;
				}

				String extension = "";
				int dotPosition = fileName.lastIndexOf('.');
				if (dotPosition != -1) {
					extension = fileName.substring(dotPosition + 1, fileName.length());
				}

				// set status code and mime type according to extension
				String mimeType = mimeTypes.get(extension);
				if (mimeType == null) {
					mimeType = "application/octet-stream";
				}
				rc.setStatusCode(200);
				rc.setMimeType(mimeType);

				// execute smscr script if path is script file
				String fileContent = new String(Files.readAllBytes(path));
				if (extension.equals("smscr")) {
					SmartScriptParser parser = new SmartScriptParser(fileContent);
					new SmartScriptEngine(parser.getDocumentNode(), rc).execute();
				} else {
					rc.setFileLength(path.toFile().length());
					rc.write(Files.readAllBytes(path));
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					csocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

		/**
		 * Checks if currently processed request is already bounded to existing
		 * session or if not then it creates new session for it.
		 * 
		 * @param request
		 *            List of header request lines
		 */
		private synchronized void checkSession(List<String> request) {
			Map<String, String> cookie = getSessionCookie(request);

			if (cookie == null) {
				generateNewSession();
			} else {
				SID = cookie.get("sid");
				SessionMapEntry existingEntry = sessions.get(SID);
				long now = new Date().getTime();
				if (existingEntry != null && now < existingEntry.validUntil) {
					existingEntry.validUntil = now + sessionTimeout * 1000;
				} else {
					sessions.entrySet().remove(existingEntry);
					generateNewSession();
				}
			}

			permPrams = sessions.get(SID).map;
		}

		/**
		 * Searches for cookie line in header request and if found, returns map
		 * of attribute,value pairs read from cookie.
		 * 
		 * @param request
		 *            List of header request lines
		 * @return returns map of attribute,value pairs read from sid cookie
		 *         line in header or {@code null} if no cookie provided
		 */
		private Map<String, String> getSessionCookie(List<String> request) {
			for (String line : request) {
				if (line.startsWith("Cookie:")) {
					Map<String, String> cookie = parseCookie(line.substring(7).trim());
					if (cookie.get("sid") != null) return cookie;
				}
			}
			return null;
		}

		/**
		 * Generates new session and bounds it to randomly generated session ID
		 * (20 uniformly distributed uppercase ASCII letters)
		 */
		private void generateNewSession() {

			char[] randomUppercaseArray = new char[20];
			for (int i = 0; i < randomUppercaseArray.length; ++i) {
				randomUppercaseArray[i] = (char) ('A' + sessionRandom.nextInt('Z' - 'A' + 1));
			}
			SID = new String(randomUppercaseArray);

			SessionMapEntry entry = new SessionMapEntry(SID,
					Calendar.getInstance().getTimeInMillis() + sessionTimeout * 1000);
			sessions.put(SID, entry);
			outputCookies.add(new RCCookie("sid", SID, null, address, "/"));
		}

		/**
		 * Takes input cookie line received from request header and returns map
		 * of attribute,value pairs found in that line.
		 * 
		 * @param line
		 *            One line from request header
		 * @return map of attribute,value pairs found in header {@code line}
		 */
		private Map<String, String> parseCookie(String line) {
			Map<String, String> parameters = new HashMap<>();
			if (!line.trim().isEmpty()) {
				String[] elements = line.trim().split(";");
				for (String entry : elements) {
					String[] entryTokens = entry.split("=");
					parameters.put(entryTokens[0], entryTokens[1].substring(1, entryTokens[1].length() - 1));
				}
			}
			return parameters;
		}

		/**
		 * Method checks if requested path is in form: /ext/[worker_name].
		 * <p>
		 * If that's the case, it will try to dynamically load and instantiate
		 * class by name [worker_name] from package
		 * hr.fer.zemris.java.webserver.workers
		 * <p>
		 * Writes error header to client stream if no such class exists in above
		 * package, or successfully instantiates worker who will process given
		 * {@code rc} context.
		 * 
		 * @param path
		 *            Requested path from HTTP request header
		 * @param rc
		 *            Request context
		 * @return returns {@code true} if path could be parsed in defined form
		 *         (regardless of result of class loading) or {@code false} if
		 *         path is not in given form
		 * @throws RuntimeException
		 *             If instantiation error occured
		 */
		private boolean tryExternalScript(String path, RequestContext rc) {
			String[] workerCall = path.split("/");
			if (workerCall.length == 2 && workerCall[0].equals("ext")) {
				Class<?> classReference = null;
				try {
					classReference = this.getClass().getClassLoader()
							.loadClass(EXT_SCRIPTS_PACKAGE + "." + workerCall[1]);
					IWebWorker worker = (IWebWorker) classReference.newInstance();
					worker.processRequest(rc);
				} catch (ClassNotFoundException e) {
					try {
						sendError(ostream, 404, path + " resource not found.");
					} catch (IOException ignorable) {
					}
					return true;
				} catch (InstantiationException | IllegalAccessException e) {
					throw new RuntimeException(e.getMessage());
				}
				return true;
			}
			return false;
		}

		/**
		 * Reads header data until any possible newline separator was found.
		 * 
		 * @return returns header data as byte array
		 */
		private byte[] readRequest() {
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			int state = 0;
			l: while (true) {
				int b = 0;
				try {
					b = istream.read();
				} catch (IOException e) {
					e.printStackTrace();
				}
				if (b == -1)
					return null;
				if (b != 13) {
					bos.write(b);
				}
				switch (state) {
				case 0:
					if (b == 13) {
						state = 1;
					} else if (b == 10)
						state = 4;
					break;
				case 1:
					if (b == 10) {
						state = 2;
					} else
						state = 0;
					break;
				case 2:
					if (b == 13) {
						state = 3;
					} else
						state = 0;
					break;
				case 3:
					if (b == 10) {
						break l;
					} else
						state = 0;
					break;
				case 4:
					if (b == 10) {
						break l;
					} else
						state = 0;
					break;
				}
			}
			return bos.toByteArray();
		}

		/**
		 * Extracts header lines from given {@code requestHeader}. Important
		 * note: Header line can actually span through more textual lines but
		 * each must end with \r\n.
		 * 
		 * @param requestHeader
		 *            Complete request header
		 * @return Returns list of header lines
		 */
		private List<String> extractHeaders(String requestHeader) {
			List<String> headers = new ArrayList<>();
			String currentLine = null;

			for (String s : requestHeader.split("\n")) {
				if (s.isEmpty())
					break;
				char c = s.charAt(0);
				if (c == 9 || c == 32) {
					currentLine += s;
				} else {
					if (currentLine != null) {
						headers.add(currentLine);
					}
					currentLine = s;
				}
			}

			if (!currentLine.isEmpty()) {
				headers.add(currentLine);
			}

			return headers;
		}

		/**
		 * Sends error response to client through output stream. Error message
		 * constructed using given {@code statusCode} and {@code statusText}.
		 * 
		 * @param cos
		 *            Output stream towards client
		 * @param statusCode
		 *            Error response status code
		 * @param statusText
		 *            Error response status message
		 * @throws IOException
		 *             If I/O error occures while writing data to output stream
		 */
		private void sendError(OutputStream cos, int statusCode, String statusText) throws IOException {
			cos.write(("HTTP/1.1 " + statusCode + " " + statusText + "\r\n" + "Server: simple java server\r\n"
					+ "Content-Type: text/plain;charset=UTF-8\r\n" + "Content-Length: 0\r\n" + "Connection: close\r\n"
					+ "\r\n").getBytes(StandardCharsets.US_ASCII));
			cos.flush();
		}

		/**
		 * Parses parameters from parameter string. Every parameter has name and
		 * value.
		 * 
		 * @param paramString
		 *            Parameter string
		 */
		private void parseParameters(String paramString) {

			if (paramString.trim().isEmpty()) {
				return;
			}

			String[] parameters = paramString.split("&");
			for (String p : parameters) {
				String[] tokens = p.split("=");
				if (tokens.length != 2) {
					throw new RuntimeException("Wrong parameter format");
				}
				params.put(tokens[0], tokens[1]);
			}

		}
	}

	/**
	 * Class defines one session.
	 * <p>
	 * Each session has its own randomly generated session ID, map of persistent
	 * parameters (which should be thread safe) and time point after which
	 * session expires.
	 * 
	 * @author Filip Dujmušić
	 * @version 1.0 (Jun 2, 2016)
	 */
	private static class SessionMapEntry {
		/**
		 * Session id
		 */
		@SuppressWarnings("unused")
		private String sid;

		/**
		 * This point in time is the number of milliseconds since January 1,
		 * 1970, 00:00:00 GMT after which session expires
		 */
		private long validUntil;

		/**
		 * Map of persistent parameters
		 */
		private Map<String, String> map;

		/**
		 * Constructor initializes new session with given id and expiration time
		 * 
		 * @param sid
		 *            Session ID
		 * @param validUntil
		 *            Expiration time
		 */
		public SessionMapEntry(String sid, long validUntil) {
			this.sid = sid;
			this.validUntil = validUntil;
			this.map = new ConcurrentHashMap<>();
		}

	}

	/**
	 * Program entry point.
	 * 
	 * @param args
	 *            Path to server configuration file
	 * 
	 */
	public static void main(String[] args) {

		String config = null;

		if (args.length == 0) {
			config = DEFAULT_CONFIG_PATH;
		} else if (args.length == 1) {
			config = args[0];
		} else {
			Utilities.errorExit("Expected one command line argument: path to server config");
		}

		// try to start server
		SmartHttpServer server = null;
		try {
			server = new SmartHttpServer(config);
		} catch (IOException e) {
			Utilities.errorExit(e.getMessage());
		}
		server.start();
	}
}
