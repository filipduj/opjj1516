package hr.fer.zemris.java.webserver.workers;

import java.io.IOException;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * This worker implementation takes provided parameters from http request and
 * displays them in tabular form.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 1, 2016)
 */
public class EchoParams implements IWebWorker {

	@Override
	public void processRequest(RequestContext context) {

		context.setMimeType("text/html");

		try {
			context.write("<html><body>");
			context.write("<h1>Parameters</h1>");
			context.write("<table border='1'>");
			context.write("<tr><th>Key</th><th>Value</th></tr>");

			for (String name : context.getParameterNames()) {
				context.write(
						"<tr>" + "<td>" + name + "</td>" + "<td>" + context.getParameter(name) + "</td>" + "</tr>");
			}
			context.write("</table></body></html>");
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}

	}
}
