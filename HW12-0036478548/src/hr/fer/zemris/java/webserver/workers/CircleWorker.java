package hr.fer.zemris.java.webserver.workers;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import hr.fer.zemris.java.webserver.IWebWorker;
import hr.fer.zemris.java.webserver.RequestContext;

/**
 * Worker implemenetation simply produces filled circle image and sends it to
 * client stream.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 1, 2016)
 */
public class CircleWorker implements IWebWorker {

	/**
	 * Image width
	 */
	private static final int WIDTH = 200;
	
	/**
	 * Image height
	 */
	private static final int HEIGHT = 200;

	@Override
	public void processRequest(RequestContext context) {

		BufferedImage bim = new BufferedImage(200, 200, BufferedImage.TYPE_3BYTE_BGR);

		Graphics2D g2d = bim.createGraphics();

		// do drawing...

		// background
		g2d.setBackground(Color.BLUE);
		g2d.clearRect(0, 0, WIDTH, HEIGHT);

		// foreground
		g2d.setColor(Color.RED);
		g2d.fillOval(0, 0, WIDTH, HEIGHT);

		g2d.dispose();

		ByteArrayOutputStream bos = new ByteArrayOutputStream();

		try {
			ImageIO.write(bim, "png", bos);
			context.setFileLength(bos.size());
			context.setMimeType("image/png");
			context.write(bos.toByteArray());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
