package hr.fer.zemris.java.webserver;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * Class defines context of one request which SimpleJavaServer received.
 * <p>
 * It holds all neccessary data used for communicating with source of request
 * through HTTP protocol.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 1, 2016)
 */
public class RequestContext {

	/**
	 * Stream to end user
	 */
	private OutputStream outputStream;

	/**
	 * Code page used for streamed data
	 */
	private Charset charset;

	/**
	 * Flag indicates if header was already written through output stream
	 */
	private boolean headerGenerated = false;

	/**
	 * Default code page
	 */
	private String encoding = "UTF-8";

	/**
	 * Default status code (indicates everything went fine while processing
	 * request)
	 */
	private int statusCode = 200;

	/**
	 * Default status message
	 */
	private String statusText = "OK";

	/**
	 * Default mime type
	 */
	private String mimeType = "text/html";

	/**
	 * Default file length. -1 means this attribute will be ignored, but
	 * sometimes file size can be determined before response, this parameter can
	 * be set to that file size.
	 */
	private long fileLength = -1;

	/**
	 * attribute,value parameters map
	 */
	private Map<String, String> parameters;

	/**
	 * Temporary parameters map
	 */
	private Map<String, String> temporaryParameters;

	/**
	 * Persistent parameters map
	 */
	private Map<String, String> persistentParameters;

	/**
	 * List of output cookies
	 */
	private List<RCCookie> outputCookies;

	/**
	 * Constructor initializes new request context with given parameters.
	 * 
	 * @param outputStream
	 *            Stream through content towards end user will be written
	 * @param parameters
	 *            Parameters map
	 * @param persistentParameters
	 *            Persistent parameters map
	 * @param outputCookies
	 *            List of output cookies
	 * @throws IllegalArgumentException
	 *             If any of these values if {@code null} reference, in other
	 *             words every parameter is required
	 */
	public RequestContext(OutputStream outputStream, Map<String, String> parameters,
			Map<String, String> persistentParameters, List<RCCookie> outputCookies) {
		super();
		if (outputStream == null || parameters == null || persistentParameters == null || outputCookies == null) {
			throw new IllegalArgumentException("All parameters must be set when creating new request context!");
		}
		this.outputStream = outputStream;
		this.parameters = parameters;
		this.persistentParameters = persistentParameters;
		this.outputCookies = outputCookies;
		this.temporaryParameters = new HashMap<>();
	}

	/**
	 * @param encoding
	 *            the encoding to set
	 * @throws NullPointerException
	 *             If {@code outputCookies} is null reference
	 * @throws RuntimeException
	 */
	public void setEncoding(String encoding) {
		Objects.requireNonNull(encoding);
		if (headerGenerated) {
			throw new RuntimeException("Error: cannot change encoding, header already written!");
		}
		this.encoding = encoding;
	}

	/**
	 * @param statusCode
	 *            the statusCode to set
	 * @throws RuntimeException
	 *             If header was already generated before calling this method
	 */
	public void setStatusCode(int statusCode) {
		if (headerGenerated) {
			throw new RuntimeException("Error: cannot change status code, header already written!");
		}
		this.statusCode = statusCode;
	}

	/**
	 * @param statusText
	 *            the statusText to set
	 * @throws NullPointerException
	 *             If {@code statusText} is null reference
	 * @throws RuntimeException
	 *             If header was already generated before calling this method
	 */
	public void setStatusText(String statusText) {
		Objects.requireNonNull(statusText);
		if (headerGenerated) {
			throw new RuntimeException("Error: cannot change status text, header already written!");
		}
		this.statusText = statusText;
	}

	/**
	 * @param mimeType
	 *            the mimeType to set
	 * @throws NullPointerException
	 *             If {@code mimeType} is null reference
	 * @throws RuntimeException
	 *             If header was already generated before calling this method
	 */
	public void setMimeType(String mimeType) {
		Objects.requireNonNull(mimeType);
		if (headerGenerated) {
			throw new RuntimeException("Error: cannot change status mime type, header already written!");
		}
		this.mimeType = mimeType;
	}

	/**
	 * @param parameters
	 *            the parameters to set
	 * @throws NullPointerException
	 *             If {@code parameters} is null reference
	 */
	public void setParameters(Map<String, String> parameters) {
		Objects.requireNonNull(parameters);
		this.parameters = parameters;
	}

	/**
	 * @param temporaryParameters
	 *            the temporaryParameters to set
	 * @throws NullPointerException
	 *             If {@code temporaryParameters} is null reference
	 */
	public void setTemporaryParameters(Map<String, String> temporaryParameters) {
		Objects.requireNonNull(temporaryParameters);
		this.temporaryParameters = temporaryParameters;
	}

	/**
	 * @param persistentParameters
	 *            the persistentParameters to set
	 * @throws NullPointerException
	 *             If {@code persistentParameters} is null reference
	 */
	public void setPersistentParameters(Map<String, String> persistentParameters) {
		Objects.requireNonNull(persistentParameters);
		this.persistentParameters = persistentParameters;
	}

	/**
	 * @param length
	 *            file length to set
	 */
	public void setFileLength(long length) {
		this.fileLength = length;
	}

	/**
	 * @param outputCookies
	 *            the outputCookies to set
	 * @throws NullPointerException
	 *             If {@code outputCookies} is null reference
	 * @throws RuntimeException
	 *             If header was already generated before calling this method
	 */
	public void setOutputCookies(List<RCCookie> outputCookies) {
		Objects.requireNonNull(outputCookies);
		if (headerGenerated) {
			throw new RuntimeException("Error: cannot change cookies, header already written!");
		}
		this.outputCookies = outputCookies;
	}

	/**
	 * @param name
	 *            parameter key
	 * @return returns value of parameter with key equals to {@code name}
	 */
	public String getParameter(String name) {
		return parameters.get(name);
	}

	/**
	 * @return Set of parameter names
	 */
	public Set<String> getParameterNames() {
		return new HashSet<>(parameters.keySet());
	}

	/**
	 * @param name
	 *            parameter key
	 * @return returns value of persistent parameter with key equals to
	 *         {@code name}
	 */
	public String getPersistentParameter(String name) {
		return persistentParameters.get(name);
	}

	/**
	 * @return Set of persistent parameter names
	 */
	public Set<String> getPersistentParameterNames() {
		return new HashSet<>(persistentParameters.keySet());
	}

	/**
	 * Sets value of persistent parameter with key {@code name} to given
	 * {@code value}.
	 * 
	 * @param name
	 *            Persistent parameter key
	 * @param value
	 *            Persistent parameter value
	 */
	public void setPersistentParameter(String name, String value) {
		persistentParameters.put(name, value);
	}

	/**
	 * Removes persistent parameter with key {@code name} from set of persistent
	 * parameters.
	 * 
	 * @param name
	 *            Key of persistent parameter to be removed
	 */
	public void removePersistentParameter(String name) {
		persistentParameters.remove(name);
	}

	/**
	 * @param name
	 *            parameter key
	 * @return returns value of temporary parameter with key equals to
	 *         {@code name}
	 */
	public String getTemporaryParameter(String name) {
		return temporaryParameters.get(name);
	}

	/**
	 * @return Set of temporary parameter names
	 */
	public Set<String> getTemporaryParameterNames() {
		return new HashSet<>(temporaryParameters.keySet());
	}

	/**
	 * Sets value of temporary parameter with key {@code name} to given
	 * {@code value}.
	 * 
	 * @param name
	 *            Temporary parameter key
	 * @param value
	 *            Temporary parameter value
	 */
	public void setTemporaryParameter(String name, String value) {
		temporaryParameters.put(name, value);
	}

	/**
	 * Removes temporary parameter with key {@code name} from set of temporary
	 * parameters.
	 * 
	 * @param name
	 *            Key of temporary parameter to be removed
	 */
	public void removeTemporaryParameter(String name) {
		temporaryParameters.remove(name);
	}

	/**
	 * Writes bytes from given {@code data} array to output stream.
	 * 
	 * @param data
	 *            Array of bytes to write to output stream
	 * @return Returns instance of this request context
	 * @throws IOException
	 *             If I/O error occures while writing data do stream
	 */
	public RequestContext write(byte[] data) throws IOException {
		checkHeader();
		outputStream.write(data);
		return this;
	}

	/**
	 * Writes given {@code text} String to output stream.
	 * 
	 * @param text
	 *            Text to be written to output stream
	 * @return Returns instance of this request context
	 * @throws IOException
	 *             If I/O error occures while writing data do stream
	 */
	public RequestContext write(String text) throws IOException {
		checkHeader();
		outputStream.write(text.getBytes(charset));
		return this;
	}

	/**
	 * Adds {@code rcCookie} to underlying list of output cookies.
	 * 
	 * @param rcCookie
	 *            Cookie to add to list
	 */
	public void addRCCookie(RCCookie rcCookie) {
		outputCookies.add(rcCookie);
	}

	/**
	 * RCCookie class is a model of cookie used for storing small pieces of data
	 * to user's web browser.
	 * 
	 * @author Filip Dujmušić
	 * @version 1.0 (Jun 1, 2016)
	 */
	public static class RCCookie {

		/**
		 * Cookie name. Mandatory cookie identificator
		 */
		private final String name;

		/**
		 * Value of cookie, must be set (just like cookie name)
		 */
		private final String value;

		/**
		 * Domain to which this cookie belongs to
		 */
		private final String domain;

		/**
		 * Requested resource
		 */
		private final String path;

		/**
		 * Expiration time in seconds
		 */
		private final Integer maxAge;

		/**
		 * Constructor initializes instance of {@link RCCookie} with given
		 * parameters.
		 * 
		 * @param name
		 *            Cookie name
		 * @param value
		 *            Value
		 * @param maxAge
		 *            Max age in seconds
		 * @param domain
		 *            Belonging domain
		 * @param path
		 *            Requested path
		 * @throws IllegalArgumentException
		 *             If {@code name} or {@code value} parameters are
		 *             {@code null} reference
		 */
		public RCCookie(String name, String value, Integer maxAge, String domain, String path) {
			if (name == null || value == null) {
				throw new IllegalArgumentException("Name and value parameters cannot be null!");
			}
			this.name = name;
			this.value = value;
			this.domain = domain;
			this.path = path;
			this.maxAge = maxAge;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the value
		 */
		public String getValue() {
			return value;
		}

		/**
		 * @return the domain
		 */
		public String getDomain() {
			return domain;
		}

		/**
		 * @return the path
		 */
		public String getPath() {
			return path;
		}

		/**
		 * @return the maxAge
		 */
		public Integer getMaxAge() {
			return maxAge;
		}

		@Override
		public String toString() {
			return 		name + "=\"" + value + "\"" + ((domain != null) ? ("; Domain=" + domain) : "")
					+ ((path != null) ? ("; Path=" + path) : "") + ((maxAge != null) ? ("; Max-Age=" + maxAge) : "") + ";HttpOnly";
		}

	}

	/**
	 * Method chekcs if header was already written to output stream of this
	 * context and writes one if not so.
	 */
	private void checkHeader() {
		if (!headerGenerated) {
			writeHeader();
			headerGenerated = true;
		}
	}

	/**
	 * Generates and writes header data to output stream of this context.
	 */
	private void writeHeader() {

		charset = Charset.forName(encoding);
		StringBuilder header = new StringBuilder();

		header.append("HTTP/1.1 " + statusCode + " " + statusText).append("\r\n")
				.append("Content-Length: " + fileLength).append("\r\n")
				.append("Content-Type: " + mimeType + ((mimeType.startsWith("text/")) ? ("; charset=" + encoding) : ""))
				.append("\r\n");

		outputCookies.forEach(c -> header.append("Set-Cookie: " + c + "\r\n"));

		header.append("\r\n");
		try {
			outputStream.write(header.toString().getBytes(StandardCharsets.ISO_8859_1));
		} catch (IOException e) {
			throw new RuntimeException("Could not write header to output stream!");
		}

	}

}
