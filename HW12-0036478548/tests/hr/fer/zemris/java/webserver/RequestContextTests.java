package hr.fer.zemris.java.webserver;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class RequestContextTests {

	@Test(expected = IllegalArgumentException.class)
	public void constructWithNullValues(){
		new RequestContext(null, null, null, null);
	}
	
	@Test(expected = RuntimeException.class)
	public void parameterChangeAfterHeaderWriteBytes() throws IOException{
		OutputStream os = new ByteArrayOutputStream();
		RequestContext rContext = new RequestContext(os, new HashMap<>(), new HashMap<>(), new LinkedList<>());
		rContext.write(new byte[]{1,2,3,4,5});
		rContext.setMimeType("test");
	}
	
	@Test(expected = RuntimeException.class)
	public void parameterChangeAfterHeaderWriteString() throws IOException{
		OutputStream os = new ByteArrayOutputStream();
		RequestContext rContext = new RequestContext(os, new HashMap<>(), new HashMap<>(), new LinkedList<>());
		rContext.write("abc");
		rContext.setMimeType("test");
	}
	
	@Test
	public void checkWritingContentWithHeader() throws IOException{
		
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		RequestContext rContext = new RequestContext(os, new HashMap<>(), new HashMap<>(), new LinkedList<>());
		
		rContext.setStatusCode(404);
		rContext.setFileLength(42);
		rContext.setStatusText("status");
		rContext.setMimeType("xyz");
		rContext.write("abc");
		
		String actual   = new String(os.toByteArray());
		String expected = "HTTP/1.1 404 status\r\n" + 
						  "Content-Length: 42\r\n"  + 
						  "Content-Type: xyz\r\n"   +
						  "\r\n"                    +
						  "abc";
				
		assertEquals(expected, actual);
	}
	
	@Test
	public void checkSecondWriting() throws IOException {
		
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		RequestContext rContext = new RequestContext(os, new HashMap<>(), new HashMap<>(), new LinkedList<>());
		rContext.write("abc");
		os.reset();
		rContext.write("abc");	
		assertEquals("abc", new String(os.toByteArray()));
	}
	
	@Test
	public void headerWithTextMimeType () throws IOException{
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		RequestContext rContext = new RequestContext(os, new HashMap<>(), new HashMap<>(), new LinkedList<>());
		
		rContext.setStatusCode(404);
		rContext.setFileLength(42);
		rContext.setStatusText("status");
		rContext.setMimeType("text/plain");
		rContext.write("abc");
		
		String actual   = new String(os.toByteArray());
		String expected = "HTTP/1.1 404 status\r\n" + 
						  "Content-Length: 42\r\n"  + 
						  "Content-Type: text/plain; charset=UTF-8\r\n"   +
						  "\r\n"                    +
						  "abc";
				
		assertEquals(expected, actual);
	}
	
	@Test
	public void writeWithCookie() throws IOException{
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		List<RequestContext.RCCookie> cookies = new LinkedList<>();
		cookies.add(new RequestContext.RCCookie("korisnik", "perica", 3600, "127.0.0.1", "/"));
		RequestContext rContext = new RequestContext(os, new HashMap<>(), new HashMap<>(), cookies);
		
		rContext.setStatusCode(404);
		rContext.setFileLength(42);
		rContext.setStatusText("status");
		rContext.setMimeType("text/plain");
		rContext.write("abc");
		
		String actual   = new String(os.toByteArray());
		String expected = "HTTP/1.1 404 status\r\n" + 
						  "Content-Length: 42\r\n"  + 
						  "Content-Type: text/plain; charset=UTF-8\r\n"   +
						  "Set-Cookie: korisnik=\"perica\"; Domain=127.0.0.1; Path=/; Max-Age=3600;HttpOnly\r\n"+
						  "\r\n"                    +
						  "abc";
				
		assertEquals(expected, actual);
	}
	
}
