package hr.fer.zemris.java.custom.collections;

/**
 * Resizable array backed collection. Collection of Objects which uses array to
 * store data. It offers usual methods and allows duplicates. Null values not
 * allowed. Dynamically doubles it's size when needed.
 * 
 * @author Filip Dujmušić
 *
 */
public class ArrayBackedIndexedCollection {

	/**
	 * Number of elements currently stored in collection. Initially set to zero.
	 */
	private int size;

	/**
	 * Maximal number of elements that can be stored in currently allocated
	 * collection.
	 */
	private int capacity;

	/**
	 * Array of objects, actual data is stored here.
	 */
	private Object[] elements;

	/**
	 * Default constructor, if no arguments passed upon creation it sets default
	 * capacity to 16.
	 */
	public ArrayBackedIndexedCollection() {
		this(16);
	}

	/**
	 * Constructor allows to set initial capacity of collection.
	 * 
	 * @param initialCapacity
	 *            initial capacity of collection, must be positive integer
	 */
	public ArrayBackedIndexedCollection(int initialCapacity) {
		if (initialCapacity < 1) {
			throw new IllegalArgumentException();
		}
		capacity = initialCapacity;
		elements = new Object[capacity];
	}

	/**
	 * Checks if number of elements stored in collection is zero.
	 * 
	 * @return returns true if empty collection, false otherwise
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Returns size of collection
	 * 
	 * @return integer representing number of elements currently stored in
	 *         collection
	 */
	public int size() {
		return size;
	}

	/**
	 * Adds new element to collection. New element is inserted at the first
	 * empty location (which is first position after last element in array). It
	 * is special case of insert method where no elements need to be shifted
	 * therefore complexity of operation is O(1)
	 * 
	 * @param value
	 *            value of type [Object] to be inserted in collection
	 */
	public void add(Object value) {
		insert(value, size);
	}

	/**
	 * Gets element from this collection at given position. If position is
	 * invalid or throws exception. Element is retrieved in constant time.
	 * 
	 * @param index
	 *            position of wanted element, must be integer between [0, size)
	 * @return returns Object at given position
	 */
	public Object get(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
		return elements[index];
	}

	/**
	 * Removes element at given position. If position is invalid exception is
	 * thrown. Average complexity is O(n/2) since some elements have to be
	 * shifted.
	 * 
	 * @param index
	 *            position of element to be removed, integer from [0, size)
	 */
	public void remove(int index) {
		if (index < 0 || index >= size) {
			throw new IndexOutOfBoundsException();
		}
		for (int i = index; i < size - 1; ++i) {
			elements[i] = elements[i + 1];
		}
		size--;
		elements[size] = null;
	}

	/**
	 * Inserts element to given position. Shifts all element starting at
	 * position for 1 to the right and inserts element to position. Throws
	 * exception if element is null or position invalid. If there is no room for
	 * new element capacity is doubled. Complexity of operation is O(n/2) on
	 * average since elements must be shifted in order to create free field.
	 * 
	 * 
	 * @param value
	 *            Element to be added to collection
	 * @param position
	 *            Position where element will be added (integer from set [0,
	 *            size])
	 */
	public void insert(Object value, int position) {
		if (value == null) {
			throw new IllegalArgumentException("Cannot insert null in array!");
		}

		if (position < 0 || position > size) {
			throw new IndexOutOfBoundsException("Index out of bounds!");
		}

		size++;

		if (size > capacity) {
			doubleSize();
		}

		for (int i = size; i > position + 1; --i) {
			elements[i] = elements[i - 1];
		}
		elements[position] = value;
	}

	/**
	 * Returns position of first occurrence of given element. Average complexity
	 * is O(n/2).
	 * 
	 * @param value
	 *            element for which collection will be searched
	 * @return index of first occurrence of element or -1 if element does not
	 *         exist in collection
	 */
	public int indexOf(Object value) {
		for (int i = 0; i < size; ++i) {
			if (value.equals(elements[i])) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Checks if element is in collection. On average takes O(n/2) time.
	 * 
	 * @param value
	 *            element for which collection will be searched
	 * @return true if element exists in this collection, false otherwise
	 */
	public boolean contains(Object value) {
		return indexOf(value) >= 0;
	}

	/**
	 * Clears collection. Removes all elements (if they exist) from collection.
	 * On average takes about O(n/2) time since some elements are shifted 1 to
	 * the left to fill empty field of removed element.
	 */
	public void clear() {
		for (int i = 0; i < size; ++i) {
			elements[i] = null;
		}
		size = 0;
	}

	/**
	 * Doubles the capacity of collection. Creates new array with double the
	 * size and copies all elements from old array.
	 */
	private void doubleSize() {
		Object[] newElements = new Object[2 * capacity];
		for (int i = 0; i < size; ++i) {
			newElements[i] = elements[i];
		}
		capacity *= 2;
		elements = newElements;
	}
}
