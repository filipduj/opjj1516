package hr.fer.zemris.java.custom.collections.demo;

import hr.fer.zemris.java.custom.collections.ObjectStack;

/**
 * Demo class for stack collection. Demonstrates some basic things that can be
 * done with stack collection like calculating POSIX math expressions. Program
 * expects one string expression as command line argument. For example: "4 2 \"
 * returns 2.
 * 
 * @author Filip Dujmušić
 *
 */
public class StackDemo {

	/**
	 * Calculates result of operation on two arguments. Prevents division by
	 * zero.
	 * 
	 * @param second
	 *            second operand (integer)
	 * @param first
	 *            first operand (integer)
	 * @param op
	 *            operation on two arguments (integer)
	 * @return returns result of [first] [operation] [second] expression
	 */
	private static Integer operation(Integer second, Integer first, char op) {
		switch (op) {
		case '+':
			return first + second;
		case '-':
			return first - second;
		case '*':
			return first * second;
		case '/':
			if (second == 0) {
				System.err.println("Divison by zero! Exiting...");
				System.exit(-1);
			}
			return first / second;
		case '%':
			return first % second;
		default:
			throw new IllegalArgumentException("Invalid operation!");
		}
	}

	/**
	 * Entry point for demo class. Everything starts here.
	 * 
	 * @param args
	 *            single command line argument
	 */
	public static void main(String[] args) {
		if (args.length != 1) {
			throw new IllegalArgumentException("Expected one command line argument!");
		}

		String[] tokens = args[0].split(" ");
		ObjectStack stack = new ObjectStack();

		for (String s : tokens) {
			try {
				stack.push(Integer.parseInt(s));
			} catch (NumberFormatException e) {
				stack.push(operation((Integer) stack.pop(), (Integer) stack.pop(), s.trim().charAt(0)));
			}
		}

		if (stack.size() != 1) {
			System.err.println("Error: invalid expression!");
		} else {
			System.out.format("Expression evaluates to %d.%n", stack.pop());
		}
	}

}
