package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayBackedIndexedCollection;
import hr.fer.zemris.java.custom.scripting.parser.Tokenizator;
import hr.fer.zemris.java.custom.scripting.tokens.Token;

/**
 * Node representing a command which generates some textual output dynamically.
 * It inherits from Node class.
 * 
 * @author Filip Dujmušić
 *
 */
public class EchoNode extends Node {

	/**
	 * Array holding token parts of parsed string. Every echo node can have many
	 * tokens.
	 */
	private Token[] tokens;

	/**
	 * Calls super constructor passing string input used to construct new node.
	 * 
	 * @param token
	 *            input token
	 */
	public EchoNode(String token) {
		super(token);
	}

	/**
	 * Gets array of tokens assigned to this node.
	 * 
	 * @return returns token array or <code>null</code> if no tokens assigned
	 */
	public Token[] getTokens() {
		return tokens;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void parse(String tag) {
		ArrayBackedIndexedCollection ret = Tokenizator.tokenize(tag);
		tokens = new Token[ret.size()];
		for (int i = 0, n = ret.size(); i < n; ++i) {
			tokens[i] = (Token) ret.get(i);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmptyTag() {
		return true;
	}

	/**
	 * String representation of echo construct. Constructs string containing
	 * echo construct in following format: {$ = [token1] [token2] ... $}
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("{$ = ");
		for (Token t : tokens) {
			sb.append(t).append(" ");
		}
		sb.append("$}");

		return sb.toString();
	}

}
