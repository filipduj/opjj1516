package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Token representation of integer.
 * 
 * @author Filip Dujmušić
 *
 */
public class TokenConstantInteger extends Token {

	/**
	 * Integer value of token
	 */
	private int value;

	/**
	 * Token constructor
	 * 
	 * @param value
	 *            integer value to set
	 */
	public TokenConstantInteger(int value) {
		this.value = value;
	}

	/**
	 * Value getter
	 * 
	 * @return returns integer assigned to this token
	 */
	public int getValue() {
		return value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return Integer.toString(value);
	}
}
