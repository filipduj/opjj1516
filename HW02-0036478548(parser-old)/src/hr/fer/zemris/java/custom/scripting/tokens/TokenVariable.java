package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Token representation of variable.
 * 
 * @author Filip Dujmušić
 *
 */
public class TokenVariable extends Token {

	/**
	 * Variable name as string
	 */
	private String name;

	/**
	 * Variable token constructor
	 * 
	 * @param name
	 *            name of variable
	 */
	public TokenVariable(String name) {
		this.name = name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return name;
	}

}
