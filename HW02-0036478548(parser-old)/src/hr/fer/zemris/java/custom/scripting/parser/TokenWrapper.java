package hr.fer.zemris.java.custom.scripting.parser;

/**
 * Class wraps string in special structure. It adds new information whether
 * token is TAG or TEXT.
 * 
 * @author Filip Dujmušić
 *
 */
public class TokenWrapper {

	/**
	 * Tag data
	 */
	private String data;

	/**
	 * true if TAG, false if TEXT
	 */
	private boolean isTag;

	/**
	 * Constructor takes string and flag and creates wrapper.
	 * 
	 * @param data
	 *            token string
	 * @param isTag
	 *            boolean: true-if data is tag, false-if data is text
	 */
	public TokenWrapper(String data, boolean isTag) {
		super();
		this.data = data;
		this.isTag = isTag;
	}

	/**
	 * data getter
	 * 
	 * @return data string
	 */
	public String getData() {
		return data;
	}

	/**
	 * tag flag getter
	 * 
	 * @return true if tag, false if text
	 */
	public boolean isTag() {
		return isTag;
	}

}
