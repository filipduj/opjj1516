package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Node representing a piece of textual data. It inherits from Node class.
 * 
 * @author Filip Dujmušić
 * 
 */
public class TextNode extends Node {

	/**
	 * Hold string data extracted from parsed script. It keeps all the raw data
	 * of string including enclosed quotation marks and escape characters.
	 */
	private String text;

	/**
	 * Constructs new node
	 * 
	 * @param token
	 *            string construct containing text
	 */
	public TextNode(String token) {
		super(token);
	}

	/**
	 * Gets the text out of string construct which means that enclosing
	 * quotation marks are removed and escaped character replaced by their
	 * representations.
	 * 
	 * if original string was "t\\est" method returns t\est
	 * 
	 * @return
	 */
	public String asText() {
		text.replace("\\{", "{");
		text.replace("\\\\", "\\");
		return text;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void parse(String tag) {
		text = tag;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmptyTag() {
		return true;
	}

	/**
	 * Returns actual raw string construct of this node. (with no characters
	 * escaped nor quotation marks removed)
	 */
	@Override
	public String toString() {
		return text;
	}

}
