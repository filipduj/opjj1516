package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Token representation of string.
 * 
 * @author Filip Dujmušić
 *
 */
public class TokenString extends Token {

	/**
	 * Token data in raw format. Keeps quotation marks and escape expressions as
	 * they are.
	 */
	private String value;

	/**
	 * String token constructor
	 * 
	 * @param value
	 *            string token
	 */
	public TokenString(String value) {
		this.value = value;
	}

	/**
	 * Converts raw string to text. Removes leading and trailing quotation marks
	 * and escapes all escaping constructs.
	 * 
	 * @return returns textual representation of token data
	 */
	public String asText() {
		return value.substring(1, value.length() - 1).replace("\\\\", "\\").replace("\\\"", "\"").replace("\\n", "\n")
				.replace("\\r", "\r").replace("\\t", "\t");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return value;
	}

}
