package hr.fer.zemris.java.custom.scripting.parser;

import hr.fer.zemris.java.custom.collections.ArrayBackedIndexedCollection;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantDouble;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantInteger;
import hr.fer.zemris.java.custom.scripting.tokens.TokenFunction;
import hr.fer.zemris.java.custom.scripting.tokens.TokenOperator;
import hr.fer.zemris.java.custom.scripting.tokens.TokenString;
import hr.fer.zemris.java.custom.scripting.tokens.TokenVariable;

/**
 * Tokenizator class provides two state machines for tokenizing input strings.
 * 
 * @author Filip Dujmušić
 *
 */
public class Tokenizator {

	/**
	 * Splits single TAG type token to its sub token elements. Possible syntax
	 * error are detected while parsing and exception is thrown.
	 * 
	 * @param input
	 *            TAG token in string form
	 * @return {@link ArrayBackedIndexedCollection} containing tokens of input
	 *         string
	 */
	public static ArrayBackedIndexedCollection tokenize(String input) {

		final int START_STATE = 0;
		final int FUNC = 1;
		final int VAR = 2;
		final int STRING = 3;
		final int NUMBER = 4;
		final int DECIMAL_NUMBER = 5;
		final int OPERATOR = 6;

		ArrayBackedIndexedCollection tokens = new ArrayBackedIndexedCollection();
		int state = START_STATE;
		char c;
		String nextToken = "";

		for (int i = 0, n = input.length(); i < n; i++) {

			c = input.charAt(i);

			if (state == START_STATE) {
				if (Character.isWhitespace(c)) {
					continue;
				} else if (Character.isLetter(c)) {
					state = VAR;
				} else if (Character.isDigit(c)
						|| (c == '-' && Character.isDigit(input.charAt((i + 1) < n ? i + 1 : i)))) {
					state = NUMBER;
				} else if (c == '@') {
					state = FUNC;
				} else if (c == '\"') {
					state = STRING;
				} else if (c == '+' || c == '*' || c == '/' || c == '-' || c == '%') {
					state = OPERATOR;
				} else {
					throw new SmartScriptParserException("Wrong tag syntax!");
				}
				nextToken = "" + c;

			}

			else if (state == FUNC) {
				if (isTerminator(c)) {
					state = START_STATE;
					tokens.add(new TokenFunction(nextToken));
					nextToken = "";
					--i;
					continue;
				} else {
					if ((input.charAt(i - 1) == '@' && !Character.isLetter(c))
							|| (!Character.isLetterOrDigit(c) && c != '_')) {
						throw new SmartScriptParserException("Wrong function name!");
					}
					nextToken += c;
				}
			} else if (state == VAR) {
				if (isTerminator(c)) {
					state = START_STATE;
					tokens.add(new TokenVariable(nextToken));
					nextToken = "";
					--i;
					continue;
				} else {
					if (!Character.isLetterOrDigit(c) && c != '_') {
						throw new SmartScriptParserException("Wrong variable name!");
					}
					nextToken += c;
				}
			} else if (state == NUMBER) {
				if (isTerminator(c)) {
					state = START_STATE;
					tokens.add(new TokenConstantInteger(Integer.parseInt(nextToken)));
					nextToken = "";
					--i;
					continue;
				} else if (c == '.') {
					state = DECIMAL_NUMBER;
					nextToken += c;
					continue;
				} else {
					if (!Character.isDigit(c)) {
						throw new SmartScriptParserException("Wrong number format!");
					}
					nextToken += c;
				}
			} else if (state == DECIMAL_NUMBER) {
				if (isTerminator(c)) {
					state = START_STATE;
					tokens.add(new TokenConstantDouble(Double.parseDouble(nextToken)));
					nextToken = "";
					--i;
					continue;
				} else {
					if (!Character.isDigit(c)) {
						throw new SmartScriptParserException("Wrong decimal number format!");
					}
					nextToken += c;
				}
			} else if (state == OPERATOR) {
				state = START_STATE;
				tokens.add(new TokenOperator(nextToken));
				nextToken = "";
				--i;
				continue;
			} else if (state == STRING) {
				if (input.regionMatches(i, "\\\\", 0, 2)) {
					nextToken += "\\\\";
					i += 1;
				} else if (input.regionMatches(i, "\\\"", 0, 2)) {
					nextToken += "\\\"";
					i += 1;
				} else if (input.regionMatches(i, "\\n", 0, 2)) {
					nextToken += "\\n";
					i += 1;
				} else if (input.regionMatches(i, "\\r", 0, 2)) {
					nextToken += "\\r";
					i += 1;
				} else if (input.regionMatches(i, "\\t", 0, 2)) {
					nextToken += "\\t";
					i += 1;
				} else if (c == '\"') {
					state = START_STATE;
					tokens.add(new TokenString(nextToken + "\""));
					nextToken = "";
					continue;
				} else {
					nextToken += c;
				}
			}
		}

		if (state == STRING) {
			throw new SmartScriptParserException("String expression not closed inside tag!");
		}

		switch (state) {
		case FUNC:
			tokens.add(new TokenFunction(nextToken));
			break;
		case VAR:
			tokens.add(new TokenVariable(nextToken));
			break;
		case NUMBER:
			tokens.add(new TokenConstantInteger(Integer.parseInt(nextToken)));
			break;
		case DECIMAL_NUMBER:
			tokens.add(new TokenConstantDouble(Double.parseDouble(nextToken)));
			break;
		case OPERATOR:
			tokens.add(new TokenOperator(nextToken));
			break;
		}

		return tokens;
	}

	/**
	 * Splits script in tags and text. While parsing error detection is handled
	 * by throwing exceptions if language syntax is not correct.
	 * 
	 * @param input
	 *            String representing document body
	 * @return Collection {@link ArrayBackedIndexedCollection} containing tokens
	 *         wrapped in {@link TokenWrapper}
	 */
	public static ArrayBackedIndexedCollection tokenizeTagText(String input) {

		final boolean TAG = true;
		final boolean TEXT = false;

		boolean readingTag = false;
		boolean stringOpened = false;

		ArrayBackedIndexedCollection ret = new ArrayBackedIndexedCollection();

		String nextToken = "";
		int position = 0;

		if (input.regionMatches(position, "{$", 0, 2)) {
			readingTag = true;
			position += 2;
		}

		while (position < input.length()) {

			while (input.regionMatches(position, "\\\\", 0, 2)) {
				nextToken += "\\\\";
				position += 2;
			}

			if (readingTag) {

				if (input.regionMatches(position, "{$", 0, 2) && !stringOpened) {
					throw new SmartScriptParserException(
							"Syntax error: Found opening tag \"{$\" before previous was closed!");
				}

				if (input.regionMatches(position, "$}", 0, 2) && !stringOpened) {
					ret.add(new TokenWrapper(nextToken.trim(), TAG));
					readingTag = false;
					position += 2;
					nextToken = "";
				} else if (input.charAt(position) == '\"') {

					if (stringOpened && input.charAt(position - 1) == '\\') {
						nextToken += Character.toString(input.charAt(position++));
					} else {
						stringOpened = !stringOpened;
						nextToken += Character.toString(input.charAt(position++));
					}

				} else {
					nextToken += Character.toString(input.charAt(position++));
				}

			} else {
				if (input.regionMatches(position, "\\{", 0, 2)) {
					nextToken += "\\{";
					position += 2;
				} else if (input.regionMatches(position, "{$", 0, 2)) {
					ret.add(new TokenWrapper(nextToken, TEXT));

					readingTag = true;
					nextToken = "";

					position += 2;
				} else {
					nextToken += Character.toString(input.charAt(position++));
				}
			}
		}

		if (stringOpened) {
			throw new SmartScriptParserException("Syntax error: Quotes opened and never closed!");
		}

		if (readingTag) {
			throw new SmartScriptParserException("Syntax error: Tag opened and never closed!");
		}

		if (!nextToken.trim().isEmpty()) {
			ret.add(new TokenWrapper(nextToken, TEXT));
		}
		return ret;
	}

	/**
	 * Checks if character is terminating character. Terminating characters
	 * split tags in sub tokens.
	 * 
	 * @param c
	 *            character to test
	 * @return returns true if c is terminating, false otherwise
	 */
	private static boolean isTerminator(char c) {
		return (Character.isWhitespace(c) || c == '+' || c == '*' || c == '/' || c == '-' || c == '%' || c == '@'
				|| c == '\"');
	}

}
