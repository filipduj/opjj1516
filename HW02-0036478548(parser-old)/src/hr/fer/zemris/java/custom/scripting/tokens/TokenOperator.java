package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Token representation of mathematical operator.
 * 
 * @author Filip Dujmušić
 *
 */
public class TokenOperator extends Token {

	/**
	 * Operator symbol as string
	 */
	private String symbol;

	/**
	 * Operator token constructor
	 * 
	 * @param symbol
	 *            operator string
	 */
	public TokenOperator(String symbol) {
		this.symbol = symbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return symbol;
	}
}
