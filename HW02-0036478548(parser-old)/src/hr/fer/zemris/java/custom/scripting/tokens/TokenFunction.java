package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Token representation of function.
 * 
 * @author Filip Dujmušić
 *
 */
public class TokenFunction extends Token {

	/**
	 * Function name
	 */
	private String name;

	/**
	 * Function token constructor
	 * 
	 * @param name
	 *            function string
	 */
	public TokenFunction(String name) {
		this.name = name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return name;
	}

}
