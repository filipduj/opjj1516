package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayBackedIndexedCollection;

/**
 * Base class for all graph nodes. Ensures that every node derived from this
 * class knows how to parse itself to elements. Also every node can have child
 * nodes. This comes handy since parsed script will be represented with
 * tree-like structure.
 * 
 * @author Filip Dujmušić
 *
 */
public abstract class Node {

	/**
	 * Collection which keeps potential child nodes of current node.
	 */
	private ArrayBackedIndexedCollection childNodes;

	/**
	 * The only constructor available to make a new node. Ensures that upon
	 * creation of node, string token must be passed to constructor. Redirects
	 * input string to parse method. Since this is the only constructor every
	 * node class derived from this one will have to call exactly this
	 * constructor.
	 * 
	 * @param tag
	 *            String token to be parsed and used for Node creation.
	 */
	protected Node(String tag) {
		parse(tag);
	}

	/**
	 * Parses passed string to element tokens.
	 * 
	 * @param tag
	 *            String representing expression to be parsed
	 */
	protected abstract void parse(String tag);

	/**
	 * Checks if node is empty tag. Empty tag is tag that has no children
	 * elements. For example ECHO is "one liner" (therefore empty) but FOR tag
	 * has block body and is not empty tag.
	 * 
	 * @return returns true if tag is empty, false otherwise
	 */
	public abstract boolean isEmptyTag();

	/**
	 * Adds node as child of current node. Memory for child nodes will be
	 * allocated when this method is called for the first time. That way memory
	 * will be spared because not all nodes will have child nodes.
	 * 
	 * @param child
	 *            Node to be inserted as child
	 */
	public void addChildNode(Node child) {
		if (childNodes == null) {
			childNodes = new ArrayBackedIndexedCollection(4);
		}
		childNodes.add(child);
	}

	/**
	 * Returns number of children for current node.
	 * 
	 * @return number of children for current node or zero if no children
	 */
	public int numberOfChildren() {
		return childNodes == null ? 0 : childNodes.size();
	}

	/**
	 * Gets child at given index. Throws exception if index out of bounds.
	 * 
	 * @param index
	 * @return
	 */
	public Node getChild(int index) {
		if (childNodes == null) {
			System.err.println("Error: requested child node but no children exist!");
			System.exit(-1);
		}
		return (Node) childNodes.get(index);
	}
}
