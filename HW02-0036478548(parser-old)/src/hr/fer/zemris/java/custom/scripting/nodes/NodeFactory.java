package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;
import hr.fer.zemris.java.custom.scripting.parser.TokenWrapper;

/**
 * Class provides methods for constructing new Node elements. It handles all
 * Node derivatives and knows how to construct new objects out of given data.
 * 
 * @author Filip Dujmušić
 *
 */
public class NodeFactory {

	/**
	 * Creates node out of given string construct. Created node will be either
	 * FOR node, ECHO node or END node. Throws exception if string construct is
	 * not syntactically correct.
	 * 
	 * @param token
	 *            expression construct
	 * @return returns newly constructed node or exception is thrown
	 */
	public static Node createNode(TokenWrapper token) {

		String data = token.getData().toLowerCase();

		if (token.isTag()) {

			if (data.startsWith("for")) {
				return new ForLoopNode(token.getData().substring(3));
			} else if (data.startsWith("=")) {
				return new EchoNode(token.getData().substring(1));
			} else if (data.startsWith("end")) {
				return new EndNode();
			}

			throw new SmartScriptParserException("Unknown tag name or wrong tag format!");
		}

		return new TextNode(token.getData());
	}

}
