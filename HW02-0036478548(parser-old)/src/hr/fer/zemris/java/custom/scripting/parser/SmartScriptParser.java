package hr.fer.zemris.java.custom.scripting.parser;

import hr.fer.zemris.java.custom.collections.ArrayBackedIndexedCollection;
import hr.fer.zemris.java.custom.collections.ObjectStack;
import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.nodes.EndNode;
import hr.fer.zemris.java.custom.scripting.nodes.Node;
import hr.fer.zemris.java.custom.scripting.nodes.NodeFactory;

/**
 * 
 * Parser for structured document format. Provides methods for parsing specially
 * formated documents and building document trees out of it.
 * 
 * @author Filip Dujmušić
 *
 */
public class SmartScriptParser {

	/**
	 * Root node for tree holding elements of parsed document.
	 */
	private DocumentNode documentNode;

	/**
	 * Constructs {@link SmartScriptParser} by getting document body as String.
	 * Takes passed string and delegates parsing to special method.
	 * 
	 * @param body
	 *            document body in String form
	 */
	public SmartScriptParser(String body) {
		parseInput(body);
	}

	/**
	 * Parser takes input string and builds documentNode tree. It uses two state
	 * machines implemented in {@link Tokenizator}. First state machine splits
	 * document in two categories: TAG and TEXT. Every tag starts with {$ and
	 * ends with $}. Everything not in those tags is considered as text. Second
	 * state machine splits each TAG token to subtokens. Stack is used then to
	 * create tree.
	 * 
	 * @param input
	 *            string representing document body
	 */
	private void parseInput(String input) {

		if (input.isEmpty()) {
			throw new SmartScriptParserException("No input provided!");
		}

		ObjectStack stack = new ObjectStack();
		documentNode = new DocumentNode();

		stack.push(documentNode);

		ArrayBackedIndexedCollection tokens = Tokenizator.tokenizeTagText(input);

		for (int i = 0, n = tokens.size(); i < n; i++) {
			TokenWrapper token = (TokenWrapper) tokens.get(i);

			Node node = NodeFactory.createNode(token);
			Node top = (Node) stack.peek();

			if (node instanceof EndNode) {
				if (stack.pop() == documentNode) {
					throw new SmartScriptParserException("Too many end nodes!");
				}
			} else {
				top.addChildNode(node);
				if (!node.isEmptyTag()) {
					stack.push(node);
				}
			}
		}
		if (stack.peek() != documentNode) {
			throw new SmartScriptParserException("Too little end nodes!");
		}
	}

	/**
	 * documentNode getter
	 * 
	 * @return returns documentNode or <code>null</code> value if document not
	 *         parsed
	 */
	public DocumentNode getDocumentNode() {
		return documentNode;
	}

}
