package hr.fer.zemris.java.custom.scripting.parser;

/**
 * Derived exception class used for various exceptions that could happen while
 * parsing document.
 * 
 * @author Filip Dujmušić
 *
 */
public class SmartScriptParserException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SmartScriptParserException() {
		super();
	}

	public SmartScriptParserException(String message) {
		super(message);
	}

}
