package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * General token class from which all other concrete type of tokens are derived.
 * 
 * @author Filip Dujmušić
 *
 */
public abstract class Token {
	
	/**
	 * Returns string representation of this token.
	 */
	public abstract String toString();
}
