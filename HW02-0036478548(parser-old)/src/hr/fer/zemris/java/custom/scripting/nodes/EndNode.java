package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * END node is used for closing non empty tags. For example FOR is empty tag and
 * its body must be closed with END. This node will never be stored in document
 * tree, but it is used to check if script syntax is correct.
 * 
 * @author Filip Dujmušić
 *
 */
public final class EndNode extends Node {

	/**
	 * Parses nothing.
	 */
	public EndNode() {
		super(null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void parse(String tag) {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmptyTag() {
		return false;
	}

}
