package hr.fer.zemris.java.custom.scripting.nodes;

import hr.fer.zemris.java.custom.collections.ArrayBackedIndexedCollection;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;
import hr.fer.zemris.java.custom.scripting.parser.Tokenizator;
import hr.fer.zemris.java.custom.scripting.tokens.Token;
import hr.fer.zemris.java.custom.scripting.tokens.TokenConstantInteger;
import hr.fer.zemris.java.custom.scripting.tokens.TokenString;
import hr.fer.zemris.java.custom.scripting.tokens.TokenVariable;

/**
 * Node representing a single for-loop construct. It inherits from Node class.
 * 
 * @author filip
 *
 */
public class ForLoopNode extends Node {

	/**
	 * Variable counter in for loop.
	 */
	private TokenVariable variable;

	/**
	 * Initial value of counter.
	 */
	private Token startExpression;

	/**
	 * Final value to count to.
	 */
	private Token endExpression;

	/**
	 * Step increment. It is not necessary so null is allowed.
	 */
	private Token stepExpression; // can be null

	/**
	 * Constructs new for loop node.
	 * 
	 * @param forTag
	 *            For construct to parse.
	 */
	public ForLoopNode(String forTag) {
		super(forTag);
	}

	/**
	 * Variable getter
	 * 
	 * @return returns variable token
	 */
	public TokenVariable getVariable() {
		return variable;
	}

	/**
	 * Start expression getter
	 * 
	 * @return returns start token
	 */
	public Token getStartExpression() {
		return startExpression;
	}

	/**
	 * End expression getter
	 * 
	 * @return returns end token
	 */
	public Token getEndExpression() {
		return endExpression;
	}

	/**
	 * Step expression getter
	 * 
	 * @return returns step token or <code>null</code> if no step given
	 */
	public Token getStepExpression() {
		return stepExpression;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected void parse(String token) {
		ArrayBackedIndexedCollection tokens = Tokenizator.tokenize(token);

		checkOperands(tokens);

		this.variable = (TokenVariable) tokens.get(0);
		this.startExpression = (Token) tokens.get(1);
		this.endExpression = (Token) tokens.get(2);
		if (tokens.size() == 4) {
			this.stepExpression = (Token) tokens.get(3);
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isEmptyTag() {
		return false;
	}

	/**
	 * Checks if for loop operands are correct. This method checks all
	 * requirements that for loop construct must fulfill: 1) number of operands
	 * has to be 3 or 4 (since last operand - step is optional) 2) variable
	 * expression must be of type TokenVariable 3) following 2 (or 3) parameters
	 * must be either integer constants, variables or strings containing
	 * parsable integer
	 * 
	 * If at least one of these conditions is not fulfilled an exception is
	 * thrown.
	 * 
	 * @param tokens
	 *            array of tokens to be checked
	 */
	private void checkOperands(ArrayBackedIndexedCollection tokens) {

		int size = tokens.size();

		if (size < 3 || size > 4) {
			throw new SmartScriptParserException("Wrong for loop syntax!");
		} else if (!(tokens.get(0) instanceof TokenVariable)) {
			throw new SmartScriptParserException("First operand in for loop must be variable!");
		} else if (!isLegalOperand((Token) tokens.get(1))) {
			throw new SmartScriptParserException("Wrong start expression!");
		} else if (!isLegalOperand((Token) tokens.get(2))) {
			throw new SmartScriptParserException("Wrong end expression!");
		} else if (tokens.size() == 4 && !isLegalOperand((Token) tokens.get(3))) {
			throw new SmartScriptParserException("Wrong step expression!");
		}

	}

	/**
	 * Checks if single token is integer constant, variable or string containing
	 * parsable integer
	 * 
	 * @param t
	 *            token to check
	 * @return returns true if conditions are met, false otherwise
	 */
	private boolean isLegalOperand(Token t) {
		return (t instanceof TokenVariable || t instanceof TokenConstantInteger
				|| (t instanceof TokenString && isParsableInteger(((TokenString) t).asText())));
	}

	/**
	 * Checks if it is possible to convert string to integer.
	 * 
	 * @param s
	 *            string to check
	 * @return returns true if string is convertible to integer, false otherwise
	 */
	private boolean isParsableInteger(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Returns String representing for loop node in following format: {$ FOR
	 * [variable] [start] [end] [step] $}
	 * 
	 * step is optional and if it's not provided it will be left out
	 */
	@Override
	public String toString() {
		return "{$ FOR " + " " + variable + " " + startExpression + " " + endExpression + " "
				+ (stepExpression == null ? "" : stepExpression) + " $}";
	}

}
