package hr.fer.zemris.java.custom.scripting.tokens;

/**
 * Token representation of decimal number.
 * 
 * @author Filip Dujmušić
 *
 */
public class TokenConstantDouble extends Token {

	/**
	 * Value of decimal number
	 */
	private double value;

	/**
	 * Constructor
	 * 
	 * @param value
	 *            decimal number (double)
	 */
	public TokenConstantDouble(double value) {
		this.value = value;
	}

	/**
	 * Value getter
	 * 
	 * @return returns decimal value of token as double
	 */
	public double getValue() {
		return value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return Double.toString(value);
	}

}
