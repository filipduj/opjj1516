package hr.fer.zemris.java.hw2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

/**
 * Class to test basic functionality of {@link SmartScriptParser}. Takes path to
 * input file as command line argument and tries to parse it into tree-like
 * structure.
 * 
 * @author Filip Dujmušić
 *
 */
public class SmartScriptTester {

	/**
	 * Main method - entry point for program.
	 * 
	 * @param args
	 *            command line argument
	 */
	public static void main(String[] args) {
		String input = null;
		try {
			input = new String(Files.readAllBytes(Paths.get(args[0])));
		} catch (IOException e) {
			System.err.println("Error reading input file. Exiting...");
			System.exit(0);
		}

		SmartScriptParser parser = null;
		try {
			parser = new SmartScriptParser(input);
		} catch (SmartScriptParserException e) {
			e.printStackTrace();
			System.out.println("Unable to parse document!");
			System.exit(-1);
		} catch (Exception e) {
			System.out.println("If this line ever executes, you have failed this class!");
			System.exit(-1);
		}

		System.out.println(parser.getDocumentNode());

	}
}