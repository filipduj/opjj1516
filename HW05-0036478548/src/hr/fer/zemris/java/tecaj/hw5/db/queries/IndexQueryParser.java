package hr.fer.zemris.java.tecaj.hw5.db.queries;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class provides method for parsing indexquery command and retrieving jmbag
 * data.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 13, 2016)
 */
public class IndexQueryParser {

	/**
	 * Syntax checking regex
	 */
	private static final Pattern INDEX_QUERY_PATTERN = Pattern.compile("^jmbag\\s*=\\s*\"(.*)\"$");

	/**
	 * Retrieves jmbag part out of given indexquery command.
	 * 
	 * @param query
	 *            indexquery data
	 * @return returns jmbag as String
	 */
	public static String getJMBAG(String query) {
		Matcher m = INDEX_QUERY_PATTERN.matcher(query);

		if (m.matches()) {
			return m.group(1);
		}

		throw new QueryParserException("Invalid indexquery command syntax!");
	}
}
