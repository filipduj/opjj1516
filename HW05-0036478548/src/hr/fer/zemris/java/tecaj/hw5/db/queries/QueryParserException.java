package hr.fer.zemris.java.tecaj.hw5.db.queries;

/**
 * This class represents new exception used in {@link QueryParser} to indicate
 * parsing error.
 * <p>
 * Program can usually recover from this kind of exceptions.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 *
 */
public class QueryParserException extends RuntimeException {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -7512996391583666805L;

	/**
	 * Default constructor
	 */
	public QueryParserException() {
		super();
	}

	/**
	 * Constructor takes message as parameter and displays it to user.
	 * 
	 * @param message
	 *            exception message
	 */
	public QueryParserException(String message) {
		super(message);
	}

}
