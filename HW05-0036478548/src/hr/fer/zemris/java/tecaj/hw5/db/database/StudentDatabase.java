package hr.fer.zemris.java.tecaj.hw5.db.database;

import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw5.collections.SimpleHashTable;
import hr.fer.zemris.java.tecaj.hw5.db.queries.IFilter;

/**
 * This class represents one database holding {@link StudentRecord} entries.
 * <p>
 * It provides {@link #filter(IFilter)} method for retrieveing all records
 * satisfying given {@link IFilter} condition.
 * <p>
 * It also creates index for fast retrieval of student records when jmbag is
 * known.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 */
public class StudentDatabase {

	/**
	 * List of student records kept in this database.
	 */
	private List<StudentRecord> studentRecords;

	/**
	 * Hashtable holding pair of (jmbag, record) values. Used for fast record
	 * retrieval based on jmbag value.
	 */
	private SimpleHashTable<String, StudentRecord> indexByJMBAG;

	/**
	 * Constructor takes list of String objects (where each string represents
	 * one row of database file) and tries to parse them into {@link List} of
	 * {@link StudentRecord} objects.
	 * <p>
	 * {@link StudentRecord} defines what each row should have.
	 * 
	 * @param records
	 *            {@code List} of String objects
	 * @throws DatabaseException
	 *             If any of database rows not consistent or input list empty.
	 * 
	 */
	public StudentDatabase(List<String> records) {

		if (records.isEmpty()) {
			throw new DatabaseException("Student records list is empty!");
		}

		studentRecords = new LinkedList<>();
		indexByJMBAG = new SimpleHashTable<>(records.size());

		for (String record : records) {
			StudentRecord r = new StudentRecord(record);
			studentRecords.add(r);
			indexByJMBAG.put(r.getJmbag(), r);
		}

	}

	/**
	 * Returns {@code StudentRecord} whose jmbag value is {@code jmbag}.
	 * Operation takes O(1) time since index is built on jmbag attribute.
	 * 
	 * @param jmbag
	 *            String representing jmbag value
	 * @return {@code StudentRecord} whose jmbag value is equal to given
	 *         {@code jmbag}
	 */
	public StudentRecord forJMBAG(String jmbag) {
		return indexByJMBAG.get(jmbag);
	}

	/**
	 * Method takes {@link IFilter} filter and returns list of
	 * {@link StudentRecord} records satisfying filter condition.
	 * 
	 * @param filter
	 *            {@code IFilter}
	 * @return list of student records
	 */
	public List<StudentRecord> filter(IFilter filter) {

		List<StudentRecord> filteredStudentRecords = new LinkedList<>();

		for (StudentRecord record : studentRecords) {
			if (filter.accepts(record)) {
				filteredStudentRecords.add(record);
			}
		}

		return filteredStudentRecords;

	}
}
