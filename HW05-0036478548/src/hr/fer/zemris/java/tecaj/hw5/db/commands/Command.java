package hr.fer.zemris.java.tecaj.hw5.db.commands;

/**
 * This class represents one command. Each command has type which is any value
 * defined by {@link CommandType}. Command also has data which is everything
 * user entered after command name.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 *
 */
public class Command {

	/**
	 * Command type
	 */
	private CommandType type;

	/**
	 * Command data
	 */
	private String data;

	/**
	 * Constructor initializes new {@code Command} object
	 * with given {@code type} and {@code data}.
	 * 
	 * @param type type of command
	 * @param data command data
	 */
	public Command(CommandType type, String data) {
		this.type = type;
		this.data = data;
	}

	/**
	 * @return the type
	 */
	public CommandType getType() {
		return type;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}

}
