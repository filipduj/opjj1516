package hr.fer.zemris.java.tecaj.hw5.db.operators;

/**
 * Interface represents abstract comparison operator and defines one method for
 * deciding if two values satisfy operator.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 *
 */
public interface IComparisonOperator {

	/**
	 * Takes two string values and returns result of operator calculated on
	 * given values.
	 * 
	 * @param value1
	 *            First string
	 * @param value2
	 *            Second string
	 * @return returns {@code true} or {@code false} based on operator result
	 */
	boolean satisfied(String value1, String value2);
}
