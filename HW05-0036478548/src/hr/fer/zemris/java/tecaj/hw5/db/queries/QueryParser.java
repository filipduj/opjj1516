package hr.fer.zemris.java.tecaj.hw5.db.queries;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static hr.fer.zemris.java.tecaj.hw5.db.getters.GetterFactory.*;
import static hr.fer.zemris.java.tecaj.hw5.db.operators.OperatorFactory.*;

/**
 * Class provides method for parsing expressions assigned to query command.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 13, 2016)
 */
public class QueryParser {

	/*
	 * SYNTAX
	 */

	/**
	 * Regex that matches one single attribute
	 */
	private static final String ATTRIBUTE_PATTERN = "(lastName|firstName|jmbag)";

	/**
	 * Regex that matches one single operator
	 */
	private static final String OPERATOR_PATTERN = "(>=|<=|>|<|!=|=|LIKE)";

	/**
	 * Regex that matches one single literal constant
	 */
	private static final String LITERAL_PATTERN = "\"([^\"]*)\"";

	/**
	 * Regex that matches case insensitive keyword `AND`
	 */
	private static final String AND_PATTERN = "(?i)AND(?c)";

	/**
	 * Regex that matches one single `[attribute] [operator] [literal]`
	 * expression
	 */
	private static final String GROUP_PATTERN = "(" + ATTRIBUTE_PATTERN + "\\s*" + OPERATOR_PATTERN + "\\s*"
			+ LITERAL_PATTERN + ")";

	/**
	 * Regex that matches every possible command line input for query syntax.
	 * Used for checking if expression is sintactically correct before condition
	 * extraction is done.
	 */
	private static final String QUERY_PATTERN = "^(" + GROUP_PATTERN + ")(\\s+" + AND_PATTERN + "\\s+" + GROUP_PATTERN
			+ ")*$";

	/**
	 * Compiled {@link #QUERY_PATTERN}
	 */
	private static final Pattern SYNTAX_CHECK = Pattern.compile(QUERY_PATTERN);

	/**
	 * Compiled {@link #GROUP_PATTERN}
	 */
	private static final Pattern FIRST_TOKEN = Pattern.compile(GROUP_PATTERN);

	/**
	 * Compiled regex: `AND [attribute] [operator] [literal]`
	 */
	private static final Pattern NEXT_TOKEN = Pattern.compile("\\s+" + AND_PATTERN + "\\s+" + GROUP_PATTERN);

	/**
	 * Method extracts expressions out of given query sting and returns them as
	 * list of {@link ConditionalExpression} objects.
	 * 
	 * @param query
	 *            query string
	 * @return list of conditional expressions
	 */
	public static List<ConditionalExpression> parse(String query) {

		List<ConditionalExpression> expressions = new LinkedList<>();

		// check syntax
		Matcher m = SYNTAX_CHECK.matcher(query);
		if (!m.matches()) {
			throw new QueryParserException("Wrong query syntax!");
		}

		// prvi expression
		Matcher nextToken = FIRST_TOKEN.matcher(query);
		nextToken.find();
		expressions.add(new ConditionalExpression(getField(nextToken.group(2)), getOperator(nextToken.group(3)),
				nextToken.group(4)));

		// svi sljedeci AND expressioni
		nextToken = NEXT_TOKEN.matcher(query);
		while (nextToken.find()) {
			expressions.add(new ConditionalExpression(getField(nextToken.group(2)), getOperator(nextToken.group(3)),
					nextToken.group(4)));
		}

		return expressions;
	}
}