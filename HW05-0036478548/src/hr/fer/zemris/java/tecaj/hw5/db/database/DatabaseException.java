package hr.fer.zemris.java.tecaj.hw5.db.database;


/**
 * This class represents new exception used in
 * {@link StudentDatabase} to indicate database error.
 * <p>
 * Program can not recover from this kind of exception
 * because it's thrown when database is not consistent.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 *
 */
public class DatabaseException extends RuntimeException {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -338200454279540052L;

	/**
	 * Default constructor
	 */
	public DatabaseException() {
		super();
	}
	
	/**
	 * Constructor takes message as parameter and displays it
	 * to user.
	 * 
	 * @param message exception message
	 */
	public DatabaseException(String message) {
		super(message);
	}

}
