package hr.fer.zemris.java.tecaj.hw5.collections;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * This class implements a hash table, which maps keys to values. Any non-
 * <code>null</code> object can be used as a key and any object (including
 * <code>null</code>) can be used as a value.
 * <p>
 * 
 * To successfully store and retrieve objects from a hashtable, the objects used
 * as keys must implement the <code>hashCode</code> method and the
 * <code>equals</code> method.
 * <p>
 * 
 * An instance of <code>SimpleHashTable</code> has one parameter that affects
 * its performance: <i>initial capacity</i>. The <i>capacity</i> is the number
 * of <i>buckets</i> in the hash table, and the <i>initial capacity</i> is
 * simply the capacity at the time the hash table is created. Note that the hash
 * table is <i>open</i>: in the case of a "hash collision", a single bucket
 * stores multiple entries, which must be searched sequentially.
 * <p>
 * 
 * This example creates a hashtable of numbers. It uses the names of the numbers
 * as keys:
 * 
 * <pre>
 * {@code
 * 	SimpleHashTable<String, Integer> numbers = new SimpleHashTable<String, Integer>();
 * 	numbers.put("one", 1);
 * 	numbers.put("two", 2);
 * 	numbers.put("three", 3);
 * }
 * </pre>
 *
 * <p>
 * To retrieve a number, use the following code:
 * 
 * <pre>
 * {@code
 * 	Integer n = numbers.get("two");
 * 	if (n != null) {
 * 		System.out.println("two = " + n);
 * 	}
 * }
 * </pre>
 * 
 * 
 * @author Filip Dujmušić
 * @version 1.0 (10.4.2016)
 *
 * @param <K>
 *            Key parameter, can be any non-{@code null} object
 * @param <V>
 *            Value parameter, can be any object
 */
public class SimpleHashTable<K, V> implements Iterable<SimpleHashTable.TableEntry<K, V>> {

	/**
	 * Default number of <i>buckets</i>
	 */
	private static final int DEFAULT_NUMBER_OF_table = 16;

	/**
	 * When filling percentage (entries/buckets) gets larger than this minimum,
	 * table is rehashed.
	 */
	private static final float MINIMUM_PERCENTAGE = 0.75f;

	/**
	 * Number of Key,Value entries stored in table
	 */
	private int size;

	/**
	 * This variable tracks structural modifications of table. Each time when
	 * Key,Value pair is added/removed, or table gets rehashed this value
	 * increases by one.
	 */
	private int modificationCount;

	/**
	 * Array holding <i>buckets</i>
	 */
	private TableEntry<K, V>[] table;

	/**
	 * Default constructor allocates new <code>SimpleHashTable</code> with 16
	 * buckets.
	 */
	public SimpleHashTable() {
		this(DEFAULT_NUMBER_OF_table);
	}

	/**
	 * Allocates new <code>SimpleHashTable</code> with number of buckets equal
	 * to the given {@code numberOftable}.
	 * 
	 * @param numberOftable
	 *            Initial capacity (number of buckets) of
	 *            {@code SimpleHashTable}
	 * @throws IllegalArgumentException
	 *             If {@code numberOftable} is less than 1
	 */
	@SuppressWarnings("unchecked")
	public SimpleHashTable(int numberOftable) {

		if (numberOftable < 1) {
			throw new IllegalArgumentException(
					"Number of table must be greater than 0 but " + numberOftable + " was given.");
		}

		table = (TableEntry<K, V>[]) new TableEntry[roundPowerOfTwo(numberOftable)];

	}

	/**
	 * This class represents one Key,Value entry for {@link SimpleHashTable}.
	 * Key is immutable, but value can be modified if necessary.
	 * 
	 * @author Filip Dujmušić
	 * @version 1.0 (10.4.2016)
	 *
	 * @param <K>
	 *            Key (anything but {@code null})
	 * @param <V>
	 *            Value (any object)
	 */
	public static class TableEntry<K, V> {

		/**
		 * Key
		 */
		private final K key;

		/**
		 * Value
		 */
		private V value;

		/**
		 * Reference to the next entry in the same bucket (in case of
		 * collision).
		 */
		private TableEntry<K, V> next;

		/**
		 * Constructor initializes new {@code TableEntry} pair with given
		 * {@code key} and {@code value}.
		 * 
		 * @param key
		 *            Key object
		 * @param value
		 *            Value object
		 */
		public TableEntry(K key, V value) {
			this.key = key;
			this.value = value;
		}

		/**
		 * @return the key
		 */
		public K getKey() {
			return key;
		}

		/**
		 * @return the value
		 */
		public V getValue() {
			return value;
		}

		/**
		 * @param value
		 *            the value to set
		 */
		public void setValue(V value) {
			this.value = value;
		}

		/**
		 * Returns string representation of one table entry.
		 * 
		 * @return string representation of table entry
		 */
		@Override
		public String toString() {
			return key.toString() + "=" + value.toString();
		}

	}

	/**
	 * Adds new {@code TableEntry} pair {@code key},{@code value} to the
	 * {@code SimpleHashTable} or updates existing entry if entry with given
	 * {@code key} already exists. Key cannot be {@code null}.
	 * <p>
	 * 
	 * This value can be retrieved by calling the <code>get</code> method with a
	 * key that is equal to the original key.
	 * <p>
	 * 
	 * @param key
	 *            the SimpleHashTable key
	 * @param value
	 *            the value
	 * @throws IllegalArgumentException
	 *             If {@code key} is {@code null} reference
	 */
	public void put(K key, V value) {

		if (key == null) {
			throw new IllegalArgumentException("Key value cannot be null!");
		}

		int slot = hash(key);

		if (table[slot] == null) {

			table[slot] = new TableEntry<>(key, value);
			modificationCount++;
			size++;

			if ((float) size / table.length >= MINIMUM_PERCENTAGE) {
				doubleCapacity();
			}

			return;
		}

		TableEntry<K, V> iterator, previous;

		for (iterator = table[slot], previous = null; iterator != null; previous = iterator, iterator = iterator.next) {
			if (iterator.key.equals(key)) {
				iterator.setValue(value);
				return;
			}
		}

		previous.next = new TableEntry<>(key, value);
		modificationCount++;
		size++;
	}

	/**
	 * Retrieves value mapped to given {@code key} if entry with {@code key}
	 * exists in SimpleHashTable or {@code null}.
	 * 
	 * @param key
	 *            the key whose associated value is to be returned
	 * @return the value to which the specified key is mapped, or {@code null}
	 *         if this map contains no mapping for the {@code key}
	 */
	public V get(Object key) {

		if (key == null) {
			return null;
		}

		for (TableEntry<K, V> entry = table[hash(key)]; entry != null; entry = entry.next) {
			if (entry.key.equals(key)) {
				return entry.value;
			}
		}

		return null;
	}

	/**
	 * @return returns number of Key,Value entries in this table
	 */
	public int size() {
		return size;
	}

	/**
	 * Tests if the specified object is a key in this hashtable.
	 * 
	 * @param key
	 *            possible key
	 * @return <code>true</code> if and only if the specified object is a key in
	 *         this SimpleHashTAble, as determined by the <tt>equals</tt>
	 *         method; <code>false</code> otherwise.
	 */
	public boolean containsKey(Object key) {
		return get(key) != null;
	}

	/**
	 * Returns true if this SimpleHashTable maps one or more keys to this value.
	 * 
	 * @param value
	 *            value value whose presence in this hashtable is to be tested
	 * @return {@code true} if this map maps one or more keys to the specified
	 *         value, {@code false} otherwise
	 */
	public boolean containsValue(Object value) {
		for (int i = 0; i < table.length; ++i) {
			for (TableEntry<K, V> entry = table[i]; entry != null; entry = entry.next) {
				if (entry.value == value || (entry.value != null && entry.value.equals(value))) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Removes entry with key equal to the given object from the table. Equality
	 * is determined by the <tt>equals</tt> method. Does nothing if there is no
	 * entry with given key.
	 * 
	 * @param key
	 *            the key that needs to be removed
	 */
	public void remove(Object key) {

		if (key == null) {
			return;
		}

		int slot = hash(key);

		if (table[slot] != null) {
			if (table[slot].key.equals(key)) {
				table[slot] = table[slot].next;
				size--;
				modificationCount++;
				return;
			} else {
				for (TableEntry<K, V> entry = table[slot]; entry != null; entry = entry.next) {
					if (entry.next != null && entry.next.key.equals(key)) {
						entry.next = entry.next.next;
						size--;
						modificationCount++;
						return;
					}
				}
			}
		}

	}

	/**
	 * @return returns {@code true} if SimpleHashTable contains no entries
	 */
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Removes all entries from table.
	 */
	public void clear() {
		for (int i = 0; i < table.length; ++i) {
			table[i] = null;
		}
		size = 0;
	}

	/**
	 * Returns string representation of this table in following form:
	 * <p>
	 * 
	 * [key1=value1, key2=value2, ... , keyN=valueN]
	 * 
	 * @return SimpleHashTable string representation
	 */
	@Override
	public String toString() {

		StringBuilder sb = new StringBuilder();
		int entriesPrinted = 0;

		sb.append("[");

		for (TableEntry<K, V> entry : this) {
			sb.append(entry);
			if (entriesPrinted++ < size - 1) {
				sb.append(", ");
			}
		}

		sb.append("]");

		return sb.toString();
	}

	/**
	 * Returns iterator over elements of type {@code TableEntry<K,V>}
	 * 
	 * @return an iterator
	 */
	@Override
	public Iterator<TableEntry<K, V>> iterator() {
		return new IteratorImpl();
	}

	/**
	 * This class implements Iterator over {@link TableEntry} elements. It takes
	 * snapshot of associated {@link SimpleHashTable} and throws
	 * {@link ConcurrentModificationException} if any outside structural change
	 * occurs while iterating over entries.
	 * 
	 * @author Filip Dujmušić
	 * @version 1.0 (10.4.2016)
	 *
	 */
	private class IteratorImpl implements Iterator<TableEntry<K, V>> {

		/**
		 * Internal modification count variable.
		 */
		private int modificationCount;

		/**
		 * Current bucket position.
		 */
		private int slot;

		/**
		 * Reference to current table entry.
		 */
		private TableEntry<K, V> currentElement;

		/**
		 * Reference to next table entry.
		 */
		private TableEntry<K, V> nextElement;

		/**
		 * Constructor initializes new Iterator associated to it's
		 * {@link SimpleHashTable}.
		 */
		public IteratorImpl() {
			this.modificationCount = SimpleHashTable.this.modificationCount;
			updateIterator();
		}

		/**
		 * Returns true if the iteration has more elements. (In other words,
		 * returns true if {@link #next()} would return an element rather than
		 * throwing an exception.)
		 * 
		 * @return returns {@code true} if iteration has more elements
		 * @throws ConcurrentModificationException
		 *             If {@code SimpleHashTable} was structurally modified from
		 *             outside before this method was called
		 */
		@Override
		public boolean hasNext() {
			modificationCheck();
			return nextElement != null;
		}

		/**
		 * Returns the next element in iteration.
		 * 
		 * @return next element
		 * @throws NoSuchElementException
		 *             If there is no next element
		 * @throws ConcurrentModificationException
		 *             If {@code SimpleHashTable} was structurally modified from
		 *             outside before this method was called
		 */
		@Override
		public TableEntry<K, V> next() {

			modificationCheck();

			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			currentElement = nextElement;
			updateIterator();
			return currentElement;
		}

		/**
		 * Removes from the {@code SimpleHashTable} the last element returned by
		 * this iterator. This method can be called only once per call to
		 * {@link #next}.
		 *
		 * @throws IllegalStateException
		 *             if the {@code next} method has not yet been called, or
		 *             the {@code remove} method has already been called after
		 *             the last call to the {@code next} method
		 * @throws ConcurrentModificationException
		 *             If {@code SimpleHashTable} was structurally modified from
		 *             outside before this method was called
		 * 
		 */
		@Override
		public void remove() {
			modificationCheck();

			if (currentElement == null) {
				throw new IllegalStateException();
			}
			SimpleHashTable.this.remove(currentElement.key);
			modificationCount++;
			currentElement = null;
		}

		/**
		 * Moves {@code nextElement} reference to next entry in current bucket
		 * or to the first entry in next non-empty bucket.
		 * <p>
		 * If none of those exist sets {@code nextElement} to {@code null}
		 * reference indicating that next entry does not exist.
		 */
		private void updateIterator() {
			if (nextElement != null && nextElement.next != null) {
				nextElement = nextElement.next;
				return;
			} else {
				while (++slot < table.length) {
					if (table[slot] != null) {
						nextElement = table[slot];
						return;
					}
				}
			}
			nextElement = null;
		}

		/**
		 * Checks if {@code SimpleHashTable} was modified from outside.
		 * 
		 * @throws ConcurrentModificationException
		 *             If table was modified from outside
		 */
		private void modificationCheck() {
			if (this.modificationCount != SimpleHashTable.this.modificationCount) {
				throw new ConcurrentModificationException();
			}
		}
	}

	/**
	 * Rehashes {@code SimpleHashTable} by doubling it's capacity (number of
	 * <i>buckets</i>) and repositioning each entry in new structure.
	 */
	@SuppressWarnings("unchecked")
	private void doubleCapacity() {

		TableEntry<K, V>[] oldtable = table;

		table = (TableEntry<K, V>[]) new TableEntry[2 * oldtable.length];
		size = 0;

		for (int i = 0; i < oldtable.length; ++i) {
			TableEntry<K, V> iterator = oldtable[i];
			while (iterator != null) {
				put(iterator.key, iterator.value);
				iterator = iterator.next;
			}
		}
	}

	/**
	 * Returns smallest power of two greater or equal to the given number
	 * {@code n}.
	 * 
	 * @param n
	 *            number
	 * @return smallest power of two greater or equal to {@code n}
	 */
	private int roundPowerOfTwo(int n) {
		int power = 1;
		while (power < n)
			power <<= 1;
		return power;
	}

	/**
	 * Calculates hash value of given object {@code obj} using <tt>hashCode</tt>
	 * method.
	 * 
	 * @param obj
	 *            Object whose hash is to be calculated
	 * @return hash value of {@code obj}
	 * @throws NullPointerException
	 *             If {@code obj} is {@code null} reference
	 */
	private int hash(Object obj) {
		return Math.abs(obj.hashCode()) % table.length;
	}

}
