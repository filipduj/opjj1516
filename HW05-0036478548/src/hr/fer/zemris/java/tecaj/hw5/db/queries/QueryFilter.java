package hr.fer.zemris.java.tecaj.hw5.db.queries;

import java.util.List;

import hr.fer.zemris.java.tecaj.hw5.db.database.StudentRecord;

/**
 * Implements {@link IFilter} interface to provide functionality for checking
 * multiple conditions on one {@link StudentRecord} record.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 13, 2016)
 */
public class QueryFilter implements IFilter {

	/**
	 * List of conditions
	 */
	private List<ConditionalExpression> conditions;

	/**
	 * Constructor takes input query string which can possibly contain multiple
	 * expressions connected with and. It extracts out expressions and returns
	 * them as list of {@link ConditionalExpression} objects.
	 * 
	 * @param queryString
	 *            string containing conditional expressions
	 */
	public QueryFilter(String queryString) {
		conditions = QueryParser.parse(queryString);
	}

	/**
	 * Checks if given {@code record} matches all conditions in
	 * {@link #conditions} list.
	 * 
	 * @param record
	 *            One student record
	 * @return {@code true} if {@code record} matches all conditions
	 */
	@Override
	public boolean accepts(StudentRecord record) {
		for (ConditionalExpression expr : conditions) {
			if (!expr.getOperator().satisfied(expr.getAttribute().get(record), expr.getLiteral())) {
				return false;
			}
		}
		return true;
	}

}
