package hr.fer.zemris.java.tecaj.hw5.db.commands;

/**
 * This class represents new exception used in
 * {@link CommandParser} to indicate parsing error.
 * <p>
 * Program can usually recover from this kind of
 * exceptions.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 *
 */
public class CommandParserException extends RuntimeException {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -498653523793861005L;

	/**
	 * Default constructor
	 */
	public CommandParserException() {
		super();
	}

	/**
	 * Constructor takes message as parameter and displays it
	 * to user.
	 * 
	 * @param message exception message
	 */
	public CommandParserException(String message) {
		super(message);
	}

}
