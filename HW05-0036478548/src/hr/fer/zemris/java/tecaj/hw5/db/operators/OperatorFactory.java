package hr.fer.zemris.java.tecaj.hw5.db.operators;

import java.text.Collator;
import java.util.Locale;
import java.util.regex.Pattern;

import hr.fer.zemris.java.tecaj.hw5.db.queries.QueryParserException;

/**
 * Factory class provides method for parsing string and returning
 * {@link IComparisonOperator} operator associated to given string.
 * <p>
 * Valid operators are: >,<,=,!=,>=,<=,LIKE
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 *
 */
public class OperatorFactory {

	/**
	 * Instance of {@link Collator} used for locale supported string comparison
	 */
	private static final Collator collator = Collator.getInstance(new Locale("hr", "HR"));

	/**
	 * Greater operator
	 */
	public static final IComparisonOperator GREATER = (value1, value2) -> collator.compare(value1, value2) > 0;

	/**
	 * Greater or eqaul operator
	 */
	public static final IComparisonOperator GREATER_EQUAL = (value1, value2) -> collator.compare(value1, value2) >= 0;

	/**
	 * Equal operator
	 */
	public static final IComparisonOperator EQUAL = (value1, value2) -> value1.equals(value2);

	/**
	 * Less or equal operator
	 */
	public static final IComparisonOperator LESS_EQUAL = (value1, value2) -> collator.compare(value1, value2) <= 0;

	/**
	 * Less operator
	 */
	public static final IComparisonOperator LESS = (value1, value2) -> collator.compare(value1, value2) < 0;

	/**
	 * Not equal operator
	 */
	public static final IComparisonOperator NOT_EQUAL = (value1, value2) -> !value1.equals(value2);

	/**
	 * LIKE operator
	 */
	public static final IComparisonOperator LIKE = (value1, value2) -> Pattern
			.matches("^" + value2.replaceAll("\\*", ".\\*") + "$", value1);

	/**
	 * Method takes input string representing operator and returns
	 * {@link IComparisonOperator}.
	 * 
	 * @param operator
	 *            String representing operator
	 * @return returns {@link IComparisonOperator} for given {@code operator}
	 * @throws QueryParserException
	 *             If {@code operator} not recognized
	 */
	public static IComparisonOperator getOperator(String operator) {
		if (operator.equals(">=")) {
			return GREATER_EQUAL;
		} else if (operator.equals(">")) {
			return GREATER;
		} else if (operator.equals("=")) {
			return EQUAL;
		} else if (operator.equals("<=")) {
			return LESS_EQUAL;
		} else if (operator.equals("<")) {
			return LESS;
		} else if (operator.equals("!=")) {
			return NOT_EQUAL;
		} else if (operator.equals("LIKE")) {
			return LIKE;
		} else {
			throw new QueryParserException("Invalid operator!");
		}
	}
}
