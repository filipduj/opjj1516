package hr.fer.zemris.java.tecaj.hw5.db.commands;

/**
 * This enum defines all valid command types.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 *
 */
public enum CommandType {
	
	/**
	 * query command
	 */
	QUERY,
	
	/**
	 * indexquery command
	 */
	INDEXQUERY,
	
	/**
	 * exit command
	 */
	EXIT;
}
