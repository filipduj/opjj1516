package hr.fer.zemris.java.tecaj.hw5.db.getters;

import hr.fer.zemris.java.tecaj.hw5.db.database.StudentRecord;

/**
 * Interface represents field value getter and defines
 * one method get used for retrieveing field value.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 *
 */
public interface IFieldValueGetter {
	
	/**
	 * Takes given {@link StudentRecord} and returns
	 * String value of specified field.
	 * 
	 * @param record student record
	 * @return returns string value of field within {@code record}
	 */
	String get(StudentRecord record);
}
