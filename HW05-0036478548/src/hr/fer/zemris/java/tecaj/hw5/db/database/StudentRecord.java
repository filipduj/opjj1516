package hr.fer.zemris.java.tecaj.hw5.db.database;

/**
 * Represents one entry in {@link StudentDatabase} database.
 * <p>
 * Every student is described with jmbag, last name, first name and grade.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 *
 */
public class StudentRecord {

	/**
	 * jmbag
	 */
	private String jmbag;
	
	/**
	 * last name
	 */
	private String lastName;
	
	/**
	 * first name
	 */
	private String firstName;
	
	/**
	 * grade
	 */
	private int grade;

	/**
	 * Constructor takes String representing one row from database file and
	 * tries to parse it into new record.
	 * <p>
	 * Each row must have 4 values separated by tab, where:
	 * <ul>
	 * 		<li>first value represents jmbag</li>
	 * 		<li>second value represents student's last name</li>
	 * 		<li>third value represents student's first name</li>
	 * 		<li>fourth value represents student's grade (1-5)</li>
	 * </ul>
	 * 
	 * @param record String representing one row from textual database
	 * @throws DatabaseException If conditions above not satisfied.
	 */
	public StudentRecord(String record) {

		String[] data = record.split("\t");

		if (data.length != 4) {
			throw new DatabaseException("Input database row not consistent!");
		}

		jmbag = data[0].trim();
		lastName = data[1].trim();
		firstName = data[2].trim();

		try {
			grade = Integer.parseInt(data[3].trim());
		} catch (NumberFormatException e) {
			throw new DatabaseException("Grade value not integer. Database inconsistent!");
		}

		if (grade < 1 || grade > 5) {
			throw new DatabaseException("Grade value not legal. Database inconsistent!");
		}
	}

	/**
	 * @return the jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the grade
	 */
	public String getGrade() {
		return String.valueOf(grade);
	}

}
