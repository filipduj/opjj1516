package hr.fer.zemris.java.tecaj.hw5.db;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;

import hr.fer.zemris.java.tecaj.hw5.db.commands.Command;
import hr.fer.zemris.java.tecaj.hw5.db.commands.CommandParser;
import hr.fer.zemris.java.tecaj.hw5.db.commands.CommandParserException;
import hr.fer.zemris.java.tecaj.hw5.db.commands.CommandType;
import hr.fer.zemris.java.tecaj.hw5.db.database.DatabaseException;
import hr.fer.zemris.java.tecaj.hw5.db.database.StudentDatabase;
import hr.fer.zemris.java.tecaj.hw5.db.database.StudentRecord;
import hr.fer.zemris.java.tecaj.hw5.db.queries.IndexQueryParser;
import hr.fer.zemris.java.tecaj.hw5.db.queries.QueryFilter;
import hr.fer.zemris.java.tecaj.hw5.db.queries.QueryParserException;

/**
 * Main program for demonstrating operations on {@link StudentDatabase}.
 * <p>
 * 
 * Program attempts to read given input file line by line and parse it into new
 * {@code StudentDatabase}.
 * <p>
 * 
 * If everything went fine program goes into interactive mode, listening to
 * commands given by user through standard input.
 * <p>
 * 
 * Valid commands are:
 * <ul>
 * 		<li>query [atrribute] [operator] [literal] < AND [atrribute] [operator] [literal] ></li><br>
 * 			<ol>
 * 				<li>Command must have first 3 parameters followed by 0 or more expressions
 * 					enclosed by <> (<> and [] brackets are not part of command, they're here to
 *	 				group this repeating expression).</li>
 * 				<li>Attribute can be any of following (case sensitive): lastName, firstName,
 * 					jmbag</li>
 * 				<li>Operator can be any of following: <,>,<=,>=,!=,LIKE</li>
 * 				<li>Literal must be enclosed by " " and can contain anything except quotes.</li>
 * 				<li>If operator is LIKE, literal can contain 0 or 1 '*' at any position and
 * 					it's interpreted as wild card.<br>
 * 					In other words, this character replaces any possible string (including empty
 * 					string).<br>
 * 					For example firstName LIKE "*a" would return all student records whose name
 * 					ends with letter a.</li>
 * 				<li>If operator is LIKE atleast one whitespace is required beween<br>
 * 					Attribute-Operator and Operator-Literal.</li>
 * 				<li>If operator is anything but LIKE, literal can be any possible string
 * 					enclosed in quotes.<br>
 * 					Space between Atrribute-Operator and Operator-Literal in this case is
 * 					optional.</li>
 * 				<li>AND keyword is not case sensitive</li>
 * 			</ol>
 * 		<li>indexquery jmbag = [literal]</li>	
 * 		<li>exit</li>
 * </ul>
 * 
 * Command names are case sensitive and whitespace before start of single
 * command or after it's end is ignored.
 * <p>
 * 
 * <b>query syntax:</b>
 * <p>
 * 
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 */
public class StudentDB {
	
	/**
	 * Path to input database file
	 */
	private static final String DATABASE_FILE = "./testData/database.txt";

	/**
	 * Program entry point.
	 */
	public static void main(String[] args) {

		try (Scanner scanner = new Scanner(System.in)) {

			List<String> lines = Files.readAllLines(Paths.get(DATABASE_FILE), StandardCharsets.UTF_8);

			StudentDatabase database = new StudentDatabase(lines);

			while (true) {

				System.out.print("> ");

				try {
					Command cmd = CommandParser.getCommand(scanner.nextLine());

					if (cmd.getType() == CommandType.QUERY) {
						displayQueryResult(database.filter(new QueryFilter(cmd.getData())));
					} else if (cmd.getType() == CommandType.INDEXQUERY) {
						displayIndexQueryResult(database.forJMBAG(IndexQueryParser.getJMBAG(cmd.getData())));
					} else {
						System.out.println("Goodbye!");
						break;
					}

				} catch (QueryParserException | CommandParserException e) {
					System.out.println(e.getMessage());
				} catch (NoSuchElementException e) {
					System.out.println("No input line given.");
				}

			}

		} catch (IOException e) {
			exit("Error reading input file!");
		} catch (DatabaseException e) {
			exit(e.getMessage());
		} catch (IllegalStateException e) {
			exit("Input stream closed!");
		}

	}

	/**
	 * Takes one {@link StudentRecord} and outputs it in formatted form.
	 * 
	 * @param record {@code StudentRecord} record
	 */
	private static void displayIndexQueryResult(StudentRecord record) {
		System.out.println("Using index for record retrieval.");
		if (record == null) {
			System.out.println("Records selected: 0");
		} else {
			List<StudentRecord> filteredRecords = new LinkedList<>();
			filteredRecords.add(record);
			displayQueryResult(filteredRecords);
		}

	}

	/**
	 * Takes list of {@link StudentRecord} records and outputs it in formatted form.
	 * 
	 * @param filteredRecords list of student records
	 */
	private static void displayQueryResult(List<StudentRecord> filteredRecords) {

		if (filteredRecords.isEmpty()) {
			System.out.println("Records selected: 0");

		} else {

			int maxFirstNameLength = 0;
			int maxLastNameLength = 0;

			for (StudentRecord record : filteredRecords) {
				if (record.getFirstName().length() > maxFirstNameLength) {
					maxFirstNameLength = record.getFirstName().length();
				}
				if (record.getLastName().length() > maxLastNameLength) {
					maxLastNameLength = record.getLastName().length();
				}
			}

			String firstNameFormat = " %-" + maxFirstNameLength + "s |";
			String lastNameFormat = " %-" + maxLastNameLength + "s |";

			displayTableBorder(10, maxLastNameLength, maxFirstNameLength, 1);
			for (StudentRecord record : filteredRecords) {
				System.out.printf("| %10s |", record.getJmbag());
				System.out.printf(lastNameFormat, record.getLastName());
				System.out.printf(firstNameFormat, record.getFirstName());
				System.out.printf(" %s |", record.getGrade());
				System.out.println();
			}
			displayTableBorder(10, maxLastNameLength, maxFirstNameLength, 1);

			System.out.printf("Records selected: %d%n", filteredRecords.size());
		}

	}

	/**
	 * Outputs table border where each column's width 
	 * is set to given parameters. 
	 * 
	 * @param jmbag Maximum jmbag length (usually 10)
	 * @param lastName Maximum last name length
	 * @param firstName Maximum first name length
	 * @param grade Maximum grade length (usually 1)
	 */
	private static void displayTableBorder(int jmbag, int lastName, int firstName, int grade) {
		System.out.print("+");
		displayNtimes('=', jmbag + 2);
		System.out.print("+");
		displayNtimes('=', lastName + 2);
		System.out.print("+");
		displayNtimes('=', firstName + 2);
		System.out.print("+");
		displayNtimes('=', grade + 2);
		System.out.print("+");
		System.out.println();
	}

	/**
	 * Outputs given character {@code c} exactly {@code n} times.
	 * 
	 * @param c character
	 * @param n number of times to display {@code c}
	 */
	private static void displayNtimes(char c, int n) {
		for (int i = 0; i < n; ++i) {
			System.out.print(c);
		}
	}

	/**
	 * Forced exit method.
	 * Exits with error status and displays message to err stream.
	 * 
	 * @param message Message to be displayed before exit
	 */ 
	private static void exit(String message) {
		System.err.println(message);
		System.exit(-1);
	}

}