package hr.fer.zemris.java.tecaj.hw5.db.commands;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.zemris.java.tecaj.hw5.db.queries.QueryParser;

/**
 * Class provides single method for parsing command input string.
 * It only checks if valid command name was given, in other words
 * it doesn't check actual command parameters syntax.
 * <p>
 * Class {@link QueryParser} deals with command syntax.
 * <p>
 * Valid commands start with query, indexquery or exit.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 *
 */
public class CommandParser {

	/**
	 * Pattern checks if command name is valid
	 */
	private static final Pattern COMMAND_PATTERN 		= Pattern.compile("^(query|indexquery|exit)(.*)$");

	/**
	 * Pattern groups command data from query command
	 */
	private static final Pattern QUERY_PATTERN 			= Pattern.compile("query\\s+(.*)");

	/**
	 * Pattern groups command data from indexquery command
	 */
	private static final Pattern INDEXQUERY_PATTERN 	= Pattern.compile("indexquery\\s+(.*)");

	/**
	 * Pattern checks if exit command is valid
	 */
	private static final Pattern EXIT_PATTERN 			= Pattern.compile("exit");

	
	/**
	 * This method takes command in string form, checks if it's
	 * valid and returns new {@link Command} object built from
	 * input string.
	 * 
	 * @param input command in string form
	 * @return returns {@code Command} object representing given command string
	 * @throws CommandParserException If input string is empty or command name not valid
	 */
	public static Command getCommand(String input) {

		input = input.trim();

		if (input.isEmpty()) {
			throw new CommandParserException("No command given.");
		}

		if (!COMMAND_PATTERN.matcher(input).matches()) {
			throw new CommandParserException("Unknown command name. Valid commands are: query, indexquery, exit");
		}

		Matcher m;

		m = QUERY_PATTERN.matcher(input);
		if (m.matches()) {
			return new Command(CommandType.QUERY, m.group(1));
		}

		m = INDEXQUERY_PATTERN.matcher(input);
		if (m.matches()) {
			return new Command(CommandType.INDEXQUERY, m.group(1));
		}

		m = EXIT_PATTERN.matcher(input);
		if (m.matches()) {
			return new Command(CommandType.EXIT, null);
		}

		throw new CommandParserException("Invalid command syntax!");

	}

}
