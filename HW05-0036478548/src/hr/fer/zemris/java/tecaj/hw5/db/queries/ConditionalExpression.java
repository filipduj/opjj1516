package hr.fer.zemris.java.tecaj.hw5.db.queries;

import hr.fer.zemris.java.tecaj.hw5.db.getters.IFieldValueGetter;
import hr.fer.zemris.java.tecaj.hw5.db.operators.IComparisonOperator;
import hr.fer.zemris.java.tecaj.hw5.db.operators.OperatorFactory;

/**
 * Class represents one conditional expression.
 * <p>
 * Each expression has following three elements: attribute, operator, literal.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 13, 2016)
 * 
 */
public class ConditionalExpression {

	/**
	 * Field value used as attribute
	 */
	private IFieldValueGetter attribute;

	/**
	 * Comparison operator
	 */
	private IComparisonOperator operator;

	/**
	 * Literal - string constant
	 */
	private String literal;

	/**
	 * Initializes new {@code ConditionalExpression} object withg given
	 * parameters.
	 * 
	 * @param attribute
	 *            attribute
	 * @param operator
	 *            operator
	 * @param literal
	 *            literal
	 */
	public ConditionalExpression(IFieldValueGetter attribute, IComparisonOperator operator, String literal) {

		if (operator == OperatorFactory.LIKE) {
			if (numberOfOccurences(literal, '*') > 1) {
				throw new QueryParserException("Invalid literal for LIKE command!");
			}
		}

		this.attribute = attribute;
		this.literal = literal;
		this.operator = operator;

	}

	/**
	 * Counts and returns number of occurences of character {@code c} in String
	 * {@code s}.
	 * 
	 * @param s
	 *            Input string
	 * @param c
	 *            Character to count number of occurences
	 * @return returns number of occurences of {@code c} in {@code s}
	 */
	private int numberOfOccurences(String s, char c) {
		int ret = 0;
		for (int i = 0; i < s.length(); ++i) {
			if (s.charAt(i) == c) {
				++ret;
			}
		}
		return ret;
	}

	/**
	 * @return the attribute
	 */
	public IFieldValueGetter getAttribute() {
		return attribute;
	}

	/**
	 * @return the literal
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * @return the operator
	 */
	public IComparisonOperator getOperator() {
		return operator;
	}

}
