package hr.fer.zemris.java.tecaj.hw5.db.getters;

import hr.fer.zemris.java.tecaj.hw5.db.database.StudentRecord;
import hr.fer.zemris.java.tecaj.hw5.db.queries.QueryParserException;

/**
 * Factory class provides method for parsing string field name and returning
 * {@link IFieldValueGetter} getter associated to given field.
 * <p>
 * Valid field names are: firstName, lastName, jmbag
 * 
 * @author Filip Dujmušić
 * @version 1.0 (13.4.2016)
 *
 */
public class GetterFactory {

	/**
	 * Getter for first name
	 */
	public static final IFieldValueGetter FIRST_NAME = StudentRecord::getFirstName;

	/**
	 * Getter for last name
	 */
	public static final IFieldValueGetter LAST_NAME = StudentRecord::getLastName;

	/**
	 * Getter for jmbag
	 */
	public static final IFieldValueGetter JMBAG = StudentRecord::getJmbag;

	/**
	 * Method takes input string representing field name and returns getter for
	 * that field.
	 * 
	 * @param fieldName
	 *            field name
	 * @return returns {@link IFieldValueGetter} getter for given
	 *         {@code fieldName}
	 * @throws QueryParserException
	 *             If field name not recognized
	 */
	public static IFieldValueGetter getField(String fieldName) {
		if (fieldName.equals("firstName")) {
			return FIRST_NAME;
		} else if (fieldName.equals("lastName")) {
			return LAST_NAME;
		} else if (fieldName.equals("jmbag")) {
			return JMBAG;
		} else {
			throw new QueryParserException("Invalid field name!");
		}
	}
}
