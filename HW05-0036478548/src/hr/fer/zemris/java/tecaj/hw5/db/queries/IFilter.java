package hr.fer.zemris.java.tecaj.hw5.db.queries;

import hr.fer.zemris.java.tecaj.hw5.db.database.StudentRecord;

/**
 * Filter interface. Defines one method for condition checking.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 13, 2016)
 */
public interface IFilter {

	/**
	 * Checks if given {@code record} meets criteria.
	 * 
	 * @param record
	 *            {@link StudentRecord} record
	 * @return returns {@code true} if {@code record} meets criteria,
	 *         {@code false} otherwise
	 */
	public boolean accepts(StudentRecord record);
}
