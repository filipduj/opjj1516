package hr.fer.zemris.java.tecaj.hw5.collections;

import static org.junit.Assert.*;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.Test;

import hr.fer.zemris.java.tecaj.hw5.collections.SimpleHashTable;
import hr.fer.zemris.java.tecaj.hw5.collections.SimpleHashTable.TableEntry;

@SuppressWarnings("javadoc")
public class SimpleHashTableTests {

	@Test(expected = IllegalArgumentException.class)
	public void nonPositiveInitialCapacityTest() {
		new SimpleHashTable<String, Integer>(0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void insertNullKeyEntryTest() {
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();
		// null key not allowed!
		hashtable.put(null, 13);
	}

	@Test
	public void putNewAndExistingEntriesTest() {
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();

		hashtable.put("key1", 1);
		hashtable.put("key2", 2);
		hashtable.put("key3", 3);

		// size should now be 3
		assertEquals(3, hashtable.size());

		hashtable.put("key1", 42);

		// size should remain 3 but value assigned to "key1" should be 42
		assertEquals(3, hashtable.size());
		assertEquals(42, hashtable.get("key1").intValue());
	}

	@Test
	public void clearEntriesTest() {
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();

		hashtable.put("key1", 1);
		hashtable.put("key2", 2);
		hashtable.put("key3", 3);

		hashtable.clear();

		// size shuold be zero after clearing
		assertEquals(0, hashtable.size());
	}

	@Test
	public void containsKeyMethodTest() {
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();

		hashtable.put("key1", 1);
		hashtable.put("key2", 2);
		hashtable.put("key3", 3);

		// should always be false since hashtable does not allow null keys
		assertEquals(false, hashtable.containsKey(null));

		// should be false since key is not the right type
		assertEquals(false, hashtable.containsKey(9));

		// should be false
		assertEquals(false, hashtable.containsKey("key4"));

		// should be true
		assertEquals(true, hashtable.containsKey("key1"));
	}

	@Test
	public void containsValueMethodTest() {
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();

		hashtable.put("key1", 1);
		hashtable.put("key2", 2);
		hashtable.put("key3", 3);
		hashtable.put("null", null);

		// should return true - null values are allowed
		assertEquals(true, hashtable.containsValue(null));

		// should return false
		assertEquals(false, hashtable.containsValue(4));

		// should return true
		assertEquals(true, hashtable.containsValue(1));

	}

	@Test
	public void getMethodTest() {
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();

		hashtable.put("key1", 1);
		hashtable.put("key2", 2);
		hashtable.put("key3", 3);
		hashtable.put("null", null);

		// get should return 2
		assertEquals(2, hashtable.get("key2").intValue());

		// get should return null because "key4" does not exist
		assertEquals(null, hashtable.get("key4"));

		// get should return null because it is assigned to "null" key
		assertEquals(null, hashtable.get("null"));
	}

	@Test
	public void removeMethodTest() {
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>(2);

		
		// many values in 2 bucket table will also make 
		//  the table dinamically grow so that part will
		//  implicitly be tested too
		 
		hashtable.put("key1", 1);
		hashtable.put("key2", 2);
		hashtable.put("key3", 3);
		hashtable.put("key4", 1);
		hashtable.put("key5", 2);
		hashtable.put("key6", 3);
		hashtable.put("key7", 1);
		hashtable.put("key8", 2);
		hashtable.put("key9", 3);
		hashtable.put("key10", 1);
		hashtable.put("key11", 2);
		hashtable.put("key12", 3);
		
		assertEquals(12, hashtable.size());

		// these two calls change nothing
		hashtable.remove("abc");
		hashtable.remove(null);

		// size remains the same
		assertEquals(12, hashtable.size());

		// check if there is "key1" in table after its removal
		assertEquals(true, hashtable.containsKey("key8"));
		hashtable.remove("key8");
		assertEquals(false, hashtable.containsKey("key8"));

		// size should now be 2
		assertEquals(11, hashtable.size());
	}

	@Test
	public void emptyMethodTest() {
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();

		hashtable.put("key1", 1);
		hashtable.put("key2", 2);

		hashtable.remove("key1");
		hashtable.remove("key2");

		assertEquals(true, hashtable.isEmpty());
	}

	@Test
	public void toStringMethodTest() {
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();
		assertEquals("[]", hashtable.toString());
		hashtable.put("key1", 1);
		assertEquals("[key1=1]", hashtable.toString());
	}

	@Test
	public void iteratorRemoveAllTest() {
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();

		hashtable.put("key1", 1);
		hashtable.put("key2", 2);

		Iterator<TableEntry<String, Integer>> it = hashtable.iterator();

		while (it.hasNext()) {
			it.next();
			it.remove();
		}

		assertEquals(true, hashtable.isEmpty());
	}

	@Test(expected = IllegalStateException.class)
	public void iteratorRemoveWithoutCallingNextFirst() {
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();

		hashtable.put("key1", 1);
		hashtable.put("key2", 2);

		Iterator<TableEntry<String, Integer>> it = hashtable.iterator();

		// should trow exception
		it.remove();
	}

	@Test(expected = IllegalStateException.class)
	public void iteratorRemoveTwiceInARowTest() {
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();
		hashtable.put("key1", 1);
		hashtable.put("key2", 2);
		Iterator<SimpleHashTable.TableEntry<String, Integer>> iter = hashtable.iterator();
		while (iter.hasNext()) {
			SimpleHashTable.TableEntry<String, Integer> pair = iter.next();
			if (pair.getKey().equals("key1")) {
				iter.remove();
				//should trow exception now!
				iter.remove();
			}
		}
	}
	
	@Test
	public void iteratorHasNextEmptyTable(){
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();
		Iterator<SimpleHashTable.TableEntry<String, Integer>> iter = hashtable.iterator();
		assertEquals(false, iter.hasNext());
	}
	
	@Test(expected = NoSuchElementException.class)
	public void iteratorManyNextCallsTest(){
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();
		hashtable.put("key1", 1);
		hashtable.put("key2", 2);
		Iterator<SimpleHashTable.TableEntry<String, Integer>> iter = hashtable.iterator();
		
		//should trow exception after few runs (python-ish)
		while(true){
			iter.next();
		}
	}
	
	@Test(expected = ConcurrentModificationException.class)
	public void iteratorOutsideStructuralModification(){
		SimpleHashTable<String, Integer> hashtable = new SimpleHashTable<>();
		hashtable.put("key1", 1);
		hashtable.put("key2", 2);
		Iterator<SimpleHashTable.TableEntry<String, Integer>> iter = hashtable.iterator();
		
		while(iter.hasNext()){
			TableEntry<String, Integer> entry = iter.next();
			if(entry.getKey().equals("key1")){
				//outside modification should cause next iterator call to throw exception
				hashtable.remove("key1");
			}
		}
	}
	
	
	

}
