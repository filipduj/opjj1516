package hr.fer.zemris.java.tecaj.hw5.db;

import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import hr.fer.zemris.java.tecaj.hw5.db.commands.Command;
import hr.fer.zemris.java.tecaj.hw5.db.commands.CommandParser;
import hr.fer.zemris.java.tecaj.hw5.db.commands.CommandParserException;
import hr.fer.zemris.java.tecaj.hw5.db.commands.CommandType;
import hr.fer.zemris.java.tecaj.hw5.db.database.StudentDatabase;
import hr.fer.zemris.java.tecaj.hw5.db.database.StudentRecord;
import hr.fer.zemris.java.tecaj.hw5.db.getters.GetterFactory;
import hr.fer.zemris.java.tecaj.hw5.db.operators.IComparisonOperator;
import hr.fer.zemris.java.tecaj.hw5.db.operators.OperatorFactory;
import hr.fer.zemris.java.tecaj.hw5.db.queries.ConditionalExpression;
import hr.fer.zemris.java.tecaj.hw5.db.queries.IndexQueryParser;
import hr.fer.zemris.java.tecaj.hw5.db.queries.QueryFilter;
import hr.fer.zemris.java.tecaj.hw5.db.queries.QueryParser;
import hr.fer.zemris.java.tecaj.hw5.db.queries.QueryParserException;

@SuppressWarnings("javadoc")
public class StudentDatabaseTests {

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	public void CommandNameParsingTests() {

		Command command = CommandParser.getCommand("    query firstName = \"Ana\" ");
		assertEquals(CommandType.QUERY, command.getType());
		assertEquals("firstName = \"Ana\"", command.getData());

		command = CommandParser.getCommand(
				" query firstName>\"A\" and firstName<\"C\" and lastName LIKE \"B*ć\" and jmbag>\"0000000002\"");
		assertEquals(CommandType.QUERY, command.getType());
		assertEquals("firstName>\"A\" and firstName<\"C\" and lastName LIKE \"B*ć\" and jmbag>\"0000000002\"",
				command.getData());

		command = CommandParser.getCommand("    indexquery   jmbag =\"0000000003\" ");
		assertEquals(CommandType.INDEXQUERY, command.getType());
		assertEquals("jmbag =\"0000000003\"", command.getData());

		command = CommandParser.getCommand("   exit    ");
		assertEquals(CommandType.EXIT, command.getType());

	}

	@Test(expected = CommandParserException.class)
	public void CommandParserException1() {
		CommandParser.getCommand("    queryfirstName = \"Ana\" ");
	}

	@Test(expected = CommandParserException.class)
	public void CommandParserException2() {
		CommandParser.getCommand("    abcdefgh ");
	}

	@Test(expected = CommandParserException.class)
	public void CommandParserException3() {
		CommandParser.getCommand("    ");
	}

	@Test(expected = CommandParserException.class)
	public void CommandParserException4() {
		CommandParser.getCommand("   query ");
	}

	@Test(expected = CommandParserException.class)
	public void CommandParserException5() {
		CommandParser.getCommand("   indexquery ");
	}
	
	@Test
	public void getterFactoryTests(){
		
		StudentRecord record = new StudentRecord("0000000004	Božić	Marin	5");
	
		assertEquals("0000000004", GetterFactory.getField("jmbag").get(record));
		assertEquals("Marin", GetterFactory.getField("firstName").get(record));
		assertEquals("Božić", GetterFactory.getField("lastName").get(record));

	}
	
	@Test
	public void operatorFactoryTests(){		

		IComparisonOperator operator = OperatorFactory.getOperator("<");
		assertEquals(true,operator.satisfied("Ćna", "Ena"));
	
		
		operator = OperatorFactory.getOperator(">");
		assertEquals(true,operator.satisfied("Ena", "Ana"));
		
		operator = OperatorFactory.getOperator("=");
		assertEquals(false,operator.satisfied("Ana", "ana"));
		
		operator = OperatorFactory.getOperator("!=");
		assertEquals(true,operator.satisfied("Ana", "ana"));
		
		operator = OperatorFactory.getOperator(">=");
		assertEquals(true,operator.satisfied("Ana", "ana"));
		
		operator = OperatorFactory.getOperator("<=");
		assertEquals(false,operator.satisfied("Ana", "ana"));
	}
	
	@Test
	public void indexQueryParserTests(){
		assertEquals("0000000003", IndexQueryParser.getJMBAG(CommandParser.getCommand("    indexquery   jmbag =\"0000000003\" ").getData()));
	}
	
	@Test (expected = QueryParserException.class)
	public void indexQueryParserException1(){
		IndexQueryParser.getJMBAG(CommandParser.getCommand("    indexquery   jmag =\"0000000003\" ").getData());
	}
	
	@Test (expected = QueryParserException.class)
	public void indexQueryParserException2(){
		IndexQueryParser.getJMBAG(CommandParser.getCommand("    indexquery   jmbag =0000000003\" ").getData());
	}
	
	@Test (expected = QueryParserException.class)
	public void indexQueryParserException3(){
		IndexQueryParser.getJMBAG(CommandParser.getCommand("    indexquery   jmbag =0000000003\" ").getData());
	}
	
	@Test (expected = QueryParserException.class)
	public void indexQueryParserException4(){
		IndexQueryParser.getJMBAG(CommandParser.getCommand("    indexquery   abddejkdfha djsh ").getData());
	}
	
	@Test 
	public void queryParserTests(){
		String query = "firstName   > \"A\" aNd firstName  <\"C\" and lastName LIKE \"B*ć\" and jmbag>\"0000000002\"";
		List<ConditionalExpression> conditions = QueryParser.parse(query);
		
		assertEquals(4, conditions.size());
		
		assertEquals(GetterFactory.FIRST_NAME, conditions.get(0).getAttribute());
		assertEquals(OperatorFactory.GREATER, conditions.get(0).getOperator());
		assertEquals("A", conditions.get(0).getLiteral());
		
		assertEquals(GetterFactory.FIRST_NAME, conditions.get(1).getAttribute());
		assertEquals(OperatorFactory.LESS, conditions.get(1).getOperator());
		assertEquals("C", conditions.get(1).getLiteral());
		
		assertEquals(GetterFactory.LAST_NAME, conditions.get(2).getAttribute());
		assertEquals(OperatorFactory.LIKE, conditions.get(2).getOperator());
		assertEquals("B*ć", conditions.get(2).getLiteral());
		
		assertEquals(GetterFactory.JMBAG, conditions.get(3).getAttribute());
		assertEquals(OperatorFactory.GREATER, conditions.get(3).getOperator());
		assertEquals("0000000002", conditions.get(3).getLiteral());

	}
	
	@Test (expected = QueryParserException.class)
	public void queryParserException1(){
		QueryParser.parse("");
	}
	
	@Test (expected = QueryParserException.class)
	public void queryParserException2(){
		QueryParser.parse("asdfklj ksjldaf jkl f");
	}
	
	@Test (expected = QueryParserException.class)
	public void queryParserException3(){
		QueryParser.parse("firstname>\"asdjk\"");
	}
	
	@Test (expected = QueryParserException.class)
	public void queryParserException4(){
		QueryParser.parse("firstName LIKE \"ab*cd*ef\"");
	}
	
	@Test
	public void databaseTest(){
		
		List<String> records = new LinkedList<>();
		
		records.add("0000000001	Akšamović	Marin	2");
		records.add("0000000002	Bakamović	Petra	3");
		records.add("0000000003	Bosnić	Andrea	4");
		records.add("0000000004	Božić	Marin	5");
		records.add("0000000005	Brezović	Jusufadis	2");
		records.add("0000000006	Cvrlje	Ivan	3");
		records.add("0000000007	Čima	Sanjin	4");
		records.add("0000000008	Ćurić	Marko	5");
		records.add("0000000009	Dean	Nataša	2");
		records.add("0000000010	Dokleja	Luka	3");
		records.add("0000000011	Dvorničić	Jura	4");
		records.add("0000000012	Franković	Hrvoje	5");
		records.add("0000000013	Gagić	Mateja	2");

		StudentDatabase database = new StudentDatabase(records);
		
		String query = "query lastName > \"A\" and lastName <\"Č\" and firstName LIKE \"*a\" AnD jmbag <= \"0000000002\"";
		Command cmd = CommandParser.getCommand(query);
		List<StudentRecord> result = database.filter(new QueryFilter(cmd.getData()));
		
		assertEquals(1,result.size());
		
		assertEquals("0000000002", result.get(0).getJmbag());
		
	}

}
