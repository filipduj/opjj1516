package hr.fer.zemris.java.hw11.notepadpp;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Comparator;
import java.util.List;

import javax.swing.ImageIcon;

/**
 * Various static utility methods used in {@link JNotepadPP}.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 21, 2016)
 */
public class UtilityMethods {

	/**
	 * Method tries to load image resource whose location is defined by given
	 * {@code path} String. If any kind of error occures in attempt to load
	 * image null is returned.
	 * 
	 * @param path
	 *            Path to image resource
	 * @return Returns {@link ImageIcon} or null if error occures
	 */
	public static ImageIcon loadImage(String path) {
		InputStream is = UtilityMethods.class.getResourceAsStream(path);
		if (is == null)
			throw new RuntimeException();

		byte[] bytes = new byte[512];
		ByteArrayOutputStream imageBytes = new ByteArrayOutputStream();

		try {
			while (is.read(bytes) > 0) {
				imageBytes.write(bytes);
			}
		} catch (IOException e) {
			return null;
		}

		return new ImageIcon(imageBytes.toByteArray());
	}

	/**
	 * Sorts list of lines by given comparator {@code cmp}.
	 * 
	 * @param lines
	 *            List of strings
	 * @param cmp
	 *            String comparator
	 * @return returns sorted list of strings
	 */
	public static String getSortedLines(List<String> lines, Comparator<String> cmp) {
		lines.sort(cmp);
		return concatenateLines(lines);
	}

	/**
	 * Concatenates list of lines into one string.
	 * 
	 * @param lines
	 *            List of lines
	 * @return Concatenated lines
	 */
	public static String concatenateLines(List<String> lines) {
		StringBuilder sb = new StringBuilder();
		lines.forEach(s -> sb.append(s).append('\n'));
		return sb.toString();
	}

}
