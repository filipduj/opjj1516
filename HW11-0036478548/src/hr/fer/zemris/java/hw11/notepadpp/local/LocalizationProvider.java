package hr.fer.zemris.java.hw11.notepadpp.local;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Localization provider implementation.
 * <p>
 * Represents localization provider which loads translations from disk.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public class LocalizationProvider extends AbstractLocalizationProvider {

	/**
	 * Default locale to start system with
	 */
	private static final String DEFAULT_LANGUAGE = "en";
	
	/**
	 * Only instance of this provider
	 */
	private static LocalizationProvider instance = null;
	
	/**
	 * Bundle from which translations are read
	 */
	private ResourceBundle bundle;

	/**
	 * Initializes localization provider with default language.
	 */
	private LocalizationProvider() {
		setLanguage(DEFAULT_LANGUAGE);
	}

	/**
	 * @return returns instance of localization provider
	 */
	public static LocalizationProvider getInstance() {
		if (instance == null) {
			instance = new LocalizationProvider();
		}
		return instance;
	}

	/**
	 * Changes locale to given {@code language}
	 * 
	 * @param language Language to set 
	 */
	public void setLanguage(String language) {
		bundle = ResourceBundle.getBundle("hr.fer.zemris.java.hw11.notepadpp.local.translations.lang",
				Locale.forLanguageTag(language));
		fire();
	}

	public String getString(String key) {
		try {
			return new String(bundle.getString(key).getBytes("ISO-8859-1"), "UTF-8");
		} catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Could not read locale!");
		}
	}

}
