package hr.fer.zemris.java.hw11.notepadpp.local.components;

import javax.swing.JMenu;

import hr.fer.zemris.java.hw11.notepadpp.local.ILocalizationListener;
import hr.fer.zemris.java.hw11.notepadpp.local.ILocalizationProvider;

/**
 * Localizable {@link JMenu} implementation.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public class LJMenu extends JMenu {
	
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Initializes menu with locale specific data.
	 * 
	 * @param key
	 *            Access key
	 * @param provider
	 *            Localization provider
	 */
	public LJMenu(String key, ILocalizationProvider provider) {
		setText(provider.getString(key));
		provider.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				setText(provider.getString(key));
			}
		});
	}
	

}
