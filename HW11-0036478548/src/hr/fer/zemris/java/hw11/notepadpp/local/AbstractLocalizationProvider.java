package hr.fer.zemris.java.hw11.notepadpp.local;

import java.util.LinkedList;
import java.util.List;

/**
 * Abstracttion of localization provider.
 * <p>
 * Acts as observable component and provides methods for attaching and detaching
 * observers.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public abstract class AbstractLocalizationProvider implements ILocalizationProvider {

	/**
	 * List of listeners subscribed to this provider
	 */
	private List<ILocalizationListener> listeners = new LinkedList<>();

	@Override
	public void addLocalizationListener(ILocalizationListener listener) {
		listeners.add(listener);
	}

	@Override
	public void removeLocalizationListener(ILocalizationListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Signals all listeners that change has occured
	 */
	public void fire() {
		listeners.forEach(l -> l.localizationChanged());
	}

}
