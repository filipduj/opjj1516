package hr.fer.zemris.java.hw11.notepadpp.local.components;

import javax.swing.AbstractAction;
import javax.swing.Action;

import hr.fer.zemris.java.hw11.notepadpp.local.ILocalizationListener;
import hr.fer.zemris.java.hw11.notepadpp.local.ILocalizationProvider;

/**
 * Localizable {@link Action} implementation.
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public abstract class LAction extends AbstractAction {
	
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Initializes action with locale specific data.
	 * @param key
	 * @param provider
	 */
	public LAction(String key, ILocalizationProvider provider) {
		putValue(NAME, provider.getString(key));
		provider.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				putValue(NAME, provider.getString(key));
				putValue(Action.SHORT_DESCRIPTION, provider.getString(key+"desc"));
			}
		});
	}

}
