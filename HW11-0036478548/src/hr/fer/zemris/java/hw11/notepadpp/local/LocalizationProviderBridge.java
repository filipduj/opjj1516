package hr.fer.zemris.java.hw11.notepadpp.local;

/**
 * Localization bridge.
 * <p>
 * Every communication with localization provider goes through this bridge.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public class LocalizationProviderBridge extends AbstractLocalizationProvider {

	/**
	 * Localization listener
	 */
	private ILocalizationListener listener;

	/**
	 * Flag indicates if bridge is connected to provider
	 */
	private boolean connected;

	/**
	 * Main localization provider
	 */
	private ILocalizationProvider parent;

	/**
	 * Constructor initalizes provider with given given {@code parent} provider.
	 * 
	 * @param parent Main localization provider
	 */
	public LocalizationProviderBridge(ILocalizationProvider parent) {
		this.parent = parent;
		listener = new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				fire();
			}
		};
	}

	/**
	 * Connects bridge to main provider
	 */
	public void connect() {
		if (!connected) {
			parent.addLocalizationListener(listener);
			connected = true;
		}
	}

	/**
	 * Disconnects bridge from main provider
	 */
	public void disconnect() {
		if (connected) {
			parent.removeLocalizationListener(listener);
			connected = false;
		}
	}

	@Override
	public String getString(String key) {
		return parent.getString(key);
	}

}
