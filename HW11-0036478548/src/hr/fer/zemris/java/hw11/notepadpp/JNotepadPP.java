package hr.fer.zemris.java.hw11.notepadpp;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.Collator;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.notepadpp.local.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.notepadpp.local.ILocalizationListener;
import hr.fer.zemris.java.hw11.notepadpp.local.LocalizationProvider;
import hr.fer.zemris.java.hw11.notepadpp.local.components.LAction;
import hr.fer.zemris.java.hw11.notepadpp.local.components.LJMenu;

/**
 * Simple notepad implementation.
 * <p>
 * JNotepad++ is standard text editor which supports multi-tabbed environment
 * and various text editing tools such as: cut, copy, paste, sorting, letter
 * case changing and duplicate line removal.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public class JNotepadPP extends JFrame {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Frame title
	 */
	private static final String TITLE = "JNotepad++";

	/**
	 * Document change indicator icon
	 */
	private static final ImageIcon DOCUMENT_CHANGED_ICON = UtilityMethods.loadImage("icons/edited.png");

	/**
	 * Document saved indicator icon
	 */
	private static final ImageIcon DOCUMENT_SAVED_ICON = UtilityMethods.loadImage("icons/saved.png");

	/**
	 * New document icon
	 */
	private static final ImageIcon NEW_DOCUMENT_ICON = UtilityMethods.loadImage("icons/new.png");

	/**
	 * Open document icon
	 */
	private static final ImageIcon OPEN_DOCUMENT_ICON = UtilityMethods.loadImage("icons/open.png");

	/**
	 * Save document icon
	 */
	private static final ImageIcon SAVE_DOCUMENT_ICON = UtilityMethods.loadImage("icons/save.png");

	/**
	 * Cut tool icon
	 */
	private static final ImageIcon CUT_ICON = UtilityMethods.loadImage("icons/cut.png");

	/**
	 * Copy tool icon
	 */
	private static final ImageIcon COPY_ICON = UtilityMethods.loadImage("icons/copy.png");

	/**
	 * Paste tool icon
	 */
	private static final ImageIcon PASTE_ICON = UtilityMethods.loadImage("icons/paste.png");

	/**
	 * Tabbed component - organizes multiple text editors in one tabbed
	 * environment
	 */
	private JTabbedPane tabPane;

	/**
	 * Statusbar component - displays basic stats at bottom of frame
	 */
	private StatusBar statusBar;

	/**
	 * List of currently opened text editors
	 */
	private List<NotepadTextArea> editors;

	/**
	 * Internal clipboard used for cut/copy/paste options
	 */
	private String clipboard;

	/**
	 * Current dot position
	 */
	private int dot;

	/**
	 * Current mark position
	 */
	private int mark;

	/**
	 * Localization provider bounded to main form
	 */
	private FormLocalizationProvider flp = new FormLocalizationProvider(LocalizationProvider.getInstance(), this);

	/**
	 * New document action.
	 */
	private Action newDocumentAction = new LAction("new", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			JPanel content = new JPanel(new BorderLayout());
			NotepadTextArea textArea = getNewEditor(null,"");
			// NotepadTextArea textArea = new NotepadTextArea(null, "");
			content.add(new JScrollPane(textArea));
			editors.add(textArea);
			tabPane.insertTab(flp.getString("untitleddoc"), DOCUMENT_SAVED_ICON, content, flp.getString("untitleddoc"),
					tabPane.getTabCount());
			tabPane.setSelectedIndex(tabPane.getTabCount() - 1);

		}
	};

	/**
	 * Action allows user to open existing file through file chooser.
	 */
	private Action openDocumentAction = new LAction("open", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {

			JFileChooser fc = new JFileChooser();
			fc.setDialogTitle(flp.getString("openfile"));

			if (fc.showOpenDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
				return;
			}

			File fileName = fc.getSelectedFile();
			Path filePath = fileName.toPath();

			for (int i = 0, n = editors.size(); i < n; ++i) {
				NotepadTextArea area = editors.get(i);
				if (filePath.equals(area.getPath())) {
					tabPane.setSelectedIndex(i);
					return;
				}
			}

			if (!Files.isReadable(filePath)) {
				JOptionPane.showMessageDialog(JNotepadPP.this,
						flp.getString("file")+ " " + fileName.getAbsolutePath() + " " + flp.getString("notexist"), flp.getString("error"),
						JOptionPane.ERROR_MESSAGE);
				return;
			}

			byte[] okteti;
			try {
				okteti = Files.readAllBytes(filePath);
			} catch (Exception ex) {
				JOptionPane.showMessageDialog(JNotepadPP.this,
						flp.getString("errorwhilereading") + " " + fileName.getAbsolutePath(), flp.getString("error"),
						JOptionPane.ERROR_MESSAGE);
				return;
			}

			String tekst = new String(okteti, StandardCharsets.UTF_8);
			JPanel content = new JPanel(new BorderLayout());
			NotepadTextArea textArea = getNewEditor(filePath, tekst);
			content.add(new JScrollPane(textArea));

			editors.add(textArea);
			tabPane.addTab(fileName.getName(), content);
			tabPane.setIconAt(tabPane.getTabCount() - 1, DOCUMENT_SAVED_ICON);
			tabPane.setSelectedIndex(tabPane.getTabCount() - 1);

		}

	};

	/**
	 * Action saves current editor's content to existing location.
	 */
	private Action saveDocumentAction = new LAction("save", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			int index = tabPane.getSelectedIndex();
			NotepadTextArea editor = editors.get(index);
			Path openedFilePath = editor.getPath();
			String fileName = null;

			if (openedFilePath == null) {
				JFileChooser jfc = new JFileChooser();
				jfc.setDialogTitle(flp.getString("savedocumentas"));
				if (jfc.showSaveDialog(JNotepadPP.this) != JFileChooser.APPROVE_OPTION) {
					return;
				}
				openedFilePath = jfc.getSelectedFile().toPath();
				if (Files.exists(openedFilePath)) {
					fileName = openedFilePath.toFile().getName();
					int status = JOptionPane.showConfirmDialog(JNotepadPP.this,
							flp.getString("file") + " " + fileName + " " + flp.getString("alreadyexistsoverwrite"), flp.getString("warning"),
							JOptionPane.WARNING_MESSAGE);
					if (status != JOptionPane.OK_OPTION) {
						return;
					}
				}
			}

			if (fileName == null) {
				fileName = openedFilePath.getFileName().toString();
			}

			byte[] podatci = editor.getText().getBytes(StandardCharsets.UTF_8);
			try {
				Files.write(openedFilePath, podatci);
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(JNotepadPP.this,
						flp.getString("errorwhilewritingfile") + " " + openedFilePath.toFile().getAbsolutePath(),
						flp.getString("error"), JOptionPane.ERROR_MESSAGE);
				return;
			}

			if (editor.isEdited()) {
				tabPane.setIconAt(index, DOCUMENT_SAVED_ICON);
				editor.setEdited(false);
			}

			String path = openedFilePath.toAbsolutePath().toString();
			tabPane.setTitleAt(index, fileName);
			tabPane.setToolTipTextAt(index, path);
			setTitle(path + " - " + "JNotepad++");

			editor.setPath(openedFilePath);

		}
	};

	/**
	 * Save As action saves current editor's content to location chosen by user
	 * through file chooser form.
	 */
	private Action saveAsDocumentAction = new LAction("saveas", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			int index = tabPane.getSelectedIndex();
			NotepadTextArea textArea = editors.get(index);
			Path backupPath = textArea.getPath();
			textArea.setPath(null);
			saveDocumentAction.actionPerformed(e);

			// if Save As action was cancelled
			if (textArea.getPath() == null) {
				textArea.setPath(backupPath);
			}
		}
	};

	/**
	 * Close document action closes currently opened editor (if any opened).
	 */
	private Action closeDocumentAction = new LAction("close", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			int index = tabPane.getSelectedIndex();
			NotepadTextArea editor = editors.get(index);

			if (editor.isEdited()) {

				int status = JOptionPane.showConfirmDialog(JNotepadPP.this,
						flp.getString("savechangestodocument")+ " \"" + tabPane.getTitleAt(index) + "\" "+ flp.getString("beforeclosing") + "?", flp.getString("warning"),
						JOptionPane.YES_NO_CANCEL_OPTION);

				if (status == JOptionPane.CANCEL_OPTION) {
					return;
				} else if (status == JOptionPane.YES_OPTION) {
					saveDocumentAction.actionPerformed(e);
					if (editor.isEdited()) {
						return;
					}
				}

			}

			tabPane.remove(index);
			editors.remove(index);

		}

	};

	/**
	 * Action detaches main frame from native handle and exits application.
	 */
	private Action exitAction = new LAction("exit", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {

			int lastTabIndex = tabPane.getTabCount() - 1;
			tabPane.setSelectedIndex(lastTabIndex);

			while (tabPane.getTabCount() > 0) {
				closeDocumentAction.actionPerformed(e);
				if (lastTabIndex == tabPane.getTabCount() - 1)
					return;
				lastTabIndex--;
			}

			statusBar.shutdown();
			dispose();
		}

	};

	/**
	 * Cuts selected part of text (if any)
	 */
	private Action cutSelectedPartAction = new LAction("cut", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			extractText(true, true);
		}

	};

	/**
	 * Copies selected part of text (if any) to internal clipboard memory.
	 */
	private Action copySelectedPartAction = new LAction("copy", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			extractText(false, true);
		}

	};

	/**
	 * Pastes clipboard content to current position in text editor.
	 */
	private Action pasteFromClipboardAction = new LAction("paste", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			if (clipboard.isEmpty())
				return;
			int index = tabPane.getSelectedIndex();
			NotepadTextArea editor = editors.get(index);
			Document doc = editor.getDocument();
			try {
				doc.insertString(editor.getCaretPosition(), clipboard, null);
			} catch (BadLocationException e1) {
				e1.printStackTrace();
			}
		}

	};

	/**
	 * Displays statistical information of currently opened document.
	 */
	private Action statisticalInfoAction = new LAction("statistics", flp) {

		private static final long serialVersionUID = 1L;

		public void actionPerformed(ActionEvent e) {
			int index = tabPane.getSelectedIndex();
			NotepadTextArea editor = editors.get(index);
			Document doc = editor.getDocument();

			String text = null;
			try {
				text = doc.getText(0, doc.getLength());
			} catch (BadLocationException ex) {
				ex.printStackTrace();
			}

			int allCharacters = text.length();
			int nonBlankCharacters = 0;
			int numberOflines = (allCharacters == 0) ? 0 : 1;

			for (int i = 0, n = text.length(); i < n; ++i) {
				char c = text.charAt(i);
				if (c == '\n')
					numberOflines++;
				if (!Character.isWhitespace(c))
					nonBlankCharacters++;
			}

			JOptionPane.showMessageDialog(JNotepadPP.this,
					flp.getString("yourdocumenthas") + " " + allCharacters + " " + flp.getString("characters") + ", " + nonBlankCharacters
							+ " " + flp.getString("nonblankcharactersand") + " " + numberOflines + " " + flp.getString("lines"),
					flp.getString("statistics"), JOptionPane.INFORMATION_MESSAGE, null);
		}
	};

	/**
	 * Changes selected text to uppercase form
	 */
	private Action toUpperCaseAction = new LAction("touppercase", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			NotepadTextArea editor = editors.get(tabPane.getSelectedIndex());
			String text = extractText(true, false);
			String edited = changeCase(text, Character::toUpperCase);
			editor.insert(edited, dot);
			editor.setCaretPosition(dot);
		}
	};

	/**
	 * Changes selected text to lower case form
	 */
	private Action toLowerCaseAction = new LAction("tolowercase", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			NotepadTextArea editor = editors.get(tabPane.getSelectedIndex());
			String text = extractText(true, false);
			String edited = changeCase(text, Character::toLowerCase);
			editor.insert(edited, dot);
			editor.setCaretPosition(dot);
		}
	};

	/**
	 * Inverts the case of selected part of text
	 */
	private Action switchCaseAction = new LAction("switchcase", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			NotepadTextArea editor = editors.get(tabPane.getSelectedIndex());
			String text = extractText(true, false);
			String edited = changeCase(text, c -> {
				if (Character.isUpperCase(c))
					return Character.toLowerCase(c);
				if (Character.isLowerCase(c))
					return Character.toUpperCase(c);
				return c;
			});
			editor.insert(edited, dot);
			editor.setCaretPosition(dot);
		}
	};

	/**
	 * Sorts selected lines of text in their natural order
	 */
	private Action sortAscendingAction = new LAction("sortasc", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			editors.get(tabPane.getSelectedIndex())
					.insert(UtilityMethods.getSortedLines(getSelectedLines(),
							(s1, s2) -> Collator.getInstance(new Locale(flp.getString("locale"))).compare(s1, s2)),
							dot);
		}
	};

	/**
	 * Sorts selected lines of text in descending alphabetical order
	 */
	private Action sortDescendingAction = new LAction("sortdesc", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			editors.get(tabPane.getSelectedIndex())
					.insert(UtilityMethods.getSortedLines(getSelectedLines(),
							(s1, s2) -> Collator.getInstance(new Locale(flp.getString("locale"))).compare(s2, s1)),
							dot);
		}
	};

	/**
	 * Deletes all duplicate lines selected lines
	 */
	private Action uniqueLinesAction = new LAction("unique", flp) {

		private static final long serialVersionUID = 1L;

		@Override
		public void actionPerformed(ActionEvent e) {
			List<String> lines = getSelectedLines().stream().distinct().collect(Collectors.toList());
			String uniqueLines = UtilityMethods.concatenateLines(lines);
			editors.get(tabPane.getSelectedIndex()).insert(uniqueLines, dot);
		}
	};

	/**
	 * Default constructor initializes JNotepad to default zero-tab state.
	 */
	public JNotepadPP() {

		tabPane = new JTabbedPane();
		tabPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		editors = new LinkedList<>();
		statusBar = new StatusBar(editors, tabPane);
		clipboard = "";

		tabPane.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {

				int tabs = tabPane.getTabCount();
				boolean hasTabs = tabs > 0;

				saveDocumentAction.setEnabled(hasTabs);
				closeDocumentAction.setEnabled(hasTabs);
				saveAsDocumentAction.setEnabled(hasTabs);
				statisticalInfoAction.setEnabled(hasTabs);
				pasteFromClipboardAction.setEnabled(hasTabs && !clipboard.isEmpty());

				String title = "JNotepad++";

				if (tabPane.getTabCount() > 0) {
					NotepadTextArea editor = editors.get(tabPane.getSelectedIndex());
					if (editor.getPath() != null)
						title = editor.getPath().toAbsolutePath().toString() + " - " + title;
					dot = editor.getCaret().getDot();
					mark = editor.getCaret().getMark();
				}

				toUpperCaseAction.setEnabled(dot != mark);
				toLowerCaseAction.setEnabled(dot != mark);
				switchCaseAction.setEnabled(dot != mark);
				uniqueLinesAction.setEnabled(dot != mark);
				sortAscendingAction.setEnabled(dot != mark);
				sortDescendingAction.setEnabled(dot != mark);

				setTitle(title);
				statusBar.update();
			}

		});

		flp.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				pack();
			}
		});

		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exitAction.actionPerformed(null);
			}
		});

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException
				| ClassNotFoundException e1) {
			e1.printStackTrace();
		}

		setTitle(TITLE);
		initGUI();
		setLocationRelativeTo(null); // center
	}

	/**
	 * Sets up scene and positions main gui components
	 */
	private void initGUI() {
		setLayout(new BorderLayout());

		JPanel content = new JPanel(new BorderLayout());
		content.add(tabPane);
		// content.add(statusBar, BorderLayout.SOUTH);

		add(content);
		add(statusBar, BorderLayout.SOUTH);

		createMenus();
		createActions();

		int width = createToolbars(content);

		setMinimumSize(new Dimension(width, 300));
	}

	/**
	 * Creates menu bar and menu item hierarchy
	 */
	private void createMenus() {

		JMenuBar menuBar = new JMenuBar();

		///////// File Menu ///////////////////////////
		LJMenu fileMenu = new LJMenu("file", flp);
		// JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);
		fileMenu.add(new JMenuItem(newDocumentAction));
		fileMenu.add(new JMenuItem(openDocumentAction));

		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(saveDocumentAction));
		fileMenu.add(new JMenuItem(saveAsDocumentAction));

		fileMenu.addSeparator();
		fileMenu.add(new JMenuItem(closeDocumentAction));
		fileMenu.add(new JMenuItem(exitAction));
		////////////////////////////////////////////////

		//////// Edit Menu /////////////////////////////
		LJMenu editMenu = new LJMenu("edit", flp);
		menuBar.add(editMenu);
		editMenu.add(new JMenuItem(cutSelectedPartAction));
		editMenu.add(new JMenuItem(copySelectedPartAction));
		editMenu.add(new JMenuItem(pasteFromClipboardAction));
		////////////////////////////////////////////////

		//////// Tools Menu ////////////////////////////
		// JMenu toolsMenu = new JMenu("Tools");
		JMenu toolsMenu = new LJMenu("tools", flp);

		JMenu changeCaseSubMenu = new LJMenu("changecase", flp);
		changeCaseSubMenu.add(new JMenuItem(toUpperCaseAction));
		changeCaseSubMenu.add(new JMenuItem(toLowerCaseAction));
		changeCaseSubMenu.add(new JMenuItem(switchCaseAction));

		JMenu sortMenu = new LJMenu("sort", flp);
		sortMenu.add(new JMenuItem(sortAscendingAction));
		sortMenu.add(new JMenuItem(sortDescendingAction));

		menuBar.add(toolsMenu);
		toolsMenu.add(new JMenuItem(statisticalInfoAction));
		toolsMenu.add(changeCaseSubMenu);
		toolsMenu.add(sortMenu);
		toolsMenu.add(new JMenuItem(uniqueLinesAction));
		/////////////////////////////////////////////////

		///////// Language Menu //////////////////////////
		LJMenu languageMenu = new LJMenu("language", flp);
		ButtonGroup languageGroup = new ButtonGroup();

		JRadioButtonMenuItem en = new JRadioButtonMenuItem(new LAction("english", flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				LocalizationProvider.getInstance().setLanguage("en");
			}
		});

		JRadioButtonMenuItem de = new JRadioButtonMenuItem(new LAction("german", flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				LocalizationProvider.getInstance().setLanguage("de");
			}
		});

		JRadioButtonMenuItem ar = new JRadioButtonMenuItem(new LAction("arabic", flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				LocalizationProvider.getInstance().setLanguage("ar");
			}
		});
		
		JRadioButtonMenuItem hr = new JRadioButtonMenuItem(new LAction("croatian", flp) {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				LocalizationProvider.getInstance().setLanguage("hr");
			}
		});

		languageGroup.add(en);
		languageGroup.add(de);
		languageGroup.add(ar);
		languageGroup.add(hr);
		en.setSelected(true);

		languageMenu.add(en);
		languageMenu.add(de);
		languageMenu.add(ar);
		languageMenu.add(hr);

		menuBar.add(languageMenu);
		///////////////////////////////////////////////////

		this.setJMenuBar(menuBar);
	}

	/**
	 * Creates toolbar and initializes it with notepad tools.
	 * 
	 * @param content
	 *            JPanel on which toolbar will be set
	 * @return returns preferred width of toolbar required to fit all the tools
	 */
	private int createToolbars(JPanel content) {
		JToolBar toolBar = new JToolBar("Tools");
		toolBar.setFloatable(true);

		toolBar.add(new JButton(newDocumentAction));
		toolBar.add(new JButton(openDocumentAction));
		toolBar.add(new JButton(saveDocumentAction));
		toolBar.addSeparator();

		toolBar.add(new JButton(cutSelectedPartAction));
		toolBar.add(new JButton(copySelectedPartAction));
		toolBar.add(new JButton(pasteFromClipboardAction));

		content.add(toolBar, BorderLayout.PAGE_START);
		return toolBar.getPreferredSize().width;
	}

	/**
	 * Initializes all actions with their confgurational constants such as key
	 * bind, mnemonic key and descriptions.
	 */
	private void createActions() {

		newDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control N"));
		newDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_N);
		newDocumentAction.putValue(Action.LARGE_ICON_KEY, NEW_DOCUMENT_ICON);

		openDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control O"));
		openDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_O);
		openDocumentAction.putValue(Action.LARGE_ICON_KEY, OPEN_DOCUMENT_ICON);

		saveDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
		saveDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		saveDocumentAction.putValue(Action.LARGE_ICON_KEY, SAVE_DOCUMENT_ICON);
		saveDocumentAction.setEnabled(false);

		saveAsDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift pressed S"));
		saveAsDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_A);
		saveAsDocumentAction.setEnabled(false);

		closeDocumentAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control W"));
		closeDocumentAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		closeDocumentAction.setEnabled(false);

		exitAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control Q"));
		exitAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_Q);

		cutSelectedPartAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift pressed X"));
		cutSelectedPartAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_T);
		cutSelectedPartAction.putValue(Action.LARGE_ICON_KEY, CUT_ICON);
		cutSelectedPartAction.setEnabled(false);

		copySelectedPartAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift pressed C"));
		copySelectedPartAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_C);
		copySelectedPartAction.putValue(Action.LARGE_ICON_KEY, COPY_ICON);
		copySelectedPartAction.setEnabled(false);

		pasteFromClipboardAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift pressed V"));
		pasteFromClipboardAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_P);
		pasteFromClipboardAction.putValue(Action.LARGE_ICON_KEY, PASTE_ICON);
		pasteFromClipboardAction.setEnabled(false);

		statisticalInfoAction.putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control shift pressed S"));
		statisticalInfoAction.putValue(Action.MNEMONIC_KEY, KeyEvent.VK_S);
		statisticalInfoAction.setEnabled(false);

		toUpperCaseAction.setEnabled(false);
		toLowerCaseAction.setEnabled(false);
		switchCaseAction.setEnabled(false);
		sortAscendingAction.setEnabled(false);
		sortDescendingAction.setEnabled(false);
		uniqueLinesAction.setEnabled(false);

	}

	/**
	 * Program entry point
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new JNotepadPP().setVisible(true);
		});
	}

	/**
	 * Extracts selected text from currently selected editor and returns it as
	 * String.
	 * 
	 * @param delete
	 *            If flag is true selected text is deleted from editor
	 * @param copyToClipboard
	 *            If flag is true selected text is copied to clipboard
	 * @return returns selected text as String
	 */
	private String extractText(boolean delete, boolean copyToClipboard) {
		NotepadTextArea editor = editors.get(tabPane.getSelectedIndex());
		Document doc = editor.getDocument();
		String text = "";
		int len = Math.abs(dot - mark);
		if (len == 0)
			return text;

		int offset = Math.min(dot, mark);
		try {
			text = doc.getText(offset, len);
			if (copyToClipboard) {
				clipboard = text;
				pasteFromClipboardAction.setEnabled(!clipboard.isEmpty());
			}
			if (delete) {
				doc.remove(offset, len);
			}
		} catch (BadLocationException e1) {
			e1.printStackTrace();
		}
		return text;
	}

	/**
	 * Changes case of {@code input} using given {@code changeCase} function.
	 * 
	 * @param input
	 *            Input string
	 * @param changeCase
	 *            Change case function by which change is done
	 * @return returns changed input
	 */
	private String changeCase(String input, Function<Character, Character> changeCase) {
		StringBuilder output = new StringBuilder();
		input.chars().forEach(c -> output.append(changeCase.apply((char) c)));
		return output.toString();
	}

	/**
	 * Creates instance of {@link NotepadTextArea} and attaches needed
	 * listeners.
	 * 
	 * @param path
	 *            Path to file on disk which will be loaded to text area
	 * @return returns instance of {@link NotepadTextArea}
	 */
	private NotepadTextArea getNewEditor(Path path, String text) {

		NotepadTextArea editor = new NotepadTextArea(path, text);

		editor.addCaretListener(new CaretListener() {
			@Override
			public void caretUpdate(CaretEvent e) {
				dot = e.getDot();
				mark = e.getMark();
				cutSelectedPartAction.setEnabled(dot != mark);
				copySelectedPartAction.setEnabled(dot != mark);
				toUpperCaseAction.setEnabled(dot != mark);
				toLowerCaseAction.setEnabled(dot != mark);
				switchCaseAction.setEnabled(dot != mark);
				sortAscendingAction.setEnabled(dot != mark);
				sortDescendingAction.setEnabled(dot != mark);
				uniqueLinesAction.setEnabled(dot != mark);
				statusBar.update();
			}
		});

		editor.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void insertUpdate(DocumentEvent e) {
				update();
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				update();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
			}

			private void update() {
				SwingUtilities.invokeLater(() -> {
					int index = tabPane.getSelectedIndex();
					if (!editor.isEdited()) {
						tabPane.setIconAt(index, DOCUMENT_CHANGED_ICON);
						editor.setEdited(true);
					}
					statusBar.update();
				});
			}
		});

		return editor;
	}

	/**
	 * Takes selected text and splits it to lines.
	 * 
	 * @return list of lines currently selected in editor
	 */
	private List<String> getSelectedLines() {
		NotepadTextArea editor = editors.get(tabPane.getSelectedIndex());

		try {

			int start = Math.min(dot, mark);
			int end = Math.max(dot, mark);

			start = editor.getLineStartOffset(editor.getLineOfOffset(start));
			end = editor.getLineEndOffset(editor.getLineOfOffset(end));

			String text = editor.getText(start, end);
			editor.getDocument().remove(start, end);

			dot = start;

			return Arrays.asList(text.split("\n"));
		} catch (BadLocationException e) {
			throw new RuntimeException();
		}
	}

}
