package hr.fer.zemris.java.hw11.notepadpp.local.components;

import javax.swing.JLabel;

import hr.fer.zemris.java.hw11.notepadpp.local.ILocalizationListener;
import hr.fer.zemris.java.hw11.notepadpp.local.ILocalizationProvider;

/**
 * Localizable {@link JLabel} implementation.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public class LJLabel extends JLabel {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Initializes label with locale specific data.
	 * 
	 * @param key
	 *            Access key
	 * @param provider
	 *            Localization provider
	 */
	public LJLabel(String key, ILocalizationProvider provider) {
		setText(provider.getString(key));

		provider.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				setText(provider.getString(key));
			}
		});
	}

}
