package hr.fer.zemris.java.hw11.notepadpp.local;

/**
 * Localization provider interface.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public interface ILocalizationProvider {
		
	/**
	 * Subscribes {@code listener} to this provider
 	 * @param listener {@link ILocalizationListener} listener
	 */
	void addLocalizationListener(ILocalizationListener listener);
	
	/**
	 * Detaches given {@code listener} from this provider.
	 * 
	 * @param listener listener to be removed 
	 */
	void removeLocalizationListener(ILocalizationListener listener);
	
	/**
	 * Gets value for given {@code key}
	 * 
	 * @param key key
	 * @return returns value mapped to given {@code key}
	 */
	String getString(String key);
}
