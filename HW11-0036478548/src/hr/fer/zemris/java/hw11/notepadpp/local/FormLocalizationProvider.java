package hr.fer.zemris.java.hw11.notepadpp.local;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

/**
 * Localization provider for one single frame.
 * <p>
 * Each frame in application should have it's own instance of this provider.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public class FormLocalizationProvider extends LocalizationProviderBridge {

	/**
	 * Initializes new form localization provider with given {@code parent} and
	 * attaches it to given frame.
	 * 
	 * @param parent
	 *            Parent provider
	 * @param frame
	 *            Frame for which provider will provide values
	 */
	public FormLocalizationProvider(ILocalizationProvider parent, JFrame frame) {
		super(parent);
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				connect();
			}

			@Override
			public void windowClosed(WindowEvent e) {
				disconnect();
			}
		});
	}

}
