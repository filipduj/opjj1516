package hr.fer.zemris.java.hw11.notepadpp.local;

/**
 * Localization listener interface.
 * <p>
 * Each listenr should implement localization changed method.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public interface ILocalizationListener {

	/**
	 * Defines procedure to do after provider has signaled that localization was
	 * changed.
	 */
	void localizationChanged();
}
