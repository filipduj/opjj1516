package hr.fer.zemris.java.hw11.notepadpp;

import java.nio.file.Path;

import javax.swing.JTextArea;

/**
 * Modified {@link JTextArea} implementation.
 * <p>
 * It keeps track of path bounded to text area, from which text is loaded.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public class NotepadTextArea extends JTextArea {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Path bounded to this text area
	 */
	private Path path;

	/**
	 * Flag indicates that text was edited
	 */
	private boolean edited;

	/**
	 * Default constructor creates path-less text editor
	 */
	public NotepadTextArea() {
		this(null);
	}

	/**
	 * Constructor intializes new text area with given {@code path} parameter
	 * 
	 * @param path
	 *            Path to file to load to text area
	 */
	public NotepadTextArea(Path path) {
		this(path, "");
	}

	/**
	 * Initializes new text area with given {@code path} and fills it with
	 * {@code text}.
	 * 
	 * @param path
	 *            Path to file bounded to this editor
	 * @param text
	 *            Text to load to editor
	 */
	public NotepadTextArea(Path path, String text) {
		this.path = path;
		setText(text);
	}

	/**
	 * @return returns path bounded to editor
	 */
	public Path getPath() {
		return path;
	}

	/**
	 * @param path
	 *            path to file this editor will be bounded to
	 */
	public void setPath(Path path) {
		this.path = path;
	}

	/**
	 * @return returns {@code true} if file was edited
	 */
	public boolean isEdited() {
		return edited;
	}

	/**
	 * Sets edited flag to given value
	 * 
	 * @param b
	 *            boolean flag
	 */
	public void setEdited(boolean b) {
		edited = b;
	}
}
