package hr.fer.zemris.java.hw11.notepadpp;

import java.awt.Color;
import java.awt.GridLayout;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

/**
 * Status bar component implementation.
 * <p>
 * Status bar displays basic document statistics such as line and column
 * position, total number of characters and length of selected part of text.
 * <p>
 * It also displays current date/time.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public class StatusBar extends JPanel {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * List of currently opened text editors
	 */
	private List<NotepadTextArea> editors;
	
	/**
	 * Tabbed component - organizes multiple text editors in one tabbed
	 * environment
	 */
	private JTabbedPane tabPane;

	/**
	 * Label which keeps total file length number
	 */
	private JLabel length;
	
	/**
	 * Label contains position data 
	 */
	private JLabel stats;
	
	/**
	 * Label which shows current date/time
	 */
	private JLabel clock;
	
	/**
	 * Date time string formatter
	 */
	private SimpleDateFormat df;
	
	/**
	 * Flag indicates that timer should be terminated
	 */
	private boolean shutdown;

	/**
	 * Constructor initializes new {@link StatusBar} with 
	 * given {@code editors} and {@code tabPane}
	 * 
	 * @param editors List of {@link NotepadTextArea} editors
	 * @param tabPane Tabbed pane container
	 */
	public StatusBar(List<NotepadTextArea> editors, JTabbedPane tabPane) {

		this.editors = editors;
		this.tabPane = tabPane;

		setLayout(new GridLayout(1, 3));
		setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
		df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		length = new JLabel("", SwingConstants.LEFT);
		stats = new JLabel("", SwingConstants.LEFT);
		clock = new JLabel("", SwingConstants.RIGHT);

		add(length);
		add(stats);
		add(clock);

		Thread timer = new Thread(() -> {
			while (true) {
				try {
					Thread.sleep(500);
				} catch (Exception ex) {
				}
				if(shutdown) break;
				SwingUtilities.invokeLater(() -> {
					clock.setText(df.format(new Date()));
				});
			}
		});

		timer.setDaemon(true);
		timer.start();

		update();

	}

	/**
	 * Updates displayed data when change in position
	 * or tabbed pane occures.
	 */
	public void update() {
		SwingUtilities.invokeLater(() -> {
			int len = 0;
			int l = 0;
			int c = 0;
			int s = 0;

			if (tabPane.getTabCount() > 0) {

				NotepadTextArea editor = editors.get(tabPane.getSelectedIndex());
				String text = editor.getText();
				int dot = editor.getCaret().getDot();
				int mark = editor.getCaret().getMark();

				len = text.length();
				s = Math.abs(dot - mark);

				for (int pos = 0; pos < dot; ++pos) {
					if (text.charAt(pos) == '\n') {
						c = 0;
						l++;
					} else {
						c++;
					}
				}

			}
			length.setText(String.format("length: %d", len));
			stats.setText(String.format("Ln: %d   Col: %d   Sel: %d", l, c, s));
		});
	}
	
	/**
	 * Terminates time updating.
	 * Usually called when exiting from application.
	 */
	public void shutdown(){
		shutdown = true;
	}
}