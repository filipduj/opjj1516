package hr.fer.zemris.java.hw11.notepadpp.local.components;

import javax.swing.JMenuItem;

import hr.fer.zemris.java.hw11.notepadpp.local.ILocalizationListener;
import hr.fer.zemris.java.hw11.notepadpp.local.ILocalizationProvider;

/**
 * Localizable {@link JMenuItem} implementation.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 26, 2016)
 */
public class LJMenuItem extends JMenuItem {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Initializes menu item with locale specific data.
	 * 
	 * @param key
	 *            Access key
	 * @param provider
	 *            Localization provider
	 */
	public LJMenuItem(String key, ILocalizationProvider provider) {
		setName(provider.getString(key));
		provider.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				setName(provider.getString(key));
			}
		});
	}

}
