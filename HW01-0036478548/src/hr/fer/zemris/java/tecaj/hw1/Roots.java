package hr.fer.zemris.java.tecaj.hw1;

import static java.lang.Math.*;

/**
 * Program calculates all requested roots of given complex number.
 * Three command line arguments are expected:
 * first: Real part of complex number (real number)
 * second: Imaginary part of complex number (real number)
 * third: n is the n-th roots to be calculated (natural number)
 * 
 * n roots are calculated in following way:
 * 
 * r = sqrt(real^2 + imaginary^2)
 * phi = atan2(imaginary/real)
 * 
 * for k in 0...n-1:
 * z_k = cos((phi + 2*k*pi)/n) + i* sin((phi + 2*k*pi)/n)
 * 
 * Every z_k is then printed to standard output in real+complex form
 * with component values rounded to three decimal places.
 *  
 * @author Filip Dujmušić
 *
 */
public class Roots {

	/**
	 * Entry point for program.
	 * Everything starts here.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {

		if(args.length != 3){
			System.err.println("Expected 3 arguments!");
			System.exit(0);
		}

		Double realPart = Double.parseDouble(args[0].trim());
		Double imagPart = Double.parseDouble(args[1].trim());
		int n = Integer.parseInt(args[2].trim());
		
		if(n <= 1){
			System.err.println("Third argument must be natural number greater than one!");
			System.exit(0);
		}

		double r = hypot(realPart, imagPart);
		double phi = atan2(imagPart, realPart);

		r = pow(r, 1.0 / n);

		System.out.printf("You requested calculation of %d. roots. Soulutions are: %n", n);
		for (int k = 0; k < n; ++k) {
		System.out.printf("%d) %.3f %+.3fi%n", k+1, r*cos((phi+2*k*PI)/n), r*sin((phi+2*k*PI)/n));
		}

	}

}
