package hr.fer.zemris.java.tecaj.hw1;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Program calculates perimeter and area of rectangle given its width and
 * height. Exactly two command line arguments can be passed while starting with
 * first representing width and second height. If two arguments are not provided
 * user can define them through standard input. Arguments must be nonnegative
 * numbers.
 * 
 * When width and height are defined (by standard input or command line
 * arguments) then program calculates and prints out perimeter and area to
 * standard output.
 * 
 * Formulas used: perimeter: P = 2 * (width + height) area: A = width * height
 * 
 * Results are rounded to three decimal places.
 * 
 * @author Filip Dujmušić
 *
 */
public class Rectangle {
	
	/**
	 * Reads number from standard input.
	 * Reads decimal number until nonnegative number was entered.
	 * 
	 * @param width if true user is defining width, otherwise height
	 * @param reader BufferedReader object used for input operations
	 * @return returns nonnegative real number entered by user
	 */
	private static double input(boolean width, BufferedReader reader) throws IOException {
		String line;
		String component = (width) ? "width" : "height";
		double value;

		while (true) {
			System.out.printf("Please provide %s: ", component);
			line = reader.readLine();

			if (line == null) {
				System.err.println("Stream is closed!");
				System.exit(0);
			}

			if (line.isEmpty()) {
				System.out.println("Nothing was given.");
				continue;
			}

			value = Double.parseDouble(line.trim());
			if (value < 0) {
				System.out.printf("%s is negative.%n", component);
				continue;
			}

			break;
		}

		return value;
	}

	/**
	 * Entry point for program. Everything starts here.
	 * 
	 * @param args width and height of rectangle
	 */
	public static void main(String[] args) throws IOException {

		double width, height;
		BufferedReader reader = new BufferedReader(new InputStreamReader(new BufferedInputStream(System.in)));

		// if width and height are provided as command line arguments then parse
		// them
		if (args.length == 2) {

			width = Double.parseDouble(args[0].trim());
			height = Double.parseDouble(args[1].trim());

			if (width < 0 || height < 0) {
				System.err.println("One or more arguments are negative! Exiting...");
				System.exit(0);
			}

		} else {
			width = input(true, reader);
			height = input(false, reader);
		}

		System.out.printf("You have specified a rectangle with width %.3f and height %.3f.%n", width, height);
		System.out.printf("Its area is %.3f and its perimeter is %.3f.%n", width * height, 2 * (width + height));

	}

}
