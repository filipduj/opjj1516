package hr.fer.zemris.java.tecaj.hw1;

/**
 * Program demonstrates simple list implementation in java. A school example of
 * difference between pointers/references in C and Java. Program first adds some
 * strings in simple list and prints out the original list, sorted list and list
 * size.
 */
public class ProgramListe {

	/**
	 * Base building block for list implementation. This class represents simple
	 * structure to be used for elements in a list.
	 */
	static class CvorListe {

		/**
		 * Holds reference to next element in the list. null if end of list.
		 */
		CvorListe sljedeci;

		/**
		 * The "actual data" in list element.
		 */
		String podatak;
	}

	/**
	 * Entry point for program. Everything starts here.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {
		CvorListe cvor = null;

		cvor = ubaci(cvor, "Jasna");
		cvor = ubaci(cvor, "Ana");
		cvor = ubaci(cvor, "Ivana");

		System.out.println("Ispisujem listu uz originalni poredak:");
		ispisiListu(cvor);

		cvor = sortirajListu(cvor);

		System.out.println("Ispisujem listu nakon sortiranja:");
		ispisiListu(cvor);

		int vel = velicinaListe(cvor);
		System.out.println("Lista sadrzi elemenata: " + vel);

	}

	/**
	 * Calculates number of elements in list. Recursively iterates through list
	 * until end of list is reached.
	 * 
	 * @param cvor reference to first element of list
	 * @return returns size of list as integer (zero if list is empty)
	 */
	private static int velicinaListe(CvorListe cvor) {
		if (cvor == null) {
			return 0;
		}
		return 1 + velicinaListe(cvor.sljedeci);
	}

	/**
	 * Inserts element in the list. Element is inserted at very beginning of
	 * list and complexity of this operation is O(1). This element becomes first
	 * element and reference to list itself.
	 * 
	 * @param prvi first element of list
	 * @param podatak string to be inserted in list
	 * @return returns modified list with podatak as first element
	 */
	private static CvorListe ubaci(CvorListe prvi, String podatak) {
		CvorListe novi = new CvorListe();
		novi.podatak = podatak;
		novi.sljedeci = prvi;
		return novi;
	}

	/**
	 * Prints out list elements to standard output. Recursively iterates through
	 * list elements and prints each element in new line, until end of list is
	 * reached (denoted by null).
	 * 
	 * @param cvor first element of list
	 */
	private static void ispisiListu(CvorListe cvor) {
		if (cvor != null) {
			System.out.println(cvor.podatak);
			ispisiListu(cvor.sljedeci);
		}
	}

	/**
	 * Sorts list in ascending order. Algorithm used is simple bubble sort with
	 * worst case complexity of O(n^2).
	 * https://en.wikipedia.org/wiki/Bubble_sort 
	 * String comparison is not case sensitive 
	 * therefore "name" == "NaMe"
	 * 
	 * @param cvor first element of list
	 * @return returns first element of sorted list in ascending order, null if list is empty
	 */
	private static CvorListe sortirajListu(CvorListe cvor) {
		boolean sorted;
		CvorListe temp = null, prethodni = null, ret = cvor;
		int compare;

		if (velicinaListe(cvor) < 2) {
			return cvor;
		}

		do {
			sorted = true;
			while (cvor != null && cvor.sljedeci != null) {
				compare = cvor.podatak.compareToIgnoreCase(cvor.sljedeci.podatak);
				if (compare > 0) {
					if (prethodni != null) {
						prethodni.sljedeci = cvor.sljedeci;
					} else {
						ret = cvor.sljedeci;
					}
					temp = cvor.sljedeci;
					cvor.sljedeci = cvor.sljedeci.sljedeci;
					temp.sljedeci = cvor;

					prethodni = temp;
					sorted = false;
				} else {
					prethodni = cvor;
					cvor = cvor.sljedeci;
				}
			}
			cvor = ret;
			prethodni = null;
		} while (!sorted);
		return ret;
	}
}
