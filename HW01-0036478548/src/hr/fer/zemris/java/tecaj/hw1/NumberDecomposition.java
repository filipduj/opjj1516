package hr.fer.zemris.java.tecaj.hw1;

/**
 * Program decomposes number to its prime factors.
 * Prime number is positive integer divisible only by 1 and itself.
 * Program expects one command line argument representing positive
 * integer.
 * This program calculates decomposition 
 * of given number to its prime factors and prints the result in list form.
 * For example if number 12 was provided as command line argument
 * expected output would be:
 * 
 * 	1) 2
 * 	2) 2
 * 	3) 3
 * 
 * By multiplying these factors we get original input: 2*2*3 = 12 
 * 
 * @author Filip Dujmušić
 * @version 1.0
 */
public class NumberDecomposition {

	/**
	 * Entry point for program.
	 * Everything starts here.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			System.err.println("Wrong number of arguments!");
			System.exit(0);
		}

		int n = Integer.parseInt(args[0].trim());

		if (n <= 1) {
			System.err.println("Argument must be natural number greater than 1!");
			System.exit(0);
		}

		int cnt = 1;
		int div = 2;

		System.out.printf("You requested decomposition of number %d onto prime factors. Here they are:%n", n);
		while (n > 1) {
			while (n % div == 0) {
				n = n / div;
				System.out.printf("%d. %d%n", cnt++, div);
			}
			div++;
		}

	}

}
