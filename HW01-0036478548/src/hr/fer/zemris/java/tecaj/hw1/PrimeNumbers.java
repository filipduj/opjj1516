package hr.fer.zemris.java.tecaj.hw1;

/**
 * Program calculates first n prime numbers.
 * Prime number is natural number greater than 1 with
 * property of being divisible only by 1 and itself.
 * 
 * There are infinitely many prime numbers 
 * (https://primes.utm.edu/notes/proofs/infinite/euclids.html)
 * and this program calculates only first n of them.
 * n is command line argument passed to program on the start.
 * 
 * For example if n=5 expected output is:
 * 
 * 	1) 2
 * 	2) 3
 * 	3) 5
 * 	4) 7
 * 	5) 11
 * 
 * @author Filip Dujmušić
 * @version 1.0
 */
public class PrimeNumbers {
	
	/**
	 * Checks if number is prime.
	 * Method uses simple Trial division approach to check if x is prime.
	 * More about Trial division on wiki: https://en.wikipedia.org/wiki/Trial_division
	 * @param x positive integer to be tested if it is prime or not
	 * @return true if x is prime, false otherwise
	 */
	private static boolean isPrime(int x) {
		if(x == 1)
			return false;
		
		for (int i = 2; i <= Math.sqrt(x); ++i) {
			if (x % i == 0) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Entry point for program.
	 * Everything starts here.
	 * 
	 * @param args command line arguments
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			System.err.println("Wrong number of arguments!");
			System.exit(0);
		}

		int n = Integer.parseInt(args[0].trim());
		int cnt = 0;

		if (n <= 0) {
			System.err.println("Argument must be positive integer!");
			System.exit(0);
		}

		System.out.printf("You requested calculation of %d prime numbers. Here they are:%n", n);
		for (int i = 2;; i++) {
			if (cnt >= n) {
				break;
			}
			if (isPrime(i)) {
				++cnt;
				System.out.printf("%d. %d%n", cnt, i);
			}
		}

	}

}
