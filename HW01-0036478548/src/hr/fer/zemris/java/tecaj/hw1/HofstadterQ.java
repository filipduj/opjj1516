package hr.fer.zemris.java.tecaj.hw1;

/**
 * Program calculates i-th number of Hofstadter's Q sequence. One command line
 * argument is expected to be passed. Argument i is positive integer and
 * represents i-th number of Hofstadter's Q sequence to be calculated.
 * 
 * @author Filip Dujmušić
 * @version 1.0
 */
public class HofstadterQ {

	/**
	 * This method calculates i-th number of H's Q sequence by recursion.
	 * Recursive formula: Q(i) = Q(i - Q(i-1)) + Q(i - Q(i-2)) with Q(1) = Q(2)
	 * = 1 Works reasonably fast for i up to 40. See more at
	 * http://mathworld.wolfram.com/HofstadtersQ-Sequence.html
	 * 
	 * @param i
	 *            represents i-th number of Hofstadter's Q sequence to be
	 *            calculated (i is positive integer)
	 * @return returns i-th Hofstadter's number
	 */
	private static long hofstadterQ(long i) {
		if (i < 3) {
			return 1;
		}
		return hofstadterQ(i - hofstadterQ(i - 1)) + hofstadterQ(i - hofstadterQ(i - 2));
	}

	/**
	 * Entry point for program. Everything starts here.
	 * 
	 * @param args
	 *            command line arguments
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			System.err.println("Wrong number of arguments!");
			System.exit(0);
		}

		long in = Long.parseLong(args[0].trim());

		if (in <= 0) {
			System.err.println("Argument must be positive!");
			System.exit(0);
		}

		System.out.printf("You requested calculation of %d. number of Hofstadter's Q-sequence.%n", in);
		System.out.printf("The requested number is %d.%n", hofstadterQ(in));

	}

}
