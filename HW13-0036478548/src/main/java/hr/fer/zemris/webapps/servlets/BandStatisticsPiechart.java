package hr.fer.zemris.webapps.servlets;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

/**
 * Servlet generates simple pie chart showing vote points distribution among
 * band names with at least one vote.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 8, 2016)
 */
@WebServlet(name = "bandPiechart", urlPatterns = { "/glasanje-grafika" })
public class BandStatisticsPiechart extends HttpServlet {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		@SuppressWarnings("unchecked")
		PieDataset dataset = createDataset((Map<String, String>) req.getServletContext().getAttribute("results"));
		JFreeChart chart = createChart(dataset, "Band votes distribution");
		ImageIO.write(chart.createBufferedImage(400, 300), "png", resp.getOutputStream());
	}

	/**
	 * Takes given {@code dataset} and {@code title} and generates new
	 * {@link JFreeChart} object.
	 * 
	 * @param dataset
	 *            Dataset for chart plotting
	 * @param title
	 *            Chart title
	 * @return Returns {@link JFreeChart} object built from given data
	 */
	private JFreeChart createChart(PieDataset dataset, String title) {
		JFreeChart chart = ChartFactory.createPieChart3D(title, // chart title
				dataset, // data
				true, // include legend
				true, false);

		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;
	}

	/**
	 * Creates dataset for band votes piechart. Considers all bands with nonzero
	 * number of votes and calculates their relative percentage of total number
	 * of votes.
	 * <p>
	 * Final dataset is map of band names mapped to it's relative percentage of
	 * total number of votes.
	 * 
	 * @param map
	 *            Band names mapped to their number of votes
	 * @return Returns {@link PieDataset} with percentage assigned to each band
	 *         name
	 */
	private PieDataset createDataset(Map<String, String> map) {
		DefaultPieDataset result = new DefaultPieDataset();

		int sum = 0;
		for (String r : map.values()) {
			sum += Integer.valueOf(r);
		}

		int currentSum = 0;
		Iterator<Map.Entry<String, String>> it = map.entrySet().iterator();
		Map.Entry<String, String> entry;
		while (it.hasNext()) {
			entry = it.next();
			int entryValue = Integer.valueOf(entry.getValue());
			if (entryValue == 0)
				continue;
			if (!it.hasNext()) {
				result.setValue(entry.getKey(), 100 - currentSum);
			} else {
				int value = entryValue * 100 / sum;
				result.setValue(entry.getKey(), value);
				currentSum += value;
			}
		}

		return result;
	}

}
