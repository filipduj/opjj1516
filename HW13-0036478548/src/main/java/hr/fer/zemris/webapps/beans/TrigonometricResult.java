package hr.fer.zemris.webapps.beans;

/**
 * Java bean structure wraps up one result of trigonometric servlet
 * calculations.
 * <p>
 * It contains argument value, sine of argument and cosine of argument results
 * available through public getters and can be used in EL expressions.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 8, 2016)
 */
public class TrigonometricResult {

	/**
	 * Argument for trigonometric functions (in degrees)
	 */
	private int x;

	/**
	 * Sine value of argument
	 */
	private double sine;

	/**
	 * Cosine value of argument
	 */
	private double cosine;

	/**
	 * Constructor initializes new bean with argument and function values for
	 * that argument.
	 * 
	 * @param x
	 *            Function argument
	 */
	public TrigonometricResult(int x) {
		this.x = x;
		this.sine = Math.sin(x * Math.PI / 180);
		this.cosine = Math.cos(x * Math.PI / 180);
	}

	/**
	 * @return the sine value
	 */
	public double getSine() {
		return sine;
	}

	/**
	 * @return the cosine value
	 */
	public double getCosine() {
		return cosine;
	}

	/**
	 * @return returns function argument value
	 */
	public int getX() {
		return x;
	}

}
