package hr.fer.zemris.webapps.servlets.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Utility class provides few helper static methods used on various places in
 * project.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 8, 2016)
 */
public class Utility {

	/**
	 * Method creates new file on disk with given {@code filePath} and writes
	 * each entry of given {@code map} to it's own line.
	 * 
	 * @param filePath
	 *            Full path newly generated file
	 * @param map
	 *            Map whos content will be written into file
	 */
	public static void createNewResults(String filePath, Map<String, String> map) {
		try {
			FileWriter writer = new FileWriter(filePath);
			for (String id : map.keySet()) {
				writer.write(id + "\t0\n");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Reads results file from given {@code filePath} and increments vote points
	 * for entry with identificator equal to given {@code id}.
	 * 
	 * @param filePath
	 *            Path to vote results file
	 * @param id
	 *            ID of entry whose score will be incremented
	 */
	public static synchronized void updateExistingResults(String filePath, String id) {

		List<String> records = new LinkedList<>();
		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(new FileInputStream(filePath), StandardCharsets.UTF_8))) {

			// read results line by line and modify voted one
			String line;
			while ((line = reader.readLine()) != null) {
				String[] tokens = line.split("\\t+");
				if (tokens[0].equals(id)) {
					line = (tokens[0]) + "\t" + (Integer.parseInt(tokens[1]) + 1);
				}
				records.add(line);
			}

			// write modified results file to the same path
			File file = Paths.get(filePath).toFile();
			FileWriter writer = new FileWriter(file);
			for (String r : records) {
				writer.write(r + "\n");
			}
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Method attempts to parse {@code parameter} with name {@code arg} received
	 * from GET request to integer. If {@code parameter} is not set, or not
	 * parsable error message is appended to {@code err} string builder.
	 * <p>
	 * Also method checks if integer value is between given {@code lower} and
	 * {@code upper} bounds and writes error to {@code err} if neccessary.
	 * 
	 * @param parameter
	 *            Parameter value from GET request
	 * @param arg
	 *            Parameter name from GET request
	 * @param lower
	 *            Required lower bound for parameter value
	 * @param upper
	 *            Required upper bound for parameter value
	 * @param err
	 *            String builder for error reporting
	 * @return Returns integer if {@code parameter} is parsable or {@code null}
	 */
	public static Integer parseParameter(String parameter, String arg, int lower, int upper, StringBuilder err) {
		Integer ret;

		// check if parameter set
		if (parameter == null) {
			err.append("Argument " + arg + " not provided!<br>");
			return null;
		}

		// check if parameter is parsable integer
		try {
			ret = Integer.parseInt(parameter);
		} catch (NumberFormatException e) {
			err.append("Argument " + arg + " is not integer!<br>");
			return null;
		}

		// check if lower and upper bounds satisfiable
		if (ret < lower || ret > upper) {
			err.append("Expected argument " + arg + " to be in interval [" + lower + "," + upper + "] but " + ret
					+ " given!<br>");
		}

		return ret;
	}

	/**
	 * Method takes {@code start} point in time and calculates difference
	 * between current moment and that point. Resulting difference is formatted
	 * in pretty days/hours/minutes/seconds output.
	 * 
	 * @param start
	 *            Starting point in time from which time is measured.
	 * @return Returns formatted time difference between current time and
	 *         {@code start} time
	 */
	public static String formatTimeDifference(long start) {
		long diff = System.currentTimeMillis() - start;
		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		long diffDays = diff / (24 * 60 * 60 * 1000);
		return ((diffDays == 0) ? "" : (diffDays + " days, "))
				+ ((diffHours == 0 && diffDays == 0) ? "" : (diffHours + " hours, "))
				+ ((diffHours == 0 && diffDays == 0 && diffMinutes == 0) ? "" : (diffMinutes + " minutes, "))
				+ diffSeconds + " seconds";
	}

}
