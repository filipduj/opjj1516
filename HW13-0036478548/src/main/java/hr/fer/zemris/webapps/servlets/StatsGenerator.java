package hr.fer.zemris.webapps.servlets;

import java.io.IOException;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

/**
 * Servlet generates simple pie chart that shows usage statistics for popular
 * operating systems: Windows, Linux and Mac.
 * <p>
 * Disclaimer: Statistics showed does not represent real values.
 * 
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 8, 2016)
 */
@WebServlet(name = "pieChart", urlPatterns = { "/reportImage" })
public class StatsGenerator extends HttpServlet {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PieDataset dataset = createDataset();
		JFreeChart chart = createChart(dataset, "OS usage statistics");
		ImageIO.write(chart.createBufferedImage(400, 300), "png", resp.getOutputStream());
	}

	/**
	 * Takes given {@code dataset} and {@code title} and generates new
	 * {@link JFreeChart} object.
	 * 
	 * @param dataset
	 *            Dataset for chart plotting
	 * @param title
	 *            Chart title
	 * @return Returns {@link JFreeChart} object built from given data
	 */
	private JFreeChart createChart(PieDataset dataset, String title) {
		JFreeChart chart = ChartFactory.createPieChart3D(title, // chart title
				dataset, // data
				true, // include legend
				true, false);

		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;
	}

	/**
	 * Creates dataset for three operating systems and their usage percentages.
	 * 
	 * @return {@link PieDataset} dataset
	 */
	private PieDataset createDataset() {
		DefaultPieDataset result = new DefaultPieDataset();
		result.setValue("Linux", 90);
		result.setValue("Mac", 7);
		result.setValue("Windows", 3);
		return result;
	}

}
