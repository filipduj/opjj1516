package hr.fer.zemris.webapps.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet starts voting process by forwarding request
 * to jsp used for rendering voting options and vote links.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 8, 2016)
 */
@WebServlet(name = "glasanje", urlPatterns ={"/glasanje"})
public class PollServlet extends HttpServlet {
	
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setAttribute("bands", getServletContext().getAttribute("bands"));
		req.setAttribute("bandsLinks", getServletContext().getAttribute("bandsLinks"));
		req.getRequestDispatcher("WEB-INF/pages/glasanjeIndex.jsp").forward(req, resp);
	}
}
