package hr.fer.zemris.webapps.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet allows dynamic background color change.
 * <p>
 * Takes input color parameter and sets chosen background color
 * to current session map of attributes.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 8, 2016)
 */
@WebServlet(name="colorChooser", urlPatterns={"/setcolor"})
public class ColorChooser extends HttpServlet {
	
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getSession().setAttribute("pickedBgCol", req.getParameter("color"));
		resp.sendRedirect("index.jsp");
	}
}
