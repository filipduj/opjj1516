package hr.fer.zemris.webapps.servlets;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.webapps.beans.TrigonometricResult;

/**
 * Servlet calculates sine and cosine function values for each integer between
 * given a and b parameters.
 * <p>
 * Results are then packed into {@link TrigonometricResult} beans and sent to
 * jsp page for rendering.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 8, 2016)
 */
@WebServlet(name = "trigonometricCalculator", urlPatterns = { "/trigonometric" })
public class TrigonometricCalculator extends HttpServlet {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		int a, b;
		int param1 = 0;
		int param2 = 0;

		try {
			param1 = getParam(req.getParameter("a"), 0);
			param2 = getParam(req.getParameter("b"), 360);
		} catch (NumberFormatException e) {
			req.setAttribute("errorMessage", "You must enter integers in input boxes (or leave them empty)!");
			req.getRequestDispatcher("WEB-INF/pages/error.jsp").forward(req, resp);
		}

		a = Math.min(param1, param2);
		b = Math.max(param1, param2);

		if (b > (a + 720))
			b = a + 720;

		List<TrigonometricResult> values = new LinkedList<>();
		for (int x = a; x <= b; ++x) {
			values.add(new TrigonometricResult(x));
		}

		req.setAttribute("trigonometricValues", values);
		req.getRequestDispatcher("WEB-INF/pages/trigonometric.jsp").forward(req, resp);
	}

	/**
	 * Helper method for retrieving parameter value from get request. If
	 * parameter {@code s} is not set {@code defaultValue} is returned.
	 * Otherwise attempt is made to parse {@code s} as integer, and if
	 * successful, that integer is returned.
	 * 
	 * @param s
	 *            Parameter received from GET request
	 * @param defaultValue
	 *            Default value to return if {@code s} is not set
	 * @return Returns parameter parsed to integer or {@code defaultValue} if
	 *         parameter is not set
	 */
	private int getParam(String s, int defaultValue) {
		if (s == null || s.isEmpty())
			return defaultValue;
		return Integer.parseInt(s);
	}

}
