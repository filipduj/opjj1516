package hr.fer.zemris.webapps.servlets;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Defines basic actions to do upon initialization and destruction of web
 * context.
 * <p>
 * Loads band names for voting and saves current time of context initialization.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 8, 2016)
 */
@WebListener
public class ApplicationListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		sce.getServletContext().setAttribute("pickedBgCol", "white"); // default
																		// bg
																		// color
																		// if
																		// none
																		// picked
		sce.getServletContext().setAttribute("startupTime", System.currentTimeMillis());

		String fileName = sce.getServletContext().getRealPath("/WEB-INF/glasanje-definicija.txt");
		Path path = Paths.get(fileName);

		Map<String, String> bandNames = new LinkedHashMap<>();
		Map<String, String> bandLinks = new LinkedHashMap<>();
		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(new FileInputStream(path.toString()), StandardCharsets.UTF_8))) {
			String line;
			while ((line = reader.readLine()) != null) {
				String[] tokens = line.split("\\t+");
				bandNames.put(tokens[0], tokens[1]);
				bandLinks.put(tokens[1], tokens[2]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		sce.getServletContext().setAttribute("bands", bandNames);
		sce.getServletContext().setAttribute("bandsLinks", bandLinks);
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// no jobs
	}

}
