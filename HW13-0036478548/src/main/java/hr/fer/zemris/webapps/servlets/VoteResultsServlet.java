package hr.fer.zemris.webapps.servlets;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.webapps.servlets.util.Utility;

/**
 * This servlet analyzes current vote points 
 * for bands and determines winners.
 * <p>
 * There can be multiple winners if every band
 * has same number of vote points.
 * <p>
 * After generating winner list, request is forwarded
 * to jsp for rendering.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 8, 2016)
 */
@WebServlet(name="glasanjeRezultati", urlPatterns={"/glasanje-rezultati"})
public class VoteResultsServlet extends HttpServlet {
	
	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		Map<String, String> bands        = (Map<String, String>)req.getServletContext().getAttribute("bands");
		Map<String, String> bandsLinks   = (Map<String, String>)req.getServletContext().getAttribute("bandsLinks");
		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
		
		if(!Files.exists(Paths.get(fileName))){
			Utility.createNewResults(fileName, bands);
		}
		
		Map<String, String> results 	 = null;
		Map<String, String> winners      = new HashMap<>();
		
		int maxPoints = 0;
		try(BufferedReader reader = new BufferedReader(
										new InputStreamReader(
											new FileInputStream(fileName), StandardCharsets.UTF_8))){
			String line;
			results = new LinkedHashMap<>();
			while((line = reader.readLine()) != null){
				String[] tokens = line.split("\\t+");
				results.put(bands.get(tokens[0]), tokens[1]);
				int points = Integer.parseInt(tokens[1]);
				if(points>maxPoints) maxPoints = points;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		String maxResult = String.valueOf(maxPoints);
		results.forEach((k,v) -> {
			if(maxResult.equals(v)){
				winners.put(k, bandsLinks.get(k));
			}	
		});
		
		req.setAttribute("winners", winners);
		req.getServletContext().setAttribute("results", results);
		
		req.getRequestDispatcher("WEB-INF/pages/glasanjeRez.jsp").forward(req, resp);
	
	}

}
