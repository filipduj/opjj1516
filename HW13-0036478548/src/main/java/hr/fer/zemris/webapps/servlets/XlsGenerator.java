package hr.fer.zemris.webapps.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;

import hr.fer.zemris.webapps.servlets.util.Utility;

/**
 * Simple servlet that generates xls file out of given a,b,n integer parameters.
 * <p>
 * Parameter n defines number of sheets in generated xls. On each sheet there
 * are two columns, in first are numbers from a to b. In second column for each
 * number a to b is it's calculated power. Index of sheet defines power to which
 * numbers are raised.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 8, 2016)
 */
@WebServlet(name = "xlsGenerator", urlPatterns = { "/powers" })
public class XlsGenerator extends HttpServlet {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		StringBuilder errorMessage = new StringBuilder();

		Integer paramA = Utility.parseParameter(req.getParameter("a"), "a", -100, 100, errorMessage);
		Integer paramB = Utility.parseParameter(req.getParameter("b"), "b", -100, 100, errorMessage);
		Integer n 	   = Utility.parseParameter(req.getParameter("n"), "n",    1,   5, errorMessage);
	
		if (errorMessage.length() != 0) {
			req.setAttribute("errorMessage", errorMessage.toString());
			req.getRequestDispatcher("WEB-INF/pages/error.jsp").forward(req, resp);
		}
		
		int a = Math.min(paramA, paramB);
		int b = Math.max(paramA, paramB);

		Workbook workbook = new HSSFWorkbook();
		for (int s = 1; s <= n; ++s) {
			Sheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("Sheet " + s));
			Row rowhead = sheet.createRow(0);
			rowhead.createCell(0).setCellValue("n");
			rowhead.createCell(1).setCellValue("n^" + s);
			int rowCount = 1;
			for (int i = a; i <= b; ++i) {
				Row row = sheet.createRow(rowCount++);
				row.createCell(0).setCellValue(i);
				row.createCell(1).setCellValue(Math.pow(i, s));
			}
		}

		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; " + "filename=\"powers.xls\"");
		workbook.write(resp.getOutputStream());
		workbook.close();
	}
}
