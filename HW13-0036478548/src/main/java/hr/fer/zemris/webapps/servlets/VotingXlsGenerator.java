package hr.fer.zemris.webapps.servlets;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;

/**
 * Servlet that generates simple xls file containing band names and number of
 * vote points for each band.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 8, 2016)
 */
@WebServlet(name = "votingXls", urlPatterns = { "/glasanje-xls" })
public class VotingXlsGenerator extends HttpServlet {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// create new workbook and one sheet
		Workbook workbook = new HSSFWorkbook();
		Sheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("Results"));

		// add beginning description row
		Row rowhead = sheet.createRow(0);
		rowhead.createCell(0).setCellValue("Band name");
		rowhead.createCell(1).setCellValue("Number of votes");

		// add result rows
		int rowCount = 1;
		for (Map.Entry<String, String> entry : ((Map<String, String>) req.getServletContext().getAttribute("results"))
				.entrySet()) {
			Row row = sheet.createRow(rowCount++);
			row.createCell(0).setCellValue(entry.getKey());
			row.createCell(1).setCellValue(entry.getValue());
		}

		// write to output stream
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; " + "filename=\"results.xls\"");
		workbook.write(resp.getOutputStream());
		workbook.close();
	}

}
