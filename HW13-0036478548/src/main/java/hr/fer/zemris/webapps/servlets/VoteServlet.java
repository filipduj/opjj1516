package hr.fer.zemris.webapps.servlets;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.webapps.servlets.util.Utility;


/**
 * Servlet processes one vote request and increments
 * selected band's vote points.
 * <p>
 * Requested band's id is sent as GET parameter.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 8, 2016)
 */
@WebServlet(name = "glasanjeGlasaj", urlPatterns = { "/glasanje-glasaj" })
public class VoteServlet extends HttpServlet {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String fileName = req.getServletContext().getRealPath("/WEB-INF/glasanje-rezultati.txt");
		@SuppressWarnings("unchecked")
		Map<String, String> bands = (Map<String, String>)req.getServletContext().getAttribute("bands");
		
		Path results = Paths.get(fileName);
		
		if (!Files.exists(results)) {
			Utility.createNewResults(fileName, bands);
		} 
		
		Utility.updateExistingResults(fileName, req.getParameter("id"));
		resp.sendRedirect(req.getContextPath() + "/glasanje-rezultati");
	}
}
