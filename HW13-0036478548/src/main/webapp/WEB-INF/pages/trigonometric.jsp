<%@page import="java.util.List" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	
	<head>
		<title>OPJJ1516 - HW13 - Trigonometry</title>
	</head>
	
	<body bgcolor="${pickedBgCol}">
	
		<a href="index.jsp">Go Home</a>
	
		<table border = "1">
		
			<tr>
				<th>x</th>
				<th>sin(x)</th>
				<th>cos(x)</th>	
			</tr>
			
			<c:forEach items="${trigonometricValues}" var="result">
			<tr>
				<td>${result.x}</td>
				<td>${result.sine}</td>
				<td>${result.cosine}</td>
			</tr>
			</c:forEach>

		</table>
		
	</body>

</html>