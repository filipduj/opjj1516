<%@page import="java.util.Map" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	
	<head>
		<title>OPJJ1516 - HW13 - Vote poll</title>	
	</head>
	
	<body bgcolor="${pickedBgCol}">
		<a href="index.jsp">Go Home</a>
		<br>
		<h1>Glasanje za omiljeni bend:</h1>
		<p>Od sljedećih bendova, koji Vam je bend najdraži? Kliknite na link kako biste glasali!</p>
		<ol>
			<c:forEach items="${bands}" var="band">
			<li><a href="glasanje-glasaj?id=${band.key}">${band.value}</a></li>
			</c:forEach>
		</ol>
	</body>

</html>