<%@ page session="true" %>

<html>
	<head>
		<title>OPJJ1516 - HW13 - WebApp</title>
	</head>	
	
	<body bgcolor="${pickedBgCol}">
		
		<h2>Welcome!</h2>
		<ul>
		<li><a href="colors.jsp">Background color chooser</a></li>
		<li><a href="stories/funny.jsp">Funny story</a></li>
		<li><a href="report.jsp">OS usage report</a></li>
		<li><a href="appinfo.jsp">Application uptime</a></li>
		<li><a href="glasanje">Vote for band</a></li>
		</ul>
		<br>		
	 	<form action = "trigonometric">
			<fieldset>
			<legend>Trigonometrija</legend>
				a: <input type = "text" name = "a" value="0">
				b: <input type = "text" name = "b" value="90">  
				   <input type="submit" value="Izracunaj">
				  
			</fieldset>
		</form>
		<br>		
	 	<form action = "powers">
			<fieldset>
			<legend>XLS generator</legend>
				a: <input type = "text" name = "a" value="1">
				b: <input type = "text" name = "b" value="100">
				n: <input type = "text" name = "n" value="3">   
				   <input type="submit" value="Generiraj">
			</fieldset>
		</form>
		<p>${trigErrorMessage}</p>
	
	</body>
	
</html>
