<%@page session="true" import="hr.fer.zemris.webapps.servlets.util.Utility"%>

<html>

	<head>
		<title>OPJJ1516 - HW13 - Uptime</title>
	</head>

	<body bgcolor="${pickedBgCol}">
	<a href="index.jsp">Go Home</a>
	<br>
	<h2>Application uptime</h2>

		<p>
			Application is up for 
				<%= Utility.formatTimeDifference((long)pageContext.getServletContext().getAttribute("startupTime"))%> 
		</p> 
	<br>
	</body>


</html>