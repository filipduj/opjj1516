<%@ page session = "true" %>

<html>
	<head>
		<title>OPJJ1516 - HW13 - Color Chooser</title>
	</head>
	
	<body bgcolor="${pickedBgCol}">
		<a href="index.jsp">Go Home</a>
		<br>
		<h2>Color picker</h2>
		<ul>
			<li><a href="setcolor?color=white">WHITE</a></li>
			<li><a href="setcolor?color=red">RED</a></li>
			<li><a href="setcolor?color=green">GREEN</a></li>
			<li><a href="setcolor?color=cyan">CYAN</a></li>
		</ul>
	</body>

</html>