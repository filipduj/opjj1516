<%@ page import="java.util.Random" session="true"%>

<html>

	<head>
		<title>OPJJ1516 - HW13 - Funny Story</title>
	</head>
	
	<a href="../index.jsp">Go Home</a>
	<br>
	<a href="funny.jsp">Refresh</a>
	<br>
	<body bgcolor="${pickedBgCol}">
	
	<div style = "color: #<%= String.format("%06X", new Random().nextInt(0xFFFFFF))%>">
	<p>The great logician Bertrand Russell once claimed that he could prove anything if given that 1+1=1.</p>
    <p>So one day, some smarty-pants asked him, "Ok.  Prove that
you're the Pope."</p>
    <p>He thought for a while and proclaimed, "I am one.  The Pope
is one.  Therefore, the Pope and I are one."</p>
	</div>
	</body>


</html>