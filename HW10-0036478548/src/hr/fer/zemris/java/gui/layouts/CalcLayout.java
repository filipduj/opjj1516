package hr.fer.zemris.java.gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager2;
import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Specific "Calculator" layout implementation.
 * <p>
 * Manager can lay out up to 31 Component in a grid of 5 rows and 7 columns
 * where each component has same width and same height, except for one 'big'
 * component spaning from (1,1) to (1,5) - boundaries inclusive.
 * <p>
 * Valid positions are (i,j) where i=1..5 and j=1..7, except positions (1,2)
 * (1,3) (1,4) (1,5)! These positions are already occupied by one big component
 * spanning from (1,1) to (1,5) inclusive.
 * <p>
 * Each Component is layed out using it's {@link RCPosition} constraint, which
 * describes i and j attributes.
 * <p>
 * Adding component to already occupied position, or position out of bounds will
 * cause exception.
 * 
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class CalcLayout implements LayoutManager2 {

	/**
	 * Number of rows
	 */
	private static final int NUM_OF_ROWS = 5;

	/**
	 * Number of columns
	 */
	private static final int NUM_OF_COLS = 7;

	/**
	 * Number of consecutive fields which large component occupies in one row
	 */
	private static final int LARGE_COMPONENT_COL_SPAN = 5;

	/**
	 * Row position of large component (zero indexed)
	 */
	private static final int LARGE_COMPONENT_ROW_POS = 0;

	/**
	 * Column position of large component (zero indexed)
	 */
	private static final int LARGE_COMPONENT_COL_POS = 0;

	/**
	 * Default horizontal and vertical gap between neighbour components
	 */
	private static final int DEFAULT_GAP = 10;

	/**
	 * References to components this layout manager is taking care of
	 */
	private Component[][] components;

	/**
	 * Horizontal/vertical gep between components (in pixels)
	 */
	private int gap;

	/**
	 * Initializes new calculator layout manager with default gap value.
	 */
	public CalcLayout() {
		this(DEFAULT_GAP);
	}

	/**
	 * Initializes new calculator layout manager with given horiontal/vertical
	 * {@code gap} value.
	 * 
	 * @param gap
	 *            Horizontal and vertical gap between neighbour compononets
	 * @throws IllegalArgumentException
	 *             If {@code gap} is negative
	 */
	public CalcLayout(int gap) {
		if (gap < 0) {
			throw new IllegalArgumentException("Expected nonnegative gap but received " + gap);
		}
		this.gap = gap;
		this.components = new Component[NUM_OF_ROWS][NUM_OF_COLS];
	}

	@Override
	public void addLayoutComponent(String name, Component comp) {

	}

	@Override
	public void removeLayoutComponent(Component comp) {
		for (int i = 0; i < NUM_OF_ROWS; ++i) {
			for (int j = 0; j < NUM_OF_COLS; ++j) {
				if (comp == components[i][j])
					components[i][j] = null;
			}
		}
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		return layoutSize(parent, Component::getPreferredSize, Math::max);
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		return layoutSize(parent, Component::getMinimumSize, Math::max);

	}

	@Override
	public void layoutContainer(Container parent) {
		Insets ins = parent.getInsets();
		Dimension maxValues = getMaxValues(Component::getPreferredSize, Math::max);

		double scaleX = (parent.getWidth() - ins.left - ins.right - gap * (NUM_OF_COLS - 1))  / (maxValues.getWidth() * NUM_OF_COLS);
		double scaleY = (parent.getHeight() - ins.top - ins.bottom - gap * (NUM_OF_ROWS - 1)) / (maxValues.getHeight() * NUM_OF_ROWS);

		int colWidth  = (int) (maxValues.width * scaleX);
		int rowHeight = (int) (maxValues.height * scaleY);

		for (int i = 0; i < NUM_OF_ROWS; ++i) {
			for (int j = 0; j < NUM_OF_COLS; ++j) {
				if (components[i][j] != null) {
					if (i == LARGE_COMPONENT_ROW_POS && j == LARGE_COMPONENT_COL_POS) {
						components[i][j].setBounds(ins.left + LARGE_COMPONENT_COL_POS * (gap + colWidth),
												   ins.top + LARGE_COMPONENT_ROW_POS * (gap + rowHeight),
												   LARGE_COMPONENT_COL_SPAN * colWidth + (LARGE_COMPONENT_COL_SPAN - 1) * gap, 
												   rowHeight);
					} else {
						components[i][j].setBounds(ins.left + j * (gap + colWidth), 
								                   ins.top + i * (gap + rowHeight),
								                   colWidth, 
								                   rowHeight);
					}
				}
			}
		}
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		RCPosition position;
		if (constraints instanceof String) {
			String[] args = ((String) constraints).trim().split(",");
			if (args.length != 2) {
				throw new IllegalArgumentException(
						"First parameter should be ordered pair x,y but " + (String) constraints + " given!");
			}
			try {
				position = new RCPosition(Integer.parseInt(args[0]), Integer.parseInt(args[1]));
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("First number is not parsable ordered pair x,y!");
			}
		} else if (constraints instanceof RCPosition) {
			position = (RCPosition) constraints;
		} else {
			throw new IllegalArgumentException("Constraints argument must be instance of RCPosition or String!");
		}

		int row = position.getRow() - 1;
		int col = position.getColumn() - 1;

		if (row < 0 || row >= NUM_OF_ROWS || col < 0 || col >= NUM_OF_COLS || (col > LARGE_COMPONENT_COL_POS
				&& col < LARGE_COMPONENT_COL_POS + LARGE_COMPONENT_COL_SPAN && row == LARGE_COMPONENT_ROW_POS)) {

			throw new IllegalArgumentException("Constraint " + constraints + " position out of layout bounds!");
		}

		if (components[row][col] != null) {
			throw new IllegalArgumentException("Component bound by constraint " + constraints + " already exists!");
		}

		components[row][col] = comp;
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		return layoutSize(target, Component::getMaximumSize, Math::min);
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0.5f;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0.5f;
	}

	@Override
	public void invalidateLayout(Container target) {

	}

	/**
	 * Determines layout size using given {@code sizeGetter} and
	 * {@code function}.
	 * <p>
	 * {@code sizeGetter} defines relevant component property and
	 * {@code function} defines how to handle that property for every component
	 * this layout manager is responsible for.
	 * <p>
	 * For example {@code sizeGetter} can be getter for component's
	 * preferredDimension, and function can be Max from {@link Math} class. In
	 * this case method will return size such that it can contain 7x5 components
	 * with size equal to maximum preferred dimension of all components.
	 * 
	 * @param target
	 *            Container associated with this layout manager
	 * @param sizeGetter
	 *            component property getter (functional interface)
	 * @param function
	 *            function to apply to every component's property determined by
	 *            {@code sizeGetter}
	 * @return returns optimal dimension for container to fit all the components
	 */
	private Dimension layoutSize(Container target, Function<Component, Dimension> sizeGetter,
			BiFunction<Integer, Integer, Integer> function) {

		Dimension maxValues = getMaxValues(sizeGetter, function);

		int rowHeight = maxValues.height;
		int colWidth = maxValues.width;

		Insets ins = target.getInsets();
		return new Dimension(ins.left + ins.right + NUM_OF_COLS * colWidth + (NUM_OF_COLS - 1) * gap,
				ins.left + ins.right + NUM_OF_ROWS * rowHeight + (NUM_OF_ROWS - 1) * gap);

	}

	/**
	 * Maximazes every component's property defined by {@code sizeGetter} using
	 * provided {@code function}.
	 * 
	 * @param sizeGetter
	 *            component property getter
	 * @param function
	 *            maximizing function
	 * @return returns maximal Dimension
	 */
	private Dimension getMaxValues(Function<Component, Dimension> sizeGetter,
			BiFunction<Integer, Integer, Integer> function) {

		Dimension dimension = new Dimension(0, 0);
		for (int i = 0; i < NUM_OF_ROWS; ++i) {
			for (int j = 0; j < NUM_OF_COLS; ++j) {
				if (components[i][j] != null) {
					Dimension dim = sizeGetter.apply(components[i][j]);
					if (dim != null) {
						if (i == LARGE_COMPONENT_ROW_POS && j == LARGE_COMPONENT_COL_POS) {
							dim.width /= LARGE_COMPONENT_COL_SPAN;
						}
						dimension.width  = function.apply(dim.width,  dimension.width);
						dimension.height = function.apply(dim.height, dimension.height);
					}
				}
			}
		}
		return dimension;
	}
}
