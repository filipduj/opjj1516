package hr.fer.zemris.java.gui.layouts;

/**
 * Constraint used in {@link CalcLayout} manager.
 * <p>
 * Defines two read-only properties which determine row and column positions of
 * Component layed out with CalcLayout manager.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class RCPosition {
	
	/**
	 * Row position of component
	 */
	private int row;
	
	/**
	 * Column position of component
	 */
	private int column;

	/**
	 * Constructor initializes new RCPosition 
	 * constraint with given {@code row} and {@code column}
	 * values.
	 * 
	 * @param row Row position
	 * @param column Column position
	 */
	public RCPosition(int row, int column) {
		this.row = row;
		this.column = column;
	}

	/**
	 * @return the row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @return the column
	 */
	public int getColumn() {
		return column;
	}

	@Override
	public String toString() {
		return row + "," + column;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + column;
		result = prime * result + row;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof RCPosition)) {
			return false;
		}
		RCPosition other = (RCPosition) obj;
		return column == other.column && row == other.row;
	}

}
