package hr.fer.zemris.java.gui.calc.view;

import static hr.fer.zemris.java.gui.calc.components.CalcComponents.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Map;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

import hr.fer.zemris.java.gui.calc.components.CalcButton;
import hr.fer.zemris.java.gui.calc.components.CalcScreen;
import hr.fer.zemris.java.gui.calc.model.CalculatorModel;
import hr.fer.zemris.java.gui.layouts.CalcLayout;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * Calculator component class.
 * <p>
 * Represents main UI class which models one simple calculator.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class CalculatorGUI extends JFrame {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 664849451202298273L;

	/**
	 * Calculator model
	 */
	private static final CalculatorModel MODEL = new CalculatorModel();

	/**
	 * All non changing buttons map
	 */
	private static final Map<RCPosition, CalcButton> BUTTONS = getButtons(MODEL);

	/**
	 * Unary function buttons map
	 */
	private static final Map<RCPosition, CalcButton> FUNCTIONS = getFunctions(MODEL);

	/**
	 * Inverse unary function buttons map
	 */
	private static final Map<RCPosition, CalcButton> INVERSE_FUNCTIONS = getInverseFunctions(MODEL);

	/**
	 * Default constructor initializes new calculator User Interface.
	 */
	public CalculatorGUI() {

		setLayout(new CalcLayout(10));
		initGUI();
		setMinimumSize(getLayout().minimumLayoutSize(this));
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Simple Calculator v1.0");
		pack();

		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				setMinimumSize(getLayout().minimumLayoutSize(CalculatorGUI.this));
			}
		});

	}

	/**
	 * Method adds all necessary components to calculator component pane.
	 */
	private void initGUI() {

		// add screen to calculator
		add(new CalcScreen(MODEL), new RCPosition(1, 1));

		// add all standard buttons
		addButtons(BUTTONS);

		// add functions that have inverses
		addButtons(FUNCTIONS);

		// add inverting check box
		JCheckBox checkBox = new JCheckBox("Inv");
		checkBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (checkBox.isSelected()) {
					removeButtons(FUNCTIONS);
					addButtons(INVERSE_FUNCTIONS);
				} else {
					removeButtons(INVERSE_FUNCTIONS);
					addButtons(FUNCTIONS);
				}
				setMinimumSize(getLayout().minimumLayoutSize(CalculatorGUI.this));
				revalidate();
				repaint();
			}
		});

		add(checkBox, new RCPosition(5, 7));
	}

	/**
	 * Adds all buttons from given map to layout.
	 * 
	 * @param buttons buttons map
	 */
	private void addButtons(Map<RCPosition, CalcButton> buttons) {
		for (Map.Entry<RCPosition, CalcButton> entry : buttons.entrySet()) {
			add(entry.getValue(), entry.getKey());
		}
	}

	/**
	 * Removes all buttons in given map from layout. 
	 * 
	 * @param buttons buttons map
	 */
	private void removeButtons(Map<RCPosition, CalcButton> buttons) {
		for (Map.Entry<RCPosition, CalcButton> entry : buttons.entrySet()) {
			remove(entry.getValue());
		}
	}

}
