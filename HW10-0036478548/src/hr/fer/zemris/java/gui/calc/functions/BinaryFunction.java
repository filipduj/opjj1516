package hr.fer.zemris.java.gui.calc.functions;

import java.awt.event.ActionEvent;
import java.util.function.BiFunction;

import javax.swing.AbstractAction;

import hr.fer.zemris.java.gui.calc.model.CalculatorModel;

/**
 * Implementation of generic action which groups together all binary functions.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class BinaryFunction extends AbstractAction {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -47791671976826699L;

	/**
	 * Model of calculator
	 */
	private CalculatorModel model;

	/**
	 * Specific function that this action performs
	 */
	private BiFunction<Double, Double, Double> function;

	/**
	 * Constructor intializes new binary function action.
	 * 
	 * @param text
	 *            Text to bound to this action (function abbreviation)
	 * @param function
	 *            Function which this action will perform
	 * @param model
	 *            Model from which function reads operands
	 */
	public BinaryFunction(String text, BiFunction<Double, Double, Double> function, CalculatorModel model) {
		super(text);
		this.function = function;
		this.model = model;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		BiFunction<Double, Double, Double> f = model.getCalculatingFunction();

		if (f == null) {
			Double screenValue = model.getScreenValue();
			if (screenValue != null) {
				model.setFirstOperand(model.getScreenValue());
				model.setCalculatingFunction(function);
				model.setResultOnDisplay(true);

			}
		} else {
			Double result = f.apply(model.getFirstOperand(), model.getScreenValue());
			model.setScreenValue(result);
			model.setFirstOperand(result);
			model.setCalculatingFunction(function);
			model.setResultOnDisplay(true);
		}

	}

}
