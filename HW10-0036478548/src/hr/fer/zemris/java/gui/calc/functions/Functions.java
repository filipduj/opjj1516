package hr.fer.zemris.java.gui.calc.functions;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * Class groups all mathematical functions as static constants.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class Functions {

	/**
	 * Sine function
	 */
	public static final Function<Double, Double> SINE = Math::sin;

	/**
	 * Cosine function
	 */
	public static final Function<Double, Double> COSINE = Math::cos;

	/**
	 * Tangent function
	 */
	public static final Function<Double, Double> TAN = Math::tan;

	/**
	 * Cotangent function
	 */
	public static final Function<Double, Double> COTAN = x -> 1.0 / (Math.tan(x));

	/**
	 * Logarithm function with base 10
	 */
	public static final Function<Double, Double> LOG = Math::log10;

	/**
	 * Natural logarithm function
	 */
	public static final Function<Double, Double> LN = Math::log;

	/**
	 * Inverse sine function
	 */
	public static final Function<Double, Double> ASINE = Math::asin;

	/**
	 * Inverse cosine function
	 */
	public static final Function<Double, Double> ACOSINE = Math::acos;

	/**
	 * Inverse tangent function
	 */
	public static final Function<Double, Double> ATAN = Math::atan;

	/**
	 * Inverse cotangent function
	 */
	public static final Function<Double, Double> ACOTAN = x -> (Math.atan(1.0 / x));

	/**
	 * Exponential function with base e
	 */
	public static final Function<Double, Double> EXP = Math::exp;

	/**
	 * Exponential function with base 10
	 */
	public static final Function<Double, Double> EXP10 = x -> Math.pow(10, x);

	/**
	 * Function calculates reciprocal value of number
	 */
	public static final Function<Double, Double> INVERSE = x -> (1 / x);

	/**
	 * Function multiplies two values
	 */
	public static final BiFunction<Double, Double, Double> MUL = (x, y) -> x * y;

	/**
	 * Function first by second value
	 */
	public static final BiFunction<Double, Double, Double> DIV = (x, y) -> x / y;

	/**
	 * Function adds two values
	 */
	public static final BiFunction<Double, Double, Double> ADD = (x, y) -> x + y;

	/**
	 * Function subtracts second value from first
	 */
	public static final BiFunction<Double, Double, Double> SUB = (x, y) -> x - y;

	/**
	 * Function raises first operand to the power of second
	 */
	public static final BiFunction<Double, Double, Double> POW = (x, y) -> Math.pow(x, y);

}
