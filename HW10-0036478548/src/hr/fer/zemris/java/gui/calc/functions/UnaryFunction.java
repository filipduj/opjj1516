package hr.fer.zemris.java.gui.calc.functions;

import java.awt.event.ActionEvent;
import java.util.function.Function;

import javax.swing.AbstractAction;

import hr.fer.zemris.java.gui.calc.model.CalculatorModel;

/**
 * Implementation of generic action which groups together all unary functions.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class UnaryFunction extends AbstractAction {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -4825923650877158413L;

	/**
	 * Model of calculator
	 */
	private CalculatorModel model;

	/**
	 * Specific function that this action performs
	 */
	private Function<Double, Double> function;

	/**
	 * Constructor intializes new unary function action.
	 * 
	 * @param text
	 *            Text to bound to this action (function abbreviation)
	 * @param function
	 *            Function which this action will perform
	 * @param model
	 *            Model from which function reads operands
	 */
	public UnaryFunction(String text, Function<Double, Double> function, CalculatorModel model) {
		super(text);
		this.model = model;
		this.function = function;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Double result = function.apply(model.getScreenValue());
		model.setScreenValue(result);
		model.setResultOnDisplay(true);
		model.setFirstOperand(result);
		model.setCalculatingFunction(null);
	}

}
