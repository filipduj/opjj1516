package hr.fer.zemris.java.gui.calc.components;

import static hr.fer.zemris.java.gui.calc.functions.Functions.*;

import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;

import javax.swing.AbstractAction;
import javax.swing.JOptionPane;

import hr.fer.zemris.java.gui.calc.functions.BinaryFunction;
import hr.fer.zemris.java.gui.calc.functions.ValueFunction;
import hr.fer.zemris.java.gui.calc.model.CalculatorModel;
import hr.fer.zemris.java.gui.calc.functions.UnaryFunction;
import hr.fer.zemris.java.gui.layouts.RCPosition;

/**
 * Calculator components factory.
 * <p>
 * Class provides methods for retrieving all calculator interface components.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class CalcComponents {

	/**
	 * All non-changing buttons on calculator UI
	 */
	private static Map<RCPosition, CalcButton> BUTTONS;

	/**
	 * Unary function buttons
	 */
	private static Map<RCPosition, CalcButton> FUNCTIONS;

	/**
	 * Inverse unary function buttons
	 */
	private static Map<RCPosition, CalcButton> INVERSE_FUNCTIONS;

	/**
	 * Returns map of constraint,button pairs of non changing buttons
	 * 
	 * @param model
	 *            Calculator model
	 * @return map of constraint,button pairs
	 */
	public static Map<RCPosition, CalcButton> getButtons(CalculatorModel model) {
		if (BUTTONS == null) {
			BUTTONS = getButtonsMap(model);
		}
		return BUTTONS;
	}

	/**
	 * Returns map of constraint,button pairs of unary function buttons
	 * 
	 * @param model
	 *            Calculator model
	 * @return map of constraint,button pairs
	 */
	public static Map<RCPosition, CalcButton> getFunctions(CalculatorModel model) {
		if (FUNCTIONS == null) {
			FUNCTIONS = getFunctionsMap(model);
		}
		return FUNCTIONS;
	}

	/**
	 * Returns map of constraint,button pairs of inverse unary function buttons
	 * 
	 * @param model
	 *            Calculator model
	 * @return map of constraint,button pairs
	 */
	public static Map<RCPosition, CalcButton> getInverseFunctions(CalculatorModel model) {
		if (INVERSE_FUNCTIONS == null) {
			INVERSE_FUNCTIONS = getInverseFunctionsMap(model);
		}
		return INVERSE_FUNCTIONS;
	}

	/**
	 * Returns map of constraint,button pairs of non changing buttons
	 * 
	 * @param model
	 *            Calculator model
	 * @return map of constraint,button pairs
	 */
	private static Map<RCPosition, CalcButton> getButtonsMap(CalculatorModel model) {

		Map<RCPosition, CalcButton> map = new HashMap<>();

		// screen value changing buttons (numbers, +/-, .)
		map.put(new RCPosition(5, 3), new CalcButton(new ValueFunction("0", model)));
		map.put(new RCPosition(4, 3), new CalcButton(new ValueFunction("1", model)));
		map.put(new RCPosition(4, 4), new CalcButton(new ValueFunction("2", model)));
		map.put(new RCPosition(4, 5), new CalcButton(new ValueFunction("3", model)));
		map.put(new RCPosition(3, 3), new CalcButton(new ValueFunction("4", model)));
		map.put(new RCPosition(3, 4), new CalcButton(new ValueFunction("5", model)));
		map.put(new RCPosition(3, 5), new CalcButton(new ValueFunction("6", model)));
		map.put(new RCPosition(2, 3), new CalcButton(new ValueFunction("7", model)));
		map.put(new RCPosition(2, 4), new CalcButton(new ValueFunction("8", model)));
		map.put(new RCPosition(2, 5), new CalcButton(new ValueFunction("9", model)));
		map.put(new RCPosition(5, 4), new CalcButton(new ValueFunction("-/+", model)));
		map.put(new RCPosition(5, 5), new CalcButton(new ValueFunction(".", model)));

		// binary functions
		map.put(new RCPosition(5, 6), new CalcButton(new BinaryFunction("+", ADD, model)));
		map.put(new RCPosition(4, 6), new CalcButton(new BinaryFunction("-", SUB, model)));
		map.put(new RCPosition(3, 6), new CalcButton(new BinaryFunction("×", MUL, model)));
		map.put(new RCPosition(2, 6), new CalcButton(new BinaryFunction("÷", DIV, model)));
		map.put(new RCPosition(5, 1), new CalcButton(new BinaryFunction("<html>x<sup>α</sup></html>", POW, model)));

		// 1/x has no inverse so put it in BUTTONS
		map.put(new RCPosition(2, 1), new CalcButton(new UnaryFunction("1/x", INVERSE, model)));

		// equals button

		map.put(new RCPosition(1, 6), new CalcButton(new AbstractAction("=") {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				BiFunction<Double, Double, Double> f = model.getCalculatingFunction();
				if (f == null) {
					Double screenValue = model.getScreenValue();
					if (screenValue != null) {
						model.setFirstOperand(model.getScreenValue());
						model.setCalculatingFunction(f);
						model.setResultOnDisplay(true);

					}
				} else {
					Double result = f.apply(model.getFirstOperand(), model.getScreenValue());
					model.setScreenValue(result);
					model.setFirstOperand(result);
					model.setCalculatingFunction(null);
					model.setResultOnDisplay(true);
				}
				model.setResultOnDisplay(true);
			}
		}));

		// clear button
		map.put(new RCPosition(1, 7), new CalcButton(new AbstractAction("clr") {
			/**
			 * Serial ID
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				model.setScreenText("");
			}
		}));

		// push button
		map.put(new RCPosition(3, 7), new CalcButton(new AbstractAction("push") {
			/**
			 * Serial ID
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				model.push(model.getScreenValue());
			}
		}));

		// pop button
		map.put(new RCPosition(4, 7), new CalcButton(new AbstractAction("pop") {
			/**
			 * Serial ID
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				Double value = model.pop();
				if (value == null) {
					JOptionPane.showMessageDialog(null, "Stack is empty!");
				} else {
					model.setScreenValue(value);
					model.setResultOnDisplay(true);
				}
			}
		}));

		// reset button
		map.put(new RCPosition(2, 7), new CalcButton(new AbstractAction("res") {
			/**
			 * Serial ID
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				model.reset();
			}
		}));

		return map;
	}

	/**
	 * Returns map of constraint,button pairs of unary function buttons
	 * 
	 * @param model
	 *            Calculator model
	 * @return map of constraint,button pairs
	 */
	private static Map<RCPosition, CalcButton> getFunctionsMap(CalculatorModel model) {
		Map<RCPosition, CalcButton> map = new HashMap<>();

		// unary functions
		map.put(new RCPosition(2, 2), new CalcButton(new UnaryFunction("sin", SINE, model)));
		map.put(new RCPosition(3, 2), new CalcButton(new UnaryFunction("cos", COSINE, model)));
		map.put(new RCPosition(4, 2), new CalcButton(new UnaryFunction("tan", TAN, model)));
		map.put(new RCPosition(5, 2), new CalcButton(new UnaryFunction("cot", COTAN, model)));
		map.put(new RCPosition(3, 1), new CalcButton(new UnaryFunction("log", LOG, model)));
		map.put(new RCPosition(4, 1), new CalcButton(new UnaryFunction("ln", LN, model)));

		return map;
	}

	/**
	 * Returns map of constraint,button pairs of inverse unary function buttons
	 * 
	 * @param model
	 *            Calculator model
	 * @return map of constraint,button pairs
	 */
	private static Map<RCPosition, CalcButton> getInverseFunctionsMap(CalculatorModel model) {
		Map<RCPosition, CalcButton> map = new HashMap<>();

		map.put(new RCPosition(2, 2), new CalcButton(new UnaryFunction("arcsin", ASINE, model)));
		map.put(new RCPosition(3, 2), new CalcButton(new UnaryFunction("arccos", ACOSINE, model)));
		map.put(new RCPosition(4, 2), new CalcButton(new UnaryFunction("arctan", ATAN, model)));
		map.put(new RCPosition(5, 2), new CalcButton(new UnaryFunction("arccot", ACOTAN, model)));
		map.put(new RCPosition(3, 1), new CalcButton(new UnaryFunction("10^", EXP10, model)));
		map.put(new RCPosition(4, 1), new CalcButton(new UnaryFunction("e^", EXP, model)));

		return map;
	}

}
