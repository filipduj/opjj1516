package hr.fer.zemris.java.gui.calc.functions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import hr.fer.zemris.java.gui.calc.model.CalculatorModel;

/**
 * Implementation of generic action which groups together all simple screen
 * value changing actions.
 * <p>
 * For example numbers or comma buttons.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class ValueFunction extends AbstractAction {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -888246101386284076L;

	/**
	 * Value this function will append to screen
	 */
	private String value;

	/**
	 * Model of calculator on which to perform action
	 */
	private CalculatorModel model;

	/**
	 * Constructor intializes new value function action.
	 * 
	 * @param text
	 *            Text to bound to this action (function abbreviation)
	 * @param model
	 *            Model from which function reads operands
	 */
	public ValueFunction(String text, CalculatorModel model) {
		super(text);
		this.value = text;
		this.model = model;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		model.appendScreenValue(value);
	}

}
