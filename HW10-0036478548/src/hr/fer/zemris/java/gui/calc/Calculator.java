package hr.fer.zemris.java.gui.calc;

import javax.swing.SwingUtilities;

import hr.fer.zemris.java.gui.calc.view.CalculatorGUI;

/**
 * Main Calculator application class.
 * <p>
 * Program accepts no arguments, it is ready to run.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class Calculator {

	/**
	 * Program entry point
	 * 
	 * @param args
	 *            No arguments required, if provided they are ignored
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> new CalculatorGUI().setVisible(true));
	}
}
