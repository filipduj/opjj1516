package hr.fer.zemris.java.gui.calc.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 * Class models calculator's screen display.
 * <p>
 * It represents customized {@link JLabel}.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class CalcScreen extends JLabel implements Observer {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 3636161741477737427L;

	/**
	 * Initializes new Calculator Screen with given observable object. Screen
	 * keeps track of observable and updates it's value.
	 * 
	 * @param o
	 */
	public CalcScreen(Observable o) {
		o.addObserver(this);
		setHorizontalTextPosition(RIGHT);
		setBackground(new Color(255, 211, 32));
		setBorder(BorderFactory.createLineBorder(Color.BLUE, 2));
		setHorizontalAlignment(SwingConstants.RIGHT);
		setMinimumSize(new Dimension(0, 0)); // rest of the buttons will fix
												// screen to some size
		setOpaque(true);

		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				setFont(new Font(Font.SANS_SERIF, Font.LAYOUT_RIGHT_TO_LEFT,
						(int) (e.getComponent().getHeight() * 0.9)));
			}
		});

	}

	@Override
	public void update(Observable o, Object arg) {
		setText((String) arg);
	}

}
