package hr.fer.zemris.java.gui.calc.model;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.function.BiFunction;

/**
 * Model abstraction of Calculator's internal state.
 * <p>
 * Calculator has sort of internal stack which can keep values on user's demand.
 * It also has memory for current operation and one operand.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class CalculatorModel extends Observable {

	/**
	 * Formatter for numeric output on calculator screen
	 */
	private static final DecimalFormat DF = new DecimalFormat("0.#########");

	/**
	 * Tells if value on display is result of immediate previous operation. If
	 * this is true then next user's input will clear screen.
	 */
	private boolean resultOnDisplay;

	/**
	 * Value currently showing on screen in string form
	 */
	private String screenValue;

	/**
	 * Holds selected function to be calculated after the second operand is
	 * entered
	 */
	private BiFunction<Double, Double, Double> calculatingFunction;

	/**
	 * First operand of some operation
	 */
	private Double firstOperand;

	/**
	 * Internal stack memory
	 */
	private List<Double> stack;

	/**
	 * If flag is true inverse functions are active, normal functions otherwise
	 */
	boolean inverseFunctions = false;

	/**
	 * Default constructor initializes new calculator model with default values.
	 */
	public CalculatorModel() {
		stack = new LinkedList<>();
		screenValue = "";
		resultOnDisplay = false;
		calculatingFunction = null;
	}

	/**
	 * Sets screen value to given {@code value} number.
	 * 
	 * @param value
	 *            Number to set screen value to
	 */
	public void setScreenValue(double value) {
		screenValue = DF.format(value);
		setChanged();
		notifyObservers(screenValue);
	}

	/**
	 * Resets calculator's states to initial state
	 */
	public void reset() {
		calculatingFunction = null;
		firstOperand = null;
		setScreenText("");
		resultOnDisplay = false;
		stack.clear();
	}

	/**
	 * Appends one digit to screen value
	 * 
	 * @param s
	 *            Digit to append
	 */
	public void appendScreenValue(String s) {
		if (resultOnDisplay && !s.equals("-/+")) {
			resultOnDisplay = false;
			screenValue = "";
		}

		if (s.equals("-/+")) { // invert sign
			if (!screenValue.isEmpty()) {
				if (screenValue.startsWith("-"))
					screenValue = screenValue.substring(1, screenValue.length());
				else
					screenValue = "-" + screenValue;
			}
		} else if (s.equals(".")) { // add decimal comma
			if (screenValue.contains(".") || screenValue.isEmpty())
				screenValue = "0";
			screenValue += '.';
		} else { // add number
			screenValue += s;
		}
		setChanged();
		notifyObservers(getScreenText());
	}

	/**
	 * @return returns screen value as Double
	 */
	public Double getScreenValue() {
		if (screenValue.isEmpty())
			return null;
		else
			return Double.parseDouble(screenValue);
	}

	/**
	 * @return returns screen value as String
	 */
	public String getScreenText() {
		return screenValue;
	}

	/**
	 * Sets screen value to given {@code text}
	 * 
	 * @param text
	 *            Text to set screen value to
	 */
	public void setScreenText(String text) {
		screenValue = text;
		setChanged();
		notifyObservers(getScreenText());
	}

	/**
	 * Sets flag indicating that data on display is result of operation
	 * 
	 * @param flag
	 *            result of operation flag
	 */
	public void setResultOnDisplay(boolean flag) {
		resultOnDisplay = flag;
	}

	/**
	 * Pushes given {@code value} to stack
	 * 
	 * @param value
	 */
	public void push(Double value) {
		stack.add(value);
	}

	/**
	 * @return Returns function to be calculated
	 */
	public BiFunction<Double, Double, Double> getCalculatingFunction() {
		return calculatingFunction;
	}

	/**
	 * Sets function to be calculated to given {@code function}.
	 * 
	 * @param function
	 *            sets function to be calculated
	 */
	public void setCalculatingFunction(BiFunction<Double, Double, Double> function) {
		this.calculatingFunction = function;
	}

	/**
	 * Pops and returns value from top of stack. Returns {@code null} if stack
	 * is empty.
	 * 
	 * @return value on top of stack or {@code null} if stack empty
	 */
	public Double pop() {
		if (stack.isEmpty()) {
			return null;
		}
		return stack.remove(stack.size() - 1);
	}

	/**
	 * Check if inverse functions flag is on
	 * 
	 * @return returns {@code true} if inverse function flag is on
	 */
	public boolean isInverseFunctions() {
		return inverseFunctions;
	}

	/**
	 * Sets inverse functions flag to given {@code flag} value
	 * 
	 * @param flag boolean
	 */
	public void setInverseFunctions(boolean flag) {
		inverseFunctions = flag;
	}

	/**
	 * @return the firstOperand
	 */
	public Double getFirstOperand() {
		return firstOperand;
	}

	/**
	 * @param firstOperand
	 *            the firstOperand to set
	 */
	public void setFirstOperand(Double firstOperand) {
		this.firstOperand = firstOperand;
	}

}
