package hr.fer.zemris.java.gui.calc.components;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JButton;

/**
 * Class decorates style of {@link JButton} buttons 
 * used in calculator.s
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class CalcButton extends JButton {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -1847149527725330359L;

	/**
	 * Constructor initializes new button with given
	 * {@code action}.
	 * 
	 * @param action Action to perform
	 */
	public CalcButton(AbstractAction action) {
		super(action);
		setBackground(new Color(115, 159, 207));
		setBorder(BorderFactory.createLineBorder(Color.BLUE, 1));
		setPreferredSize(new Dimension(60,60));
	}
	
}
