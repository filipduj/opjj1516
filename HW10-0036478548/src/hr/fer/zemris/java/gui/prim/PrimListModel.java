package hr.fer.zemris.java.gui.prim;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

/**
 * Simple list model containing ordered prime numbers.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class PrimListModel implements ListModel<Integer> {

	/**
	 * List of prime numbers
	 */
	private List<Integer> primes;

	/**
	 * List of listeners interested in this model
	 */
	private List<ListDataListener> listeners;

	/**
	 * Default constructor intializes new model and adds first prime (number 1)
	 * to list.
	 */
	public PrimListModel() {
		primes = new ArrayList<>();
		listeners = new LinkedList<>();
		primes.add(1);
	}

	/**
	 * Method generates first prime number greater than the last calculated
	 * value.
	 */
	public void next() {
		// add next prime to internal list
		int next = primes.get(primes.size() - 1) + 1;
		while (!isPrime(next)) {
			next++;
		}
		primes.add(next);

		// make copy of current listeners and notify them for changed data
		int lastIndex = primes.size() - 1;
		new LinkedList<>(listeners).forEach((l) -> {
			l.intervalAdded(new ListDataEvent(this, ListDataEvent.INTERVAL_ADDED, lastIndex, lastIndex));
		});
	}

	@Override
	public int getSize() {
		return primes.size();
	}

	@Override
	public Integer getElementAt(int index) {
		return primes.get(index);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		listeners.add(l);
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		listeners.remove(l);
	}

	/**
	 * Checks if number is prime. Method uses simple Trial division approach to
	 * check if x is prime. More about Trial division on wiki:
	 * https://en.wikipedia.org/wiki/Trial_division
	 * 
	 * @param x
	 *            positive integer to be tested if it is prime or not
	 * @return true if x is prime, false otherwise
	 */
	private boolean isPrime(int x) {
		if (x == 1)
			return false;

		for (int i = 2; i <= Math.sqrt(x); ++i) {
			if (x % i == 0) {
				return false;
			}
		}
		return true;
	}

}
