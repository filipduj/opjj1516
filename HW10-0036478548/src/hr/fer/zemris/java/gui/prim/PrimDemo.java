package hr.fer.zemris.java.gui.prim;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

/**
 * Demonstration program for Prime nubmer lists.
 * <p>
 * Program accepts no arguments and displays two lists of prime numbers
 * containing same data.
 * <p>
 * Clicking on button on bottom triggers calculation of next prime number which
 * is added to both lists.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class PrimDemo extends JFrame {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 2552614965736703542L;

	/**
	 * Window title
	 */
	private static final String TITLE = "PrimDemo v1.0";

	/**
	 * Frame width
	 */
	private static final int WIDTH = 300;

	/**
	 * Frame height
	 */
	private static final int HEIGHT = 300;

	/**
	 * Constructor initializes new Frame on which lists will be displayed.
	 */
	public PrimDemo() {
		setTitle(TITLE);
		setLocationRelativeTo(null);
		setLayout(new BorderLayout());
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		initGUI();
		setSize(WIDTH, HEIGHT);
	}

	/**
	 * This method lays out all necessary components to content pane.
	 */
	private void initGUI() {

		// create lists with reference to same listModel
		PrimListModel listModel = new PrimListModel();

		// add scrollable lists to panel and put panel to CENTER field in
		// PrimDemo layout
		JPanel topPanel = new JPanel(new GridLayout(1, 2));
		topPanel.add(new JScrollPane(new JList<Integer>(listModel)));
		topPanel.add(new JScrollPane(new JList<Integer>(listModel)));
		add(topPanel);

		// create button and put it to SOUTH field in PrimDemo layout
		add(new JButton(new AbstractAction("Next") {
			/**
			 * Serial ID
			 */
			private static final long serialVersionUID = -7651343772076567202L;

			@Override
			public void actionPerformed(ActionEvent e) {
				listModel.next();
			}
		}), BorderLayout.SOUTH);
	}

	/**
	 * Program entry point.
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(() -> {
			new PrimDemo().setVisible(true);
		});
	}

}
