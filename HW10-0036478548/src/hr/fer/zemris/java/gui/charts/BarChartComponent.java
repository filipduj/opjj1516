package hr.fer.zemris.java.gui.charts;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.geom.AffineTransform;

import javax.swing.JComponent;

/**
 * Component model representation of {@link BarChart} object.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class BarChartComponent extends JComponent {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -6775457331154091971L;
	
	/**
	 * Gap between elements on display
	 */
	private static final int GAP = 10;

	/**
	 * Arrow head size in pixels
	 */
	private static final int ARROW_SIZE = 4;

	/**
	 * Background color on chart
	 */
	private static final Color BACKGROUND_COLOR = new Color(255, 255, 204);

	/**
	 * Bars color
	 */
	private static final Color BAR_COLOR = new Color(244, 119, 72);

	/**
	 * Font
	 */
	private static final Font FONT = new Font(Font.SERIF, Font.TRUETYPE_FONT, 12);

	/**
	 * Reference to model of chart which this class represents
	 */
	private BarChart chart;

	/**
	 * Font metrics used for various text measurements
	 */
	private FontMetrics metrics;

	/**
	 * x-axis textual description
	 */
	private String xAxisDescription;

	/**
	 * y-axis textual description
	 */
	private String yAxisDescription;

	/**
	 * Length of x-axis description in pixels
	 */
	private int xAxisDescriptionLength;

	/**
	 * Length of y-axis description in pixels
	 */
	private int yAxisDescriptionLength;

	/**
	 * Maximal width of y-values in pixels
	 */
	private int maxYValuesWidth;

	/**
	 * Number of bars to display
	 */
	private int numberOfBars;

	/**
	 * Number of discrete y values
	 */
	private int numberOfDiscreteValues;

	/**
	 * x position left accessible point
	 */
	private int bottomLeftX;

	/**
	 * y position most bottom accessible point
	 */
	private int bottomLeftY;

	/**
	 * Top left y coordinate which is accessible for drawing
	 */
	private int topLeftY;

	/**
	 * Margin from top border
	 */
	private int marginTop;

	/**
	 * Margin from right border
	 */
	private int marginRight;

	/**
	 * Content pane width
	 */
	private int width;

	/**
	 * Content pane height
	 */
	private int height;

	/**
	 * x position of origin of chart
	 */
	private int centerX;

	/**
	 * y position of origin of chart
	 */
	private int centerY;

	/**
	 * step on x axis
	 */
	private int deltaX;

	/**
	 * step on y axis
	 */
	private int deltaY;
	
	/**
	 * Minimal x value in x,y data pairs
	 */
	private int minX;
	
	/**
	 * Maximal x value in x,y data pairs
	 */
	private int maxX;

	/**
	 * Constructor initializs instance of new BarChart component with given
	 * {@code chart} model.
	 * 
	 * @param chart
	 *            BarChart model
	 * @throws IllegalArgumentException
	 *             If {@code chart} is {@code null} reference
	 */
	public BarChartComponent(BarChart chart) {

		if (chart == null) {
			throw new IllegalArgumentException("Expected BarChart object but NULL given!");
		}

		setFont(FONT);

		this.chart = chart;
		this.metrics = getFontMetrics(getFont());
		this.maxYValuesWidth = metrics.stringWidth(Integer.toString(chart.getyMax()));

		this.marginTop = GAP;
		this.marginRight = 3 * GAP;

//		this.numberOfBars = chart.getData().stream().mapToInt(d -> d.getX()).max().getAsInt();
		this.numberOfDiscreteValues = (chart.getyMax() - chart.getyMin()) / chart.getDistance();
		this.topLeftY = getInsets().top;
		this.xAxisDescription = chart.getXDescription();
		this.xAxisDescriptionLength = metrics.stringWidth(xAxisDescription);
		this.yAxisDescription = chart.getYDescription();
		this.yAxisDescriptionLength = metrics.stringWidth(yAxisDescription);
		
		this.minX = chart.getData().stream().mapToInt(v -> v.getX()).min().getAsInt(); 
		this.maxX = chart.getData().stream().mapToInt(v -> v.getX()).max().getAsInt();
		this.numberOfBars = maxX - minX + 1;
				
		setBaseValues();

		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				setBaseValues();
				repaint();
			}
		});

	}

	@Override
	protected void paintComponent(Graphics g) {

		Graphics2D g2d = (Graphics2D) g;

		drawBackground(g2d);

		drawAxes(g2d);

		drawVerticalValues(g2d);

		drawHorizontalValues(g2d);

		drawGrid(g2d);

		drawBars(g2d);

	}

	/**
	 * Draws background on chart
	 * 
	 * @param g2d
	 *            graphics to draw to
	 */
	private void drawBackground(Graphics2D g2d) {
		g2d.setColor(BACKGROUND_COLOR);
		g2d.fillRect(centerX, centerY - numberOfDiscreteValues * deltaY, numberOfBars * deltaX,
				numberOfDiscreteValues * deltaY);
	}

	/**
	 * Draws x,y arrow axes with their textual descriptions.
	 * 
	 * @param g2d
	 *            graphics to draw to
	 */
	private void drawAxes(Graphics2D g2d) {

		g2d.setColor(Color.BLACK);

		// draw X-axis description and arrow
		g2d.drawString(xAxisDescription, centerX + (width - centerX - xAxisDescriptionLength - marginRight) / 2,
				bottomLeftY - GAP);
		int xStart = centerX - GAP / 2;
		int yStart = centerY;
		int xEnd = bottomLeftX + width - 2 * GAP;
		int yEnd = centerY;

		g2d.drawLine(xStart, yStart, xEnd, yEnd);
		g2d.drawLine(xEnd, yEnd, xEnd - ARROW_SIZE, yEnd - ARROW_SIZE);
		g2d.drawLine(xEnd, yEnd, xEnd - ARROW_SIZE, yEnd + ARROW_SIZE);

		// draw Y-axis description and arrow
		AffineTransform at = g2d.getTransform();
		g2d.setTransform(AffineTransform.getQuadrantRotateInstance(3));

		g2d.drawString(yAxisDescription, -(centerY - (numberOfDiscreteValues * deltaY - yAxisDescriptionLength) / 2
				+ (centerY - numberOfDiscreteValues * deltaY)), GAP + metrics.getAscent());
		g2d.setTransform(at);
		xStart = centerX;
		yStart = centerY + GAP / 2;
		xEnd = centerX;
		yEnd = 0;
		g2d.drawLine(xStart, yStart, xEnd, yEnd);
		g2d.drawLine(xEnd, yEnd, xEnd + ARROW_SIZE, yEnd + ARROW_SIZE);
		g2d.drawLine(xEnd, yEnd, xEnd - ARROW_SIZE, yEnd + ARROW_SIZE);

	}

	/**
	 * Draws bars on chart
	 * 
	 * @param g2d
	 *            graphics to draw to
	 */
	private void drawBars(Graphics2D g2d) {
//		g2d.setColor(BAR_COLOR);

		int width = deltaX - 1;
		int yMin = chart.getyMin();
		int d = chart.getDistance();
		int height;
		for (XYValue value : chart.getData()) {
			g2d.setColor(BAR_COLOR);
			height = (int) (((value.getY() - yMin) * 1.0 / d) * deltaY);
			g2d.fillRect(centerX + (value.getX() - minX) * deltaX + 1, centerY - height, width, height);
		}
	}

	/**
	 * Draws grid of horizontal and vertical lines on chart
	 * 
	 * @param g2d
	 *            graphics to draw to
	 */
	private void drawGrid(Graphics2D g2d) {

		g2d.setColor(Color.LIGHT_GRAY);

		// horizontal lines
		int startX = centerX - GAP / 2;
		int startY = centerY - deltaY;
		int endX = bottomLeftX + width - 2 * GAP;
		int endY = startY;
		for (int i = 0; i < numberOfDiscreteValues; ++i) {
			g2d.drawLine(startX, startY, endX, startY);
			startY -= deltaY;
		}

		// vertical lines
		startX = centerX + deltaX;
		startY = centerY + GAP / 2;
		endX = startX;
		endY = 0;
		for (int i = 0; i < numberOfBars; ++i) {
			g2d.drawLine(startX, startY, startX, endY);
			startX += deltaX;
		}

	}

	/**
	 * Draws y values along y axis
	 * 
	 * @param g2d
	 *            graphics to draw to
	 */
	private void drawVerticalValues(Graphics2D g2d) {
		// draw horizontal numbers
		int textHeight = metrics.getAscent();
		int x = 2 * GAP + textHeight;
		int y = bottomLeftY - (3 * GAP + 2 * textHeight - textHeight / 2);
		String value;
		int valueWidth;
		for (int yMin = chart.getyMin(), yMax = chart.getyMax(), d = chart.getDistance(); yMin <= yMax; yMin += d) {
			value = Integer.toString(yMin);
			valueWidth = metrics.stringWidth(value);
			g2d.drawString(value, x + (maxYValuesWidth - valueWidth), y);
			y -= deltaY;
		}
	}

	/**
	 * Draws x values along x axis
	 * 
	 * @param g2d
	 *            graphics to draw to
	 */
	private void drawHorizontalValues(Graphics2D g2d) {
		int x = centerX;
		String value;
		int valueWidth;
		for (int i = minX; i <= maxX; ++i) {
			value = Integer.toString(i);
			valueWidth = metrics.stringWidth(value);
			g2d.drawString(value, x + (deltaX - valueWidth) / 2, height - 2 * GAP - metrics.getAscent());
			x += deltaX;
		}
	}

	/**
	 * Recalculates and sets referent points after resize of content pane
	 * occured.
	 */
	private void setBaseValues() {

		Insets ins = getInsets();
		int textHeight = metrics.getAscent();
		int realWidth = getWidth();
		int realHeight = getHeight();

		width = realWidth - ins.left - ins.right;
		height = realHeight - ins.top - ins.bottom;

		bottomLeftX = ins.left;
		bottomLeftY = realHeight - ins.bottom;

		centerX = bottomLeftX + 3 * GAP + textHeight + maxYValuesWidth;
		centerY = bottomLeftY - 3 * GAP - 2 * textHeight;

		deltaX = (width - centerX - marginRight) / numberOfBars;
		deltaY = (centerY - topLeftY - marginTop) / numberOfDiscreteValues;

	}
}
