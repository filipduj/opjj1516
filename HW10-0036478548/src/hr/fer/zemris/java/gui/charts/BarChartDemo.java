package hr.fer.zemris.java.gui.charts;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

/**
 * BarChart demonstration program.
 * <p>
 * Program accepts single command line argument: path to barchart descriptor
 * file.
 * <p>
 * Program then loads and parses given input file and draws it's graphical
 * representation.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class BarChartDemo extends JFrame {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 4552009112093120703L;

	/**
	 * Frame width
	 */
	private static final int WIDTH = 400;

	/**
	 * Frame height
	 */
	private static final int HEIGHT = 400;

	/**
	 * Window title
	 */
	private static final String TITLE = "BarChart Demo v1.0";

	/**
	 * Constructor initializes new Frame containing barchart visualization.
	 * 
	 * @param chart
	 *            BarChart component
	 * @param path
	 *            Path to chart description file
	 */
	public BarChartDemo(BarChartComponent chart, Path path) {
		setSize(WIDTH, HEIGHT);
		setTitle(TITLE);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setLocationRelativeTo(null); // center frame on screen
		setLayout(new BorderLayout());

		JLabel label = new JLabel(path.toFile().getAbsolutePath()/*path.getFileName().toString()*/, SwingConstants.CENTER);
		label.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

		add(label, BorderLayout.NORTH);
		add(chart, BorderLayout.CENTER);
	}

	/**
	 * Program entry point
	 * 
	 * @param args
	 *            One argument, path to chart description file
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			errorExit("Expected one command line argument - path to chart description!");
		}

		Path path = getPathToInput(args[0]);
		BarChart chart = parseChart(path.toFile());

		SwingUtilities.invokeLater(() -> {
			new BarChartDemo(new BarChartComponent(chart), path).setVisible(true);
		});

	}

	/**
	 * Method takes input string and tries to interpret it as path.
	 * <p>
	 * Returns interpreted path if possible.
	 * 
	 * @param line
	 *            input string line
	 * @return Path represented by given input string
	 */
	private static Path getPathToInput(String line) {
		Path path = Paths.get(line.trim());

		if (!Files.exists(path)) {
			errorExit("Invalid path or file does not exist.");
		}

		if (!Files.isRegularFile(path)) {
			errorExit("File is not regular file.");
		}

		if (!Files.isReadable(path)) {
			errorExit("Access denied.");
		}

		return path;
	}

	/**
	 * Method parses chart description contained within {@code file}.
	 * 
	 * @param file
	 *            chart description file
	 * @return returns BarChart model constructed out of parsed {@code file}
	 */
	private static BarChart parseChart(File file) {

		BarChart chart = null;

		try (BufferedReader reader = new BufferedReader(
										new InputStreamReader(
												new FileInputStream(file), "UTF-8"))) {

			String xAxisDescription = inputLineCheck(reader.readLine());
			String yAxisDescription = inputLineCheck(reader.readLine());

			List<XYValue> values = parseValues(reader.readLine());

			int minY = Integer.parseInt(inputLineCheck(reader.readLine()));
			int maxY = Integer.parseInt(inputLineCheck(reader.readLine()));
			int distance = Integer.parseInt(inputLineCheck(reader.readLine()));

			chart = new BarChart(values, xAxisDescription, yAxisDescription, minY, maxY, distance);

		} catch (IOException | IllegalArgumentException e) {
			errorExit(e.getMessage());
		}

		return chart;
	}

	/**
	 * Method parses one line of input chart file containing x,y data pairs.
	 * 
	 * @param in
	 *            Input line
	 * @return returns List of x,y value data pairs represented by
	 *         {@link XYValue} objects
	 */
	private static List<XYValue> parseValues(String in) {
		return Arrays.asList(inputLineCheck(in).split("\\s+")).stream().map((String s) -> {
			String[] pair = s.split(",");
			if (pair.length != 2)
				errorExit("Wrong format of input file!");
			return new XYValue(Integer.parseInt(pair[0]), Integer.parseInt(pair[1]));
		}).collect(Collectors.toList());
	}

	/**
	 * Method checks line read from input file. Returns line if line not empty.
	 * 
	 * @param in
	 *            Input line
	 * @return returns trimmed line from input file
	 */
	private static String inputLineCheck(String in) {
		in = in.trim();
		if (in == null) {
			errorExit("Stream was closed.");
		}
		if (in.isEmpty()) {
			errorExit("Wrong input file format");
		}
		return in;
	}

	/**
	 * Error exit method. Displays given {@code message} and forces program
	 * termination.
	 * 
	 * @param message Message to display before exiting
	 */
	private static void errorExit(String message) {
		System.out.println(message);
		System.exit(-1);
	}

}