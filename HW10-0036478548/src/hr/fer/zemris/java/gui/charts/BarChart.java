package hr.fer.zemris.java.gui.charts;

import java.util.List;

/**
 * Class models one bar chart.
 * <p>
 * Every chart has x,y data pairs, descriptions of x and y axis, density of
 * y-axis values, and y values range.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class BarChart {

	/**
	 * List of x,y data pairs
	 */
	private List<XYValue> data;

	/**
	 * Description of x-axis
	 */
	private String xDescription;

	/**
	 * Description of y-axis
	 */
	private String yDescription;

	/**
	 * Minimal y value
	 */
	private int yMin;

	/**
	 * Maximal y value
	 */
	private int yMax;

	/**
	 * Value step on y axis
	 */
	private int distance;

	/**
	 * Constructor allocates new BarChart model with given input parameters.
	 * 
	 * @param data
	 *            List of x,y data pairs
	 * @param xDescription
	 *            Description of x-axis
	 * @param yDescription
	 *            Description of y-axis
	 * @param yMin
	 *            Minimal y value
	 * @param yMax
	 *            Maximal y value
	 * @param distance
	 *            Value step on y axis
	 */
	public BarChart(List<XYValue> data, String xDescription, String yDescription, int yMin, int yMax, int distance) {
		constraintCheck(data, xDescription, yDescription, yMin, yMax, distance);
		yMax = (yMax - yMin) % distance != 0 ? (yMax - yMin) / distance + 1 : yMax;
		this.data = data;
		this.yMin = yMin;
		this.yMax = yMax;
		this.xDescription = xDescription;
		this.yDescription = yDescription;
		this.distance = distance;
	}

	/**
	 * Method checks constraints on given input parameters for BarChart.
	 * 
	 * @param data
	 *            List of x,y data pairs
	 * @param xDescription
	 *            Description of x-axis
	 * @param yDescription
	 *            Description of y-axis
	 * @param yMin
	 *            Minimal y value
	 * @param yMax
	 *            Maximal y value
	 * @param distance
	 *            Value step on y axis
	 * 
	 * @throws IllegalArgumentException
	 *             If constraints not fullfiled
	 */
	private void constraintCheck(List<XYValue> data, String xDescription, String yDescription, int yMin, int yMax,
			int distance) {
		if (data == null) {
			throw new IllegalArgumentException("Expected list of data values but NULL reference given!");
		}

		if (data.isEmpty()) {
			throw new IllegalArgumentException("Expected atleast one data value but 0 given!");
		}

		if (xDescription == null) {
			throw new IllegalArgumentException("Expected x axis string description but NULL given!");
		}

		if (yDescription == null) {
			throw new IllegalArgumentException("Expected y axis string description but NULL given!");
		}

		if (yMax <= yMin) {
			throw new IllegalArgumentException("Expected yMax to be larger than yMin! yMin=" + yMin + ",yMax=" + yMax);
		}

		if (distance < 1) {
			throw new IllegalArgumentException("Expected distance to be positive number but " + distance + " given!");
		}
	}

	/**
	 * @return the data
	 */
	public List<XYValue> getData() {
		return data;
	}

	/**
	 * @return the xDescription
	 */
	public String getXDescription() {
		return xDescription;
	}

	/**
	 * @return the yDescription
	 */
	public String getYDescription() {
		return yDescription;
	}

	/**
	 * @return the yMin
	 */
	public int getyMin() {
		return yMin;
	}

	/**
	 * @return the yMax
	 */
	public int getyMax() {
		return yMax;
	}

	/**
	 * @return the distance
	 */
	public int getDistance() {
		return distance;
	}

}
