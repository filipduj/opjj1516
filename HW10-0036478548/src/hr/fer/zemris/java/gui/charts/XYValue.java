package hr.fer.zemris.java.gui.charts;

/**
 * Model of one data pair for {@link BarChart}.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (May 19, 2016)
 */
public class XYValue {
	
	/**
	 * X value
	 */
	private int x;
	
	/**
	 * Y value
	 */
	private int y;
	
	
	/**
	 * Constructor initializes new data pair
	 * with given {@code x} and {@code y} values.
	 * 
	 * @param x x value
	 * @param y y value
	 */
	public XYValue(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}


	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}


	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}
	
	
	
	
	

}
