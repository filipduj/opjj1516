package hr.fer.zemris.java.custom.collections;

/**
 * Stack collection offering standard stack methods. Uses
 * {@link ArrayBackedIndexedCollection} under the hood as data storage
 * collection but adapts methods so that stack functionality is achieved.
 * 
 * @author Filip Dujmušić
 *
 */
public class ObjectStack {

	/**
	 * Data storage collection. Elements on stack are stored here.
	 */
	private ArrayIndexedCollection arrayCollection;

	/**
	 * Default constructor. Creates empty stack with capacity of 16 elements.
	 */
	public ObjectStack() {
		arrayCollection = new ArrayIndexedCollection();
	}

	/**
	 * Checks if stack is empty.
	 * 
	 * @return returns true if no elements on stack, false otherwise
	 */
	public boolean isEmpty() {
		return arrayCollection.isEmpty();
	}

	/**
	 * Gets size of stack
	 * 
	 * @return returns number of elements currently on stack
	 */
	public int size() {
		return arrayCollection.size();
	}

	/**
	 * Pushes new element to the top of stack. Internally it actually adds it to
	 * the last position in array. Throws exception if passed element is null.
	 * 
	 * @param value
	 *            element to be pushed on top
	 */
	public void push(Object value) {
		try {
			arrayCollection.add(value);
		} catch (IllegalArgumentException e) {
			System.out.println("ERROR: Cannot push null value to stack!");
		}
	}

	/**
	 * Pops one element from top of stack. If stack is empty exception is
	 * thrown.
	 * 
	 * @return returns element removed from top of stack
	 */
	public Object pop() {
		if (arrayCollection.isEmpty()) {
			throw new EmptyStackException();
		}
		int lastPosition = arrayCollection.size() - 1;
		Object ret = arrayCollection.get(lastPosition);
		arrayCollection.remove(lastPosition);
		return ret;
	}

	/**
	 * Returns element at top of stack. Gets the top element on stack but DOES
	 * NOT remove it from stack. Throws exception if stack is empty.
	 * 
	 * @return element on top of stack
	 */
	public Object peek() {
		if (arrayCollection.isEmpty()) {
			throw new EmptyStackException();
		}
		return arrayCollection.get(arrayCollection.size() - 1);
	}

	/**
	 * Removes all elements currently on stack.
	 */
	public void clear() {
		arrayCollection.clear();
	}
}
