package hr.fer.zemris.java.custom.scripting.nodes;

/**
 * Node representing a piece of textual data. It inherits from Node class.
 * 
 * @author Filip Dujmušić
 * 
 */
public class TextNode extends Node {

	/**
	 * String data from script. Data token from TEXT part of script will be
	 * stored here.
	 */
	private String text;

	/**
	 * Constructs new node
	 * 
	 * @param token
	 *            string construct containing text
	 */
	public TextNode(String token) {
		text = token;
	}

	/**
	 * text getter.
	 * 
	 * @return returns text data of node
	 */
	public String asText() {
		return text;
	}

	/**
	 * Returns text data but in a way that was read from a script, with escaped
	 * elements unescaped again.
	 */
	@Override
	public String toString() {
		return text.replace("\\", "\\\\").replace("{$", "\\{$");
	}

}
