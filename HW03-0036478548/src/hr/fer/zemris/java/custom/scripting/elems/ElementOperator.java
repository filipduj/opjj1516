package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Element representation of mathematical operator.
 * 
 * @author Filip Dujmušić
 *
 */
public class ElementOperator extends Element {

	/**
	 * Operator symbol as string
	 */
	private String symbol;

	/**
	 * Operator element constructor
	 * 
	 * @param symbol
	 *            operator string
	 */
	public ElementOperator(String symbol) {
		this.symbol = symbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText() {
		return symbol;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return asText();
	}
}
