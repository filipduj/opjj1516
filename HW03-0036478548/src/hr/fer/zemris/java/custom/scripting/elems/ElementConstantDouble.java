package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Element representation of decimal number.
 * 
 * @author Filip Dujmušić
 *
 */
public class ElementConstantDouble extends Element {

	/**
	 * Value of decimal number
	 */
	private double value;

	/**
	 * Constructor that takes double and initializes element value.
	 * 
	 * @param value
	 *            decimal number (double)
	 */
	public ElementConstantDouble(double value) {
		this.value = value;
	}

	/**
	 * Value getter
	 * 
	 * @return returns decimal value of token as double
	 */
	public double getValue() {
		return value;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String asText() {
		return Double.toString(value);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toString() {
		return asText();
	}

}
