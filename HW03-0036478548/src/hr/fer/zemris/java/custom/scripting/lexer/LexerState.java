package hr.fer.zemris.java.custom.scripting.lexer;

/**
 * Enum provides possible {@link Lexer} states.
 * 
 * @author Filip Dujmušić
 *
 */
public enum LexerState {
	
	/**
	 * Lexer is positioned witihn {$ $} tags.
	 */
	TAG, 
	
	/**
	 * Lexer is positioned outside {$ $} tags.
	 */
	TEXT;
	
}
