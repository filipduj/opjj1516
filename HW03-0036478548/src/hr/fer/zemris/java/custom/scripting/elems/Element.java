package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Base element class. Element is base building block of expressions.
 * 
 * @author Filip Dujmušić
 *
 */
public class Element {

	/**
	 * String representation of element value.
	 * 
	 * @return string representation
	 */
	public String asText() {
		return "";
	}

	/**
	 * String representation of element as read from script file.
	 * 
	 * @return string representation
	 */
	@Override
	public String toString() {
		return "";
	}
}
