package hr.fer.zemris.java.custom.scripting.elems;

/**
 * Element representation of string.
 * 
 * @author Filip Dujmušić
 *
 */
public class ElementString extends Element {

	/**
	 * Element data in parsed format. String here is escaped and without
	 * quotation marks.
	 */
	private String value;

	/**
	 * String element constructor
	 * 
	 * @param value
	 *            string token
	 */
	public ElementString(String value) {
		this.value = value;
	}

	/**
	 * {@inheritDoc}
	 */
	public String asText() {
		return value;
	}

	/**
	 * {@inheritDoc} 
	 * Appends leading and trailing quotation marks, and
	 * "unescapes" escaped characters.
	 */
	@Override
	public String toString() {
		return "\"" + value.replace("\\", "\\\\").replace("\"", "\\\"").replace("\n", "\\n").replace("\r", "\\r")
				.replace("\t", "\\t") + "\"";
	}

}
