package hr.fer.zemris.java.tecaj.hw3.prob1;

/**
 * Simple lexic analyzer.
 * Groups characters to form tokens and provides
 * methods for calculating next token or retrieving last token.
 * 
 * Characters are grouped by following rules:
 * 
 * 		1) Input data consists of words, numbers and symbols
 * 		2) Word is every sequence of one or more letters
 * 		3) Number is every sequence of one or more digits
 * 		4) Symbol is every single character in string after removing whitespace, words and numbers 
 * 
 * @author Filip Dujmušić
 *
 */
public class Lexer {

	/**
	 * Script text data. 
	 */
	private char[] data;
	
	/**
	 * Last calculated token.
	 */
	private Token token;
	
	/**
	 * Current position at input data.
	 */
	private int currentIndex;
	
	/**
	 * Current state of lexer.
	 * Can be any state in LexerState.
	 */
	private LexerState state;

	/**
	 * Lexer constructor. Takes input string and 
	 * sets lexer to initial state.
	 * 
	 * @param text input string
	 */
	public Lexer(String text) {
		if (text == null) {
			throw new IllegalArgumentException();
		}
		data = text.trim().toCharArray();
		currentIndex = 0;
		state = LexerState.BASIC;
	}

	/**
	 * Calculates next token starting from current position.
	 * Follows token rules and form next token.
	 * 
	 * @return returns {@link Token} representing next calculated token
	 * @throws LexerException if token syntax is wrong
	 */
	public Token nextToken() {
		StringBuilder sb = new StringBuilder();

		while (currentIndex < data.length && Character.isWhitespace(data[currentIndex])) { // ignore
																							// whitespace
			currentIndex++;
		}

		if (currentIndex == data.length) {
			currentIndex++;
			token = new Token(TokenType.EOF, null);
		} else if (currentIndex > data.length) {
			throw new LexerException("Error");
		} else {
			if (state == LexerState.BASIC) {

				if (Character.isLetter(data[currentIndex]) || data[currentIndex] == '\\') { // rijec
					while (currentIndex < data.length
							&& (Character.isLetter(data[currentIndex]) || data[currentIndex] == '\\')) {
						if (Character.isLetter(data[currentIndex])) {
							sb.append(data[currentIndex++]);
						} else {
							if (currentIndex + 1 < data.length) {
								currentIndex++;
								if (data[currentIndex] == '\\' || Character.isDigit(data[currentIndex])) {
									sb.append(data[currentIndex++]);
								} else {
									throw new LexerException("Wrong escaping syntax!");
								}
							} else {
								throw new LexerException("Wrong escaping syntax!");
							}
						}
					}
					token = new Token(TokenType.WORD, sb.toString());
				} else if (Character.isDigit(data[currentIndex])) { // broj
					while (currentIndex < data.length && Character.isDigit(data[currentIndex])) {
						sb.append(data[currentIndex++]);
					}

					try {
						token = new Token(TokenType.NUMBER, Long.parseLong(sb.toString()));
					} catch (NumberFormatException e) {
						throw new LexerException("Wrong input. Number not parsable as long!");
					}
				} else { // simbol
					token = new Token(TokenType.SYMBOL, new Character(data[currentIndex++]));
				}
			} else {
				if (currentIndex < data.length
						&& (Character.isLetterOrDigit(data[currentIndex]) || data[currentIndex] == '\\')) {
					while (currentIndex < data.length
							&& (Character.isLetterOrDigit(data[currentIndex]) || data[currentIndex] == '\\')) {
						sb.append(data[currentIndex++]);
					}
					token = new Token(TokenType.WORD, sb.toString());

				} else {
					token = new Token(TokenType.SYMBOL, new Character(data[currentIndex++]));
				}
			}
		}

		return token;
	}
	
	/**
	 * Token getter.
	 * Gets last calculated token.
	 * Can be null if nextToken() was never called.
	 * 
	 * @return returns last calculated token or null
	 * 
	 */
	public Token getToken() {
		return token;
	}

	/**
	 * State setter.
	 * Allows state of Lexer to be changed from outside.
	 * 
	 * @param state state of lexer to be changed to. Valid states are {@link LexerState}
	 * @throws LexerException if passed state parameter is null
	 */
	public void setState(LexerState state) {
		if (state == null) {
			throw new IllegalArgumentException();
		}
		this.state = state;
	}

}
