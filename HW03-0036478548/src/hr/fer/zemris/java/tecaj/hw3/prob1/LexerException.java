package hr.fer.zemris.java.tecaj.hw3.prob1;

/**
 * Basic exception class used in {@link Lexer}.
 * 
 * @author Filip Dujmušić
 *
 */
public class LexerException extends RuntimeException {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 6946717675457465100L;

	/**
	 * Empty constructor
	 */
	public LexerException() {
		super();
	}

	/**
	 * Constructor takes input message and displays it to user
	 * 
	 * @param message
	 *            message describing exception which occured
	 */
	public LexerException(String message) {
		super(message);
	}

}
