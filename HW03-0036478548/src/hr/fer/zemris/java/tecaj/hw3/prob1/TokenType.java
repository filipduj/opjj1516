package hr.fer.zemris.java.tecaj.hw3.prob1;

/**
 * Enum provides possible {@link Token} types.
 * 
 * @author Filip Dujmušić
 *
 */
public enum TokenType {
	/**
	 * End of file
	 */
	EOF, 
	
	/**
	 * Word token
	 */
	WORD, 
	
	/**
	 * Number token
	 */
	NUMBER, 
	
	/**
	 * Symbol token
	 */
	SYMBOL;
}
