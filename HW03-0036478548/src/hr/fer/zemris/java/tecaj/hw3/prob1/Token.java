package hr.fer.zemris.java.tecaj.hw3.prob1;

/**
 * Token wrapper class.
 * Wraps any kind of token in the way that
 * it keeps it's value and type.
 * 
 * @author Filip Dujmušić
 *
 */
public class Token {

	/**
	 * Token type
	 */
	private TokenType type;
	
	/**
	 * Token value
	 */
	private Object value;

	/**
	 * Constructor takes value and type
	 * and initializes new Token.
	 * 
	 * @param value any value 
	 * @param type legal types are in {@link TokenType} enum
	 * @throws IllegalArgumentException if type is null reference
	 */
	public Token(TokenType type, Object value) {
		this.type = type;
		this.value = value;
	}

	/**
	 * Value getter
	 * 
	 * @return returns token value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * Type getter
	 * 
	 * @return returns token type
	 */
	public TokenType getType() {
		return type;
	}
}
