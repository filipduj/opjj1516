package hr.fer.zemris.java.tecaj.hw3.prob1;

/**
 * Class defines all states for {@link Lexer}
 * 
 * @author Filip Dujmušić
 *
 */
public enum LexerState {

	/**
	 * Basic state
	 */
	BASIC,

	/**
	 * Extended state
	 */
	EXTENDED;
}
