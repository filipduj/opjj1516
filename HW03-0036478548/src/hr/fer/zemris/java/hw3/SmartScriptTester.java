package hr.fer.zemris.java.hw3;

import java.io.IOException;


import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import hr.fer.zemris.java.custom.scripting.nodes.DocumentNode;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParser;
import hr.fer.zemris.java.custom.scripting.parser.SmartScriptParserException;

/**
 * Class to test basic functionality of {@link SmartScriptParser}. Takes path to
 * input file as command line argument and tries to parse it into tree-like
 * structure.
 * 
 * @author Filip Dujmušić
 *
 */
public class SmartScriptTester {

	/**
	 * Main method - entry point for program.
	 * 
	 * @param args
	 *            command line argument
	 */
	public static void main(String[] args) {
		String input = null;
		try {
			input = new String(Files.readAllBytes(Paths.get(args[0])), StandardCharsets.UTF_8);
		} catch (IOException e) {
			System.err.println("Error reading input file. Exiting...");
			System.exit(-1);
		}

		SmartScriptParser parser = null;
		try {
			parser = new SmartScriptParser(input.trim());
		} catch (SmartScriptParserException e) {
			e.printStackTrace();
			System.out.println("Unable to parse document!");
			System.exit(-1);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("If this line ever executes, you have failed this class!");
			System.exit(-1);
		}
		
		SmartScriptParser parser2 = null;
		try {
			parser2 = new SmartScriptParser(parser.getDocumentNode().toString());
		} catch (SmartScriptParserException e) {
			e.printStackTrace();
			System.out.println("Unable to parse document!");
			System.exit(-1);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("If this line ever executes, you have failed this class!");
			System.exit(-1);
		}
		
		
		DocumentNode document = parser.getDocumentNode(); 		//tree structure
		DocumentNode document2 = parser2.getDocumentNode();		//should be the same

		System.out.println(document);
		System.out.println();
		System.out.println(document2);
	}
}