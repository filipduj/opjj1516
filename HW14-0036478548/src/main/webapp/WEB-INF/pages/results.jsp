<%@page import="java.util.List" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>

<head>
<style type="text/css">
			table.rez td {
				text-align: center;
			}
		</style>

<title>OPJJ1516 - HW14 - Results</title>
</head>

<body>
	<a href="index.html">Go Home</a>
	<hr>
	<table border="1" class="rez">

		<thead>
			<tr>
				<th>${poll.title}</th>
				<th>Broj glasova</th>
			</tr>
		</thead>

		<tbody>
			<c:forEach items="${options}" var="option">
				<tr>
					<td>${option.title}</td>
					<td>${option.votes}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
	<h2>Grafički prikaz rezultata</h2>
	<img alt="Pie-chart" src="results-piechart?pollID=${poll.id}" width="400" height="400" />
	
	<h2>Rezultati u XLS formatu</h2>
 	<p>Rezultati u XLS formatu dostupni su <a href="results-xls?pollID=${poll.id}">ovdje</a></p>
		
	<h2>Razno</h2>
 		<p>Pobjednici:</p>
		<ul>
		<c:forEach items="${winners}" var="winner" > 
			<li><a href="${winner.link}" target="_blank">${winner.title}</a></li>	
		</c:forEach>
		</ul>
		
</body>

</html>