<%@page import="java.util.List" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
	
	<head>
		<title>OPJJ1516 - HW14 - Vote Application</title>
		<link rel="stylesheet" type="text/css" href="styles/style.css">	
	</head>
	
	<body>
		<h1>Ankete</h1>

		<table>
		
		<c:forEach items="${polls}" var="poll">
		<tr style="outline: thin solid">
		<td>
			<h2>${poll.title}</h2>
		</td>
		<td>
			<a class="btn" href="glasanje?pollID=${poll.id}">Glasaj</a>
			<a class="btn" href="results?pollID=${poll.id}">Rezultati</a>
				
		</td>	
		</tr>
		</c:forEach>
		
		</table>
	</body>

</html>