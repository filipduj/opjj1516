<%@page import="java.util.List" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>

<head>
<title>OPJJ1516 - HW14 - Vote</title>
</head>

<body>
	<a href="index.html">Go Home</a>
	<hr>
	<h2>${poll.title}</h2>
	<p>${poll.message}</p>
	<hr>
	<ol>
		<c:forEach items="${options}" var="option">
			<li><a href="glasanje-glasaj?optionID=${option.id}">${option.title}</a></li>
		</c:forEach>
	</ol>
	<hr>
</body>

</html>