package hr.fer.zemris.java.tecaj_13.model;

/**
 * Structure models one poll option.
 * <p>
 * Every option has title, link, id of poll to which
 * it belongs and current votes count.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 16, 2016)
 */
public class PollOption implements Comparable<PollOption> {
	
	/**
	 * Unique option id
	 */
	private final int id;
	
	/**
	 * Option title
	 */
	private final String title;
	
	/**
	 * Option link
	 */
	private final String link;
	
	/**
	 * ID of poll to which option belongs
	 */
	private final int pollID;
	
	/**
	 * Votes count
	 */
	private final int votes;

	/**
	 * Default constructor initializes poll option with given parameters.
	 * 
	 * @param id Unique option id
	 * @param title Option title
	 * @param link Option link
	 * @param pollID ID of poll to which option belongs
	 * @param votes Initial votes count
	 */
	public PollOption(int id, String title, String link, int pollID, int votes) {
		super();
		this.id = id;
		this.title = title;
		this.link = link;
		this.pollID = pollID;
		this.votes = votes;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the link
	 */
	public String getLink() {
		return link;
	}

	/**
	 * @return the pollID
	 */
	public int getPollID() {
		return pollID;
	}

	/**
	 * @return the votes
	 */
	public int getVotes() {
		return votes;
	}
	
	/**
	 * @return option id
	 */
	public int getId(){
		return id;
	}

	@Override
	public int compareTo(PollOption o) {
		return o.votes - this.votes;
	}
	
	

}
