package hr.fer.zemris.java.tecaj_13.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAO;
import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.PollOption;

/**
 * Utility class provides different static methods for work with this database
 * web app.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 16, 2016)
 */
public class WebappUtility {

	/**
	 * Path to first poll definition
	 */
	private static final String TEST_POLL_1 = "poll1.txt";

	/**
	 * Path to second poll definition
	 */
	private static final String TEST_POLL_2 = "poll2.txt";

	/**
	 * Method cheks if polls exist and if not it loads test polls and fills
	 * database with loaded data.
	 */
	public static void checkTableExists() {
		if (DAOProvider.getDao().getPolls().isEmpty()) {
			addPoll(TEST_POLL_1);
			addPoll(TEST_POLL_2);
		}
	}

	/**
	 * Method attempts to parse given {@code parameter} from get request as
	 * integer value.
	 * 
	 * @param parameter
	 *            Get parameter
	 * @return Returns integer if parsing was succesful
	 * @throws RuntimeException
	 *             If parsing failed
	 */
	public static int parseIntegerParameter(String parameter) {

		int retValue;

		if (parameter == null) {
			throw new RuntimeException("Error: " + parameter + " not given!");
		}

		try {
			retValue = Integer.parseInt(parameter);
		} catch (NumberFormatException e) {
			throw new RuntimeException("Error: " + parameter + " parameter is not number!");
		}

		return retValue;
	}

	/**
	 * This method attempts to read all poll options for poll with id equal to
	 * {@code pollID}. If no such poll exists request is redirected to error
	 * page.
	 * 
	 * @param pollID
	 *            Poll id
	 * @param req
	 *            {@link HttpServletRequest} request
	 * @param resp
	 *            {@link HttpServletResponse} response
	 * @return Returns List of poll options
	 */
	public static List<PollOption> getPollOptions(int pollID, HttpServletRequest req, HttpServletResponse resp) {
		List<PollOption> pollOptions = DAOProvider.getDao().getPollOptions(pollID);

		if (pollOptions.isEmpty()) {
			String param = req.getParameter("pollID");
			String errorMessage = (param == null) ? "Poll id not given!" : "Poll with id=" + param + " does not exist!";
			req.setAttribute("errorMessage", errorMessage);
			try {
				req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			} catch (Exception ingorable) {
			}
		}

		return pollOptions;
	}

	/**
	 * Method attempts to extract value with {@code parameter} key from
	 * {@code req} request and parse it as integer.
	 * 
	 * @param parameter
	 *            Parameter name
	 * @param req
	 *            {@link HttpServletRequest} request
	 * @param resp
	 *            {@link HttpServletResponse} response
	 * @return Returns parsed integer
	 */
	public static int getRequestParameter(String parameter, HttpServletRequest req, HttpServletResponse resp) {
		String param = req.getParameter(parameter);
		int p = -1;
		try {
			p = WebappUtility.parseIntegerParameter(param);
		} catch (RuntimeException e) {
			req.setAttribute("errorMessage", e.getMessage());
			try {
				req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
			} catch (Exception ignorable) {
			}
		}
		return p;
	}

	/**
	 * Method takes given {@code file} path to poll definition and inserts its
	 * contents into database.
	 * 
	 * @param file
	 *            Path to poll file descriptor
	 */
	private static void addPoll(String file) {
		DAO dao = DAOProvider.getDao();
		Path poll = Paths.get(WebappUtility.class.getClassLoader().getResource(file).getPath());
		try (BufferedReader reader = Files.newBufferedReader(poll)) {
			String title = reader.readLine();
			String message = reader.readLine();
			int pollID = dao.addPoll(title, message);
			String line;
			while ((line = reader.readLine()) != null) {
				String[] tokens = line.split("\\t+");
				dao.addPollOption(tokens[0], tokens[1], pollID, 0);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
