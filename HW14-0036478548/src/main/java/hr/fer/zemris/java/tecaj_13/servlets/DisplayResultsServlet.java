package hr.fer.zemris.java.tecaj_13.servlets;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.Poll;
import hr.fer.zemris.java.tecaj_13.model.PollOption;
import hr.fer.zemris.java.tecaj_13.util.WebappUtility;

/**
 * Servlet collects information and all options for selected poll, determines
 * winners with highest number of votes and sends results to jsp rendering page.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 16, 2016)
 */
@WebServlet(name = "resultsServlet", urlPatterns = { "/results" })
public class DisplayResultsServlet extends HttpServlet {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		int pollID = WebappUtility.getRequestParameter("pollID", req, resp);
		List<PollOption> options = WebappUtility.getPollOptions(pollID, req, resp);
		Poll poll = DAOProvider.getDao().getPoll(pollID);
		options.sort(null);

		req.setAttribute("options", options);
		req.setAttribute("poll", poll);
		req.setAttribute("winners", getWinners(options));

		req.getRequestDispatcher("/WEB-INF/pages/results.jsp").forward(req, resp);

	}

	/**
	 * Takes list of options for some poll and returns list of options with
	 * highest number of votes. If more than one option have the same number of
	 * votes all of them are returned in list.
	 * 
	 * @param options
	 *            List of poll options
	 * @return List of winner poll options
	 */
	private List<PollOption> getWinners(List<PollOption> options) {
		int maxVoteCount = options.stream().mapToInt(PollOption::getVotes).max().getAsInt();
		return options.stream().filter(o -> o.getVotes() == maxVoteCount).collect(Collectors.toList());
	}

}
