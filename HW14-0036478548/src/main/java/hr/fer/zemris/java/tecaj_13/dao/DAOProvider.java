package hr.fer.zemris.java.tecaj_13.dao;

import hr.fer.zemris.java.tecaj_13.dao.sql.SQLDAO;

/**
 * Singleton class which knows which object to return as service provider for
 * accessing persistent data structure.
 * 
 * @author marcupic
 */
public class DAOProvider {

	/**
	 * Data access service provider
	 */
	private static DAO dao = new SQLDAO();

	/**
	 * Provider getter.
	 * 
	 * @return returns object capable of providing access to persistent data
	 *         structure
	 */
	public static DAO getDao() {
		return dao;
	}

}