package hr.fer.zemris.java.tecaj_13.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.Poll;
import hr.fer.zemris.java.tecaj_13.model.PollOption;
import hr.fer.zemris.java.tecaj_13.util.WebappUtility;

/**
 * Servlet prepares all the vote options for selected poll and sends them to jsp
 * accountable for voting process.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 16, 2016)
 */
@WebServlet(name = "vote", urlPatterns = { "/glasanje" })
public class VoteServlet extends HttpServlet {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int pollID = WebappUtility.getRequestParameter("pollID", req, resp);
		List<PollOption> pollOptions = WebappUtility.getPollOptions(pollID, req, resp);
		Poll poll = DAOProvider.getDao().getPoll(pollID);
		req.setAttribute("options", pollOptions);
		req.setAttribute("poll", poll);
		req.getRequestDispatcher("/WEB-INF/pages/vote.jsp").forward(req, resp);

	}

}
