package hr.fer.zemris.java.tecaj_13.dao;

/**
 * Exception instances of this class denote something wrong happend while
 * interacting with persistent data structure.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 16, 2016)
 */
public class DAOException extends RuntimeException {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor
	 */
	public DAOException() {
	}

	/**
	 * Constructor initializes new exception with given {@code message} and
	 * {@code cause} parameters.
	 * 
	 * @param message
	 *            Exception description
	 * @param cause
	 *            Object describing cause of exception
	 */
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}

}