package hr.fer.zemris.java.tecaj_13.dao;

import java.util.List;

import hr.fer.zemris.java.tecaj_13.model.Poll;
import hr.fer.zemris.java.tecaj_13.model.PollOption;

/**
 * Interface towards data persistent data structure
 * 
 * @author marcupic
 *
 */
public interface DAO {

	/**
	 * Method adds new poll defined by {@code title} and {@code name} to
	 * database.
	 * 
	 * @param title
	 *            Poll title
	 * @param message
	 *            Poll message
	 * @return returns auto generated id of newly added poll
	 */
	public int addPoll(String title, String message);

	/**
	 * Adds new poll option defined by {@code optionTitle} and
	 * {@code optionLink} to database. Option is connected to one poll with id
	 * equal to {@code pollID} and initial votes count is set to
	 * {@code votesCount}
	 * 
	 * @param optionTitle
	 *            Option title
	 * @param optionLink
	 *            Option link
	 * @param pollID
	 *            ID ID of poll to which this option is assigned
	 * @param votesCount
	 *            Initial number of vote counts
	 */
	public void addPollOption(String optionTitle, String optionLink, int pollID, int votesCount);

	/**
	 * Returns the list of polls currently stored in persistent data structure.
	 * 
	 * @return List of {@link Poll} objects
	 */
	public List<Poll> getPolls();

	/**
	 * Returns the list of poll options of poll defined with its {@code pollID}.
	 * 
	 * @param pollID
	 *            Unique ID of poll
	 * @return List of {@link PollOption} objects
	 */
	public List<PollOption> getPollOptions(int pollID);

	/**
	 * Returns {@link Poll} with given {@code pollID}.
	 * 
	 * @param pollID
	 *            ID of wanted poll
	 * 
	 * @return {@link Poll}
	 */
	public Poll getPoll(int pollID);

	/**
	 * Updates option with unique id equal to {@code optionID}. Increases total
	 * votes count by one.
	 * 
	 * @param optionID
	 *            Unique ID of option
	 * @return Returns ID of poll to which this option belongs
	 */
	public int updateVote(int optionID);

}