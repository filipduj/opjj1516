package hr.fer.zemris.java.tecaj_13.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.java.tecaj_13.dao.DAO;
import hr.fer.zemris.java.tecaj_13.dao.DAOException;
import hr.fer.zemris.java.tecaj_13.model.Poll;
import hr.fer.zemris.java.tecaj_13.model.PollOption;

/**
 * Implementation of Data Access Object subsystem with SQL technology.
 * <p>
 * This implementation expects connection to be set up for usage through
 * {@link SQLConnectionProvider} class which means someone else should set up
 * that parameter before entering methods of this implementation.
 *
 * @author marcupic
 * 
 */
public class SQLDAO implements DAO {

	/**
	 * Query for inserting new poll in database
	 */
	private static final String INSERT_POLL = "INSERT INTO POLLS (TITLE, MESSAGE) VALUES (?, ?)";
	
	
	/**
	 * Query for inserting new poll option in database
	 */
	private static final String INSERT_POLL_OPTION = "INSERT INTO POLLOPTIONS (OPTIONTITLE, OPTIONLINK, POLLID, VOTESCOUNT) VALUES (?, ?, ?, ?)";
	
	/**
	 * Query for retrieving all polls from database
	 */
	private static final String GET_POLLS = "SELECT * FROM POLLS";
	
	/**
	 * Query for retrieving all poll with given id from database
	 */
	private static final String GET_POLL = "SELECT * FROM POLLS WHERE ID=?";
	
	/**
	 * Query for retrieving all poll options for given poll id
	 */
	private static final String GET_POLL_OPTIONS = "SELECT * FROM POLLOPTIONS WHERE POLLID=?";
	
	/**
	 * Query updates votes count for option with given option id
	 */
	private static final String UPDATE_VOTE = "UPDATE POLLOPTIONS SET VOTESCOUNT=VOTESCOUNT+1 WHERE ID=?";
	
	/**
	 * Retrieves unique poll id for given poll option
	 */
	private static final String GET_POLL_ID = "SELECT POLLID FROM POLLOPTIONS WHERE ID=?";

	@Override
	public int addPoll(String title, String message) {
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		int generatedKey = -1;

		try {
			pst = con.prepareStatement(INSERT_POLL, Statement.RETURN_GENERATED_KEYS);
			pst.setString(1, title);
			pst.setString(2, message);
			pst.executeUpdate();
			ResultSet rs = pst.getGeneratedKeys();
			try {
				if (rs != null && rs.next()) {
					generatedKey = rs.getInt(1);
				}
			} finally {
				try {
					rs.close();
				} catch (Exception ignorable) {
				}
			}

		} catch (Exception ex) {
			throw new DAOException("Pogreška prilikom dodavanja ankete!", ex);
		}

		return generatedKey;
	}

	@Override
	public void addPollOption(String title, String link, int pollID, int votesCount) {

		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;

		try {
			pst = con.prepareStatement(INSERT_POLL_OPTION);
			pst.setString(1, title);
			pst.setString(2, link);
			pst.setInt(3, pollID);
			pst.setInt(4, votesCount);
			try {
				pst.executeUpdate();
			} finally {
				try {
					pst.close();
				} catch (Exception ignorable) {
				}
			}
		} catch (Exception ex) {
			throw new DAOException("Pogreška prilikom dodavanja opcije ankete!", ex);
		}

	}

	@Override
	public List<Poll> getPolls() {
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		List<Poll> pollsList = new LinkedList<>();

		try {
			pst = con.prepareStatement(GET_POLLS);
			ResultSet rs = pst.executeQuery();
			try {
				rs = pst.getResultSet();
				if (rs != null) {
					while (rs.next()) {
						pollsList.add(new Poll(rs.getInt(1), rs.getString(2), rs.getString(3)));
					}
				}
			} finally {
				try {
					rs.close();
				} catch (Exception ignorable) {
				}
			}
		} catch (Exception ex) {
			throw new DAOException("Pogreška prilikom dohvata ankete!", ex);
		}

		return pollsList;
	}

	@Override
	public List<PollOption> getPollOptions(int pollID) {
		Connection con = SQLConnectionProvider.getConnection();
		List<PollOption> pollOptionsList = new LinkedList<>();

		try {
			PreparedStatement pst = con.prepareStatement(GET_POLL_OPTIONS);
			pst.setInt(1, pollID);
			ResultSet rs = pst.executeQuery();
			try {
				if (rs != null) {
					while (rs.next()) {
						pollOptionsList.add(new PollOption(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4),
								rs.getInt(5)));
					}
				}
			} finally {
				try {
					rs.close();
				} catch (Exception ignorable) {
				}
			}

		} catch (Exception ex) {
			throw new DAOException("Pogreška prilikom dohvata opcija ankete!", ex);
		}

		return pollOptionsList;
	}

	@Override
	public Poll getPoll(int pollID) {
		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pst = null;
		Poll poll = null;

		try {
			pst = con.prepareStatement(GET_POLL);
			pst.setInt(1, pollID);
			ResultSet rs = pst.executeQuery();
			try {
				if (rs != null && rs.next()) {
					poll = new Poll(rs.getInt(1), rs.getString(2), rs.getString(3));
				}
			} finally {
				try {
					rs.close();
				} catch (Exception ignorable) {
				}
			}

		} catch (Exception ex) {
			throw new DAOException("Pogreška prilikom dohvata opcija ankete!", ex);
		}

		return poll;
	}

	@Override
	public int updateVote(int optionID) {

		Connection con = SQLConnectionProvider.getConnection();
		PreparedStatement pstUpdate = null;
		PreparedStatement pstGetID = null;
		int poll = -1;

		try {

			pstUpdate = con.prepareStatement(UPDATE_VOTE);
			pstUpdate.setInt(1, optionID);

			pstGetID = con.prepareStatement(GET_POLL_ID);
			pstGetID.setInt(1, optionID);

			pstUpdate.executeUpdate();
			ResultSet rs = pstGetID.executeQuery();
			try {
				if (rs != null && rs.next())
					poll = rs.getInt(1);
			} finally {
				try {
					rs.close();
				} catch (Exception ignorable) {
				}
			}

		} catch (Exception ex) {
			throw new DAOException("Pogreška prilikom upisivanja glasa!", ex);
		}
		return poll;

	}

}