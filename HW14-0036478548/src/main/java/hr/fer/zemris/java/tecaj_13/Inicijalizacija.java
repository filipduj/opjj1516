package hr.fer.zemris.java.tecaj_13;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

import hr.fer.zemris.java.tecaj_13.dao.sql.SQLConnectionProvider;
import hr.fer.zemris.java.tecaj_13.util.WebappUtility;

/**
 * Sets up web application in initializing moment.
 * <p>
 * Upon initialization connection to database is 
 * established. If database does not contain required
 * Polls and PollOptions tables they are created at
 * this place.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 16, 2016)
 */
@WebListener
public class Inicijalizacija implements ServletContextListener {

	/**
	 * Path to database connection configuration file
	 */
	private static final String DB_CONFIG_PATH = "WEB-INF/dbsettings.properties";

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		try {

			Properties dbConfig = new Properties();
			InputStream iStream = sce.getServletContext().getResourceAsStream(DB_CONFIG_PATH);
			dbConfig.load(iStream);

			String connectionURL = new StringBuilder().append("jdbc:derby://")
												      .append(dbConfig.getProperty("host")).append(":")
												      .append(dbConfig.getProperty("port")).append("/")
												      .append(dbConfig.getProperty("name")).append(";user=")
												      .append(dbConfig.getProperty("user")).append(";password=")
												      .append(dbConfig.getProperty("password"))
												      .toString();

			ComboPooledDataSource cpds = new ComboPooledDataSource();
			cpds.setDriverClass("org.apache.derby.jdbc.ClientDriver");
			cpds.setJdbcUrl(connectionURL);
			sce.getServletContext().setAttribute("hr.fer.zemris.dbpool", cpds);

			try {
				// create polls table
				cpds.getConnection()
						.prepareStatement(
								"CREATE TABLE Polls \n" + 
										"(id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, \n" + 
										"title VARCHAR(150) NOT NULL, \n" + 
										"message CLOB(2048) NOT NULL)"
										).executeUpdate();

				// create poll options table
				cpds.getConnection()
						.prepareStatement("CREATE TABLE PollOptions \n" + 
									      "(id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY, \n" + 
									      "optionTitle VARCHAR(100) NOT NULL, \n" + 
									      "optionLink VARCHAR(150) NOT NULL, \n"  + 
									      "pollID BIGINT, \n" + 
									      "votesCount BIGINT, \n" + 
									      "FOREIGN KEY (pollID) REFERENCES Polls(id))"
									      ).executeUpdate();

			} catch (SQLException e) {
				if (!e.getSQLState().equals("X0Y32")) { // if error code is not
														// "table exists"
														// rethrow!
					throw new RuntimeException(e.getMessage());
				}
			}

			Connection connection = cpds.getConnection();
			SQLConnectionProvider.setConnection(connection);
			WebappUtility.checkTableExists();
			SQLConnectionProvider.setConnection(null);
			connection.close();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(-1);
		} 

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ComboPooledDataSource cpds = (ComboPooledDataSource) sce.getServletContext()
				.getAttribute("hr.fer.zemris.dbpool");
		if (cpds != null) {
			try {
				DataSources.destroy(cpds);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}