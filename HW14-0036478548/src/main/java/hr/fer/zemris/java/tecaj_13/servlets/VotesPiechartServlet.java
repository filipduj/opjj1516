package hr.fer.zemris.java.tecaj_13.servlets;

import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.util.Rotation;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.PollOption;
import hr.fer.zemris.java.tecaj_13.util.WebappUtility;

/**
 * Generates pie chart graphical representation
 * of results for selected poll.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 16, 2016)
 */
@WebServlet(name = "votePieChart", urlPatterns = { "/results-piechart" })
public class VotesPiechartServlet extends HttpServlet {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int pollID = 0;
		String pollIDParameter = req.getParameter("pollID");

		try {
			pollID = WebappUtility.parseIntegerParameter(pollIDParameter);
		} catch (RuntimeException e) {
			return;
		}

		List<PollOption> options = DAOProvider.getDao().getPollOptions(pollID);

		PieDataset dataset = createDataset((List<PollOption>) options);
		JFreeChart chart = createChart(dataset, "Band votes distribution");
		ImageIO.write(chart.createBufferedImage(400, 300), "png", resp.getOutputStream());
	}

	/**
	 * Takes given {@code dataset} and {@code title} and generates new
	 * {@link JFreeChart} object.
	 * 
	 * @param dataset
	 *            Dataset for chart plotting
	 * @param title
	 *            Chart title
	 * @return Returns {@link JFreeChart} object built from given data
	 */
	private JFreeChart createChart(PieDataset dataset, String title) {
		JFreeChart chart = ChartFactory.createPieChart3D(title, // chart title
				dataset, // data
				true, // include legend
				true, false);

		PiePlot3D plot = (PiePlot3D) chart.getPlot();
		plot.setStartAngle(290);
		plot.setDirection(Rotation.CLOCKWISE);
		plot.setForegroundAlpha(0.5f);
		return chart;
	}

	/**
	 * Creates dataset for vote options. 
	 * 
	 * @param options
	 *            List of options belonging to some poll
	 * @return Returns {@link PieDataset} with percentage assigned to each option
	 *         
	 */
	private PieDataset createDataset(List<PollOption> options) {
		final DefaultPieDataset result = new DefaultPieDataset();
		options.forEach(opt -> {
			if (opt.getVotes() != 0) {
				result.setValue(opt.getTitle(), opt.getVotes());
			}
		});
		return result;
	}

}
