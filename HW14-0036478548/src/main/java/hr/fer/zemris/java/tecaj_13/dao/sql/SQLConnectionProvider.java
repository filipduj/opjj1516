package hr.fer.zemris.java.tecaj_13.dao.sql;

import java.sql.Connection;


/**
 * Storage of connection into {@link ThreadLocal} objects.
 * 
 * @author marcupic
 */
public class SQLConnectionProvider {
	
	/**
	 * Map of thread,connection pairs
	 */
	private static ThreadLocal<Connection> connections = new ThreadLocal<>();
	
	/**
	 * Sets up connection for current thread (or deletes entry from map if {@code con} is {@code null} reference
	 * 
	 * @param con Connection towards database
	 */
	public static void setConnection(Connection con) {
		if(con==null) {
			connections.remove();
		} else {
			connections.set(con);
		}
	}
	
	/**
	 * Gets connection bound to current thread
	 * 
	 * @return connection towards database
	 */
	public static Connection getConnection() {
		return connections.get();
	}
	
}