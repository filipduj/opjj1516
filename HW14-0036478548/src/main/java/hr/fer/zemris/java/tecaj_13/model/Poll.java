package hr.fer.zemris.java.tecaj_13.model;

/**
 * Structure models one Poll object.
 * <p>
 * Every poll has it's title and message.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 16, 2016)
 */
public class Poll {
	
	/**
	 * Unique poll id
	 */
	private final int id;
	
	/**
	 * Poll title
	 */
	private final String title;
	
	/**
	 * Poll message
	 */
	private final String message;
	
	/**
	 * Default constructor initializes instance of poll
	 * with given parameters.
	 * 
	 * @param id Unique poll id
	 * @param title Title
	 * @param message Message
	 */
	public Poll(int id, String title, String message) {
		this.id = id;
		this.title = title;
		this.message = message;
	}
	
	/**
	 * @return poll id
	 */
	public int getId(){
		return id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}


}
