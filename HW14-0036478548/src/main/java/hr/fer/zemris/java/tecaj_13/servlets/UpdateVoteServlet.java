package hr.fer.zemris.java.tecaj_13.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.util.WebappUtility;

/**
 * Servlet increases vote count of selected poll option 
 * and redirects to jsp used for displaying poll results.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 16, 2016)
 */
@WebServlet(name="updateVote", urlPatterns={"/glasanje-glasaj"})
public class UpdateVoteServlet extends HttpServlet {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int optionID = WebappUtility.getRequestParameter("optionID", req, resp);
		int pollID = DAOProvider.getDao().updateVote(optionID);	
		if(pollID == -1){ 
			req.setAttribute("errorMessage", "Option with id=" + optionID + " does not exist!");
			req.getRequestDispatcher("/WEB-INF/pages/error.jsp").forward(req, resp);
		}
		req.getRequestDispatcher("results?pollID="+pollID).forward(req, resp);
	}
	
}
