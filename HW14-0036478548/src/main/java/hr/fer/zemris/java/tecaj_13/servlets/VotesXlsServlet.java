package hr.fer.zemris.java.tecaj_13.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;

import hr.fer.zemris.java.tecaj_13.dao.DAOProvider;
import hr.fer.zemris.java.tecaj_13.model.PollOption;
import hr.fer.zemris.java.tecaj_13.util.WebappUtility;

/**
 * Generates xls file containing vote results
 * of selected poll.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Jun 16, 2016)
 */
@WebServlet(name = "voteXls", urlPatterns = { "/results-xls" })
public class VotesXlsServlet extends HttpServlet {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		int pollID = 0;
		String pollIDParameter = req.getParameter("pollID");

		try {
			pollID = WebappUtility.parseIntegerParameter(pollIDParameter);
		} catch (RuntimeException e) {
			return;
		}

		List<PollOption> options = DAOProvider.getDao().getPollOptions(pollID);

		// create new workbook and one sheet
		Workbook workbook = new HSSFWorkbook();
		Sheet sheet = workbook.createSheet(WorkbookUtil.createSafeSheetName("Results"));

		// add beginning description row
		Row rowhead = sheet.createRow(0);
		rowhead.createCell(0).setCellValue("Option name");
		rowhead.createCell(1).setCellValue("Number of votes");

		// add result rows
		int rowCount = 1;
		for (PollOption option : options) {
			Row row = sheet.createRow(rowCount++);
			row.createCell(0).setCellValue(option.getTitle());
			row.createCell(1).setCellValue(option.getVotes());

		}

		// write to output stream
		resp.setContentType("application/vnd.ms-excel");
		resp.setHeader("Content-Disposition", "attachment; " + "filename=\"results.xls\"");
		workbook.write(resp.getOutputStream());
		workbook.close();

	}

}
