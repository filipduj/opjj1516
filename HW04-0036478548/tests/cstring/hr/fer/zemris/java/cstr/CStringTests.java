package hr.fer.zemris.java.cstr;

import static org.junit.Assert.*;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class CStringTests {

	@Test(expected = NullPointerException.class)
	public void testConstructorWithNullCharacterArray() {
		char[] input = null;
		new CString(input);
	}

	@Test(expected = NullPointerException.class)
	public void testConstructorWithNullString() {
		String input = null;
		new CString(input);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testConstructorWithNullCString() {
		CString input = null;
		new CString(input);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testNegativeLengthConstructor() {
		char[] input = { 'a', 'b', 'c' };
		new CString(input, 0, -1);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testTooBigLengthConstructor() {
		char[] input = { 'a', 'b', 'c' };
		new CString(input, 0, 4);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testNegativeOffsetConstructor() {
		char[] input = { 'a', 'b', 'c' };
		new CString(input, -1, 0);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testTooBigOffsetConstructor() {
		char[] input = { 'a', 'b', 'c' };
		new CString(input, 3, 0);
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testTooBigOffsetLengthConstructor() {
		char[] input = { 'a', 'b', 'c', 'd', 'e', 'f' };
		new CString(input, 2, 5);
	}

	public void testBorderOffsetLengthConstructor() {
		char[] input = { 'a', 'b', 'c', 'd', 'e', 'f' };
		assertArrayEquals(new char[] { 'c', 'd', 'e', 'f' }, new CString(input, 2, 4).toCharArray());
	}

	@Test
	public void testConstructorFromString() {
		String s = new CString("testString").toString();
		assertEquals("testString", s);
	}

	@Test
	public void testConstructorFromEmptyString() {
		assertEquals("", new CString("").toString());
	}

	@Test
	public void testConstructorFromCharArray() {
		char[] array = new char[] { 'a', 'b', 'c', 'd' };
		assertArrayEquals(array, new CString(array).toCharArray());
	}

	@Test
	public void testConstructorFromEmptyCharArray() {
		char[] array = new char[] {};
		assertArrayEquals(array, new CString(array).toCharArray());
	}

	@Test
	public void testConstructorFromCString() {
		CString cs1 = new CString("test");
		CString cs2 = new CString(cs1);
		assertEquals(cs1, cs2);
	}

	@Test
	public void testIndexOf() {
		CString s1 = new CString("yolo");
		assertEquals(s1.indexOf('l'), 2);
		assertEquals(s1.indexOf('z'), -1);
	}

	@Test
	public void testCharAt() {
		CString s1 = new CString("yolo");
		assertEquals(s1.charAt(2), 'l');
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testCharAtUnderLowerBound() {
		CString s1 = new CString("yolo");
		assertEquals(s1.charAt(-1), 'l');
	}

	@Test(expected = IndexOutOfBoundsException.class)
	public void testCharAtAboveUpperBound() {
		CString s1 = new CString("yolo");
		assertEquals(s1.charAt(4), 'l');
	}

	@Test
	public void testStartsWith(){
		CString s1 = new CString("prvistring");
		CString s2 = new CString("prvi");
		
		// "prvistring" does start with "prvi"
		assertEquals(true, s1.startsWith(s2));
		
		// "prvi" does not start with "prvistring"
		assertEquals(false, s2.startsWith(s1));
		
		// "prvistring" does start with "prvistring"
		assertEquals(true, s1.startsWith(s1));
		
		// "prvistring" does start with empty string
		assertEquals(true, s1.startsWith(new CString("")));
	}

	
	@Test
	public void testEndsWith(){
		CString s1 = new CString("prvistring");
		CString s2 = new CString("string");
		assertEquals(true, s1.endsWith(s2));
		assertEquals(false, s2.endsWith(s1));
		assertEquals(true, s2.endsWith(s2));
		assertEquals(true, s2.endsWith(new CString("")));
	}
	
	@Test
	public void  testContains(){
		CString s = new CString("TEst string za contains metodu.");
		assertEquals(true, s.contains(new CString("string")));
		assertEquals(false, s.contains(new CString("strang")));
		assertEquals(true, s.contains(new CString("")));
		assertEquals(true, new CString("").contains(new CString("")));
		assertEquals(true, s.contains(new CString("TEst string za contains metodu.")));
	}
	
	@Test
	public void testSubstring(){
		CString s = new CString("test za substring metodu");
		assertEquals(new CString("t"), s.substring(0, 1));
		assertEquals(new CString("za"),s.substring(5, 7));
		assertEquals(s, s.substring(0, s.length()));
	}
	
	@Test
	public void testtLeft(){
		CString s = new CString("test za left metodu");
		assertEquals(new CString("test"), s.left(4));
		assertEquals(s, s.left(s.length()));
		assertEquals(new CString(""), s.left(0));
	}
	
	@Test
	public void testtRight(){
		CString s = new CString("test za right metodu");
		assertEquals(new CString("metodu"), s.right(6));
		assertEquals(s, s.right(s.length()));
		assertEquals(new CString(""), s.right(0));
	}
	
	@Test
	public void testAdd(){
		CString s = new CString("jabuka");
		assertEquals(new CString("jabuka"), s.add(new CString("")));
		assertEquals(new CString("jabukaKruska"), s.add(new CString("Kruska")));
	}
	
	@Test
	public void testReplaceAllCharacters(){
		CString s = new CString("test data for replace");
		assertEquals("*es* da*a for replace", s.replaceAll('t', '*').toString());
		assertEquals(" es  da a for replace", s.replaceAll('t', ' ').toString());
		assertEquals(s, s.replaceAll('a', 'a'));
	}
	
	@Test
	public void testReplaceAllSubstrings(){
		CString s = new CString("testtest");
		CString ss = s.replaceAll(new CString("test"), new CString(""));
		assertEquals("", ss.toString());
		assertEquals("kvakva", s.replaceAll(new CString("test"), new CString("kva")).toString());
		assertEquals("abababababab", new CString("ababab").replaceAll(new CString("ab"), new CString("abab")).toString());
		
	//	assertEquals("")
	}	
	
}
