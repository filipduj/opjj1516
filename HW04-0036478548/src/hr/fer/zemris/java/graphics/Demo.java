package hr.fer.zemris.java.graphics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import hr.fer.zemris.java.graphics.raster.BWRaster;
import hr.fer.zemris.java.graphics.raster.BWRasterMem;
import hr.fer.zemris.java.graphics.shapes.GeometricShape;
import hr.fer.zemris.java.graphics.shapes.GeometricShapeException;
import hr.fer.zemris.java.graphics.shapes.ShapeFactory;
import hr.fer.zemris.java.graphics.views.SimpleRasterView;

/**
 * Demonstration class.
 * 
 * <p>
 * Program takes command line arguments for raster width and height. Then it
 * enters in interactive mode where user enters arbitrary number of commands
 * telling the program what to draw on raster. Every line after needed number of
 * lines is ignored and input ends after empty line is entered (for example
 * <ENTER> key). After that raster content is displayed on standard output.
 * </p>
 * 
 * <p>
 * Every line can be one of following commands (without quotes):
 * <ul>
 * <li>"FLIP" - turns on flipping mode of raster</li>
 * <li>"RECTANGLE x y w h" - draws rectangle with upper left corner at x,y and
 * width w and height h</li>
 * <li>"SQUARE x y s" - draws square with upper left corner at x,y and side
 * length s</li>
 * <li>"CIRCLE x y r" - draws circle with center at x,y and radius r</li>
 * <li>"ELLIPSE x y a b" - draws ellipse with center at x,y horizontal radius a
 * and vertical radius b</li>
 * </ul>
 * x,y can be arbitrary integers but w,h,s,r must be greater than zero!
 * </p>
 * 
 * @author Filip Dujmušić (6.4.2016)
 *
 */
public class Demo {

	/**
	 * Main method. Program starts here.
	 * 
	 * @param args
	 *            <ul>
	 *            <li>One command line argument - represents raster height and
	 *            width</li>
	 *            <li>Two command line arguments - first is width, second is
	 *            height</li>
	 *            </ul>
	 * @throws IOException
	 *             If reading from standard input but stream closed
	 */
	public static void main(String[] args) {
		try (BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in))) {

			BWRaster raster = createRaster(args);
			String line;
			int currentNumOfShapes = 0;

			System.out.printf("Enter number of lines: ");
			int numberOfLines = Integer.parseInt(stdin.readLine());
			
			if(numberOfLines <= 0){
				throw new IllegalArgumentException("Number of lines must be positive!");
			}

			GeometricShape[] shapes = new GeometricShape[numberOfLines];

			while ((line = stdin.readLine().trim()) != null && line.length() != 0) {
				if (currentNumOfShapes < numberOfLines) {
					GeometricShape shape = ShapeFactory.parseShape(line);
					shapes[currentNumOfShapes++] = shape;
				}
			}

			if (currentNumOfShapes < numberOfLines) {
				exit("Insufficient number of lines entered. Exiting...");
			}

			drawShapes(shapes, raster);

			SimpleRasterView view = new SimpleRasterView();

			view.produce(raster);

		} catch (NumberFormatException ex) {
			exit("Can not parse to integer!");

		} catch (GeometricShapeException ex) {
			exit(ex.getMessage());

		} catch (IllegalArgumentException ex) {
			exit(ex.getMessage());

		} catch (IOException ex) {
			exit("I/O error occured.");
		}
	}

	/**
	 * Method is called just before forced exit (after error occurs).
	 * 
	 * @param message
	 *            Message to be displayed to user
	 */
	private static void exit(String message) {
		System.err.println(message);
		System.exit(-1);
	}

	/**
	 * Draws each shape from {@code shapes} on given {@code raster}.
	 * 
	 * @param shapes
	 *            Array of {@code GeometricShape} objects
	 * @param raster
	 *            {@code BWRaster} raster
	 */
	private static void drawShapes(GeometricShape[] shapes, BWRaster raster) {
		for (GeometricShape shape : shapes) {
			if (shape == null) {
				if (raster.getFlipMode())
					raster.disableFlipMode();
				else
					raster.enableFlipMode();
			} else {
				shape.draw(raster);
			}
		}
	}

	/**
	 * Creates new {@code BWRaster} object from given arguments.
	 * 
	 * @param args
	 *            One or two command line arguments (see {@link Demo})
	 * @return returns new {@code BWRaster} object.
	 * @throws IllegalArgumentException
	 *             If number of {@code args} is not 1 or 2
	 */
	private static BWRaster createRaster(String[] args) {
		if (args.length == 1) {

			int size = Integer.parseInt(args[0]);

			return new BWRasterMem(size, size);

		} else if (args.length == 2) {

			int width = Integer.parseInt(args[0]);
			int height = Integer.parseInt(args[1]);

			return new BWRasterMem(height, width);

		}

		throw new IllegalArgumentException("Wrong number of command line arguments!");
	}

}