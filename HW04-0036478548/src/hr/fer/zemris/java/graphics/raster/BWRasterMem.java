package hr.fer.zemris.java.graphics.raster;

/**
 * {@code BWRasterMem} is implementation of {@link BWRaster} which keeps all the
 * data in memory.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (5.4.2016)
 *
 */
public class BWRasterMem implements BWRaster {

	/**
	 * Width of raster in pixels
	 */
	private int width;

	/**
	 * Height of raster in pixels
	 */
	private int height;

	/**
	 * Pixel memory
	 */
	private boolean data[][];

	/**
	 * State of raster's flipping mode
	 */
	private boolean flipMode;

	/**
	 * Allocates new {@link BWRasterMem} object with width and height set to
	 * {@code width} and {@code height} pixels. Initialy turns off all pixels in
	 * raster and sets non flipping state.
	 * 
	 * @param width
	 *            Width of raster in pixels
	 * @param height
	 *            Height of raster in pixels
	 * @throws IllegalArgumentException
	 *             If {@code width} or {@code height} are less than 1
	 */
	public BWRasterMem(int height, int width) {
		if (width <= 0 || height <= 0) {
			throw new IllegalArgumentException("Raster dimensions must be positive!");
		}
		this.width = width;
		this.height = height;
		this.data = new boolean[height][width];
		this.flipMode = false;
		clear();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getWidth() {
		return width;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getHeight() {
		return height;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void clear() {
		for (int i = 0; i < width; ++i) {
			for (int j = 0; j < height; ++j) {
				data[j][i] = false;
			}
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws IndexOutOfBoundsException
	 *             If {@code x} or {@code y} values out of raster bounds
	 */
	@Override
	public void turnOn(int x, int y) {

		checkCoordinate(x, y);

		if (flipMode) {
			data[y][x] = !data[y][x];
		} else {
			data[y][x] = true;
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws IndexOutOfBoundsException
	 *             If {@code x} and {@code y} values out of raster bounds
	 */
	@Override
	public void turnOff(int x, int y) {

		checkCoordinate(x, y);

		data[y][x] = false;

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void enableFlipMode() {
		flipMode = true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void disableFlipMode() {
		flipMode = false;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean getFlipMode() {
		return flipMode;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws IndexOutOfBoundsException
	 *             If {@code x} and {@code y} values out of raster bounds
	 * 
	 */
	@Override
	public boolean isTurnedOn(int x, int y) {
		checkCoordinate(x, y);
		return data[y][x];
	}

	/**
	 * Checks if {@code x} and {@code y} coordinates are within {@code
	 * BWRasterMem} dimensions. Throws exception for invalid coordinates,
	 * otherwise does nothing.
	 * 
	 * @param x
	 *            x coordinate in pixels
	 * @param y
	 *            y coordinate in pixels
	 * @throws IndexOutOfBoundsException
	 *             If pixel coordinate out of bounds
	 */
	private void checkCoordinate(int x, int y) {
		if (x < 0 || x >= width || y < 0 || y >= height) {
			throw new IndexOutOfBoundsException("Tried to access pixel outside raster bounds!");
		}
	}

}
