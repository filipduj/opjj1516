package hr.fer.zemris.java.graphics.raster;

/**
 * <p>
 * Black and white raster abstraction. Can represent all raster devices of fixed
 * width and height for which each pixel can be painted with only two colors:
 * black (when pixel is turned off) and white (when pixel is turned on).
 * </p>
 * 
 * <p>
 * The coordinate system for raster has (0,0) at the top-left corner of raster;
 * positive x-axis is to the right and positive y-axis is toward the bottom.
 * </p>
 * 
 * 
 * @author Filip Dujmušić
 * @version 1.0 (5.4.2016)
 *
 */
public interface BWRaster {

	/**
	 * Gets the width of raster in pixels
	 * 
	 * @return raster width
	 */
	int getWidth();

	/**
	 * Gets the height of raster in pixels
	 * 
	 * @return raster height
	 */
	int getHeight();

	/**
	 * Clears all the data in raster. Basically it turns off every pixel.
	 * 
	 */
	void clear();

	/**
	 * Unconditionally turns on pixel at position {@code x},{@code y} if flip
	 * mode is turned off, or flips pixel's state if flip mode is turned on.
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 */
	void turnOn(int x, int y);

	/**
	 * Turns of pixel at position {@code x}, {@code y}
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 */
	void turnOff(int x, int y);

	/**
	 * Enables flip mode of raster
	 */
	void enableFlipMode();

	/**
	 * Disables flip mode of raster
	 */
	void disableFlipMode();
	
	/**
	 * Returns state of flipping mode
	 * 
	 * @return {@code true} if flip mode on, {@code false} otherwise
	 */
	boolean getFlipMode();

	/**
	 * Checks if pixel positioned at {@code x},{@code y} is turned on.
	 * 
	 * @param x
	 *            x cooridnate
	 * @param y
	 *            y coordinate
	 * @return returns {@code true} if pixel turned on, {@code false} otherwise
	 */
	boolean isTurnedOn(int x, int y);

}
