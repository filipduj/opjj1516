package hr.fer.zemris.java.graphics.shapes;

/**
 * Rectangle shape implementation.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (5.4.2016)
 *
 */
public class Rectangle extends RectangularShape {

	/**
	 * Allocates new {@code Rectanngle} object. Initializes it's position with
	 * given values.
	 * 
	 * @param x
	 *            x coordinate of top left corner
	 * @param y
	 *            y coordinate of top left corner
	 * @param width
	 *            Rectangle width
	 * @param height
	 *            Rectangle height
	 * @throws IllegalArgumentException
	 *             If width or height less than 1
	 */
	public Rectangle(int x, int y, int width, int height) {
		super(x, y, width, height);
	}

	/**
	 * @return rectangle width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width
	 *            rectangle width
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return rectangle height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height
	 *            rectangle height
	 */
	public void setHeight(int height) {
		this.height = height;
	}

}
