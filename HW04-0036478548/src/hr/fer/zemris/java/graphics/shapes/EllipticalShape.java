package hr.fer.zemris.java.graphics.shapes;

import hr.fer.zemris.java.graphics.raster.BWRaster;

/**
 * Elliptical shape abstraction.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (6.4.2016)
 *
 */
public abstract class EllipticalShape extends GeometricShape {

	/**
	 * x coordinate of center
	 */
	protected int x;

	/**
	 * y coordinate of center
	 */
	protected int y;

	/**
	 * Horizontal radius
	 */
	protected int xRadius;

	/**
	 * Vertical radius
	 */
	protected int yRadius;

	/**
	 * Initializes elliptical shape with given data.
	 * 
	 * @param x
	 *            x coordinate of center of shape
	 * @param y
	 *            y coordinate of center of shape
	 * @param xRadius
	 *            horizontal radius
	 * @param yRadius
	 *            vertical radius
	 * @throws GeometricShapeException
	 *             If {@code xRadius} or {@code yRadius} are less than 1.
	 */
	public EllipticalShape(int x, int y, int xRadius, int yRadius) {

		if (xRadius <= 0 || yRadius <= 0) {
			throw new GeometricShapeException("Radius can not be negative or zero!");
		}

		this.x = x;
		this.y = y;
		this.xRadius = xRadius;
		this.yRadius = yRadius;
	}

	/**
	 * {@inheritDoc} Efficient drawing method. Will only check for pixels in
	 * minimal rectangle enclosing this geometric shape.
	 * 
	 * @throws IllegalArgumentException
	 *             If {@code r} is {@code null} reference
	 * @throws IndexOutOfBoundsException
	 *             If tried accessing pixel out of raster bounds
	 */
	@Override
	public void draw(BWRaster r) {

		if (r == null) {
			throw new IllegalArgumentException();
		}

		int height = r.getHeight();
		int width = r.getWidth();

		int xMin = (x - xRadius) < 0 ? 0 : (x - xRadius);
		int yMin = (y - yRadius) < 0 ? 0 : (y - yRadius);
		int xMax = (x + xRadius) >= width ? width : x + xRadius;
		int yMax = (y + yRadius) >= height ? height : y + yRadius;

		for (int i = xMin; i < xMax; ++i) {
			for (int j = yMin; j < yMax; ++j) {
				if (containsPoint(i, j)) {
					r.turnOn(i, j);
				}
			}
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsPoint(int x, int y) {
		int xRadSq = xRadius * xRadius;
		int yRadSq = yRadius * yRadius;
		return (x - this.x) * (x - this.x) * yRadSq + (y - this.y) * (y - this.y) * xRadSq < xRadSq * yRadSq;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x
	 *            the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y
	 *            the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}
}
