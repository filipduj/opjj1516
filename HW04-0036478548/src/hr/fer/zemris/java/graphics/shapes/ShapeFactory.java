package hr.fer.zemris.java.graphics.shapes;

import hr.fer.zemris.java.graphics.Demo;

/**
 * Factory clas provides single method for parsing shape description string and
 * creating new {@code GeometricShape} objects.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (6.4.2016)
 *
 */
public class ShapeFactory {

	/**
	 * flip command
	 */
	private static final String FLIP_ME = "FLIP";

	/**
	 * rectangle command
	 */
	private static final String RECTANGLE = "RECTANGLE";

	/**
	 * square command
	 */
	private static final String SQUARE = "SQUARE";

	/**
	 * ellipse command
	 */
	private static final String ELLIPSE = "ELLIPSE";

	/**
	 * circle command
	 */
	private static final String CIRCLE = "CIRCLE";

	/**
	 * Parses shape input line command and creates {@code GeometricShape} out of
	 * given string.
	 * 
	 * @param s
	 *            Input line describing one shape (see {@link Demo} for syntax)
	 * @return Returns {@code GeometricShape}
	 * @throws GeometricShapeException
	 *             If shape name is invalid
	 */
	public static GeometricShape parseShape(String s) {
		GeometricShape shape = null;

		if (s.equals(FLIP_ME)) {
			return null;
		} else {
			String[] tokens = s.split(" ");
			if (tokens[0].equals(ELLIPSE)) {
				shape = createEllipse(tokens);
			} else if (tokens[0].equals(CIRCLE)) {
				shape = createCircle(tokens);
			} else if (tokens[0].equals(RECTANGLE)) {
				shape = createRectangle(tokens);
			} else if (tokens[0].equals(SQUARE)) {
				shape = createSquare(tokens);
			} else {
				throw new GeometricShapeException("Invalid shape name!");
			}
		}

		return shape;
	}

	/**
	 * Helper method for creating new {@link Circle} object.
	 * 
	 * @param tokens
	 *            Tokens of one input line describing {@code Circle} object
	 * @return Returns new {@code Circle} object
	 * @throws NumberFormatException
	 *             If one of tokens not parsable to integer
	 * @throws GeometricShapeException
	 *             If shape cannot be created out of given tokens
	 */
	private static GeometricShape createCircle(String[] tokens) {

		if (tokens.length != 4) {
			throw new GeometricShapeException("Invalid number of arguments for CIRCLE!");
		}

		int x = Integer.parseInt(tokens[1]);
		int y = Integer.parseInt(tokens[2]);
		int r = Integer.parseInt(tokens[3]);

		return new Circle(x, y, r);

	}

	/**
	 * Helper method for creating new {@link Square} object.
	 * 
	 * @param tokens
	 *            Tokens of one input line describing {@code Square} object
	 * @return Returns new {@code Square} object
	 * @throws NumberFormatException
	 *             If one of tokens not parsable to integer
	 * @throws GeometricShapeException
	 *             If shape cannot be created out of given tokens
	 */
	private static GeometricShape createSquare(String[] tokens) {
		if (tokens.length != 4) {
			throw new GeometricShapeException("Invalid number of arguments for SQUARE!");
		}

		int x = Integer.parseInt(tokens[1]);
		int y = Integer.parseInt(tokens[2]);
		int size = Integer.parseInt(tokens[3]);

		return new Square(x, y, size);
	}

	/**
	 * Helper method for creating new {@link Rectangle} object.
	 * 
	 * @param tokens
	 *            Tokens of one input line describing {@code Rectangle} object
	 * @return Returns new {@code Rectangle} object
	 * @throws NumberFormatException
	 *             If one of tokens not parsable to integer
	 * @throws GeometricShapeException
	 *             If shape cannot be created out of given tokens
	 */
	private static GeometricShape createRectangle(String[] tokens) {
		if (tokens.length != 5) {
			throw new GeometricShapeException("Invalid number of arguments for RECTANGLE!");
		}

		int x = Integer.parseInt(tokens[1]);
		int y = Integer.parseInt(tokens[2]);
		int width = Integer.parseInt(tokens[3]);
		int height = Integer.parseInt(tokens[4]);

		return new Rectangle(x, y, width, height);

	}

	/**
	 * Helper method for creating new {@link Ellipse} object.
	 * 
	 * @param tokens
	 *            Tokens of one input line describing {@code Ellipse} object
	 * @return Returns new {@code Ellipse} object
	 * @throws NumberFormatException
	 *             If one of tokens not parsable to integer
	 * @throws GeometricShapeException
	 *             If shape cannot be created out of given tokens
	 */
	private static GeometricShape createEllipse(String[] tokens) {
		if (tokens.length != 5) {
			throw new GeometricShapeException("Invalid number of arguments for ELLIPSE!");
		}

		int x = Integer.parseInt(tokens[1]);
		int y = Integer.parseInt(tokens[2]);
		int a = Integer.parseInt(tokens[3]);
		int b = Integer.parseInt(tokens[4]);

		return new Ellipse(x, y, a, b);
	}

}
