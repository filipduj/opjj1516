package hr.fer.zemris.java.graphics.shapes;

/**
 * Circle shape implementation.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (5.4.2016)
 *
 */
public class Circle extends EllipticalShape {

	/**
	 * Allocates new {@code Circle} object. Initializes it's position with given
	 * values.
	 * 
	 * @param x
	 *            x coordinate of center
	 * @param y
	 *            y coordinate of center
	 * @param r
	 *            radius of circle
	 * @throws IllegalArgumentException
	 *             If {@code r} is less than 1
	 */
	public Circle(int x, int y, int r) {
		super(x, y, r, r);
	}

	/**
	 * @return the radius
	 */
	public int getR() {
		return this.xRadius;
	}

	/**
	 * @param r
	 *            the radius of circle to set
	 */
	public void setR(int r) {
		this.xRadius = this.yRadius = r;
	}

}
