package hr.fer.zemris.java.graphics.shapes;

/**
 * Ellipse shape implementation.
 * 
 * @author Filip Dujmušič
 * @version 1.0 (5.4.2016)
 */
public class Ellipse extends EllipticalShape {

	/**
	 * Allocates new {@code Ellipse} object. Initializes it's position with
	 * given values.
	 * 
	 * @param x
	 *            x coordinate of center of ellipse
	 * @param y
	 *            y coordinate of center of ellipse
	 * @param a
	 *            Horizontal radius
	 * @param b
	 *            Vertical radius
	 * @throws IllegalArgumentException
	 *             If {@code a} or {@code b} parameters less than 1
	 */
	public Ellipse(int x, int y, int a, int b) {
		super(x, y, a, b);
	}

	/**
	 * @return the horizontal radius
	 */
	public int getHorizontalRadius() {
		return xRadius;
	}

	/**
	 * @param xRadius
	 *            the horizontal radius to set
	 */
	public void setHorizontalRadius(int xRadius) {
		this.xRadius = xRadius;
	}

	/**
	 * @return the vertical radius
	 */
	public int getVerticalRadius() {
		return yRadius;
	}

	/**
	 * @param yRadius
	 *            the vertical radius to set
	 */
	public void setVerticalRadius(int yRadius) {
		this.xRadius = yRadius;
	}

}
