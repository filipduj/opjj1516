package hr.fer.zemris.java.graphics.shapes;

import hr.fer.zemris.java.graphics.raster.BWRaster;

/**
 * Rectangular shape abstraction. Can represent any geometric shape defined with
 * it's top left point and dimensions.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (6.4.2016)
 *
 */
public abstract class RectangularShape extends GeometricShape {

	/**
	 * x coordinate of top left corner
	 */
	protected int x;

	/**
	 * y coordinate of top left corner
	 */
	protected int y;

	/**
	 * Width of shape
	 */
	protected int width;

	/**
	 * Height of shape
	 */
	protected int height;

	/**
	 * Initializes rectangular shape with given data.
	 * 
	 * @param x
	 *            x coordinate of center of shape
	 * @param y
	 *            y coordinate of center of shape
	 * @param width
	 *            width
	 * @param height
	 *            height
	 * @throws GeometricShapeException
	 *             If {@code width} or {@code height} are less than 1.
	 */
	public RectangularShape(int x, int y, int width, int height) {

		if (width <= 0) {
			throw new GeometricShapeException("Width can not be negative or zero!");
		} else if (height <= 0) {
			throw new GeometricShapeException("Height can not be negative or zero!");
		}

		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Efficient drawing method. Will only check for pixels in minimal rectangle
	 * enclosing this geometric shape.
	 * 
	 * @throws IllegalArgumentException
	 *             If {@code r} is {@code null} reference
	 * @throws IndexOutOfBoundsException
	 *             If tried accessing pixel out of raster bounds
	 */
	@Override
	public void draw(BWRaster r) {

		if (r == null) {
			throw new IllegalArgumentException();
		}

		int height = r.getHeight();
		int width = r.getWidth();

		int xMin = (x < 0) ? 0 : x;
		int yMin = (y < 0) ? 0 : y;
		int xMax = (x + this.width) >= width ? width : (x + this.width);
		int yMax = (y + this.height >= height) ? height : (y + this.height);

		for (int i = xMin; i < xMax; ++i) {
			for (int j = yMin; j < yMax; ++j) {
				r.turnOn(i, j);
			}
		}

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean containsPoint(int x, int y) {
		return x >= this.x && y >= this.y && x < (this.x + width) && y < (this.y + height);
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @param x
	 *            the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param y
	 *            the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}

}
