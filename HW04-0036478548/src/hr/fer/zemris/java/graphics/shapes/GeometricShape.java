package hr.fer.zemris.java.graphics.shapes;

import hr.fer.zemris.java.graphics.raster.BWRaster;

/**
 * Abstraction of geometric shape. Offers method for drawing itself to given
 * {@link BWRaster}.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (5.4.2016)
 */
public abstract class GeometricShape {

	/**
	 * Checks if point given by {@code x},{@code y} coordinates is contained
	 * within this {@code GeometricShape}.
	 * 
	 * @param x
	 *            x coordinate
	 * @param y
	 *            y coordinate
	 * @return returns {@code true} if point is within geometric shape,
	 *         {@code false} otherwise
	 */
	public abstract boolean containsPoint(int x, int y);

	/**
	 * Draws this geometric shape on given {@link BWRaster}.
	 * 
	 * @param r
	 *            {@code BWRaster} on which shape will be drawn
	 * @throws IllegalArgumentException
	 *             If {@code r} is {@code null} reference
	 * @throws IndexOutOfBoundsException
	 *             If tried to access pixel out of raster bounds
	 */
	public void draw(BWRaster r) {
		if (r == null) {
			throw new IllegalArgumentException();
		}

		for (int i = 0, width = r.getWidth(); i < width; ++i) {
			for (int j = 0, height = r.getHeight(); j < height; ++j) {
				if (containsPoint(i, j)) {
					r.turnOn(i, j);
				}
			}
		}
	}

}
