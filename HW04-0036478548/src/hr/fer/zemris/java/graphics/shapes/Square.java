package hr.fer.zemris.java.graphics.shapes;

/**
 * Square shape implementation.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (5.4.2016)
 *
 */
public class Square extends RectangularShape {

	/**
	 * Allocates new {@code Square} object. Initializes it's position with given
	 * values.
	 * 
	 * @param x
	 *            x coordinate of top left corner
	 * @param y
	 *            y coordinate of top left corner
	 * @param size
	 *            Square side length
	 */
	public Square(int x, int y, int size) {
		super(x, y, size, size);
	}

	/**
	 * @return square side length
	 */
	public int getSize() {
		return this.height;
	}

	/**
	 * @param size
	 *            square side length to set
	 */
	public void setSize(int size) {
		this.height = this.width = size;
	}

}
