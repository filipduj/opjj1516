package hr.fer.zemris.java.graphics.shapes;

/**
 * Derived exception class used for errors while parsing shapes from line input.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (6.4.2016)
 *
 */
public class GeometricShapeException extends RuntimeException {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = 6867816270265575370L;

	/**
	 * Creates new {@code GeometricShapeException} with no parameters
	 */
	public GeometricShapeException() {
		super();
	}

	/**
	 * Creates new {@code GeometricShapeException} with error message as
	 * parameter
	 * 
	 * @param message
	 *            Exception message
	 */
	public GeometricShapeException(String message) {
		super(message);
	}

}
