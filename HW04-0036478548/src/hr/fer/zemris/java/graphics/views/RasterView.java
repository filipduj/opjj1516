package hr.fer.zemris.java.graphics.views;

import hr.fer.zemris.java.graphics.raster.BWRaster;

/**
 * Abstraction of raster representation.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (6.4.2016)
 *
 */
public interface RasterView {

	/**
	 * Produces output for given {@link BWRaster}.
	 * 
	 * @param raster
	 *            {@code BWRaster} object
	 */
	Object produce(BWRaster raster);

}
