package hr.fer.zemris.java.graphics.views;

import hr.fer.zemris.java.graphics.raster.BWRaster;

/**
 * {@code SimpleRasterView} is implementation of {@link RasterView} which prints
 * raster data to standard output.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (6.4.2016)
 *
 */
public class SimpleRasterView implements RasterView {

	/**
	 * Character representing turned on pixel
	 */
	private char pixelOn;

	/**
	 * Character representing turned off pixel
	 */
	private char pixelOff;

	/**
	 * Default constructor sets turned on pixel to '*' and turned off character
	 * to '.'
	 */
	public SimpleRasterView() {
		this('*', '.');
	}

	/**
	 * Constructor sets turned on pixel representation to {@code pixelOn} and
	 * turned off pixel representation to {@code pixelOff}
	 * 
	 * @param pixelOn
	 *            Character for turned on pixel
	 * @param pixelOff
	 *            Character for turned off pixel
	 */
	public SimpleRasterView(char pixelOn, char pixelOff) {
		this.pixelOn = pixelOn;
		this.pixelOff = pixelOff;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Prints {@code raster} content to standard output.
	 * 
	 * @return returns {@code null}
	 * 
	 * @throws IllegalArgumentException
	 *             If {@code raster} is {@code null} reference
	 */
	@Override
	public Object produce(BWRaster raster) {

		if (raster == null) {
			throw new IllegalArgumentException();
		}

		for (int j = 0, height = raster.getHeight(); j < height; ++j) {
			for (int i = 0, width = raster.getWidth(); i < width; ++i) {
				System.out.print((raster.isTurnedOn(i, j) ? pixelOn : pixelOff) + " ");
			}
			System.out.println();
		}

		return null;
	}

}
