package hr.fer.zemris.java.graphics.views;

import hr.fer.zemris.java.graphics.raster.BWRaster;

/**
 * {@code StringRasterView} is implementation of {@link RasterView} which prints
 * raster data to String.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (6.4.2016)
 *
 */
public class StringRasterView implements RasterView {
	/**
	 * Character representing turned on pixel
	 */
	protected char pixelOn;

	/**
	 * Character representing turned off pixel
	 */
	protected char pixelOff;

	/**
	 * Default constructor sets turned on pixel to '*' and turned off character
	 * to '.'
	 */
	public StringRasterView() {
		this('*', '.');
	}

	/**
	 * Constructor sets turned on pixel representation to {@code pixelOn} and
	 * turned off pixel representation to {@code pixelOff}
	 * 
	 * @param pixelOn
	 *            Character for turned on pixel
	 * @param pixelOff
	 *            Character for turned off pixel
	 */
	public StringRasterView(char pixelOn, char pixelOff) {
		this.pixelOn = pixelOn;
		this.pixelOff = pixelOff;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Prints {@code raster} content to {@code String}.
	 * 
	 * @return Returns String representing {@code raster} content
	 * 
	 * @throws IllegalArgumentException
	 *             If {@code raster} is {@code null} reference
	 */
	@Override
	public Object produce(BWRaster raster) {
		if (raster == null) {
			throw new IllegalArgumentException();
		}
		StringBuilder sb = new StringBuilder();

		for (int j = 0, height = raster.getHeight(); j < height; ++j) {
			for (int i = 0, width = raster.getWidth(); i < width; ++i) {
				sb.append((raster.isTurnedOn(i, j) ? pixelOn : pixelOff)).append(" ");
			}
			sb.append("\n");
		}

		return sb.toString();
	}

}
