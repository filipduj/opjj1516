package hr.fer.zemris.java.cstr;

import java.util.Arrays;

/**
 * The {@code CString} class represents character strings. CStrings are
 * constant; their values cannot be changed after they are created. Multiple
 * instances of CStrings to share a single character array and to remember which
 * part of the array belongs to each instance. This means that method
 * {@code substring} takes O(1) time.
 * 
 * @author Filip Dujmušić
 * @version 1.0
 * 
 */
public class CString {

	/**
	 * Internal array containing characters of string
	 */
	private final char[] data;

	/**
	 * First character of string represented by this class.
	 */
	private final int offset;

	/**
	 * Length of string represented by this class.
	 */
	private final int length;

	/**
	 * Allocates a new {@link CString} that contains characters from subarray of
	 * the character array argument: {@code data}. {@code length} is number of
	 * characters to take from subarray starting from {@code position}.
	 * {@code data} is deep copied; subsequent modification of array <b>will not
	 * modify</b> newly created {@link CString}.
	 * 
	 * @param data
	 *            array of characters
	 * @param offset
	 *            Offset within {@code data}
	 * @param length
	 *            Length of subarray
	 * @throws NullPointerException
	 *             If {@code data} array is {@code null}
	 * @throws IllegalArgumentException
	 *             Will be thrown if any of following occures:
	 *             <ul>
	 *             <li>If the {@code length} is negative</li>
	 *             <li>If the {@code offset} is out of bounds of the
	 *             {@code data} array</li>
	 *             <li>If the {@code offset} and {@code length} arguments index
	 *             </li>
	 *             <li>characters outside the bounds of the {@code data} array
	 *             </li>
	 *             </ul>
	 * 
	 * 
	 */
	public CString(char[] data, int offset, int length) {

		// null character array
		if (data == null) {
			throw new IllegalArgumentException();
		}

		if (data.length == 0) { // empty character array
			if (offset != 0 || length != 0) {
				throw new IndexOutOfBoundsException();
			}
		} else { // non empty character array
			if (offset < 0 || length < 0 || offset >= data.length || (offset + length) > data.length) {
				throw new IndexOutOfBoundsException();
			}
		}

		/*
		 * Constructor is public so make deep copy of character elements into
		 * internal character array. This has to be done since CString is
		 * immutable and we cant trust outside world in the way that we could
		 * only take reference to given character array and hope that it will
		 * never be changed.
		 */
		this.data = getSubArray(data, offset, length);
		this.offset = 0;
		this.length = length;
	}

	/**
	 * Allocates a new {@link CString} that contains characters from subarray of
	 * the character array argument: {@code data}. {@code length} is number of
	 * characters to take from subarray starting from {@code position}.
	 * {@code data} is deep copied; subsequent modification of array <b>will not
	 * modify</b> newly created {@link CString}.
	 * 
	 * @param data
	 *            array of characters
	 * @param offset
	 *            Offset within {@code data}
	 * @param length
	 *            Length of subarray
	 */
	private CString(int offset, int length, char[] data) {
		this.data = data;
		this.offset = offset;
		this.length = length;
	}

	/**
	 * Allocates new {@link CString} that contains all the characters from
	 * {@code data} character array. {@code data} is copied by reference;
	 * subsequent modification of array <b>will modify</b> newly created
	 * {@link CString}.
	 * 
	 * @param data
	 *            shared array of characters representing string data
	 * @throws NullPointerException
	 *             If {@code data} is {@code null} reference
	 */
	public CString(char[] data) {
		this(data, 0, data.length);
	}

	/**
	 * Allocates new {@link CString} which is the same as {@code original}. The
	 * only difference is in {@code data} character array. If this array is
	 * larger than the substring which {@code original} is representing then
	 * subarray is deep copied and assigned to new {@link CString}.
	 * 
	 * @param original
	 *            {@link CString} to copy
	 * @throws NullPointerException
	 *             If {@code original} is {@code null} reference
	 */
	public CString(CString original) {

		if (original == null) {
			throw new IllegalArgumentException();
		}

		if (original.length < original.data.length) {
			data = original.toCharArray();
			offset = 0;
		} else {
			data = original.data;
			offset = original.offset;
		}

		length = original.length;

	}

	/**
	 * Allocates new {@link CString} with the same content as input
	 * {@link String} {@code s}.
	 * 
	 * @param s
	 *            Input {@link String}
	 * @throws NullPointerException
	 *             If {@code s} is null reference
	 */
	public CString(String s) {
		this(s.toCharArray());
	}

	/**
	 * {@code length} getter.
	 * 
	 * @return Returns {@code length}
	 */
	public int length() {
		return length;
	}

	/**
	 * Creates new {@link CString} object which has the same character data as
	 * given Java's {@link String} object.
	 * 
	 * @param s
	 *            {@link String} object
	 * @return Returns {@link CString} object which is copy of {@code s}
	 * @throws IllegalArgumentException
	 *             If {@code s} is null reference
	 */
	public static CString fromString(String s) {
		if (s == null) {
			throw new IllegalArgumentException();
		}
		return new CString(s);
	}

	/**
	 * Returns character at given {@code index}.
	 * 
	 * @param index
	 *            Character position
	 * @return Character at {@code index}
	 * @throws IndexOutOfBoundsException
	 *             If {@code index} is out of bounds
	 */
	public char charAt(int index) {
		if (index < 0 || index >= length) {
			throw new IndexOutOfBoundsException();
		}
		return data[index + offset];
	}

	/**
	 * Allocates a new array of length equal to the length of this
	 * {@link CString} object. (not its internal array which may be larger!)
	 * Copies string content into it and returns it.
	 * 
	 * @return Character array representing string content
	 */
	public char[] toCharArray() {
		return getSubArray(data, offset, length);
	}

	/**
	 * Returnes new {@link String} object. It's content is copied from this
	 * {@link CString} object.
	 * 
	 * @return new {@link String} object
	 */
	public String toString() {
		return new String(toCharArray());
	}

	/**
	 * Returns index of first occurence of character {@code c} in this
	 * {@link CString} object.
	 * 
	 * @param c
	 * @return index of first occurence of {@code c} or -1 if nothing was found
	 */
	public int indexOf(char c) {
		for (int i = 0; i < length; ++i) {
			if (c == charAt(i)) {
				return i;
			}
		}

		return -1;

	}

	/**
	 * Checks if this {@link CString} starts with {@code s}.
	 * 
	 * @param s
	 *            {@link CString}
	 * @return {@code true} if this {@link CString} begins with {@code s},
	 *         {@code false} otherwise
	 * @throws IllegalArgumentException
	 *             If {@code s} is {@code null} reference
	 */
	public boolean startsWith(CString s) {
		if (s == null) {
			throw new IllegalArgumentException();
		}
		if (s.length > this.length) {
			return false;
		}
		return compareIntervals(s, 0) == s.length;
	}

	/**
	 * Checks if this {@link CString} ends with {@code s}.
	 * 
	 * @param s
	 *            {@link CString}
	 * @return {@code true} if this {@link CString} ends with {@code s},
	 *         {@code false} otherwise
	 * @throws IllegalArgumentException
	 *             If {@code s} is {@code null} reference
	 */
	public boolean endsWith(CString s) {
		if (s == null) {
			throw new IllegalArgumentException();
		}

		if (s.length() > this.length()) {
			return false;
		}
		return compareIntervals(s, this.length - s.length) == s.length;
	}

	/**
	 * Checks if this string contains given string {@code s} at any position.
	 * 
	 * @param s
	 *            {@link CString}
	 * @return Returns {@code true} if given string is completely contained
	 *         somewhere in this string, {@code false} otherwise
	 */
	public boolean contains(CString s) {

		if (s == null) {
			throw new IllegalArgumentException();
		}

		if (s.length() > this.length()) {
			return false;
		}

		int position = 0;

		while (position + s.length <= this.length) {

			int charactersEqual = compareIntervals(s, position);

			if (charactersEqual == s.length) {
				return true;
			}

			if (charactersEqual > 0)
				position += charactersEqual;
			else
				position++;
		}

		return false;
	}

	/**
	 * Allocates and returns new {@link CString} object which represents part of
	 * original string which starts with {@code startIndex} and ends with first
	 * character before {@code endIndex}.
	 * 
	 * @param startIndex
	 *            Represents first character's position within this string
	 * @param endIndex
	 *            Represents last character's position within this string
	 *            (exclusive)
	 * @return {@link CString} representing part of this string
	 * @throws IndexOutOfBoundsException
	 *             If {@code startIndex} is negative or greater then
	 *             {@code endIndex}
	 */
	public CString substring(int startIndex, int endIndex) {
		if (startIndex < 0 || endIndex < startIndex) {
			throw new IndexOutOfBoundsException();
		}
		return new CString(offset + startIndex, endIndex - startIndex, data);
	}

	/**
	 * Allocates and returns new {@link CString} object representing first
	 * {@code n} characters of this string.
	 * 
	 * @param n
	 *            Number of characters
	 * @return {@link CString} with first {@code n} characters of this string
	 * @throws IllegalArgumentException
	 *             If {@code n} is negative or greater then this string's length
	 */
	public CString left(int n) {
		if (n < 0 || n > length) {
			throw new IndexOutOfBoundsException();
		} else if (n == 0) {
			return new CString("");
		} else {
			return substring(0, n);
		}
	}

	/**
	 * Allocates and returns new {@link CString} object representing last
	 * {@code n} characters of this string.
	 * 
	 * @param n
	 *            Number of characters
	 * @return {@link CString} with last {@code n} characters of this string
	 * @throws IllegalArgumentException
	 *             If {@code n} is negative or greater then this string's length
	 */
	public CString right(int n) {
		if (n < 0 || n > length) {
			throw new IndexOutOfBoundsException();
		} else if (n == 0) {
			return new CString("");
		} else {
			return substring(length - n, length);
		}
	}

	/**
	 * Creates a new {@link CString} which is concatenation of current and given
	 * {@link CString}.
	 * 
	 * @param s
	 *            {@link CString}
	 * @return {@link CString} which represents {@code s} concatenated to this
	 *         string
	 */
	public CString add(CString s) {

		if (s == null) {
			throw new IllegalArgumentException();
		}

		char[] data = new char[this.length + s.length];

		int index = 0;

		for (int i = 0, n = this.length(); i < n; ++i) {
			data[index++] = charAt(i);
		}

		for (int i = 0; i < s.length; ++i) {
			data[index++] = s.charAt(i);
		}

		return new CString(0, data.length, data);
	}

	/**
	 * Creates a new {@link CString} in which each occurrence of {@code oldChar}
	 * is replaced with {@code newChar}.
	 * 
	 * @param oldChar
	 *            Character to be replaced
	 * @param newChar
	 *            Replacement for all occurences of {@code oldChar}
	 * @return Returns new {@link CString} with replaced characters
	 */
	public CString replaceAll(char oldChar, char newChar) {
		char data[] = new char[length];
		for (int i = 0; i < length; ++i) {
			char c = charAt(i);
			if (c == oldChar) {
				data[i] = newChar;
			} else {
				data[i] = c;
			}
		}
		return new CString(0, data.length, data);
	}

	/**
	 * Creates a new {@link CString} in which each occurrence of old substring
	 * {@code oldStr} is replaces with the new substring {@code newStr}.
	 * 
	 * @param oldStr
	 *            {@link CString} Old substring
	 * @param newStr
	 *            {@link CString} New substring
	 * @return Returns modified {@link CString}
	 */
	public CString replaceAll(CString oldStr, CString newStr) {

		if (oldStr == null || newStr == null) {
			throw new IllegalArgumentException();
		}

		int position = 0;
		int firstNotMatched = 0;

		CString modifiedString = new CString("");

		while (position + oldStr.length <= this.length) {
			int charactersMatched = compareIntervals(oldStr, position);

			if (charactersMatched == oldStr.length) {
				if (position - firstNotMatched > 0) {
					modifiedString = modifiedString.add(this.substring(firstNotMatched, position));
				}
				modifiedString = modifiedString.add(newStr);
				position += oldStr.length;
				firstNotMatched = position;

			} else if (charactersMatched > 0) {
				position += charactersMatched;

			} else {
				position++;
			}
		}

		if (this.length - firstNotMatched > 0) {
			modifiedString = modifiedString.add(right(length - firstNotMatched));
		}

		return modifiedString;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(data);
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof CString)) {
			return false;
		}

		CString other = (CString) obj;

		for (int i = 0; i < length; ++i) {
			if (charAt(i) != other.charAt(i)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Allocates new array of length {@code length} and copies data from
	 * {@code data} starting from index {@code offset}.
	 * 
	 * 
	 * @param inputArray
	 *            Character array
	 * @param offset
	 *            Index of first character that should be copied
	 * @param length
	 *            Number of characters to copy from {@code inputArray}
	 * @return Returns newly allocated character array
	 */
	private char[] getSubArray(char[] inputArray, int offset, int length) {
		char[] outputArray = new char[length];
		for (int i = 0; i < length; ++i) {
			outputArray[i] = inputArray[i + offset];
		}
		return outputArray;
	}

	/**
	 * Compares this string and passed string {@code s} on interval starting
	 * from {@code start} character by character until first different character
	 * or {@code maxLength} characters compared. Returns number of characters
	 * matched.
	 * 
	 * @param s
	 *            {@link CString}
	 * @param start
	 *            Starting position of comparison
	 * @param maxLength
	 *            Maximum number of consecutive characters to compare
	 * @return Returns number of consecutive equal characters in interval
	 */
	private int compareIntervals(CString s, int start) {
		int charactersMatched = 0;
		for (int i = start, j = 0; j < s.length; ++i, ++j) {
			if (this.charAt(i) != s.charAt(j)) {
				break;
			}
			charactersMatched++;
		}
		return charactersMatched;
	}
}
