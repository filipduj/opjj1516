package hr.fer.zemris.java.tecaj.hw07.crypto;

import static org.junit.Assert.*;

import org.junit.Test;

@SuppressWarnings("javadoc")
public class HexToByteTests {
	
	@Test(expected = IllegalArgumentException.class)
	public void nullInputTest(){
		Crypto.hexToByte(null);
	}
	
	@Test
	public void upperCseTest(){
		assertArrayEquals(Crypto.hexToByte("ABCDEF"), new byte[]{(byte)171, (byte)205, (byte)239});
	}
	
	@Test
	public void lowerCaseTest(){
		assertArrayEquals(Crypto.hexToByte("abcdef"), new byte[]{(byte)171, (byte)205, (byte)239});
	}
	
	@Test
	public void oddNumberOfBytesTest(){
		assertArrayEquals(Crypto.hexToByte("abcde"), new byte[]{(byte)171, (byte)205, (byte)14});
	}
	
	
	
	
	
}
