package hr.fer.zemris.java.tecaj.hw07.shell.commands.implementations;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw07.environments.IEnvironment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.Commands;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.util.ConstraintCheckUtil;
import hr.fer.zemris.java.tecaj.hw07.shell.util.PathParserUtil;

/**
 * Command copies file from one location to another.
 * <p>
 * Syntax: copy [source_path] [destination_path]
 * <p>
 * The copy command expects two arguments:
 * <ol>
 * <li>source file name</li>
 * <li>destination file name</li>
 * </ol>
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class CopyCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "copy";

	/**
	 * Command description
	 */
	private static final String COMMAND_DESCRIPTION = Commands.getCommandDescription(COMMAND_NAME);

	@Override
	public ShellStatus execute(IEnvironment env, String arguments) throws IOException {

		List<Path> args = PathParserUtil.parsePaths(arguments);
		ConstraintCheckUtil.numberOfArguments(2, args.size());

		Path originalFile = args.get(0);
		Path destinationFile = args.get(1);

		ConstraintCheckUtil.filesExist(originalFile);
		ConstraintCheckUtil.filesRegular(originalFile);

		if (Files.isDirectory(destinationFile)) {
			destinationFile = Paths.get(destinationFile.toString() + "/" + originalFile.getFileName().toString());
		}

		if (Files.exists(destinationFile)) {
			env.writeln(destinationFile.getFileName() + " already exists.");

			String confirmation;
			while (true) {
				env.writeln("Overwrite? (YES/NO): ");
				confirmation = env.readLine();

				if (confirmation.equalsIgnoreCase("YES")) {
					break;
				} else if (confirmation.equalsIgnoreCase("NO")) {
					return ShellStatus.CONTINUE;
				}

				env.writeln("Please answer with yes or no.");
			}
		} else {
			Files.createDirectories(destinationFile.getParent());
		}

		Files.deleteIfExists(destinationFile);
		destinationFile = Files.createFile(destinationFile);

		try (BufferedReader input = Files.newBufferedReader(originalFile);
				BufferedWriter output = Files.newBufferedWriter(destinationFile)) {

			char[] buffer = new char[4096];
			int len = 0;
			while ((len = input.read(buffer)) > 0) {
				output.write(buffer, 0, len);
			}
			output.flush();
			env.writeln("Copying done.");

		}

		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandDescription() {
		return COMMAND_DESCRIPTION;
	}

}
