package hr.fer.zemris.java.tecaj.hw07.environments;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import hr.fer.zemris.java.tecaj.hw07.shell.commands.Commands;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;

/**
 * This class represents simple {@link IEnvironment} implementation.
 * <p>
 * It offers usual methods for communicating with end user.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class MyShellEnv implements IEnvironment {

	/**
	 * Sets input to standard input stream
	 */
	private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));

	/**
	 * Sets output to standard output stream
	 */
	private BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(System.out, StandardCharsets.UTF_8));

	/**
	 * Multiline symbol
	 */
	private Character multilineSymbol = '|';

	/**
	 * Morelines symbol
	 */
	private Character morelinesSymbol = '\\';

	/**
	 * Prompt symbol
	 */
	private Character promptSymbol = '>';

	@Override
	public String readLine() throws IOException {
		return reader.readLine();
	}

	@Override
	public void write(String text) throws IOException {
		writer.write(text);
		writer.flush();
	}

	@Override
	public void write(char[] chars, int offset, int length) throws IOException {
		writer.write(chars, offset, length);
		writer.flush();
	}

	@Override
	public void writeln(String text) throws IOException {
		writer.write(text + "\n");
		writer.flush();
	}

	@Override
	public Map<String, ShellCommand> commands() {
		return Commands.getAllCommands();
	}

	@Override
	public Character getMultilineSymbol() {
		return multilineSymbol;
	}

	@Override
	public void setMultilineSymbol(Character symbol) {
		multilineSymbol = symbol;
	}

	@Override
	public Character getPromptSymbol() {
		return promptSymbol;
	}

	@Override
	public void setPromptSymbol(Character symbol) {
		promptSymbol = symbol;
	}

	@Override
	public Character getMorelinesSymbol() {
		return morelinesSymbol;
	}

	@Override
	public void setMorelinesSymbol(Character symbol) {
		morelinesSymbol = symbol;
	}

	@Override
	public void close() {
		try {
			if (reader != null) {
				reader.close();
			}
			if (writer != null) {
				writer.flush();
				writer.close();
			}
		} catch (Exception ignorable) {
		}
	}

}
