package hr.fer.zemris.java.tecaj.hw07.shell.exceptions;

import hr.fer.zemris.java.tecaj.hw07.shell.MyShell;

/**
 * Exception used for indicating something wrong happened while interacting with
 * {@link MyShell}.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class MyShellException extends RuntimeException {

	/**
	 * Serial ID
	 */
	private static final long serialVersionUID = -4863636304353860048L;

	/**
	 * Default constructor
	 */
	public MyShellException() {
		super();
	}

	/**
	 * Constructor takes message as parameter and displays it to user.
	 * 
	 * @param message
	 *            exception message
	 */
	public MyShellException(String message) {
		super(message);
	}
}
