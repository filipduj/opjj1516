package hr.fer.zemris.java.tecaj.hw07.shell.commands.implementations;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw07.environments.IEnvironment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.Commands;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.util.ConstraintCheckUtil;
import hr.fer.zemris.java.tecaj.hw07.shell.util.PathParserUtil;

/**
 * Cat command displays file content.
 * <p>
 * Syntax: cat [file_path] [charset | optional]
 * <p>
 * This command opens given file and writes its content to console.
 * <p>
 * Command cat takes one or two arguments:
 * <ol>
 * <li>the first argument is path to some file and is mandatory</li>
 * <li>the second argument is charset name that should be used to interpret
 * chars from bytes</li>
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class CatCommand implements ShellCommand {

	/**
	 * Buffer for I/O operations
	 */
	private static final int BUFFER_SIZE = 4096;

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "cat";

	/**
	 * Command description
	 */
	private static final String COMMAND_DESCRIPTION = Commands.getCommandDescription(COMMAND_NAME);

	@Override
	public ShellStatus execute(IEnvironment env, String arguments) throws IOException {

		List<Path> args = PathParserUtil.parsePaths(arguments);
		ConstraintCheckUtil.numberOfArguments(1, 2, args.size());
		Path path = args.get(0);
		ConstraintCheckUtil.filesExist(path);
		Charset cSet = Charset.defaultCharset();

		if (args.size() == 2) {
			String cs = args.get(1).toString();
			if (Charset.isSupported(cs)) {
				cSet = Charset.forName(cs);
			} else {
				env.writeln("Charset " + cs + " invalid or not supported.\n" + "Using default " + cSet + " charset.\n");
			}
		}

		try (BufferedReader reader = new BufferedReader(
				new InputStreamReader(new BufferedInputStream(new FileInputStream(path.toFile())), cSet))) {

			int len;
			char[] buffer = new char[BUFFER_SIZE];

			while ((len = reader.read(buffer)) > 0) {
				env.write(new String(buffer, 0, len));
			}

		}

		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandDescription() {
		return COMMAND_DESCRIPTION;
	}

}
