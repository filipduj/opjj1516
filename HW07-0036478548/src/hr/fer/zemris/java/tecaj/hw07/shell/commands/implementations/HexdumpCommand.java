package hr.fer.zemris.java.tecaj.hw07.shell.commands.implementations;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw07.environments.IEnvironment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.Commands;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.util.ConstraintCheckUtil;
import hr.fer.zemris.java.tecaj.hw07.shell.util.PathParserUtil;

/**
 * Command displays hexadecimal view of given file.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class HexdumpCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "hexdump";

	/**
	 * Command description
	 */
	private static final String COMMAND_DESCRIPTION = Commands.getCommandDescription(COMMAND_NAME);

	/**
	 * Number of bytes per one line
	 */
	private static final int BYTES_PER_LINE = 16;

	/**
	 * Number of bytes in block
	 */
	private static final int BYTES_PER_BLOCK = 8;

	@Override
	public ShellStatus execute(IEnvironment env, String arguments) throws IOException {

		List<Path> args = PathParserUtil.parsePaths(arguments);
		ConstraintCheckUtil.numberOfArguments(1, args.size());

		Path path = args.get(0);
		ConstraintCheckUtil.filesExist(path);

		try (InputStream reader = new BufferedInputStream(new FileInputStream(path.toFile()))) {

			byte[] buffer = new byte[BYTES_PER_LINE];
			int bufferLen;
			int lineNumber = 0;

			StringBuilder representation = new StringBuilder();
			while ((bufferLen = reader.read(buffer, 0, BYTES_PER_LINE)) > 0) {
				representation.append(String.format("%08x:", lineNumber))
							  .append(getHexLine(buffer, bufferLen))
							  .append("\n");
				lineNumber += BYTES_PER_LINE;
			}

			env.writeln(representation.toString());
		}

		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandDescription() {
		return COMMAND_DESCRIPTION;
	}

	/**
	 * Constructs one line of hexadecimal data view.
	 * 
	 * @param buffer Buffer containing data
	 * @param length Number of bytes in buffer
	 * @return returns String representing one line of hex view
	 */
	private String getHexLine(byte[] buffer, int length) {

		StringBuilder result = new StringBuilder();
		StringBuilder representation = new StringBuilder();

		for (int i = 1; i <= BYTES_PER_LINE; ++i) {
			if (i <= length) {
				byte c = buffer[i - 1];
				representation.append((c < 32 || c > 127) ? '.' : (char) c);
				result.append(String.format(" %02x", c));
			} else {
				result.append("   ");
			}
			if (i % BYTES_PER_BLOCK == 0) result.append(" | ");
		}

		return result.append(representation.toString()).toString();
	}

}
