package hr.fer.zemris.java.tecaj.hw07.shell.util;

import java.nio.file.Files;
import java.nio.file.Path;

import hr.fer.zemris.java.tecaj.hw07.shell.exceptions.MyShellException;

/**
 * Utility class provides basic methods for checking some constraints on files.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class ConstraintCheckUtil {

	/**
	 * Method chekcs if given number is between lower and upper bound
	 * (inclusive).
	 * 
	 * @param expectedLower
	 *            lower bound
	 * @param expectedUpper
	 *            upper bound
	 * @param given
	 *            number of parameters
	 * @throws MyShellException
	 *             If {@code given} not between bounds
	 */
	public static void numberOfArguments(int expectedLower, int expectedUpper, int given) {
		if (given < expectedLower || given > expectedUpper) {
			if (expectedLower == expectedUpper) {
				error("Expected " + expectedLower + " arguments.");
			} else {
				error("Expected " + expectedLower + " to " + expectedUpper + " arguments.");
			}
		}
	}

	/**
	 * Method checks if given number is equal to some expected value.
	 * 
	 * @param expected
	 *            expected value
	 * @param given
	 *            given value
	 * @throws MyShellException
	 *             If {@code expected} not equal to {@code given}
	 */
	public static void numberOfArguments(int expected, int given) {
		numberOfArguments(expected, expected, given);
	}

	/**
	 * Method checks if given file/s exist on disk.
	 * 
	 * @param p
	 *            Variable file array
	 * @throws MyShellException
	 *             If any of given files does not exist
	 */
	public static void filesExist(Path... p) {
		for (Path path : p) {
			if (!Files.exists(path)) {
				error(path.getFileName() + " does not exist.");
			}
		}
	}

	/**
	 * Method checks if given file/s are regular. A regular file is a sequence
	 * of bytes stored permanently in a file system.
	 * 
	 * @param p
	 *            Variable file array
	 * @throws MyShellException
	 *             If any of given files is not regular
	 */
	public static void filesRegular(Path... p) {
		for (Path path : p) {
			if (!Files.isRegularFile(path)) {
				error(path.getFileName() + " is not regular file.");
			}
		}
	}

	/**
	 * Method checks if given file/s are directories.
	 * 
	 * @param p
	 *            Variable file array
	 * @throws MyShellException
	 *             If any of given files is not directory
	 */
	public static void filesDirectory(Path... p) {
		for (Path path : p) {
			if (!Files.isDirectory(path)) {
				error(path.getFileName() + " is not directory.");
			}
		}
	}

	/**
	 * Displays error message wrapped in {@link MyShellException}.
	 * 
	 * @param message
	 *            error message to be displayed
	 * @throws MyShellException
	 */
	private static void error(String message) {
		throw new MyShellException("Error: " + message);
	}

}
