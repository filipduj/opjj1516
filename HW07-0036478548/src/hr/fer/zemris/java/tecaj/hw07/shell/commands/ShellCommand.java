package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.IOException;

import hr.fer.zemris.java.tecaj.hw07.environments.IEnvironment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;

/**
 * Abstraction of MyShell supported command.
 * <p>
 * Each command has it's name, description and provides execute method.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public interface ShellCommand {
	
	/**
	 * @return returns command name as String
	 */
	String getCommandName();
	
	/**
	 * @return returns command description as String
	 */
	String getCommandDescription();
	
	/**
	 * Executes command with given {@code arguments} as parameters to command.
	 * 
	 * @param env Shell environment
	 * @param arguments Command arguments as one String
	 * @return returns CONTINUE or TERMINATE from {@link ShellStatus}
	 * @throws IOException If I/O error occures in user environment communication
	 */
	ShellStatus execute(IEnvironment env, String arguments) throws IOException;
	
}
