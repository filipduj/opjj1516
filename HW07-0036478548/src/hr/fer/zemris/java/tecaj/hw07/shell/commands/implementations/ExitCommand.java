package hr.fer.zemris.java.tecaj.hw07.shell.commands.implementations;

import hr.fer.zemris.java.tecaj.hw07.environments.IEnvironment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.Commands;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.exceptions.MyShellException;

/**
 * Command terminates shell.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class ExitCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "exit";

	/**
	 * Command description
	 */
	private static final String COMMAND_DESCRIPTION = Commands.getCommandDescription(COMMAND_NAME);

	@Override
	public ShellStatus execute(IEnvironment env, String arguments) {

		if (!arguments.trim().isEmpty()) {
			throw new MyShellException("Did you mean: exit ?");
		}
		return ShellStatus.TERMINATE;
	}

	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandDescription() {
		return COMMAND_DESCRIPTION;
	}

}
