package hr.fer.zemris.java.tecaj.hw07.shell.commands;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import hr.fer.zemris.java.tecaj.hw07.shell.exceptions.MyShellException;

/**
 * This class initializes all commands supported by MyShell.
 * <p>
 * It provides methods for retrieving supported commands.
 * 
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class Commands {

	/**
	 * Map contains all the supported commands
	 */
	private static Map<String, ShellCommand> commands = new HashMap<>();

	static {
		try {
			String path = Commands.class.getClassLoader().getResource("resources/commands.properties").getPath();
			Properties properties = new Properties();
			properties.load(Files.newInputStream(Paths.get(path)));
			for (Map.Entry<Object, Object> entry : properties.entrySet()) {
				String name = entry.getKey().toString();
				String fqdn = entry.getValue().toString();
				ShellCommand command = (ShellCommand) Class.forName(fqdn).newInstance();
				commands.put(name, command);
			}
		} catch (Exception ignore) {
		}
	}

	/**
	 * Returns {@link ShellCommand} by given {@code commandName}.
	 * 
	 * @param commandName
	 *            command name
	 * @return {@link ShellCommand} or {@code null} if command does not exist
	 */
	public static ShellCommand getCommand(String commandName) {
		return commands.get(commandName);
	}

	/**
	 * @return returns map of all commands supported by this shell
	 */
	public static Map<String, ShellCommand> getAllCommands() {
		return commands;
	}

	/**
	 * Returns textual description for given command.
	 * 
	 * @param commandName
	 *            command name
	 * @return String representing textual description of given command
	 * @throws MyShellException
	 *             If command description file not readable
	 */
	public static String getCommandDescription(String commandName) {

		String line;
		StringBuilder sb = new StringBuilder();

		String descriptionResource = "resources/cmdDescriptions/" + commandName + ".desc";

		Path descriptionFile = Paths.get(
				Commands.class.getClassLoader()
							  .getResource(descriptionResource)
							  .getPath()
		);

		try (BufferedReader reader = Files.newBufferedReader(descriptionFile)) {
			while ((line = reader.readLine()) != null) {
				sb.append(line).append("\n");
			}
		} catch (IOException ignorable) {
			throw new MyShellException("Error: could not load command description.");
		}

		return sb.toString();
	}

}
