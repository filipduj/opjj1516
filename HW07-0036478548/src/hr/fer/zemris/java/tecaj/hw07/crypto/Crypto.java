package hr.fer.zemris.java.tecaj.hw07.crypto;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * This class demonstrates basic encryption, decryption and hash algorithms.
 * <p>
 * Program expects two or three command line arguments.
 * <p>
 * If first command line argument is `checksha` then one command line argument
 * must follow: path to file whose hash will be calculated usgin SHA-256
 * algorithm. Program takes user calculated hash value, and calculates it's own
 * value on given file and then prints out result of matching.
 * <p>
 * If first command line argument is `encrypt` then two arguments must follow:
 * <ol>
 * <li>Path to file which will be encrypted using AES encryption</li>
 * <li>Path where resulting encrypted file will be stored</li>
 * </ol>
 * <p>
 * If first command line argument is `decrypt` then two arguments must follow:
 * <ol>
 * <li>Path to encrypted file which will be decrypted</li>
 * <li>Path where resulting decrypted file will be stored</li>
 * </ol>
 * <p>
 * During both encryptiong and decryption user must provide key and
 * initialization vector.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class Crypto {

	/**
	 * Program entry point
	 */
	public static void main(String[] args) throws IOException, GeneralSecurityException {

		Path path = Paths.get(args[1]);

		if (!Files.isRegularFile(path)) {
			exit("Invalid input file");
		}

		if (args.length == 2) {
			if (args[0].trim().equals("checksha")) {
				try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
						InputStream in = new BufferedInputStream(Files.newInputStream(path, StandardOpenOption.READ))) {

					System.out.print("Please provide expected sha-256 digest for " + args[1] + ":\n> ");
					byte[] calculated = checksha(in);
					byte[] expected = hexToByte(br.readLine().trim());
					System.out.print("Digesting completed.");

					if (Arrays.equals(calculated, expected)) {
						System.out.print("Digest of " + args[1] + " matches expected digest.\n");
					} else {
						System.out.print("Digest of " + args[1] + " does not match the expected digest.\n");
						System.out.println("Digest was " + byteToHex(calculated));
					}
				}
			} else {
				exit("Unknown command " + args[0]);
			}

		} else if (args.length == 3) {
			try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					InputStream in = new BufferedInputStream(Files.newInputStream(path, StandardOpenOption.READ));
					OutputStream out = new BufferedOutputStream(new FileOutputStream(args[2]))) {

				System.out.print("Please provide password as hex-encoded text (16 bytes, i.e. 32 hex-digits): \n> ");
				String key = br.readLine().trim();

				System.out.print("Please provide initialization vector as hex-encoded text (32 hex-digits): \n> ");
				String vector = br.readLine().trim();

				if (args[0].equals("encrypt")) {
					crypt(in, out, key, vector, true);
				} else if (args[0].equals("decrypt")) {
					crypt(in, out, key, vector, false);
				} else {
					exit("Unknown command " + args[0]);
				}
			}
		} else {
			exit("Expected 2 or 3 command line arguments!");
		}

	}

	/**
	 * Encrypts or decrypts given input stream and stores resulting encrypted
	 * data to given output stream.
	 * 
	 * @param in
	 *            Input stream
	 * @param out
	 *            Output stream
	 * @param key
	 *            Encryption key
	 * @param vector
	 *            Initialization vector
	 * @param encrypt
	 *            Flag: {@code true} for encryption, {@code false} for
	 *            decryption
	 * @throws GeneralSecurityException
	 *             If algorithm provided with bad parameters
	 * @throws IOException
	 *             If I/O error occurs
	 */
	private static void crypt(InputStream in, OutputStream out, String key, String vector, boolean encrypt)
			throws GeneralSecurityException, IOException {
		String keyText = key;
		String ivText = vector;
		SecretKeySpec keySpec = new SecretKeySpec(hexToByte(keyText), "AES");
		AlgorithmParameterSpec paramSpec = new IvParameterSpec(hexToByte(ivText));
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(encrypt ? Cipher.ENCRYPT_MODE : Cipher.DECRYPT_MODE, keySpec, paramSpec);

		byte[] buffer = new byte[4096];
		int len = 0;

		while ((len = in.read(buffer)) > 0) {
			byte[] crypted = cipher.update(buffer, 0, len);
			if (crypted != null) {
				out.write(crypted);
			}
		}
		byte[] crypted = cipher.doFinal();
		out.write(crypted);
		out.flush();

	}

	/**
	 * Method calculates hash of given input stream using SHA-256 algorithm.
	 * 
	 * @param bis
	 *            Input stream
	 * @return Returns hash value of given input in hexadecimal view
	 */
	private static byte[] checksha(InputStream bis) {

		byte[] ret = null;

		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");

			byte[] buffer = new byte[4096];
			int len = 0;

			while ((len = bis.read(buffer)) > 0) {
				md.update(buffer, 0, len);
			}

			ret = md.digest();

		} catch (IOException | NoSuchAlgorithmException e) {
			exit(e.getMessage());
		}

		return ret;
	}

	/**
	 * Converts String of hexadecimal digits to byte array
	 * 
	 * @param keyText
	 *            input text data
	 * @return returns byte array
	 * @throws IllegalArgumentException
	 *             If {@code keyText} is null reference
	 */
	public static byte[] hexToByte(String keyText) {

		if (keyText == null) {
			throw new IllegalArgumentException();
		}

		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		int pos = 0;
		int len = keyText.length();

		while (pos < len) {
			String part = keyText.substring(pos, Math.min(pos + 2, len));
			try {
				baos.write(Integer.parseInt(part, 16));
			} catch (NumberFormatException e) {
				exit("Wrong hexadecimal format.");
			}
			pos += part.length();
		}

		return baos.toByteArray();
	}

	/**
	 * Converts byte array to hexadecimal digits
	 * 
	 * @param array
	 *            byte array
	 * @return String of hexadecimal digits
	 */
	private static String byteToHex(byte[] array) {
		StringBuilder sb = new StringBuilder();
		for (byte b : array) {
			sb.append(String.format("%02x", b));
		}
		return sb.toString();
	}

	/**
	 * Displays given message and exits program.
	 * 
	 * @param message
	 *            Message
	 */
	private static void exit(String message) {
		System.out.println(message);
		System.exit(-1);
	}

}
