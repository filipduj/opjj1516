package hr.fer.zemris.java.tecaj.hw07.shell.commands.implementations;

import java.io.IOException;

import hr.fer.zemris.java.tecaj.hw07.environments.IEnvironment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.Commands;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.exceptions.MyShellException;

/**
 * Command displays help.
 * <p>
 * If called without arguments it displays list of supported commands.
 * <p>
 * If called with command name, it displays command description.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class HelpCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "help";

	/**
	 * Command description
	 */
	private static final String COMMAND_DESCRIPTION = Commands.getCommandDescription(COMMAND_NAME);

	@Override
	public ShellStatus execute(IEnvironment env, String arguments) throws IOException {

		if (arguments.isEmpty()) {
			env.writeln("Supported commands: ");
			for (ShellCommand cmd : env.commands().values()) {
				env.writeln(cmd.getCommandName());
			}
		} else {
			ShellCommand cmd = env.commands().get(arguments);
			if (cmd == null) {
				throw new MyShellException("Error: Unknonw command");
			}
			env.writeln(cmd.getCommandDescription());
		}

		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandDescription() {
		return COMMAND_DESCRIPTION;
	}

}
