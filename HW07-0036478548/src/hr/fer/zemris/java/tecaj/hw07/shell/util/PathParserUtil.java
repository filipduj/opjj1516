package hr.fer.zemris.java.tecaj.hw07.shell.util;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.zemris.java.tecaj.hw07.shell.exceptions.MyShellException;

/**
 * Utility class provides method for path extraction out of given string data.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class PathParserUtil {

	/**
	 * Regex used for extracting both quoteless and quoted paths from string
	 */
	private static final Pattern PATH = Pattern.compile("((\"(.*)\")|([^\\s]+))");

	/**
	 * Takes input string and constructs list of paths contained in that given
	 * string.
	 * 
	 * @param s
	 *            input String
	 * @return returns List of paths extracted from {@code s}
	 */
	public static List<Path> parsePaths(String s) {
		List<Path> ret = new LinkedList<>();
		Matcher matcher = PATH.matcher(s.trim());

		while (matcher.find()) {
			String path = matcher.group();
			if (path.charAt(0) == '\"') {
				if (path.charAt(path.length() - 1) != '\"') {
					throw new MyShellException("Error: quotes not closed");
				} else {
					path = path.substring(1, path.length() - 1);
				}
			}
			ret.add(Paths.get(path.trim()));
		}

		return ret;
	}

}
