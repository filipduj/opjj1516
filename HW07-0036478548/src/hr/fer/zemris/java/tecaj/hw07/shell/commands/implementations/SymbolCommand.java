package hr.fer.zemris.java.tecaj.hw07.shell.commands.implementations;

import java.io.IOException;

import hr.fer.zemris.java.tecaj.hw07.environments.IEnvironment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.Commands;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.exceptions.MyShellException;
import hr.fer.zemris.java.tecaj.hw07.shell.util.ConstraintCheckUtil;

/**
 * Class provides methods for executing symbol command.
 * <p>
 * Symbol command allows displaying or changing symbol used for one of
 * following: multiline, morelines or prompt.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class SymbolCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "symbol";

	/**
	 * Command description
	 */
	private static final String COMMAND_DESCRIPTION = Commands.getCommandDescription(COMMAND_NAME);

	/**
	 * Prompt keyword
	 */
	private static final String PROMPT = "PROMPT";

	/**
	 * Multiline keyword
	 */
	private static final String MULTILINE = "MULTILINE";

	/**
	 * Morelines keyword
	 */
	private static final String MORELINES = "MORELINES";

	@Override
	public ShellStatus execute(IEnvironment env, String arguments) throws IOException {

		String args[] = arguments.trim().split("\\s+");
		ConstraintCheckUtil.numberOfArguments(1, 2, args.length);

		if (args.length == 1) {
			getSymbol(args[0].trim(), env);
		} else {
			setSymbol(args[0].trim(), args[1].trim(), env);
		}

		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandDescription() {
		return COMMAND_DESCRIPTION;
	}

	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	/**
	 * Retrieves character used for given keyword.
	 * 
	 * @param s
	 *            MULTILINE, MORELINES or PROMPT keywords
	 * @param env
	 *            Shell environment
	 * @throws IOException
	 *             If I/O environment error occurs
	 */
	private static void getSymbol(String s, IEnvironment env) throws IOException {
		if (s.equals(MORELINES)) {
			env.writeln("Symbol for " + MORELINES + " is: '" + env.getMorelinesSymbol() + "'");
		} else if (s.equals(MULTILINE)) {
			env.writeln("Symbol for " + MULTILINE + " is: '" + env.getMultilineSymbol() + "'");
		} else if (s.equals(PROMPT)) {
			env.writeln("Symbol for " + PROMPT + " is: '" + env.getPromptSymbol() + "'");
		} else {
			env.writeln(
					"Error: uknonwn first parameter\n" + "Valid values: " + PROMPT + "," + MORELINES + "," + MULTILINE);
			;
		}
	}

	/**
	 * Sets character used for given keyword.
	 * 
	 * @param s
	 *            MULTILINE, MORELINES or PROMPT keywords
	 * @param newSymbol
	 *            new character to be used for given keyword
	 * @param env
	 *            Shell environment
	 * @throws IOException
	 *             If I/O environment error occurs
	 */
	private void setSymbol(String s, String newSymbol, IEnvironment env) throws IOException {

		if (newSymbol.length() != 1) {
			throw new MyShellException("Error: New symbol muste be single character");
		}

		if (s.equals(MORELINES)) {
			Character old = env.getMorelinesSymbol();
			env.setMorelinesSymbol(newSymbol.charAt(0));
			env.writeln(
					"Symbol for " + MORELINES + " changed from '" + old + "' to '" + env.getMorelinesSymbol() + "'");
		} else if (s.equals(MULTILINE)) {
			char old = env.getMultilineSymbol();
			env.setMultilineSymbol(newSymbol.charAt(0));
			env.writeln(
					"Symbol for " + MULTILINE + " changed from '" + old + "' to '" + env.getMultilineSymbol() + "'");
		} else if (s.equals(PROMPT)) {
			Character old = env.getPromptSymbol();
			env.setPromptSymbol(newSymbol.charAt(0));
			env.writeln("Symbol for " + PROMPT + " changed from '" + old + "' to '" + env.getPromptSymbol() + "'");
		} else {
			env.writeln(
					"Error: uknonwn first parameter\n" + "Valid values: " + PROMPT + "," + MORELINES + "," + MULTILINE);
			;
		}

	}
}
