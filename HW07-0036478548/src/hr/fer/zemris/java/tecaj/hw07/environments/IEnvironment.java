package hr.fer.zemris.java.tecaj.hw07.environments;

import java.io.IOException;
import java.util.Map;

import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;

/**
 * This class represents abstract environment template
 * shell.
 * <p>
 * All interaction between shell and user goes through
 * this environment.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public interface IEnvironment {
	
	/**
	 * Reads one input line from input stream.
	 * 
	 * @return String representing one line from input stream
	 * @throws IOException If I/O error occurs
	 */
	String readLine() throws IOException;

	/**
	 * Writes given {@code text} to output stream.
	 * 
	 * @param text Text to write to output stream
	 * @throws IOException If I/O error occurs
	 */
	void write(String text) throws IOException;

	/**
	 * Writes given character array to output stream.
	 * 
	 * @param chars Character array
	 * @param offset Position of first character to be written
	 * @param length Number of characters to be written (starting from {@code offset})
	 * @throws IOException If I/O error occurs
	 */
	void write(char[] chars, int offset, int length) throws IOException;
	
	/**
	 * Writes one line to output stream.
	 * 
	 * @param text Line of text to be written to output stream
	 * @throws IOException If I/O error occurs
	 */
	void writeln(String text) throws IOException;

	/**
	 * Returns map containing all commands used in this shell.
	 * 
	 * @return map of (commandName, command) entries
	 */
	Map<String, ShellCommand> commands();

	/**
	 * @return returns character used as multiline symbol
	 */
	Character getMultilineSymbol();

	/**
	 * Sets symbol used as multiline to given character
	 * 
	 * @param symbol
	 */
	void setMultilineSymbol(Character symbol);

	/**
	 * @return returns character used as prompt symbol
	 */
	Character getPromptSymbol();

	/**
	 * Sets symbol used as prompt to given character
	 * 
	 * @param symbol
	 */
	void setPromptSymbol(Character symbol);

	/**
	 * @return returns character used as morelines symbol
	 */
	Character getMorelinesSymbol();

	/**
	 * Sets symbol used as morelines to given character
	 * 
	 * @param symbol
	 */
	void setMorelinesSymbol(Character symbol);
	
	/**
	 * Closes all open I/O streams
	 */
	void close();
}
