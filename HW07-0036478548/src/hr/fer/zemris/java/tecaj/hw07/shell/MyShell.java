package hr.fer.zemris.java.tecaj.hw07.shell;

import java.io.IOException;

import hr.fer.zemris.java.tecaj.hw07.environments.IEnvironment;
import hr.fer.zemris.java.tecaj.hw07.environments.MyShellEnv;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.Commands;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.exceptions.MyShellException;

/**
 * This program is a simple shell implementation.
 * <p>
 * It accepts no arguments from command line and starts with prompting user for
 * input.
 * <p>
 * Shell supports following commands:
 * <ul>
 * <li>cat</li>
 * <li>charsets</li>
 * <li>copy</li>
 * <li>exit</li>
 * <li>help</li>
 * <li>hexdump</li>
 * <li>ls</li>
 * <li>mkdir</li>
 * <li>symbol</li>
 * <li>tree</li>
 * </ul>
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class MyShell {

	/**
	 * Environment used for communication with user
	 */
	private static final IEnvironment SHELL_ENV = new MyShellEnv();

	/**
	 * Program entry point
	 */
	public static void main(String[] args) {

		ShellStatus status = null;

		try {

			SHELL_ENV.writeln("Welcome to MyShell v 1.0");

			do {
				try {
					status = executeLine(readLines());
				} catch (MyShellException e) {
					SHELL_ENV.writeln(e.getMessage());
					continue;
				} catch (SecurityException e) {
					SHELL_ENV.writeln("Error: permission denied");
				}
			} while (status != ShellStatus.TERMINATE);

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}

	}

	/**
	 * Reads multiple line input until end of line is reached.
	 * 
	 * @return returns one input line user provided
	 * @throws IOException
	 *             If I/O error occurs
	 */
	private static String readLines() throws IOException {

		StringBuilder sb = new StringBuilder();

		SHELL_ENV.write(SHELL_ENV.getPromptSymbol() + " ");

		while (true) {
			String line = SHELL_ENV.readLine();
			if (line.endsWith(" " + SHELL_ENV.getMorelinesSymbol())) {
				sb.append(" " + line.substring(0, line.length() - 1).trim());
				SHELL_ENV.write(String.valueOf(SHELL_ENV.getMultilineSymbol()) + " ");
			} else {
				sb.append(" " + line);
				break;
			}
		}

		return sb.toString().trim();
	}

	/**
	 * Executes one user provided input line.
	 * 
	 * @param line
	 *            User provided input line as a String
	 * @return returns TERMINATE if command provided by user was exit, CONTINUE
	 *         otherwise (values from {@link ShellStatus})
	 * @throws IOException
	 *             If I/O error occurs
	 */
	private static ShellStatus executeLine(String line) throws IOException {

		int index = line.indexOf(' ');

		String command;
		String arguments = "";

		if (index == -1) {
			command = line.trim();
		} else {
			command = line.substring(0, index).trim();
			arguments = line.substring(index + 1, line.length()).trim();
		}

		ShellCommand cmd = Commands.getCommand(command);

		if (cmd == null) {
			SHELL_ENV.writeln("Invalid command");
			return ShellStatus.CONTINUE;
		} else {
			return Commands.getCommand(command).execute(SHELL_ENV, arguments);
		}
	}

}
