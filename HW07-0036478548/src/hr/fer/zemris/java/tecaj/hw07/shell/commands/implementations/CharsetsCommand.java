package hr.fer.zemris.java.tecaj.hw07.shell.commands.implementations;

import java.io.IOException;
import java.nio.charset.Charset;

import hr.fer.zemris.java.tecaj.hw07.environments.IEnvironment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.Commands;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.exceptions.MyShellException;

/**
 * Command displays supported charsets.
 * <p>
 * Command charsets takes no arguments and lists names of supported charsets for
 * your Java platform.
 * <p>
 * A single charset name is written per line.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class CharsetsCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "charsets";

	/**
	 * Command description
	 */
	private static final String COMMAND_DESCRIPTION = Commands.getCommandDescription(COMMAND_NAME);

	@Override
	public ShellStatus execute(IEnvironment env, String arguments) throws IOException {
		if (!arguments.trim().isEmpty()) {
			throw new MyShellException("Did you mean: charsets ?");
		}

		env.writeln("List of supported charsets: ");
		for (String charset : Charset.availableCharsets().keySet()) {
			env.writeln("\t" + charset);
		}
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandDescription() {
		return COMMAND_DESCRIPTION;
	}

}
