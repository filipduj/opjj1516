package hr.fer.zemris.java.tecaj.hw07.shell.commands.implementations;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import hr.fer.zemris.java.tecaj.hw07.environments.IEnvironment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.Commands;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.util.ConstraintCheckUtil;
import hr.fer.zemris.java.tecaj.hw07.shell.util.PathParserUtil;

/**
 * Command lists content of given directory.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class LsCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "ls";

	/**
	 * Command description
	 */
	private static final String COMMAND_DESCRIPTION = Commands.getCommandDescription(COMMAND_NAME);

	/**
	 * Date format used in output statistics
	 */
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Override
	public ShellStatus execute(IEnvironment env, String arguments) throws IOException {

		List<Path> args = PathParserUtil.parsePaths(arguments);
		ConstraintCheckUtil.numberOfArguments(1, args.size());

		Path path = args.get(0);
		ConstraintCheckUtil.filesExist(path);
		ConstraintCheckUtil.filesDirectory(path);

		StringBuilder sb = new StringBuilder();
		for (Path p : Files.list(path).collect(Collectors.toList())) {
			String info = fileInfo(p);
			if (info != null)
				sb.append(fileInfo(p) + "\n");
		}

		env.writeln(sb.toString());
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandDescription() {
		return COMMAND_DESCRIPTION;
	}

	/**
	 * Creates string containing following tabular information about given file:
	 * <ul>
	 * <li>[d][r][w][x]</li>
	 * <li>file size (in bytes)</li>
	 * <li>last modification time</li>
	 * <li>file name</li>
	 * </ul>
	 * <p>
	 * Flag meanings:
	 * <ul>
	 * <li>d -> directory</li>
	 * <li>r -> readable</li>
	 * <li>w -> writable</li>
	 * <li>x -> executable</li>
	 * </ul>
	 * 
	 * @param p
	 *            File of interest
	 * @return returns String containing tabular information or {@code null} if
	 *         permission to file is denied
	 */
	private String fileInfo(Path p) {

		StringBuilder sb = new StringBuilder();

		try {
			sb.append(Files.isDirectory(p)  ? "d" : "-")
			  .append(Files.isReadable(p)   ? "r" : "-")
			  .append(Files.isWritable(p)   ? "w" : "-")
			  .append(Files.isExecutable(p) ? "x" : "-")
			  .append(" ")
			  .append(String.format("%10s", Files.size(p)))
			  .append(" ")
			  .append(DATE_FORMAT.format(new Date(Files.getLastModifiedTime(p).toMillis())))
			  .append(" ")
			  .append(p.getFileName());
		} catch (IOException e) {
			return null;
		}

		return sb.toString();
	}
}
