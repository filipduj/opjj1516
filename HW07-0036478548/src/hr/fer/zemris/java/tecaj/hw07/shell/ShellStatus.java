package hr.fer.zemris.java.tecaj.hw07.shell;

/**
 * Enum containts possible return values for shell commands.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public enum ShellStatus {
	
	/**
	 * Shell continues normally after command returns this state
	 */
	CONTINUE, 
	
	/**
	 * Shell terminates after command returns this state
	 */
	TERMINATE;
}
