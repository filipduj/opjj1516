package hr.fer.zemris.java.tecaj.hw07.shell.commands.implementations;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw07.environments.IEnvironment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.Commands;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.exceptions.MyShellException;
import hr.fer.zemris.java.tecaj.hw07.shell.util.ConstraintCheckUtil;
import hr.fer.zemris.java.tecaj.hw07.shell.util.PathParserUtil;

/**
 * Command creates new directory structure on given path.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class MkdirCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "mkdir";

	/**
	 * Command description
	 */
	private static final String COMMAND_DESCRIPTION = Commands.getCommandDescription(COMMAND_NAME);

	@Override
	public ShellStatus execute(IEnvironment env, String arguments) throws IOException {

		List<Path> args = PathParserUtil.parsePaths(arguments);
		ConstraintCheckUtil.numberOfArguments(1, args.size());

		Path path = args.get(0);
		if (Files.isDirectory(path)) {
			throw new MyShellException("Directory " + path.getFileName() + " already exists.");
		}

		Files.createDirectories(path);
		env.writeln("Directory created successfully.");

		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandDescription() {
		return COMMAND_DESCRIPTION;
	}

}
