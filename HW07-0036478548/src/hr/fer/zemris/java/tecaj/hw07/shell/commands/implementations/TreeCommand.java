package hr.fer.zemris.java.tecaj.hw07.shell.commands.implementations;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;

import hr.fer.zemris.java.tecaj.hw07.environments.IEnvironment;
import hr.fer.zemris.java.tecaj.hw07.shell.ShellStatus;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.Commands;
import hr.fer.zemris.java.tecaj.hw07.shell.commands.ShellCommand;
import hr.fer.zemris.java.tecaj.hw07.shell.exceptions.MyShellException;
import hr.fer.zemris.java.tecaj.hw07.shell.util.ConstraintCheckUtil;
import hr.fer.zemris.java.tecaj.hw07.shell.util.PathParserUtil;

/**
 * Command displays hierarchical structure of given directory.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 28, 2016)
 */
public class TreeCommand implements ShellCommand {

	/**
	 * Command name
	 */
	private static final String COMMAND_NAME = "tree";

	/**
	 * Command description
	 */
	private static final String COMMAND_DESCRIPTION = Commands.getCommandDescription(COMMAND_NAME);

	/**
	 * Visitor to be used for directory traversal
	 */
	private static final SimpleVisitor VISITOR = new SimpleVisitor();

	@Override
	public ShellStatus execute(IEnvironment env, String arguments) throws IOException {

		List<Path> args = PathParserUtil.parsePaths(arguments);
		ConstraintCheckUtil.numberOfArguments(1, args.size());

		Path path = args.get(0);
		ConstraintCheckUtil.filesExist(path);
		ConstraintCheckUtil.filesDirectory(path);

		VISITOR.setEnv(env);
		Files.walkFileTree(path, VISITOR);
		env.writeln("");
		return ShellStatus.CONTINUE;
	}

	@Override
	public String getCommandName() {
		return COMMAND_NAME;
	}

	@Override
	public String getCommandDescription() {
		return COMMAND_DESCRIPTION;
	}

	/**
	 * Class wraps {@link SimpleFileVisitor} and binds shell environment to it.
	 * 
	 * @author Filip Dujmušić
	 * @version 1.0 (Apr 28, 2016)
	 */
	private static class SimpleVisitor extends SimpleFileVisitor<Path> {

		/**
		 * Current depth. Zero depth is root directory.
		 */
		private int level;

		/**
		 * Shell environment
		 */
		private IEnvironment env;

		@Override
		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
			print(dir);
			level++;
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
			level--;
			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
			print(file);
			return FileVisitResult.CONTINUE;
		}

		/**
		 * @param env
		 *            Sets environment to this parameter
		 */
		private void setEnv(IEnvironment env) {
			this.env = env;
		}

		/**
		 * Prints one file's name in hierarchy.
		 * 
		 * @param file
		 *            file whose name will be displayed
		 */
		private void print(Path file) {
			try {
				if (level == 0) {
					env.write(file.toString());
				} else {
					env.write(String.format("%n%" + 2 * level + "s%s", "", file.getFileName()));
				}
			} catch (IOException e) {
				throw new MyShellException(e.getMessage());
			}
		}
	}
}
