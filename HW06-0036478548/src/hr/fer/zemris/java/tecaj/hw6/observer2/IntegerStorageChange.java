package hr.fer.zemris.java.tecaj.hw6.observer2;

/**
 * Wraps {@link IntegerStorage} with additional information of old and new
 * value, when change occurs.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class IntegerStorageChange {

	/**
	 * IntegerStorage which is wrapped into this class
	 */
	private IntegerStorage istorage;

	/**
	 * Old value in wrapped IntegerStorage before change occured
	 */
	private int oldValue;

	/**
	 * New value stored in wrapped IntegerStorage after change occured
	 */
	private int currentValue;

	/**
	 * Constructor initializes new IntegerStorageChange object with given
	 * parameters.
	 * 
	 * @param istorage
	 *            IntegerStorage
	 * @param oldValue
	 *            Old value in {@code istorage} before change occured
	 */
	public IntegerStorageChange(IntegerStorage istorage, int oldValue) {
		this.istorage = istorage;
		this.oldValue = oldValue;
		this.currentValue = this.istorage.getValue();
	}

	/**
	 * @return the istorage
	 */
	public IntegerStorage getStorage() {
		return istorage;
	}

	/**
	 * @return the oldValue
	 */
	public int getOldValue() {
		return oldValue;
	}

	/**
	 * @return the currentValue
	 */
	public int getCurrentValue() {
		return currentValue;
	}

}
