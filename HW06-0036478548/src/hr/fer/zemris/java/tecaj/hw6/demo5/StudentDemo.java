package hr.fer.zemris.java.tecaj.hw6.demo5;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Demonstration class which tests {@link Stream} sequence.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class StudentDemo {

	/**
	 * Database file
	 */
	private static final String DATABASE_FILE = "./studenti.txt";

	/**
	 * Program entry point
	 */
	@SuppressWarnings("unused")
	public static void main(String[] args) {

		List<StudentRecord> records = null;

		try {
			List<String> lines = Files.readAllLines(Paths.get(DATABASE_FILE), StandardCharsets.UTF_8);
			records = convert(lines);
		} catch (IllegalArgumentException e) {
			exit(e.getMessage());
		} catch (IOException e) {
			exit("Error reading file " + DATABASE_FILE);
		}

		long broj = records.stream().filter(r -> (r.getBodoviMI() + r.getBodoviZI() + r.getBodoviLAB()) > 25.0).count();

		long broj5 = records.stream().filter(r -> r.getOcjena() == 5).count();

		List<StudentRecord> odlikasi = records.stream().filter(r -> r.getOcjena() == 5).collect(Collectors.toList());

		List<StudentRecord> odlikasiSortirano = records.stream().filter(r -> r.getOcjena() == 5)
				.sorted((r1,
						r2) -> -Double.valueOf(r1.getBodoviMI() + r1.getBodoviZI() + r1.getBodoviLAB())
								.compareTo(Double.valueOf(r2.getBodoviMI() + r2.getBodoviZI() + r2.getBodoviLAB())))
				.collect(Collectors.toList());

		List<String> nepolozeniJMBAGovi = records.stream().filter(r -> r.getOcjena() == 1).sorted()
				.map(StudentRecord::getJmbag).collect(Collectors.toList());

		Map<Integer, List<StudentRecord>> mapaPoOcjenama = records.stream()
				.collect(Collectors.groupingBy(StudentRecord::getOcjena));

		Map<Integer, Integer> mapaPoOcjenama2 = records.stream()
				.collect(Collectors.toMap(StudentRecord::getOcjena, r -> 1, (r1, r2) -> r1 + r2));

		Map<Boolean, List<StudentRecord>> prolazNeprolaz = records.stream()
				.collect(Collectors.partitioningBy(r -> r.getOcjena() > 1));

		System.out.println("test");
	}

	/**
	 * Forced exit method. Displays given {@code message} and exits with
	 * negative exit status.
	 * 
	 * @param message
	 *            Message to be displayed
	 */
	private static void exit(String message) {
		System.err.println(message);
		System.exit(-1);
	}

	/**
	 * Converts list of Strings into list of StudentRecords.
	 * 
	 * @param lines
	 *            List of strings representing lines from student database
	 * @return returns list of {@link StudentRecord} objects
	 */
	private static List<StudentRecord> convert(List<String> lines) {
		List<StudentRecord> records = new LinkedList<>();
		for (String line : lines) {
			records.add(StudentRecord.fromString(line));
		}
		return records;
	}

}
