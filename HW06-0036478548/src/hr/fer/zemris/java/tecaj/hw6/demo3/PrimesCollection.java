package hr.fer.zemris.java.tecaj.hw6.demo3;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Simple collection of prime numbers.
 * <p>
 * This collection provides first n prime numbers.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class PrimesCollection implements Iterable<Integer> {

	/**
	 * Number of prime numbers
	 */
	private int count;

	/**
	 * Constructor initializes new prime collection with given parameter.
	 * 
	 * @param count
	 *            Number of prime numbers in a row that belong to this
	 *            collection
	 * @throws IllegalArgumentException
	 *             If {@code count} is not positive
	 */
	public PrimesCollection(int count) {
		if (count <= 0) {
			throw new IllegalArgumentException(
					"Expected positive number of primes in collection but received: " + count);
		}
		this.count = count;
	}

	@Override
	public Iterator<Integer> iterator() {
		return new Iterator<Integer>() {
			
			/**
			 * Total number of primes generated
			 */
			private int primesGenerated = 0;

			/**
			 * Last prime number that was calculated
			 */
			private int nextPrime = 1;

			@Override
			public boolean hasNext() {
				return primesGenerated < count;
			}

			@Override
			public Integer next() {
				if (!hasNext()) {
					throw new NoSuchElementException();
				}
				primesGenerated++;
				return nextPrime();
			}

			/**
			 * Generates next prime number.
			 * 
			 * @return next prime number in sequence of prime numbers
			 */
			private int nextPrime() {
				for (int i = nextPrime + 1;; ++i) {
					if (isPrime(i)) {
						nextPrime = i;
						return nextPrime;
					}
				}
			}

			/**
			 * Checks if number is prime. Method uses simple Trial division
			 * approach to check if x is prime. More about Trial division on
			 * wiki: https://en.wikipedia.org/wiki/Trial_division
			 * 
			 * @param x
			 *            positive integer to be tested if it is prime or not
			 * @return true if x is prime, false otherwise
			 */
			private boolean isPrime(int x) {
				if (x == 1)
					return false;

				for (int i = 2, n = (int) Math.sqrt(x); i <= n; ++i) {
					if (x % i == 0) {
						return false;
					}
				}
				return true;
			}

		};
	}

}
