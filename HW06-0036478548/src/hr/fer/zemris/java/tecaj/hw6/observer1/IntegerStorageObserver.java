package hr.fer.zemris.java.tecaj.hw6.observer1;

/**
 * Interface declares one method each {@link IntegerStorage} observer should
 * have.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public interface IntegerStorageObserver {

	/**
	 * This method is called each time IntegerStorage's state changes.
	 * 
	 * @param istorage IntegerStorage
	 */
	void valueChanged(IntegerStorage istorage);
}
