package hr.fer.zemris.java.tecaj.hw6.demo3;

/**
 * Demonstration class.
 * <p>
 * Demonstrates {@link PrimesCollection} in a way that it creates one collection
 * of prime numbers and displays cartesian product of primes in that collection.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class PrimesDemo2 {

	/**
	 * Program entry point
	 */
	public static void main(String[] args) {
		PrimesCollection primesCollection = new PrimesCollection(2);
		for (Integer prime : primesCollection) {
			for (Integer prime2 : primesCollection) {
				System.out.println("Got prime pair: " + prime + ", " + prime2);
			}
		}
	}
}
