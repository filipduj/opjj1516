package hr.fer.zemris.java.tecaj.hw6.demo2;

/**
 * Demonstration class.
 * <p>
 * Program demonstrates functionality of {@link LikeMedian} collection. No
 * command line arguments are accepted. It inserts few Strings into collection
 * and then asks for median.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class MedianDemo2 {
	
	/**
	 * Program entry point
	 */
	public static void main(String[] args) {
		LikeMedian<String> likeMedian = new LikeMedian<String>();
		likeMedian.add("Joe");
		likeMedian.add("Jane");
		likeMedian.add("Adam");
		likeMedian.add("Zed");
		String result = likeMedian.get().get();
		System.out.println(result); // Writes: Jane
	}
}
