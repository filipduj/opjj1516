package hr.fer.zemris.java.tecaj.hw6.demo3;

/**
 * Demonstration class.
 * <p>
 * Program accepts no command line arguments and demonstrates
 * {@link PrimesCollection} in a way that it creates collection with 5 prime
 * numbers, iterates over collection and outputs primes to standard output.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class PrimesDemo1 {

	/**
	 * Program entry point
	 */
	public static void main(String[] args) {
		PrimesCollection primesCollection = new PrimesCollection(5); // 5: how
																		// many
																		// of
																		// them
		for (Integer prime : primesCollection) {
			System.out.println("Got prime: " + prime);
		}

	}
}
