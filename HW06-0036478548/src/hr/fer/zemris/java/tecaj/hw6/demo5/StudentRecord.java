package hr.fer.zemris.java.tecaj.hw6.demo5;

/**
 * This class represents one student record from student database.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class StudentRecord implements Comparable<StudentRecord> {

	/**
	 * Jmbag
	 */
	private String jmbag;

	/**
	 * Student last name
	 */
	private String prezime;

	/**
	 * Student first name
	 */
	private String ime;

	/**
	 * Midterm points
	 */
	private Double bodoviMI;

	/**
	 * Final exam points
	 */
	private Double bodoviZI;

	/**
	 * Laboratory exercise points
	 */
	private Double bodoviLAB;

	/**
	 * Grade
	 */
	private int ocjena;

	/**
	 * Constructor allocates new {@code StudentRecord} object with given
	 * parameters.
	 * 
	 * @param jmbag
	 * @param prezime
	 * @param ime
	 * @param bodoviMI
	 * @param bodoviZI
	 * @param bodoviLAB
	 * @param ocjena
	 */
	public StudentRecord(String jmbag, String prezime, String ime, Double bodoviMI, Double bodoviZI, Double bodoviLAB,
			int ocjena) {
		this.jmbag = jmbag;
		this.prezime = prezime;
		this.ime = ime;
		this.bodoviMI = bodoviMI;
		this.bodoviZI = bodoviZI;
		this.bodoviLAB = bodoviLAB;
		this.ocjena = ocjena;
	}

	/**
	 * Static method parses input String and returns new {@code StudentRecord}
	 * object.
	 * 
	 * @param line
	 *            Input string
	 * @return new {@code StudentRecord} object
	 */
	public static StudentRecord fromString(String line) {

		if (line == null) {
			throw new IllegalArgumentException("Expected String but null given.");
		}

		String[] fields = line.split("\t");

		if (fields.length != 7) {
			throw new IllegalArgumentException("Missing fields in database line: " + line);
		}

		try {
			String jmbag = fields[0];
			String prezime = fields[1];
			String ime = fields[2];
			Double bodoviMI = Double.parseDouble(fields[3]);
			Double bodoviZI = Double.parseDouble(fields[4]);
			Double bodoviLAB = Double.parseDouble(fields[5]);
			Integer ocjena = Integer.parseInt(fields[6]);

			return new StudentRecord(jmbag, prezime, ime, bodoviMI, bodoviZI, bodoviLAB, ocjena);

		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Wrong number format in database line: " + line);
		}
	}

	/**
	 * @return the jmbag
	 */
	public String getJmbag() {
		return jmbag;
	}

	/**
	 * @return the prezime
	 */
	public String getPrezime() {
		return prezime;
	}

	/**
	 * @return the ime
	 */
	public String getIme() {
		return ime;
	}

	/**
	 * @return the bodoviMI
	 */
	public Double getBodoviMI() {
		return bodoviMI;
	}

	/**
	 * @return the bodoviZI
	 */
	public Double getBodoviZI() {
		return bodoviZI;
	}

	/**
	 * @return the bodoviLAB
	 */
	public Double getBodoviLAB() {
		return bodoviLAB;
	}

	/**
	 * @return the ocjena
	 */
	public int getOcjena() {
		return ocjena;
	}

	@Override
	public int compareTo(StudentRecord other) {
		return this.jmbag.compareTo(other.jmbag);
	}
}
