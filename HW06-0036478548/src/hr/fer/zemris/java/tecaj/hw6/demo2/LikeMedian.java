package hr.fer.zemris.java.tecaj.hw6.demo2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * This class represents general collection of items whose natural ordering is
 * defined.It provides methods for adding new element to collection and
 * retrieveing median.
 * <p>
 * Median is element in the middle of all elements in collection, sorted by
 * their natural ordering.If two elements are in the middle (when number of
 * elements is even) then median is smaller of two.
 * <p>
 * Collection preserves order in which elements were added.
 * <p>
 * Null values are not allowed.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 * @param <T>
 *            Type for which natural order is defined
 */
public class LikeMedian<T extends Comparable<T>> implements Iterable<T> {

	/**
	 * List of added elements
	 */
	private List<T> elements = new LinkedList<>();
	
	/**
	 * Sorted list of added elements
	 */
	private List<T> sortedElements = new ArrayList<>();

	/**
	 * Method adds new {@code element} to collection.
	 * 
	 * @param element
	 *            Element to be added to collection
	 * @throws IllegalArgumentException
	 *             If {@code element} is null reference
	 */
	public void add(T element) {

		if (element == null) {
			throw new IllegalArgumentException();
		}

		elements.add(element);
		sortedElements.add(element);
		sortedElements.sort(null);
	}

	/**
	 * Retrieves median of currently stored elements wrapped in {@link Optional}.
	 * 
	 * @return median or empty {@code Optional} if collection is empty
	 */
	public Optional<T> get() {

		if (elements.isEmpty()) {
			return Optional.empty();
		}

		return Optional.of( sortedElements.get((elements.size() - 1) / 2));
	}

	@Override
	public Iterator<T> iterator() {
		return elements.iterator();
	}

}
