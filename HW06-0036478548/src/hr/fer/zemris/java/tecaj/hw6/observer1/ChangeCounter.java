package hr.fer.zemris.java.tecaj.hw6.observer1;

/**
 * ChangeCounter observer. Counts every change of it's subject and displays it
 * to user.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class ChangeCounter implements IntegerStorageObserver {

	/**
	 * Counter
	 */
	private int count = 0;

	@Override
	public void valueChanged(IntegerStorage istorage) {
		++count;
		System.out.println("Number of value changes since tracking: " + count);
	}

}
