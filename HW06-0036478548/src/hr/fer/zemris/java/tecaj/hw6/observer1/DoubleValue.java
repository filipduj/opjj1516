package hr.fer.zemris.java.tecaj.hw6.observer1;

/**
 * DoubleValue observer. Each time subject value changes this observer displays
 * double value of new changed value. It does so for limited number of times
 * after which it removes itself from observing subject.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class DoubleValue implements IntegerStorageObserver {

	/**
	 * Counter
	 */
	private int counter;

	/**
	 * Initializes new DoubleValue observer with given {@code counter}.
	 * 
	 * @param counter
	 *            Counter
	 * @throws IllegalArgumentException
	 *             If counter not positive
	 */
	public DoubleValue(int counter) {
		if (counter <= 0) {
			throw new IllegalArgumentException("Counter must be positive but " + counter + "given.");
		}
		this.counter = counter;
	}

	@Override
	public void valueChanged(IntegerStorage istorage) {
		System.out.println("Double value: " + 2 * istorage.getValue());
		--counter;
		if (counter == 0) {
			istorage.removeObserver(this);
		}
	}
}
