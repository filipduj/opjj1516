package hr.fer.zemris.java.tecaj.hw6.demo2;

import java.util.Optional;

/**
 * Demonstration class.
 * <p>
 * Program demonstrates functionality of {@link LikeMedian} collection. No
 * command line arguments are accepted. It inserts few numbers into collection
 * and then asks for median. Iterator is also tested to check if order of
 * insertion is preserved.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class MedianDemo1 {

	/**
	 * Program entry point
	 */
	public static void main(String[] args) {
		LikeMedian<Integer> likeMedian = new LikeMedian<Integer>();
		likeMedian.add(new Integer(10));
		likeMedian.add(new Integer(5));
		likeMedian.add(new Integer(3));
		Optional<Integer> result = likeMedian.get();
		System.out.println(result.get());
		for (Integer elem : likeMedian) {
			System.out.println(elem);
		}
	}
}
