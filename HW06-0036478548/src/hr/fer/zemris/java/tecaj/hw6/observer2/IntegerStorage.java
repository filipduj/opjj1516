package hr.fer.zemris.java.tecaj.hw6.observer2;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents wrapper for primitive integer value. Wrapped value
 * access is read-write. It also provides methods for registering and removing
 * observers.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class IntegerStorage {

	/**
	 * Integer value
	 */
	private int value;
	
	/**
	 * List of registered observers
	 */
	private List<IntegerStorageObserver> observers;
	
	/**
	 * List of observers which are about to be removed from observers list
	 */
	private List<IntegerStorageObserver> pendingRemove;

	/**
	 * Constructor allocates new IntegerStorage object and initializes it with
	 * given {@code initialValue}.
	 * 
	 * @param initialValue
	 *            Value which this object wraps
	 */
	public IntegerStorage(int initialValue) {
		this.value = initialValue;
		observers = new ArrayList<>();
		pendingRemove = new ArrayList<>();
	}

	/**
	 * Adds new {@code observer} to list of observers registered to this
	 * subject.
	 * 
	 * @param observer
	 *            Observer
	 * @throws IllegalArgumentException
	 *             If {@code observer} is null reference
	 */
	public void addObserver(IntegerStorageObserver observer) {

		if (observer == null) {
			throw new IllegalArgumentException();
		}
		
		if (!observers.contains(observer)) {
			observers.add(observer);
		}
	}

	/**
	 * Removese given {@code observer} from list of observers registered to this
	 * subject.
	 * 
	 * @param observer
	 */
	public void removeObserver(IntegerStorageObserver observer) {
		pendingRemove.add(observer);
	}

	/**
	 * Removese all observers from registered observers list.
	 */
	public void clearObservers() {
		observers.clear();
		pendingRemove.clear();
	}

	/**
	 * @return int value this object wraps
	 */
	public int getValue() {
		return value;
	}

	/**
	 * Sets value this object wraps to new givne {@code value}. If it's
	 * different than the old one, it notifies all registered observers.
	 * 
	 * @param value
	 *            int value to be set
	 */
	public void setValue(int value) {


		// Only if new value is different than the current value:
		if (this.value != value) {

			if (!pendingRemove.isEmpty()) {
				for (IntegerStorageObserver observer : pendingRemove) {
					observers.remove(observer);
				}
				pendingRemove.clear();
			}
			
			int oldValue = this.value;
			
			// Update current value
			this.value = value;
			
			IntegerStorageChange storageChange = new IntegerStorageChange(this, oldValue); 

			// Notify all registered observers
			if (observers != null) {
				for (IntegerStorageObserver observer : observers) {
					observer.valueChanged(storageChange);
				}
			}

		}
		

		
		
	}

}
