package hr.fer.zemris.java.tecaj.hw6.observer1;

/**
 * Square value observer.Each time subject value changes this observer displays
 * square of new changed value.
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class SquareValue implements IntegerStorageObserver {
	@Override
	public void valueChanged(IntegerStorage istorage) {
		int value = istorage.getValue();
		System.out.printf("Provided new value: %d, square is %d%n", value, value * value);
	}
}
