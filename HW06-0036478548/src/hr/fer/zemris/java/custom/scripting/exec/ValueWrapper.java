package hr.fer.zemris.java.custom.scripting.exec;

import java.util.function.BiFunction;

/**
 * Wrapper class that wraps any Object.
 * <p>
 * For objects of type Double, Integer, String parsable to number or null (which
 * is treated as zero) this class provides five arithmetic operations:
 * increment, decrement, multiply, divide and compare.
 * 
 * 
 * @author Filip Dujmušić
 * @version 1.0 (Apr 19, 2016)
 */
public class ValueWrapper {

	/**
	 * Function that returns sum of two given double values
	 */
	private static final BiFunction<Double, Double, Double> ADD = (d1, d2) -> d1 + d2;

	/**
	 * Function that returns difference of two given double values
	 */
	private static final BiFunction<Double, Double, Double> SUB = (d1, d2) -> d1 - d2;

	/**
	 * Function that returns result of multiplication of two given double values
	 */
	private static final BiFunction<Double, Double, Double> MUL = (d1, d2) -> d1 * d2;

	/**
	 * Function that returns result of division of two given double valuse
	 */
	private static final BiFunction<Double, Double, Double> DIV = (d1, d2) -> d1 / d2;

	/**
	 * Wrapped object value
	 */
	private Object value;

	/**
	 * Constructor that initializes new wrapper.
	 * 
	 * @param value
	 *            Object that is being wrapped
	 * @throws IllegalArgumentException
	 *             If given {@code value} is {@code null} reference.
	 */
	public ValueWrapper(Object value) {
		if (value == null) {
			throw new IllegalArgumentException("Cannot wrap null value.");
		}
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

	/**
	 * Performs addition on current {@code value} and given {@code incValue} and
	 * stores result in {@code value}.
	 * 
	 * @param incValue
	 *            Value for which internal {@code value} will be increased
	 * @throws RuntimeException
	 *             If given {@code incValue} is not Double,String,null or
	 *             parsable String
	 */
	public void increment(Object incValue) {
		value = calculate(process(value), process(incValue), ADD);
	}

	/**
	 * Performs subtraction on current {@code value} and given {@code decValue}
	 * and stores result in {@code value}.
	 * 
	 * @param decValue
	 *            Value for which internal {@code value} will be decreased
	 * @throws RuntimeException
	 *             If given {@code decValue} is not Double,String,null or
	 *             parsable String
	 */
	public void decrement(Object decValue) {
		value = calculate(process(value), process(decValue), SUB);
	}

	/**
	 * Performs multiplication on current {@code value} and given
	 * {@code mulValue} and stores result in {@code value}.
	 * 
	 * @param mulValue
	 *            Value which multiplies wrapped {@code value}
	 * @throws RuntimeException
	 *             If given {@code mulValue} is not Double,String,null or
	 *             parsable String
	 */
	public void multiply(Object mulValue) {
		value = calculate(process(value), process(mulValue), MUL);
	}

	/**
	 * Performs division on current {@code value} and given {@code divValue} and
	 * stores result in {@code value}.
	 * 
	 * @param divValue
	 *            Value which divides wrapped {@code value}
	 * @throws RuntimeException
	 *             If given {@code divValue} is not Double,String,null or
	 *             parsable String
	 * @throws IllegalArgumentException
	 *             If givne {@code} divValue is zero
	 */
	public void divide(Object divValue) {
		value = calculate(process(value), process(divValue), DIV);
	}

	/**
	 * Compares internal {@code value} with given {@code withValue}. Returns
	 * negative integer, zero or positiv integer if {@code value} is less than,
	 * equal or greater than {@code withValue} in natural order.
	 * 
	 * @param withValue
	 *            Value to compare to
	 * @return Result of comparison as integer value
	 * @throws RuntimeException
	 *             If given {@code withValue} is not Double,String,null or
	 *             parsable String
	 */
	public int numCompare(Object withValue) {
		return ((Number) calculate(process(value), process(withValue), SUB)).intValue();
	}

	/**
	 * Applies given {@link BiFunction} {@code func} to {@code obj1} and
	 * {@code obj2} and returns result.
	 * <p>
	 * If both {@code obj1} and {@code obj2} are Integers, result returned is of
	 * type Integer.
	 * <p>
	 * If at least one of two arguments id Double, result returned is of type
	 * Double
	 * 
	 * @param obj1
	 *            First argument
	 * @param obj2
	 *            Second argument
	 * @param func
	 *            Function to perform
	 * @return Returns result of {@code func} called upon {@code obj1} and
	 *         {@code obj2}
	 * @throws RuntimeException
	 *             If obj1 or obj2 are not one of following: Integer, Double,
	 *             String parsable to number or null(treated as zero integer)
	 * @throws IllegalArgumentException
	 *             If obj2 is zero and given function is division
	 */
	private Object calculate(Object obj1, Object obj2, BiFunction<Double, Double, Double> func) {
		Double first = ((Number) obj1).doubleValue();
		Double second = ((Number) obj2).doubleValue();
		if (second == 0 && func == DIV)
			throw new IllegalArgumentException("Division by zero.");
		if (obj1 instanceof Integer && obj2 instanceof Integer) {
			return ((Number) func.apply(first, second)).intValue();
		} else {
			return func.apply(first, second);
		}
	}

	/**
	 * Checks if given object meets following criteria:
	 * <ul>
	 * <li>If given object is Double or Integer the object itself is returned.
	 * </li>
	 * <li>If given object is {@code null} zero Integer is returned.</li>
	 * <li>if given object is String parsable to Double or Integer, parsed value
	 * is returned</li>
	 * </ul>
	 * If none of upper is fulfilled exception is thrown.
	 * 
	 * @param obj
	 *            Object value
	 * @return Numerical representation of {@code obj}
	 * @throws RuntimeException
	 *             If {@code obj} can't be represented as number
	 */
	private Object process(Object obj) {
		if (obj == null) {
			return Integer.valueOf(0);
		} else if (obj instanceof String) {
			try {
				return Integer.valueOf((String) obj);
			} catch (NumberFormatException e) {
				try {
					return Double.valueOf((String) obj);
				} catch (NumberFormatException f) {
					throw new RuntimeException();
				}
			}
		} else if (obj instanceof Double || obj instanceof Integer) {
			return obj;
		} else {
			throw new RuntimeException();
		}
	}

}