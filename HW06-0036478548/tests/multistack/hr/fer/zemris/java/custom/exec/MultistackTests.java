package hr.fer.zemris.java.custom.exec;

import static org.junit.Assert.*;

import java.util.EmptyStackException;

import org.junit.Test;

import hr.fer.zemris.java.custom.scripting.exec.ObjectMultistack;
import hr.fer.zemris.java.custom.scripting.exec.ValueWrapper;

@SuppressWarnings("javadoc")
public class MultistackTests {
	
	@Test
	public void valueWrapperOperationTests(){
		
		ValueWrapper v = null; 
		
		v = new ValueWrapper("13.56");
		v.increment("12.44");
		assertEquals(26.0, v.getValue());
		
		v.setValue(null);
		v.increment(null);
		assertEquals(0, v.getValue());
		
		v.setValue(null);
		v.decrement(-3);
		assertEquals(3, v.getValue());
		
		v.setValue("14");
		v.divide("2");
		assertEquals(7, v.getValue());
	
		v.setValue("3");
		v.multiply("4");
		assertEquals(12, v.getValue());
		
		v.setValue(null);
		
		assertEquals(true, v.numCompare("13.56")<0);
		assertEquals(true, v.numCompare(null)==0);
		assertEquals(true, v.numCompare(-1)>0);
	
	}
	

	//sum of number and stringbuilder > exception 
	@Test(expected = RuntimeException.class)
	public void valueWrapperException1(){
		ValueWrapper v = new ValueWrapper("13.56");
		v.increment(new StringBuilder());	
	}
	
	//division by zero	
	@Test(expected = RuntimeException.class)
	public void valueWrapperException2(){
		ValueWrapper v = new ValueWrapper("13.56");
		v.divide(null);	
	}	
	
	@Test
	public void objectMultistackTest(){

		ObjectMultistack mstack = new ObjectMultistack();
		
		assertEquals(true, mstack.isEmpty("test"));
		
		mstack.push("test", new ValueWrapper(42));
		
		assertEquals(42,mstack.peek("test").getValue());
		
		mstack.push("test1", new ValueWrapper(42));
		mstack.push("test1", new ValueWrapper("something"));
		mstack.push("test1", new ValueWrapper(13));

		assertEquals(13, mstack.pop("test1").getValue());
		mstack.pop("test1");
		mstack.pop("test1");
		
		assertEquals(true, mstack.isEmpty("test1"));
		assertEquals(false, mstack.isEmpty("test"));
	}
	
	@Test(expected = EmptyStackException.class)
	public void objectStackException1(){
		ObjectMultistack mstack = new ObjectMultistack();
		mstack.peek("test");
	}
	
	@Test(expected = EmptyStackException.class)
	public void objectStackException2(){
		ObjectMultistack mstack = new ObjectMultistack();
		mstack.pop("test");
	}
	
	@Test(expected = EmptyStackException.class)
	public void objectStackException3(){
		ObjectMultistack mstack = new ObjectMultistack();
		mstack.push("test", new ValueWrapper(1));
		mstack.push("test", new ValueWrapper(2));
		mstack.push("test", new ValueWrapper(3));

		while(true){
			mstack.pop("test");
		}
	
	}

	@Test(expected = IllegalArgumentException.class)
	public void objectStackException4(){
		ObjectMultistack mstack = new ObjectMultistack();
		mstack.push(null, new ValueWrapper("a"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void objectStackException5(){
		ObjectMultistack mstack = new ObjectMultistack();
		mstack.push("test", null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void objectStackException6(){
		ObjectMultistack mstack = new ObjectMultistack();
		mstack.pop(null);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void objectStackException7(){
		ObjectMultistack mstack = new ObjectMultistack();
		mstack.peek(null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
